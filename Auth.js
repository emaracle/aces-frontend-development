'use strict';

angular.module('acesApp')
    .factory('Auth',
        ['$http', '$state', 'alertService', '$injector', '$rootScope', '$timeout', 'AuthToken', '$q', function
            ($http, $state, alertService, $injector, $rootScope, $timeout, AuthToken, $q) {

            console.log("Auth - Service");
            //localStorage.removeItem('user');
            var accessLevels = routingConfig.accessLevels
                , userRoles = routingConfig.userRoles
                , currentUser = null
                , currentUserSetting = {
                    version: "1.0" //changing this value will reset all saved user setting.
                };

            // If user exists in local storage, make them current user.
            var getCurrentUser = function(){
                var currentUser;
                if (sessionStorage.getItem('user')) {
                    currentUser = JSON.parse(sessionStorage.getItem('user'));
                    // debugger;
                    // if (sessionStorage.getItem('phu') !== null) {
                    //     $rootScope.phu = JSON.parse(sessionStorage.getItem('phu'));
                    // }
                } else {
                    currentUser = { username: '', userRole: userRoles.public};
                }
                return currentUser;
            }

            currentUser = getCurrentUser();

            // Change user updates the current user
            function changeUser(user) {
                sessionStorage.setItem('user', JSON.stringify(user));
                managePermissions(user);
                // angular.extend(currentUser, user);
                angular.copy(user, currentUser);
            }

            // Manage permissions sets the logged variables used to customize the view
            function managePermissions(user) {
                if (user.userRole.title != "public") {
                    authService.isLogged = true;
                    if (user.userRole.title == "admin") {
                        authService.isAdmin = true;
                    } else {
                        authService.isAdmin = false;
                    }
                }
                else {
                    sessionStorage.removeItem('user');
                    sessionStorage.removeItem('token');
                    AuthToken.set('');
                    authService.isLogged = false;
                    authService.isAdmin = false;
                }
            }

            var authService = {
                getHash: function (str) {
                    var shaObj = new jsSHA(str, "TEXT");
                    return shaObj.getHash("SHA-512", "B64");
                },

                // Bitmask check of a state's access level against the accessing user's roles.
                authorize: function (accessLevel, role) {
                    if (role === undefined) {
                        role = currentUser.userRole;
                    }
                    return accessLevel.bitMask & role.bitMask;
                },

                isLoggedIn: function (user) {
                  //  debugger;
                    if (user === undefined) {
                        user = currentUser;
                    }
                    return user.userRole.title === userRoles.user.title
                        || user.userRole.title === userRoles.admin.title;
                },
                restrictedUsers: function(){
                    //temporary - if qa server block MOHLTC access to line listing 
                    if (isQAServer()) {
                        return ['2DEC0BB4-6761-41E1-AD97-785D26252536', '55DC87B2-99B4-4E72-9BA1-1C2AC0DC447F', 'AD9A301C-F5D0-4B3B-BC2C-5943E1BCAF3A'];
                    } else {
                        return ['5994C620-75FB-4142-9021-6EBE33A507E9', '5661EB85-0A56-4D36-93C3-723B44ED0D82', '59916399-7E77-495B-878C-72B14D198ED6', 'C0695E03-38A8-4267-B4EA-367356A3F31F', '383E8385-8F22-4A8C-B339-40175A6E862D', '6A542501-5791-4E87-B7D5-2F89721E85FE', '6DCF6BDB-8EFA-4EAA-8738-6AAADDAE004B', 'EAC722EF-61AA-4251-B569-4FBC6E25ACFC', 'E2787000-877E-40C5-90E8-B670EE3916ED', '80760069-A0B7-44B7-AE99-3285E2BFF9DA'];
                    }
                },
                hasAccessToState: function (stateName) {
                    //states with explicit rules
                    var rules = {
                        'app.linelistings': {
                            blocked: (function () {
                               return authService.restrictedUsers();
                            })()
                        }
                    };

                    if (rules[stateName] !== undefined) {
                        //if has blocked property and userid is in the blocked list, shouldn't have access
                        if (rules[stateName].blocked !== undefined && _.indexOf(rules[stateName].blocked, currentUser.useruuid) >= 0) {
                            return false;
                        }
                        //if has allow property and userid is not in the allow list, shouldn't have access
                        if (rules[stateName].allow !== undefined && _.indexOf(rules[stateName].allow, currentUser.useruuid) === -1) {
                            return false;
                        }
                    }
                    //by default, no restrictions
                    return true;
                },
                isUserRestricted: function(){              
                    if (authService.restrictedUsers().indexOf(currentUser.useruuid) >= 0){
                        return true;
                    } else {
                        return false;
                    }
                },
                getBasicData: function () {
                    var deferred = $q.defer();
                    var tasksCompleted = 0;
                    var totalTasks = 0;
                    var taskCompleted= function(){
                        tasksCompleted++;
                        if (tasksCompleted >= totalTasks) {
                            deferred.resolve();
                        }
                    };

                    var hospitals = $injector.get("hospitals");
                    var ctas = $injector.get("ctas");
                    var classifications = $injector.get("classifications");
                    var classifiers = $injector.get("classifiers");

                    totalTasks++;

                    //watch for phu change
                    $rootScope.$watch('phu', function(newVal, oldVal) {
                        if (newVal !== undefined) {
                            sessionStorage.setItem('phu', JSON.stringify(newVal));    
                        }
                    });

                    hospitals.getData().then(function () {
                        $rootScope.phus = hospitals.phus;
                        var validPhu = false;
                        //if $rootScope.phu is not set
                        //try to get it from local storage
                        if ($rootScope.phu === undefined) {
                            try {
                                var phu = JSON.parse(sessionStorage.getItem('phu'));
                                if (phu !== null) {
                                    $rootScope.phu = phu;
                                }                                
                            } catch(e) {
                                //do nothing, will be handled below
                            }
                        }

                        //if phu not set and is admin, set it to KFLA
                        if ($rootScope.phu === undefined && authService.isAdmin) {
                            $rootScope.phu = {id: 'KFLA'};
                        }
                        //check is $rootScope.phu is set and valid
                        if ($rootScope.phu !== undefined) {
                            for (var i in hospitals.phus) {
                                if ($rootScope.phu.id == hospitals.phus[i].id) {
                                    $rootScope.phu = hospitals.phus[i];
                                    validPhu = true;
                                    break;
                                }
                            }
                        }

                        if (!validPhu) {
                            //if not valid phu, set to the first
                            $rootScope.phu = hospitals.phus[0];
                        }

                        $rootScope.hospital = hospitals.phus[0].hospitals[0];
                        $rootScope.hospitals = hospitals.hospitals;
                        taskCompleted();
                    });

                    totalTasks++;
                    ctas.getData().then(function (data) {
                        $rootScope.ctas = data;
                        taskCompleted();
                    });

                    totalTasks++;
                    classifications.getData().then(function (data) {
                        $rootScope.classifications = data;
                        taskCompleted();
                    });

                    totalTasks++;
                    classifiers.getData().then(function (data) {
                        $rootScope.classifiers = data;
                        taskCompleted();
                    });

                    //Init user settings
                    totalTasks++;
                    _loadSettings().then(function () {
                        taskCompleted();
                    });


                    // totalTasks++;
                    // setTimeout(function() {taskCompleted()}, 3000);
                    return deferred.promise;
                },

                login: function (user, success, error) {
                    //These two values will be used for changePassword process
                    var userid = user.userid;
                    //hash the password for login
                    user.pswd = authService.getHash(user.pswd);
                    //Hash the hashed password for storing locally
                    //It will be used to compare current password when changing password
                    var pswd = authService.getHash(user.pswd);


                    var xsrf = $.param(user);
                    var url = $rootScope.endpoint + '/authenticate/login';

                    $http({
                        method: 'POST',
                        url: url,
                        data: xsrf,
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                        custom: {
                            //Instruct AuthHttpInterceptor not to handle 401. We will handle it in fail handler below
                            //AuthHttpInterceptor will not show "Unauthorized" message
                            handles: [401]
                        }
                    })
                    .success(function (user) {
                        alertService.add('success', 'Logged in');
                        if (user[0].userRole == "userRoles.admin") {
                            user[0].userRole = userRoles.admin
                        }
                        else if (user[0].userRole == "userRoles.user") {
                            user[0].userRole = userRoles.user
                        }
                        user[0].pswd = pswd;
                        user[0].username = userid;
                        AuthToken.set(user[0].token);
                        changeUser(user[0]);

                        //reload the page to reset everything
                        //@todo let the services reload data instead of reloading the whole page
                        //$rootScope.$broadcast("login");
                        location.href = "/?t=" + (new Date()).valueOf() + '#' + $state.href("app.user");
                    })
                    .error(function (error, status) {
                        if (status === 401) {
                            alertService.add("danger", "Sorry, couldn't log you in!");    
                        }
                        else if (status === 423){
                            alertService.add("danger", "Sorry, your account has been deactivated due to excess failed login attempts. Please contact an administrator to reactivate your account."); 
                        }
                        else if (status === 429){
                            alertService.add("warning", "Warning: One more failed login attempt will lock this account. Please use the forget password function or email kflaphi@kflapublichealth.ca for further assistance"); 
                        }
                    });
                },

                logout: function (success, error) {
                    var clearLocalData = function(){
                        //clear sessionStorage
                        // sessionStorage.clear();

                        //broadcast logout event
                        $rootScope.$broadcast("logout");

                        changeUser({
                                username: '',
                                useruuid: '',
                                userRole: userRoles.public
                            });
                    }
                    //Check if auth token is available
                    //if not available, there is no point in sending logout to server, it will be rejected
                    //just clear the local data
                    if (AuthToken.get() == '') {
                        clearLocalData();
                        $state.go("app.home");
                    } else {
                        $http.get($rootScope.endpoint + '/authenticate/logout')
                        .then(function () {
                            clearLocalData();
                            alertService.add("warning", "Logged out");
                            //reload the page to reset everything
                            location.href = "/?t=" + (new Date()).valueOf();
                        });
                    }
                },

                forcePasswordChange: function () {
                    return (authService.isLoggedIn() && authService.getSetting("forcePasswordChange") !== false);
                },
                /**
                 * Public properties
                 */

                accessLevels: accessLevels,
                userRoles: userRoles,
                user: currentUser,
                pendingStateChange: null,
                isLogged: null,
                isAdmin: null
            };

            authService.userRole = userRoles.public;
            authService.isLogged = false;
            authService.isAdmin = false;


            var _loadSettings = function () {
                var user = getCurrentUser();
                var defer = $q.defer();
                var url = $rootScope.endpoint + '/authenticate/getSettings/' + user.useruuid;
                $http
                    .get(url)
                    .success(function (data) {
                        if (data.length) {
                            if (undefined !== data[0].Settings) {
                                var settings = JSON.parse(data[0].Settings);
                                if (undefined !== settings.version && currentUserSetting.version === settings.version) {
                                    currentUserSetting = settings;
                                }
                            }
                        }
                        defer.resolve(currentUserSetting);
                    })
                    .error(function (error, status) {
                        defer.reject(error);
                    })
                    ;
                return defer.promise;
            };
            var _saveSettings = function () {
                var user = getCurrentUser();
                var defer = $q.defer();
                var url = $rootScope.endpoint + '/authenticate/setSettings/' + user.useruuid;
                var xsrf = $.param({
                    value: JSON.stringify(currentUserSetting)
                });
                $http({
                        method: 'POST',
                        url: url,
                        data: xsrf,
                        headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                    })
                    .success(function (data){
                        defer.resolve(data);
                    })
                    .error(function (error, status) {
                        defer.reject(error);
                    });
                return defer.promise;
            }

            /*
            * Updates the users session password
            */
            authService.updateSession = function (pswd) {
                var currentUser = getCurrentUser();
                currentUser.pswd = pswd;
                changeUser(currentUser)
            };

            /*
            * reload if optional and is false by default
            * if reload is true, promise is returned
            */
            authService.getSetting = function (name, reload) {
                if (reload === undefined) reload = false;

                if (reload) {
                    var defer = $q.defer();
                    _loadSettings()
                        .then(
                            function () {
                                defer.resolve(authService.getSetting(name));
                            },
                            function () {//fail
                                defer.reject();
                            }
                        );
                    return defer.promise;
                } else {
                    if (currentUserSetting.appSettings !== undefined && currentUserSetting.appSettings[name] !== undefined) {
                        return currentUserSetting.appSettings[name];
                    }
                }
            };
            /**
            * if saveToEndPoint if true, returns promise
            */
            authService.setSetting = function (name, value, saveToEndPoint) {
                //if saveToEndPoint argument is not passed, default it to true
                if (saveToEndPoint === undefined) saveToEndPoint = true;

                if (undefined === currentUserSetting.appSettings) {
                    currentUserSetting.appSettings = {};
                }
                currentUserSetting.appSettings[name] = value;
                if (saveToEndPoint) {
                    return _saveSettings();
                } else {
                    return true;
                }
            };

            var _lastPingTime = 0;
            var _pingTimeout = (20 * 60 * 1000); //20 minutes
            var _pingServer = function () {
                var defer = $q.defer();
                var now = (new Date()).getTime();
                //perform server request only if it has passed 20 minutes since the last check
                if ((now - _lastPingTime) > _pingTimeout) {
                    _loadSettings().then(function () {
                        _lastPingTime = now;
                        defer.resolve();
                    });
                } else {
                    defer.resolve();
                }
                return defer.promise;
            };
            //performs an asynchronize secure request to server to check if user session has timeout or not
            authService.pingServer = _pingServer;


            // authService.getCurrentUser = getCurrentUser;

            //
            managePermissions(currentUser);

            return authService;
        }])

    /*  Alert Service
     *       This service is responsible for adding alerts to the main view
     */
    .factory('alertService', ['$rootScope', '$timeout', function ($rootScope, $timeout) {
        var alertService = {};

        // create an array of alerts available globally
        $rootScope.alertMsgs = [];

        alertService.add = function (type, msg, timeout) {
            if (timeout === undefined) timeout = (15 * 1000); //default to 15 seconds

            var uuid = _.uniqueId('alertService-');
            $rootScope.latestMsg = msg;
            $rootScope.latestMsgUUID = uuid;
            $rootScope.latestClass = type;
            $rootScope.alertMsgs.unshift({'type': type, 'msg': msg});
            $timeout(function () {
                if (uuid == $rootScope.latestMsgUUID) {
                    $rootScope.latestMsg = '';
                    $rootScope.latestClass = '';
                }
            }, timeout);
        };

        alertService.closeAlert = function (index) {
            $rootScope.alertMsgs.splice(index, 1);
            if ($rootScope.alertMsgs[0] == undefined) {
                $rootScope.latestMsg = "No messages";
                $rootScope.latestClass = "primary";
            }
        };

        return alertService;
    }])

    /*  Auth HTTP Interceptor
     *       This intercept adds the token (if it exists) to the header
     *       And also updates the token if it is included in the response header
     */
    .factory('AuthHttpInterceptor', ['$rootScope', '$q', '$timeout', 'alertService', '$injector', 'AuthToken', 'SystemStatus', function ($rootScope, $q, $timeout, alertService, $injector, AuthToken, SystemStatus) {
        var $http;

        // Function to reduce pending requests then disable loading state if no requests are pending
        var endRequest = function (response) {
            //this should prevent flicker effect caused when session has time out (using expired/invalid token)
            $timeout(function () {
                //if requester handles loading, don't do anything
                if (!response.config.custom.handlesLoading) {
                    SystemStatus.processCompleted();
                }
            }, 200);
        };

        return {
            'request': function (config) {
                // console.log(moment().format('MMMM Do YYYY, h:mm:ss a'));
                // // console.log(config);
                // console.log(config.url);
                var canceler = $q.defer();
                var token = AuthToken.get();//localStorage.getItem('token');
                var now = new Date().getTime();
                config['url'] = config['url'].replace('km-prod/', 'km-prod:8000/');
                config['url'] = config['url'].replace('km-dev/', 'km-dev:8000/');
                console.log(config.url);
                config.timeout = canceler.promise;
                var url = config['url'];

                //
                config.custom = config.custom || {};

                config.custom.secure = false;
                config.custom.startedOn = moment().format('MMMM Do YYYY, h:mm:ss a');
                config.custom.token = token;
                config.custom.retries = config.custom.retries || 0;

                console.log(config.custom.retries);
                // if (config.custom.retries > 10) {
                //     console.log(config.custom.retries);
                //     return;
                // }

                $http = $http || $injector.get('$http');
                // Add token to header if accessing secure endpoint
                if (url.indexOf("secure") > 0 || url.indexOf("authenticate") > 0) {
                    config.custom.secure = true;
                    console.log('Using ' + token);
                    config['headers']['token'] = token;
                    //
                    


                    // Add current time to params if accessing endpoint (to prevent getting 304s)
                    if (url.indexOf($rootScope.endpoint) == 0) {
                        //remove previous now querystring variable if any
                        url = url.replace(/[\?&]*now=\d+/g, '');
                        //add a new now value
                        config['url'] = url + (url.indexOf('?') >= 0 ? '&' : '?') + "now=" + now;
                    }
                }
                //Loading is not handled by the requester
                if (config.custom.handlesLoading === undefined || !config.custom.handlesLoading) {
                    //set it false incase if it is undefined
                    config.custom.handlesLoading = false;
                    SystemStatus.processStarted(); 
                } 
				console.log('----------ALTER ENDPOINT IF USING NODEJS --------------------');  
                        var chk = (config.url).split("lookup/phus-and-hosps");
                        var chk2 = (config.url).split("lookup/ctas");
                        var chk3 = (config.url).split("lookup/classifications");
                        var chk4 = (config.url).split("lookup/classifiers"); 
                        var chk5 = (config.url).split("lookup/aces");
                        var chk6 = (config.url).split("lookup/aces/buckets");
                        var chk7 = (config.url).split("aces/lookup");
                        var chk8 = (config.url).split("aces/secure"); 
                        if(chk.length > 1 || 
                            chk2.length > 1 || 
                            chk3.length > 1 || 
                            chk4.length > 1 || 
                            chk5.length > 1 || 
                            chk6.length > 1 || 
                            chk7.length > 1 || 
                            chk8.length > 1){ 
                            config.url = (config.url).replace('localhost', 'localhost:8080');
                            config.url = (config.url).replace('localhost:8080:8080', 'localhost:8080');
                        }  
                        console.log('------------------------------');

                return config;
            },
            'requestError': function (response) {
                endRequest(response);
            },

            'response': function (response) {
                endRequest(response);
                // If response includes a token header and it is not same as the one it used, get it and replace token in storage
                if (response.headers()['token'] && response.headers()['token'] != response.config.custom.token) {
                    AuthToken.set(response.headers()['token']);
                    // console.log('Changed token from ' + response.config.custom.token + ' to ' + response.headers()['token']);
                    // debugger;
                }

                return response;
            },

            'responseError': function (response) {
                endRequest(response);

                var Auth = $injector.get('Auth');
                var State = $injector.get('$state');
                //if requester handles the response
                //check login method of Auth service
                if (response.config.custom.handles !== undefined && response.config.custom.handles.indexOf(response.status) !== -1) {
                    //don't do anything, just fail it and pass the response to the requester
                    return $q.reject(response);
                }
                // console.log('Response Errors', response);
                // console.log('Error Headers', response.headers());
                if (response.status == 401 || response.status == 403) {
                    //if response if 403 and token used for the request is same as the current one
                    //It means session has timeout, just clear the token, log out and chnage the status code 440
                    if (response.status == 403 && response.config.custom.token == AuthToken.get()) {
                        response.status = 440;
                        AuthToken.set('');
                        alertService.add('warning', "Your session has timedout. Please login again.");
                        Auth.logout();
                    }
                    else {
                        //Check if authtoken is available and retry limit has not reached
                        if (AuthToken.get() !== '' && response.config.custom.retries <= 3 && (!response.headers()['token'] || (response.headers()['token'] != response.config.custom.token))) {
                            // debugger;
                            response.config.custom.retries++;
                            // console.log('Retry # ' + response.config.custom.retries)
                            $http = $http || $injector.get('$http');
                            // console.log("Retyring after " + (100 * response.config.custom.retries));
                            return $timeout(function () {
                                return $http(response.config);
                            }, 100);
                        } else {
                            if (response.status == 401) {
                                alertService.add('danger', "Unauthorized");
                                Auth.logout();
                            } else {
                                // debugger;
                                alertService.add('danger', "Forbidden");
                                // Auth.isLogged = false;
                                // localStorage.clear();
                                // sessionStorage.clear();
                                // State.go("app.home");                            
                            }
                        }
                    }
                } else if (response.status == 500) {
                    alertService.add('danger', "Internal Server Error Occurred.");
                }
                return $q.reject(response);
            }



        };
    }])
    

    //AuthToken Service
    .service('AuthToken', function () {
        var _token = sessionStorage.getItem('token');
        var _tokens = [];
        return {
            get: function(){
                if (_token === null) _token = '';
                return _token;
            },
            set: function(token){
                _token = token;
                sessionStorage.setItem('token', token);
                _tokens.push(token);
            }
        };
    });


