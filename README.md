This repo is to provide developers a means of working on ACES front-end using a nodejs server. All changes will ultimately need to be copied over to the ACESEpiEndpoints project as that's where the main ACES build happens.

Prerequites

- ACESEPIEndpoints project or ACES Backend Development
- nodejs https://nodejs.org/en/
- git bash https://gitforwindows.org/

Instructions

1. Clone Repo in your nodejs folder (ex. C:/Program Files/nodejs/aces/)
2. Clone ACESEPIEndpoints to (ex. C:/sts-workspace/ACESEPIEndpoints/)
3. Open CMD and go to C:/Program Files/nodejs/
3. Run copyApp.bat
4. Run 'npm install' and 'npm bower install'
6. a) Launch the ACESEpiEndpoints app. (For more info visit the repo).
		'ant clean build glassfish-deploy'

6. b) alternatively, you can pull the ACES Backend Development repo and run that from Eclipse/Spring Tool Suite. 

7. In CMD, go to C:/Program Files/nodejs/aces/ and run 'grunt serve'




*NOTE: If there's ever changes to the scripts/service/Auth.js or scripts/config.js from ACESEPIEndpoints then you'll need to update these files manually to work with the backend localhost:8080:

Open 'scripts/service/Auth.js' and paste code below before the 'return config' line (approx. 514)

			console.log('----------ALTER ENDPOINT IF USING NODEJS --------------------');  
								var chk = (config.url).split("lookup/phus-and-hosps");
								var chk2 = (config.url).split("lookup/ctas");
								var chk3 = (config.url).split("lookup/classifications");
								var chk4 = (config.url).split("lookup/classifiers"); 
								var chk5 = (config.url).split("lookup/aces");
								var chk6 = (config.url).split("lookup/aces/buckets");
								var chk7 = (config.url).split("aces/lookup");
								var chk8 = (config.url).split("aces/secure"); 
								if(chk.length > 1 || 
									chk2.length > 1 || 
									chk3.length > 1 || 
									chk4.length > 1 || 
									chk5.length > 1 || 
									chk6.length > 1 || 
									chk7.length > 1 || 
									chk8.length > 1){ 
									config.url = (config.url).replace('localhost', 'localhost:8080');
									config.url = (config.url).replace('localhost:8080:8080', 'localhost:8080');
								}  
								console.log('------------------------------');
			
 Open 'scripts/config.js' and change 'var path' on line 8 like below 

					//var path = '/AcesEpiEndpoint/aces';
					var path = 'http://localhost:8080/aces';
					
