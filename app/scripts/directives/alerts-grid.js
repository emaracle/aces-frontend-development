'use strict';

angular.module('acesApp')
    .directive('alertsGrid', ['$modal', '$q', '$sortService', 'Alerts', 'hospitals', 'ctas', 'SystemStatus', 'AlertTypes', 'classifications', 'GeogTypes', function ($modal, $q, $sortService, Alerts, hospitals, ctas, SystemStatus, AlertTypes, classifications, GeogTypes) {
        return {
            templateUrl: 'views/directives/alerts-grid.html',
            restrict: 'E',
            scope: {
                data: '=', //data for the list
                caption: "@", //title as dom attribute
                autoHeight: '=',
                hiddenColumns: '=',
                enableCsvExportPlugin: '=',
                geoNameCellTemplate: "=", // 0 - full (default), 1 - short with tooltip
                enablePaging: '='
            },
            // link: function ($scope, element, attrs) {
            compile: function() {
                return {
                    pre: function($scope, iElement, iAttrs) {
                        
                        // Creates a modal when called
                        $scope.open = function (row) {
                            Alerts.openAlertDetailModal(row.entity);
                        };

                        var plugins = [];
                        if ($scope.enableCsvExportPlugin !== false) {
                            plugins.push(new ngGridCsvExportPlugin({
                                // customDataWatcher: function () {
                                //     return $scope.data;
                                // }
                            }));
                        }
                        //
                        // if ($scope.autoHeight === true) {
                        //     //set the max height to windows less others (approx 200px)
                        //     var otherHeights = 300;
                        //     plugins.push(new ngGridFlexibleHeightPlugin({maxHeight: $(window).height() - otherHeights, minHeight: $(window).height() - otherHeights}));
                        // }

                        //default geoname cell template
                        var geoNameCellTemplate = '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text>{{row.entity.fullGeog}}</span></div>';
                        var geoNameCellDisplayName = 'Geog Name - (Value)';
                        if ($scope.geoNameCellTemplate == 1) {
                            geoNameCellTemplate = '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text  tooltip-placement="top" tooltip-append-to-body="true" tooltip="{{row.entity.GeogName}}">{{row.entity.GeogValue}}</span></div>';
                            geoNameCellDisplayName = 'Geog Name';
                        }

                        // var enablePaging = ($scope.enablePaging !== false ? true : false);

                        var columnDefs = [
                            {field: 'AlertOn', displayName: 'Alert Date/Time', width: '160px'},
                            {field: 'AlertEnd', displayName: 'Alert End Date/Time', width: '160px'},
                            {field: 'AlertFamilyID', displayName: 'Alert Class', width: '100px'},
                            {field: 'AlertTypeID', displayName: 'Alert Type', width: '100px', cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text tooltip-placement="top" tooltip-append-to-body="true" tooltip="{{row.entity.alertType}}">{{row.entity[col.field]}}</span></div>'},
                            {field: 'ClassID', displayName: 'Syndrome', width: '100px', cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text tooltip-placement="top" tooltip-append-to-body="true" tooltip="{{row.entity.classTitle}}">{{row.entity[col.field]}}</span></div>'},
                            {field: 'GeogType', displayName: 'Geog Type', width: '100px', cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text tooltip-placement="top" tooltip-append-to-body="true" tooltip="{{row.entity.geogTypeTitle}}">{{row.entity[col.field]}}</span></div>'},
                            {field: 'GeogName', displayName: geoNameCellDisplayName, cellTemplate: geoNameCellTemplate}
                        ];


                        var getSyndromeTitle = function (classID) {
                            for (var j in classifications.list) {
                                if (classifications.list[j].id === "S2014") {
                                    for (var i in classifications.list[j].classes) {
                                        if (classifications.list[j].classes[i].id == classID) {
                                            return classifications.list[j].classes[i].title;
                                        }
                                    }
                                }
                            }
                        };

                        //add/generate additional fields 
                        $scope.callbackModifyDataRow = function (row) {
                            //used as syndrome tooltip
                            if (row.ClassID !== undefined) {
                                row.classTitle = getSyndromeTitle(row.ClassID);
                            }
                            //used for geog name display
                            //also used in alertModal.html
                            row.fullGeog = row.GeogName + ' - (' + row.GeogValue + ') ';

                            //used for alert type tooltip
                            AlertTypes.get().then(function (alertTypes) {
                                // console.log(alertTypes);
                                for(var j in alertTypes) {
                                    if (row.AlertTypeID == alertTypes[j].AlertTypeID) {
                                        row.alertType =  alertTypes[j].AlertType;
                                        break;
                                    }
                                }
                            });

                            //used for GeogType tooltip
                            GeogTypes.get().then( function (list) {
                                for(var j in list) {
                                    if (row.GeogType == list[j].GeogTypeID) {
                                        row.geogTypeTitle =  list[j].GeogName;
                                        break;
                                    }
                                }
                            } );
                        };
                        $scope.sortInfo = {
                            fields: ['AlertOn'],
                            directions: ['desc']
                        };
                        $scope.gridOptions = {
                            plugins: plugins,
                            rowTemplate: '<div ng-style="{\'cursor\': row.cursor, \'z-index\': col.zIndex() }" ng-click="open(row)" ng-repeat="col in renderedColumns" ng-class="col.colIndex()" class="ngCell {{col.cellClass}}" ng-cell></div>',
                            columnDefs: columnDefs
                        };
                    }
                }
            }
        };
    }]);
