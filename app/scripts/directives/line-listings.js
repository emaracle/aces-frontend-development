'use strict';

angular.module('acesApp')
    .directive('lineListings', ['$modal', '$q', '$sortService', 'lineListings', 'hospitals', 'ctas', 'SystemStatus', 'Auth', '$window', 'alertService', function ($modal, $q, $sortService, lineListings, hospitals, ctas, SystemStatus, Auth, $window, alertService) {
      return {
        templateUrl: 'views/directives/line-listings.html?v=' + encodeURIComponent(ACES.version),
        restrict: 'E',
        scope: {
          data: '=', //data for the list
          caption: "@", //title as dom attribute
          autoHeight: '=',
          hiddenColumns: '=',
          disabledColumns: '='
        },
        // link: function ($scope, element, attrs) {
        compile: function() {
          return {
            pre: function($scope, iElement, iAttrs) {
              $scope.lineListings = lineListings;
              // Creates a modal when called
              $scope.open = function (row) {

                $scope.visitId = row.entity.VisitUUID;
                var modalInstance;

                // Initialize arrays
                var listingDetails = [];
                var cusum1, cusum2, cusum3 = 0;

                modalInstance = $modal.open({
                  templateUrl: 'views/lineListingModal.html',
                  controller: 'LineListingsModalCtrl',
                  size: 'lg',
                  resolve: {
                    'lineListingData': function () {
                      var deferred = $q.defer();
                      var includeAlert = (row.entity.IsAdmission ? false : true);
                      var visitType = (row.entity.IsAdmission ? 'ad' : 'ed');
                      lineListings.getLineListingsData($scope.visitId, includeAlert, visitType).then(
                        function (success) {
                          var dData = success.data[0];
                          console.log('data init', dData);

                          var allData =
                          {
                            'details': dData,
                            'rowData': row.entity,
                            'alerts': success.alerts
                          };

                          var rawData = success.data[1];
                          var sortedData = {};

                          // Cycle through the classification/syndrome data
                          for (var data in rawData) {
                            if (rawData.hasOwnProperty(data)) {
                              sortedData[data] = {};
                              for (var id in rawData[data]) {
                                if (rawData[data].hasOwnProperty(id)) {
                                  switch (id) {
                                    case 'ME':
                                      sortedData[data]["MAXIMUM ENTROPY"] = rawData[data][id];
                                      break;
                                    case 'BW':
                                      sortedData[data]["BALANCED WINNOW"] = rawData[data][id];
                                      break;
                                    case 'C45':
                                      sortedData[data]["C4.5 DECISION TREE"] = rawData[data][id];
                                      break;
                                    case 'NB':
                                      sortedData[data]["NAIVE BAYES"] = rawData[data][id];
                                      break;
                                    case 'W':
                                      sortedData[data]["WINNOW2"] = rawData[data][id];
                                      break;
                                    case 'MCME':
                                      sortedData[data]["MCME"] = rawData[data][id];
                                      break;
                                    default:
                                  }
                                }
                              }
                            }
                          }

                          allData.details.classifications = sortedData;
                          allData.keys = [
                            "MAXIMUM ENTROPY",
                            "BALANCED WINNOW",
                            "C4.5 DECISION TREE",
                            "NAIVE BAYES",
                            "WINNOW2",
                            "MCME"];
                          console.log(allData);
                          deferred.resolve(allData);
                        },
                        function (error) {
                          alertService.add('danger', 'Failed to get data. Please notify kflaphi@kflapublichealth.ca ');
                        }
                      );
                      return deferred.promise;
                    }
                  }
                });

                // This function allows us to get at the result of user clicking OK or CANCEL
                modalInstance.result.then(function () {
                    console.log("Modal: OK");
                }, function () {
                    console.log('Modal: cancel');
                });
              };

              var smallScreen = ($window.innerWidth <= 1024);

              var columnDefs = [
                //groupField property is not supported by ngGrid 2.0.14
                {field: 'VisitTimestamp', groupField: 'VisitDate', cellFilter: 'date:"yyyy-MM-dd"', aggLabelFilter: 'date:"yyyy-MM-dd"', displayName: 'Date', width: (smallScreen ? '90px' : '100px')},
                {field: 'VisitTime', displayName: 'Time', width: (smallScreen ? '70px' : '80px')},
                {field: 'Age', displayName: 'Age', width: '*', cellClass: 'text-center'},
                {field: 'Gender', displayName: 'Sex', width: '*', cellClass: 'text-center'},
                {field: 'ClassName', displayName: 'Syndrome', width: '100px'},
                {field: 'ChiefComplaint', displayName: 'Complaint', width: '400px'},
                {field: 'HospitalID', displayName: 'Hospital', width: '*', cellClass: 'line-listing-hospital-cell', cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text class="line-listings-cell-tooltip" tooltip-placement="top" tooltip-append-to-body="true" tooltip="{{row.entity.HospitalTitle}}">{{row.entity[col.field]}}</span></div>'},
                {field: 'FSALDU', displayName: 'FSALDU', width: '*'},
                {field: 'CTAS', displayName: 'CTAS', width:  '*', cellClass: 'line-listing-cta-cell text-center', cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text class="line-listings-cell-tooltip" tooltip-placement="top" tooltip-append-to-body="true" tooltip="{{row.entity.CTASTitle}}">{{row.entity[col.field]}}</span></div>'},
                {field: 'AdmissionType', displayName: 'Admission Type', width: '*'},
                {field: 'EMSArrival', displayName: 'EMS Arrival', width: '*', cellClass: 'text-center'},
                {field: 'PatientLHIN', displayName: 'Patient LHIN', width: '*'},
                {field: 'PatientPHU', displayName: 'Patient PHU', width: '*'},
              ];

              //remove columns which are in disabledColumns
              if (undefined !== $scope.disabledColumns) {
                columnDefs = _.filter(columnDefs, function (o) {
                  return ($scope.disabledColumns.indexOf(o.field) === -1);
                });
              }

              var hiddenColumns = []; //['FRIScore', 'ClassName'];
              if ($scope.hiddenColumns !== undefined) {
                hiddenColumns = $scope.hiddenColumns;
              }

              //hide hidden columns
              for (var i in columnDefs) {
                if (_.indexOf(hiddenColumns, columnDefs[i].field) >= 0) {
                  columnDefs[i].visible = false;
                }
              }

              var getHospitalTitle = function (HospitalID) {
                for (var i in hospitals.hospitals) {
                  if (hospitals.hospitals[i].id == HospitalID) {
                    return hospitals.hospitals[i].title;
                  }
                }
                return "";
              };

              var getCTATitle = function (CTAS) {
                for (var i in ctas.list) {
                  if (ctas.list[i].TriageID == CTAS) {
                    return ctas.list[i].Triage;
                  }
                }
                return "";
              };

              //add/generate additional fields
              $scope.callbackModifyDataRow = (function (row) {
                //used as hospital tooltip
                if (row.HospitalID !== undefined) {
                  row.HospitalTitle = getHospitalTitle(row.HospitalID);
                }
                //used as hospital tooltip
                if (row.CTAS !== undefined) {
                  row.CTASTitle = getCTATitle(row.CTAS);
                }
              });

              $scope.gridOptions = {
                rowTemplate: '<div ng-style="{\'cursor\': row.cursor, \'z-index\': col.zIndex() }" ng-click="open(row)" ng-repeat="col in renderedColumns" ng-class="col.colIndex()" class="ngCell {{col.cellClass}}" ng-cell></div>',
                columnDefs: columnDefs
              };

              //set the default sort to VisitTime
              $scope.sortInfo = { fields: ['VisitTimestamp'], directions: ['desc']};
            }
          }
        }
      };
    }]);
