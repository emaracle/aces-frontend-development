'use strict';

angular.module('acesApp')
    .directive('epicurvesForm', ['ctas', 'hospitals', 'classifications', 'buckets', 'syndromes', 'classifiers', 'fsas', 'alertService', 'utils', '$log',
        function ( ctas, hospitals, classifications, buckets, syndromes, classifiers, fsas, alertService, utils, $log) {
        return {
            templateUrl: 'views/epicurves-form.html?v=' + encodeURIComponent(ACES.buildnumber),
            restrict: 'E',
            scope: {
                params: '=', //will have all the data required to init form
                submit: '=', //this method will be called when a user submits the form
                reset: '=' //this method will be called when user clicks on reset button
            },
            link: function ($scope, element, attrs) {

                //Local Variables
                $scope.form = {};

                //Advanced filter count
                $scope.fltrcount = 0;

                //Count active filters
                $scope.updateFilterCount = function(){
                    var tempFilterCount = 0;

                    if ($scope.params.patientLocality.id !== 'local'){ tempFilterCount ++; } //Patient Locality (Patients)
                    if ($scope.params.hospitalLocality.id !== 'All'){ tempFilterCount ++; } //Hospitals
                    if ($scope.params.fsa.title !== 'All'){ tempFilterCount ++; } //FSA
                    if ($scope.params.admissionType === 'AD' && $scope.params.fltrAmType.id !== 'All'){ tempFilterCount ++; } //Admission Type if on AD page

                    $scope.fltrcount = tempFilterCount;
                }

                //Update filter count on form load
                $scope.updateFilterCount();

                //Convert to date type for UI Bootstrap datepicker
                $scope.params.dateFrom = moment($scope.params.dateFrom).toDate();
                $scope.params.dateTo = moment($scope.params.dateTo).toDate();

                //Load Grouping
                $scope.groupingWarning = "";
                $scope.clearGroupingWarning = function () {
                    $scope.groupingWarning = "";
                };

                $scope.groupings = [
                    {id: 'D', title: 'Day'},
                    {id: 'W', title: 'Week'},
                    {id: 'M', title: 'Month'},
                    {id: 'Y', title: 'Year'}
                ];

                $scope.setGrouping = function (grouping) {
                    $scope.params.grouping = grouping;
                    $scope.params.selectedGroup = grouping.id;

                    $scope.groupingWarning = "";

                    if (grouping.id != 'D'){
                        $scope.groupingWarning = "Moving Average and Standard Deviation are not available while grouping dates by " + grouping.title + ".";
                    }
                };

                $scope.getGrouping = function(groupingId){
                    var selectedGroup = _.find($scope.groupings, function(o) { return o.id == groupingId; });
                    if (selectedGroup === undefined) {
                        selectedGroup = $scope.groupings[0];
                    };
                    $scope.setGrouping(selectedGroup);
                }

                $scope.getGrouping($scope.params.selectedGroup);

                //Only trigger the watch if grouping value is empty
                $scope.$watch('params.selectedGroup', function (newVal) {
                    if (newVal !== undefined){
                        if ($scope.params.grouping === undefined){
                            $scope.getGrouping(newVal);
                        }
                    }
                });

                $scope.$watch('params.grouping', function () {
                    if ($scope.params.grouping === undefined){
                        $scope.getGrouping('D');
                    }
                });

                //Load Week Start
                $scope.weekStarts = [
                    {id: 7, title: 'Sunday'},
                    {id: 1, title: 'Monday'}
                ];

                $scope.setWeekStart = function (weekStart) {
                    $scope.params.weekStart = weekStart.id;
                };

                var weekStart = _.find($scope.weekStarts, function(o) { return o.id == $scope.params.weekStart; });
                if (weekStart === undefined) {
                    weekStart = $scope.weekStarts[0];
                };
                $scope.setWeekStart(weekStart);

                //Adjust dates depending on grouping option selected
                $scope.adjustDateFrom = function() {

                    //Update 'date from' depending on grouping option
                    var newDateFrom;
                    switch($scope.params.selectedGroup) {
                        case 'W':
                            if ($scope.params.weekStart != 7){
                                newDateFrom = moment($scope.params.dateFrom).startOf('isoWeek').startOf('day');
                            } else {
                                newDateFrom = moment($scope.params.dateFrom).startOf('week').startOf('day');
                            };
                            break;
                        case 'M':
                            newDateFrom = moment($scope.params.dateFrom).startOf('month').startOf('day');
                            break;
                        case 'Y':
                            newDateFrom = moment($scope.params.dateFrom).startOf('year').startOf('day');
                            break;
                        default:
                            newDateFrom = moment($scope.params.dateFrom).startOf('day');
                    };

                    $scope.params.dateFrom = moment(newDateFrom).toDate();
                };

                $scope.adjustDateTo = function(clickWeekStart) {

                    //Update 'date to' depending on grouping option
                    var newDateTo;
                    switch($scope.params.selectedGroup) {
                        case 'W':
                            newDateTo = moment($scope.params.dateTo);
                            //If toggling between week start sunday or monday, stay within same week
                            if (clickWeekStart){
                                newDateTo.subtract(1,'d');
                            };

                            if ($scope.params.weekStart != 7){
                                newDateTo.endOf('isoWeek').endOf('day');
                            } else {
                                newDateTo.endOf('week').endOf('day');
                            };

                            break;
                        case 'M':
                            newDateTo = moment($scope.params.dateTo).endOf('month').endOf('day');
                            break;
                        case 'Y':
                            newDateTo = moment($scope.params.dateTo).endOf('year').endOf('day');
                            break;
                        default:
                            newDateTo = moment($scope.params.dateTo).endOf('day');
                    }

                    $scope.params.dateTo = moment(newDateTo).toDate();
                };

                $scope.adjustDateFrom();
                $scope.adjustDateTo(false);

                //Date Range datepicker settings
                $scope.form.openDateFromPopup = function($event) {
                    $event.preventDefault();
                    $event.stopPropagation();

                    $scope.form.dateFromOpened = true;
                    $scope.form.dateToOpened = false;
                };

                $scope.form.openDateToPopup = function($event) {
                    $event.preventDefault();
                    $event.stopPropagation();

                    $scope.form.dateToOpened = true;
                    $scope.form.dateFromOpened = false;
                };

                $scope.form.dateOptions = {
                    formatDay: {
                        formatDay: 'dd',
                        formatMonth: 'MM',
                        formatYear: 'yyyy',
                        startingDay: 0,
                        minMode: 'day',
                        showWeeks:'false'
                    },
                    formatWeek: {
                        formatDay: 'dd',
                        formatMonth: 'MM',
                        formatYear: 'yyyy',
                        startingDay: 0,
                        minMode: 'day',
                        showWeeks:'true'
                    },
                    formatIsoWeek: {
                        formatDay: 'dd',
                        formatMonth: 'MM',
                        formatYear: 'yyyy',
                        startingDay: 1,
                        minMode: 'day',
                        showWeeks:'true'
                    },
                    formatMonth: {
                        formatMonth: 'MMMM',
                        formatYear: 'yyyy',
                        startingDay: 0,
                        minMode: 'month',
                        showWeeks:'false'
                    },
                    formatYear: {
                        formatYear: 'yyyy',
                        startingDay: 0,
                        minMode: 'year',
                        showWeeks:'false'
                    },
                    minDate: moment("01-01-2001", "DD-MM-YYYY").toDate(),
                    maxDate: moment().endOf('day').toDate()
                };

                //reset form
                $scope.resetForm = function(){
                    $scope.form.dateFromOpened = false;
                    $scope.form.dateToOpened = false;
                }

                //Load admission type
                $scope.fltrAmTypes = [
                    {id: 'All', title: 'All'},
                    {id: 'EL', title: 'Elective'},
                    {id: 'EM', title: 'Emergent'}
                ];

                $scope.setFltrAmType = function (fltrAmType) {
                    $scope.params.fltrAmType = fltrAmType;
                };

                $scope.getFltrAmType = function(fltrAmTypeId){
                    var selectedFltrAmType = _.find($scope.fltrAmTypes, function(o) { return o.id == fltrAmTypeId; });
                    if (selectedFltrAmType === undefined) {
                        selectedFltrAmType = $scope.fltrAmTypes[0];
                    };
                    $scope.setFltrAmType(selectedFltrAmType);
                }

                $scope.getFltrAmType($scope.params.fltrAmType.id);

                //Load phus/hospitals/fsas
                $scope.fsas = [];
                $scope.setFsa = function (fsa) {
                    $scope.params.fsa = fsa;
                };


                $scope.hospitals = [];
                $scope.setHospital = function (hospital) {
                    $scope.params.hospital = hospital;
                };

                hospitals.getData(($scope.params.includeAllPhus == true ? true : undefined)).then( function(data) {
                    $scope.phus = data.phus;
                    //init
                    angular.forEach($scope.phus, function (phu) {
                        if (phu.id == $scope.params.phu.id) {
                            $scope.setPhu(phu);
                            return;
                        }
                    });
                });

                $scope.phus = [];
                $scope.setPhu = function (phu) {
                    $scope.params.phu = phu;
                    fsas.getData({phu: phu.id}).then(function (data) {
                        $scope.fsas = data.fsas;
                    });

                    $scope.hospitals = phu.hospitals;
                    //Change the selected hospital to the first if it doesn't exists in the hospitals list
                    var found = false;
                    for (var i in $scope.hospitals) {
                        if ($scope.params.hospital.id == $scope.hospitals[i].id) {
                            $scope.setHospital($scope.hospitals[i]);
                            found = true;
                            break;
                        }
                    }
                    if (!found) {
                        //select the first hospital
                        $scope.setHospital($scope.hospitals[0]);
                    }
                };

                //Load Gender
                $scope.genders = [
                    {id: 'All', title: 'All'},
                    {id: 'M', title: 'Male'},
                    {id: 'F', title: 'Female'}
                ];

                //Load Age Groups
                $scope.ageGroups = [
                    {id: 'All', title: 'All', min: 0, max: 130},
                    {id: 'Child', title: 'Child', min: 0, max: 17},
                    {id: 'School Child', title: 'School Child', min: 5, max: 17},
                    {id: 'Adult', title: 'Adult', min: 18, max: 64},
                    {id: 'Senior', title: 'Senior', min: 65, max: 130},
                    {id: 'Adult + Senior', title: 'Adult + Senior', min: 18, max: 130}
                ];

                $scope.setAge = function (ageGroup) {
                    $scope.params.toAge = ageGroup.max;
                    $scope.params.fromAge = ageGroup.min;
                };

                //this variable will be used to save last values
                //since getAgeDesc is bounded, this function will be called frequently
                //we do not want to go thru loop and check the values everytime
                //if values have not changed, will return the last title
                var currentSelectedAgeGroup = null;
                $scope.getAgeDesc = function () {
                    if (currentSelectedAgeGroup !== null && currentSelectedAgeGroup.min == $scope.params.fromAge && currentSelectedAgeGroup.max == $scope.params.toAge) {
                        return currentSelectedAgeGroup.title;
                    }

                    currentSelectedAgeGroup = {
                        title: 'Custom',
                        min: $scope.params.fromAge,
                        max: $scope.params.toAge
                    };

                    angular.forEach($scope.ageGroups, function(ageGroup) {
                        if ($scope.params.fromAge == ageGroup.min && $scope.params.toAge == ageGroup.max) {
                            currentSelectedAgeGroup.title = ageGroup.title;
                            return;//exits loop
                        }
                    });
                    return currentSelectedAgeGroup.title;
                }

                //Load classifications/buckets/syndromes/classifiers
                $scope.classifications = [];
                $scope.buckets = [];
                $scope.syndromes = [];
                $scope.classifiers = [];

                $scope.setClassifier = function (classifier) {
                    $scope.params.classifier = classifier;
                }

                classifiers.getData().then(function (data) {
                    $scope.classifiers = data.list;
                    //Set the classifier to ME by default
                    angular.forEach($scope.classifiers, function (classifier) {
                        if (classifier.ClassifierID == 'ME') {
                           $scope.setClassifier(classifier);
                            return;
                        }
                    });
                });

                $scope.setSyndrome = function (syndrome) {
                    $scope.params.syndrome = syndrome;
                };

                $scope.setBucket = function(bucket) {
                    $scope.params.bucket = bucket;
                    //Load syndromes
                    syndromes.getData($scope.params.classification.id, bucket.BucketID).then(function(list){
                        $scope.syndromes = list;
                        $scope.initializeSyndromes();
                        //Check if param syndromes array is empty
                        if ($scope.params.syndromes.length == 0){
                            $scope.toggleSyndrome($scope.syndromes[0]);
                        } else {
                            //Check which syndromes have already been selected
                            for (var i in $scope.syndromes){
                                for (var j in $scope.params.syndromes){
                                    if ($scope.params.syndromes[j].id == $scope.syndromes[i].id){
                                        $scope.toggleSyndrome($scope.syndromes[i]);
                                    }
                                }
                            }
                        };
                    });
                };

                $scope.resetBucket = function(bucket) {
                    $scope.params.bucket = bucket;
                    //Load syndromes
                    syndromes.getData($scope.params.classification.id, bucket.BucketID).then(function(list){
                        $scope.syndromes = list;
                        $scope.clearSyndromes();

                        //if All bucket is selected, select 'all' syndrome otherwise select all syndromes except for 'all'
                        if (bucket.BucketID === 'All'){
                            $scope.toggleSyndrome($scope.syndromes[0]);
                        } else {
                            for (var i in $scope.syndromes){
                                if (i > 0) {
                                    $scope.toggleSyndrome($scope.syndromes[i]);
                                };
                            };
                        };
                    });
                };

                $scope.toggleSyndrome = function (syndrome) {

                    var maxSyndromes = 5;

                    if (!syndrome.selected) {
                        if ($scope.selectedSyndromes < maxSyndromes){
                            syndrome.selected = true;
                            $scope.selectedSyndromes++;
                            $scope.manageSydromeList(true,syndrome);
                        }
                    } else {
                        syndrome.selected = false;
                        $scope.selectedSyndromes--;
                        $scope.manageSydromeList(false,syndrome);
                    }
                };

                $scope.clearSyndromes = function () {
                    $scope.selectedSyndromes = 0;
                    $scope.syndromeList = '';
                    $scope.params.syndromes = [];
                    for (var i in $scope.syndromes) {
                        $scope.syndromes[i].selected = false;
                    }
                };

                $scope.setClassification = function (classification) {
                    $scope.params.classification = classification;
                    //refresh buckets list
                    buckets.getData(classification.id).then(function(list){
                        $scope.buckets = list
                        //check if current selected bucket is present in the list
                        //if not set or the the current selected as the first element in the list
                        var found = false;
                        //check only if buckets is init
                        angular.forEach(list, function(bucket){
                            if (bucket.BucketID == $scope.params.bucket.BucketID) {
                                found = true;
                                return;//exit the loop
                            }
                        });
                        //Set bucket to first in the list
                        if (!found) {
                            $scope.setBucket(list[0]);
                        } else {
                            //this is important as this will refresh classes list
                            $scope.setBucket($scope.params.bucket);
                        }
                    });
                };

                //Syndrome Selection Button
                $scope.initializeSyndromes = function (){
                    $scope.selectedSyndromes = 0;
                    $scope.syndromeList = '';
                    //Check syndrome array status
                    if (!angular.isArray($scope.params.syndromes)){
                        var tempValue = $scope.params.syndromes;
                        $scope.params.syndromes = [];
                        for (var i in $scope.syndromes){
                            if ($scope.syndromes[i].id == tempValue.id){
                                $scope.params.syndromes.push($scope.syndromes[i]);
                            }
                        }
                    };
                };

                $scope.manageSydromeList = function (add, syndrome){
                    if (add) {
                        //Adding a syndrome to
                        var inArray = false;
                        for (var i in $scope.params.syndromes){
                            if ($scope.params.syndromes[i].id == syndrome.id){
                                inArray = true;
                            }
                        }
                        if  (!inArray){
                            $scope.params.syndromes.push(syndrome);
                        }
                    } else {
                        //Removing a syndrome
                        var position = -1;
                        for (var i in $scope.params.syndromes){
                            if ($scope.params.syndromes[i].id == syndrome.id){
                                position =  i;
                            }
                        }
                        $scope.params.syndromes.splice(position,1);
                    }

                    //Sort syndrome id
                    var syndromesSorted = [];
                    for (var i in $scope.params.syndromes){
                        syndromesSorted[i] = $scope.params.syndromes[i].id;
                    }

                    $scope.syndromeList = '';

                    //Build string from sorted array
                    for (var i in syndromesSorted.sort()){
                        if (i < 1){
                            $scope.syndromeList = syndromesSorted[i];
                        } else {
                            $scope.syndromeList = $scope.syndromeList + ", " + syndromesSorted[i];
                        }
                    }
                };

                $scope.$watch('params.classification', function (newVal) {
                    // console.log(arguments);
                    if (newVal !== undefined) {
                        $scope.setClassification(newVal);
                    }
                });

                classifications.getData().then(function (data) {
                    $scope.classifications = data.list;
                    //init
                    angular.forEach($scope.classifications, function (classification) {
                        if ($scope.params.classification.id == classification.id) {
                            $scope.params.classification = classification;
                            // $scope.setClassification(classification);
                            return;
                        }
                    });
                });


                //Load ctas
                $scope.ctas = [];
                var indexOfAll = 0;
                var allSelectedValue = '';
                var noneSelectedValue = '';
                ctas.getData().then( function (factory) {
                    $scope.ctas = factory.list;
                    //last element will be all
                    indexOfAll = (factory.list.length - 1);
                    for (var i = 0; i <= indexOfAll; i++) {
                        allSelectedValue += 'Y';
                        noneSelectedValue += 'N';
                    }
                });
                $scope.getCtaTooltipText = function (cta) {
                    if (cta.Triage === undefined)
                        return '';

                    return '<div class="cta-tooltip-text"><div class="header">' + cta.Triage + '</div><div class="description">' + cta.Description + '</div></div>';
                };
                $scope.isActiveCta = function (index) {
                    // console.log(index + $scope.params.cta.charAt(index) + ' - ' +  moment().valueOf());
                    if (index == indexOfAll) {
                        return ($scope.params.cta == allSelectedValue);
                    } else {
                        return ($scope.params.cta.charAt(index) == 'Y' && $scope.params.cta != allSelectedValue);
                    }
                };
                var replaceCharAt = function (str, index, newVal){
                    return str.substr(0, index) + newVal + str.substr(index+newVal.length);
                };
                $scope.setCta = function (index) {
                    var str = $scope.params.cta;
                    var currentVal = str.charAt(index);
                    if (index == indexOfAll) {
                        if (currentVal == 'Y') {
                            str = noneSelectedValue;
                        } else {
                            str = allSelectedValue;
                        }
                    } else {
                        if (str == allSelectedValue) {
                            str = noneSelectedValue;
                        }
                        currentVal = str.charAt(index);
                        var newVal = (currentVal == 'Y' ? 'N' : 'Y');

                        str = replaceCharAt(str, index, newVal);

                        //if all the ctas are selected
                        //set all also selected
                        if (str.substr(0, indexOfAll) == allSelectedValue.substr(0, indexOfAll)) {
                            str = allSelectedValue;
                        }
                    }
                    $scope.params.cta = str;
                };

                //Load patientLocalities
                $scope.localityError = "";
                $scope.clearLocalityError = function () {
                    $scope.localityError = "";
                };
                $scope.patientLocalities = utils.getPatientLocalities();
                $scope.setPatientLocality = function (patientLocality) {
                    if (patientLocality.id != 'local' && $scope.params.hospitalLocality.id != 'local') {
                        $scope.localityError = "You must have either Hospital Locality or Patient Locality set to 'Local'";
                    } else {
                        $scope.params.patientLocality = patientLocality;
                    }
                };
                //init
                angular.forEach($scope.patientLocalities, function (patientLocality) {
                    if (patientLocality.id == $scope.params.patientLocality.id) {
                        $scope.setPatientLocality(patientLocality);
                        return;
                    }
                });

                $scope.$watch("params.patientLocality", function () {
                    $scope.clearLocalityError();
                });

                //Load hospitalLocalities
                $scope.hospitalLocalities = utils.getHospitalLocalities();
                $scope.setHospitalLocality = function (hospitalLocality) {
                    if (hospitalLocality.id != 'local' && $scope.params.patientLocality.id != 'local') {
                        $scope.localityError = "You must have either Hospital Locality or Patient Locality set to 'Local'";
                    } else {
                        $scope.params.hospitalLocality = hospitalLocality;
                    }
                };
                //init
                angular.forEach($scope.hospitalLocalities, function (hospitalLocality) {
                    if (hospitalLocality.id == $scope.params.hospitalLocality.id) {
                        $scope.setHospitalLocality(hospitalLocality);
                        return;
                    }
                });

                $scope.$watch("params.hospitalLocality", function () {
                    $scope.clearLocalityError();
                });

                $scope.doSubmit = function () {
                    $scope.errors = {};
                    //perform validations
                     //validate
                    var dateFrom = moment($scope.params.dateFrom);
                    var dateTo = moment($scope.params.dateTo);

                    var now = moment().endOf('day');
                    if (dateFrom > now) {
                        $scope.errors.dateFrom = {
                            futureDate: true
                        };
                    } else {
                        if (dateFrom > dateTo) {
                            $scope.errors.dateFrom = {
                                mustBeSmallerThanDateTo: true
                            };
                        }
                    }
                    if (dateTo > now) {
                        $scope.errors.dateTo = {
                            futureDate: true
                        };
                    }
                    //validate age
                    var ageRegex = /^(-1|[0-9]|([1-9][0-9])|([1-9][0-9][0-9]))$/;
                    if (!ageRegex.test($scope.params.fromAge)) {
                        $scope.errors.fromAge = {
                            invalid: true
                        };
                    }
                    if (!ageRegex.test($scope.params.toAge)) {
                        $scope.errors.toAge = {
                            invalid: true
                        };
                    }
                    //if not age error
                    if (undefined === $scope.errors.toAge && undefined === $scope.errors.fromAge) {
                        //check if toAge is higher than fromAge
                        if (parseInt($scope.params.toAge) < parseInt($scope.params.fromAge)) {
                            $scope.errors.fromAge = {
                                mustBeSmallerThanToAge: true
                            };
                        }
                    }

                    //no syndrome selected
                    if ($scope.params.syndromes.length == 0){
                        $scope.errors.syndromes = {
                            empty: true
                        };
                    }

                    //no errors
                    if (angular.equals($scope.errors, {})) {
                        if (typeof $scope.submit === 'function') {
                            $scope.submit($scope.params);
                            $scope.updateFilterCount();
                        }
                    }
                    // alert("Submitting...");
                };

                /**
                * @param button int button number
                */
                $scope.doReset = function (button) {
                    if (typeof $scope.reset === 'function') {
                        $scope.reset(button);
                        $scope.resetForm();

                        //Clear filter count
                        $scope.fltrcount = 0;
                    }
                };


                //Syndrome filter
                $scope.filterSyndromeFn = function(syndrome){
                    var type = $scope.params.admissionType;
                    if (!syndrome[type]) {
                        return false;
                    }
                    return true;
                };

            }
        };
    }])
    /**
    * This filter is used to orderBy property but will put the property with value equal to exception to the top of the list
    *
    * Usage e.g. ng-repeat="hospital in hospitals | orderByWithException: 'id': 'All'"
    * The example above will order the hospitals list by id and will put the hospital with id 'All' to the top of the list
    */
    .filter('orderByWithException', ['$filter', function($filter) {
        return function(arr, field, exception) {
            arr = ($filter('orderBy')(arr, field));
            var newArr = [];
            for (var i in arr) {
                if (arr[i][field] == exception) {
                    //add to the beginning
                    newArr.unshift(arr[i]);
                } else {
                    //add to the end
                    newArr.push(arr[i]);
                }
            }
            return newArr;
        };
    }])
;
