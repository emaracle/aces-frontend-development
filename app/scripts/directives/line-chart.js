'use strict';

angular.module('acesApp')
	.directive('lineChart', ['$rootScope', 'SystemStatus', 'utils','$log', 'colourList', function ($rootScope, SystemStatus, utils,$log, colourList) {

		return {
      template: '<div id="chart"><svg></svg></div><div id="chart-buttons"><button class="btn btn-default btn-sm" ng-click="download()"><i class="glyphicon glyphicon-download-alt"></i> Download Chart</button></div>',
      restrict: 'E',
      scope: {
        val: '=',
        tooltipContentHandler: '=',
        legendToolTipContentHandler: '=',
        alertTooltipContentHandler: '=',
        disabledLegends: '=' //list of disbaled legends
      },
      link: function postLink(scope, element, attrs) {

        if (scope.disabledLegends === undefined) {
          var disabledLegends = [];
        } else {
          var disabledLegends = scope.disabledLegends;
        }

        var parseDate = d3.time.format("%Y-%m-%d").parse;
        var data = [];
        var alerts = [];

        scope.$watch('val', function (newVal, oldVal) {
          if (!newVal.ready) {
            return;
          }

          data = angular.copy(newVal.data);

 					//convert StartDate and EndDate to number(timestamp) from date string
          alerts = newVal.alerts.map(function (alert) {
            if (typeof alert.StartDate == "string") {
              alert.StartDate = moment(alert.StartDate).valueOf();
              alert.EndDate = moment(alert.EndDate).valueOf();
            }
            return alert;
          });

          //Add alerts to data array
          for (var i in data) {
            //running in local scope
            (function (baseData) {
              var colors = {
                Bias: '#4b79f0',
                CD: '#539440',
                CUSUM1: '#a768eb',
                CUSUM2: '#6564c3',
                CUSUM3: '#cc7753',
                Extreme: '#342de1',
                Ignorance: '#8d8fb0',
                NMG: '#a39f53',
                NRC: '#2b0ec6',
                OnEdge: '#236d21',
                Oscillation: '#935fa3',
                RLS: '#0c4445',
                Tendency: '#dd7df2',
                Trend: '#a94186'
              }

              for (var j in alerts) {
                //verify if the alert is for the baseData
                if (
                  //If FSA is selected and key for baseData is 'All'
                  (baseData.key == 'All' && alerts[j].GeogType == "FSA" && newVal.alertParams.fsa == alerts[j].GeogValue)
                  || //If phu and hospital is selected and key for baseData is 'All'
                  (baseData.key == 'All' && newVal.alertParams.fsa == 'All' && alerts[j].GeogType == "HOSP" && newVal.alertParams.hospital == alerts[j].GeogValue)
                  || //If phu and hospital is selected and key for baseData is 'All'
                  (baseData.key == 'All' && newVal.alertParams.fsa == 'All' && newVal.alertParams.hospital == 'All' && alerts[j].GeogType == "PHU" && newVal.alertParams.phu == alerts[j].GeogValue)
                  || //If phu and hospital is selected
                  (alerts[j].GeogValue == baseData.key && newVal.alertParams.fsa == 'All' && (newVal.alertParams.class == 'All' || (newVal.alertParams.class == alerts[j].Class)))
              	){
                  //allow
                } else {
                  continue;
                  //skip
                }

                var alert = alerts[j];
                var newObj = angular.copy(baseData);
                newObj.area = true;
                newObj.areaOnly = true;
                //set color based on AlertType, if color defination not found, use default
                newObj.color = (colors[alert.AlertType] !== undefined ? colors[alert.AlertType] : '#0000ff');
								newObj.alert = alert;

                //modify the values
                newObj.values = (function (values) {
                    var sTime = alert.StartDate;
                    var eTime = alert.EndDate;
                    var newArr = [];
                    var sAdded = false;
                    var lastY = -1;

                    //calculate the value of the y propotionately based on previous and next y values relative to the time
                    var calculateRelativeY = function (prevObj, nextObj, alertStartTime) {
                        var xDiff = nextObj.x - prevObj.x;
                        var yDiff = nextObj.y - prevObj.y
                        var interval = yDiff / xDiff;

                        var y = prevObj.y + ((alertStartTime - prevObj.x) * interval);
                        //set the y to 0 if less than 0
                        if (y < 0) y = 0;
                        return y;
                    };

                    //add alert start and end points to the values
                    //and remove values which are before and after alert start and end date respectively
                    for (var i in values) {

                      if (values[i].x >= sTime) {
                        //add the alert start date if not previously
                        if (!sAdded && values[0].x <= sTime) {
                          if (i > 0) {
                            newArr.push({
                              x: sTime,
                              //set the value of the y propotionately based on previous y and current y values relative to the time
                              y: calculateRelativeY( values[i-1], values[i], sTime),
                              interactive: false //hide this point from interaction
                            });
                            sAdded = true;
                          }
                        }

                        if (values[i].x >= eTime) {
                          if (i > 0) {
                            newArr.push({
                              x: eTime,
                              //set the value of the y propotionately based on current and next y values relative to the time
                              y: calculateRelativeY( values[i-1], values[i], eTime),
                              interactive: false //hide this point from interaction
                            });
                          }
                          break;
                        } else {
                          newArr.push(values[i]);
                        }
                      }
                    }
                    return newArr;
                    //sort the new array
                    return newArr.sort(
                      function (a,b) {
                        if (a.x < b.x) {
                          return -1;
                        } else if (a.x > b.x) {
                          return 1;
                        } else {
                          return 0;
                        }
                      });
                    return newArr;
                })(newObj.values);

                newObj.key = newObj.key + ' - ' + alert.AlertType + ' - ' + j;
                //add to main data table
                if (newObj.values.length > 1) {
                    data.push(newObj);
                }
              }
            } )( data[i] );
          }

          //Calculate tickValues
          var lowestX = 0;
          var highestX = 0;
          var lowestY = 0;
          var highestY = 0;
          var maxValuesX = 0;
          var maxTicksX = 8;
          var maxTicksY = 7;
          data.forEach(function(item){
            item.values.forEach(function(value){
              if (highestX === 0 || value.x > highestX) {
                highestX = value.x;
              }
              if (lowestX === 0 || value.x < lowestX) {
                lowestX = value.x;
              }
              if (highestY === 0 || value.y > highestY) {
                highestY = value.y;
              }
              if (lowestY === 0 || value.y < lowestY) {
                lowestY = value.y;
              }
            });
          });

          var tickValuesX = [];
          var xAxisLabel = '';
          (function () {
            var interval;
            //can be Y for year, M for month, W for week and D for day. Defaults to D
            switch(newVal.xInterval) {
              case 'Y':
                interval = 'years';
                xAxisLabel = 'Year';
              break;
              case 'M':
                interval = 'months';
                xAxisLabel = 'Month';
              break;
              case 'W':
                interval = 'weeks';
                xAxisLabel = 'Week';
              break;
              default:
                interval = 'days';
                xAxisLabel = 'Date';
            }
            var j = 0;
            for(var i = lowestX; i < highestX; (i = (moment(i).add(1, interval).valueOf()))) {
              tickValuesX.push(i);
            }
          })();
          (function () {
            var i = 1;
            if (tickValuesX.length > maxTicksX) {
              i = Math.round(tickValuesX.length / maxTicksX);
              tickValuesX = _.filter(tickValuesX, function (v, index) {
                return index % i === 0;
              });
            }
          })();

          var bar = d3.select('#chart svg');

          nv.addGraph(function () {
            if (scope.chart) {
              //remove old chart svg
              d3.select( '#chart svg' ).remove();
              //add new svg
              d3.select( '#chart' ).append( 'svg' );
            }

            var chart = nv.models.lineWithFocusChart();

            chart.lines.dispatch.on( 'elementClick', function ( event ) {
              /* this needs to be reviewed */
              var isAvgCheck = (event.series.key).split('day AVG').length;
              var isNormalizeCheck = (event.series.key).split('Normalized').length;
              var isSTDCheck1 = (event.series.key).split('STD 1').length;
              var isSTDCheck2 = (event.series.key).split('STD 2').length;

              if(isAvgCheck == 1 &&  isNormalizeCheck == 1 && isSTDCheck1 == 1 && isSTDCheck2 == 1){
                scope.$emit( 'lineChartElementClick', event );
              }
            });

            //disable series which are disabled before
            data = data.map(function (d) {
              if (scope.disabledLegends !==undefined && scope.disabledLegends.indexOf(d.key) !== -1) {
                d.disabled = true;
              }
              return d;
            });

            //customize the rendered svg
            var customize = (function () {
              if (typeof maxY != 'undefined' && typeof getMaxY != 'undefined') {

                /*adjusts max Y axis value when the data changes. This is applicable in scenario
                  where when specific line is hidden, the chart adjust the max y based on the
                  remaining line to make it more readable. This also applies when a selection
                  is made in focus chart.*/

                //get the data for the remaining lines.
                var d = d3.select(".nv-focus .nv-line .nv-groups").datum();
                var lastMaxY = scope.chart.forceY()[1];
                var newMaxY = getMaxY(d);
                //if the last maxY value is different than the current max y value
                //this check is important to avoid infinite loop
                if (newMaxY != maxY) {
                  maxY = newMaxY;
                  //redraw it
                  scope.chart.forceY([0, maxY]).update();
                  return;
                }
              }

              //add stroke to alert areas in both focus and context charts
              d3.selectAll('path.nv-area')
                .each(function (d) {
                  if (d.color !== undefined) {
                    d3.select(this).style("stroke", d.color);
                  } else if (d.original && d.original.color !== undefined) {
                    d3.select(this).style("stroke", d.original.color);
                  };
                });

              d3.selectAll('.nv-focus path.nv-area')
                .on('click', function (d) {
                    var alert = d.original.alert;
                    scope.$emit( 'alertChartElementClick', event, alert );
                })

                .on('mouseover.nvArea', function () {
                	//if alertTooltipContentHandler method defined
                  if (scope.alertTooltipContentHandler) {
                    var box = this.getBoundingClientRect();
                    var left = box.left + (box.width / 2), top = box.top;
                    var content = '';
                    var d = d3.select(this).datum();
                    if (d.original && d.original.alert) {
                        content = scope.alertTooltipContentHandler(d.original.alert);
                    }

                    nv.tooltip.show([left, top], content, null, null);
                  }

                  d3.select(this)
                    .style("stroke", "#000")
                    .style("stroke-dasharray", [5, 1, 3, 1]);
                })
                .on('mouseout.nvArea', function (d) {
                  nv.tooltip.cleanup();
                  if (d.original && d.original.color !== undefined) {
                     d3.select(this)
                        .style("stroke", d.original.color)
                        .style("stroke-dasharray", null);
                  } else {
                     d3.select(this).style("stroke", "none");
                  }
                });


                //get list of disabled legends
                var disabledLegends = [];
                d3.select('.nv-legend').each(function (d) {
                  for (var i in d) {
                    if (d[i].disabled) {
                      disabledLegends.push(d[i].key);
                    }
                  }
                });
                scope.$emit( 'lineChartDisabledLegends', disabledLegends );
            });

            // Hospital legend marker turn off associated lines
            chart.legend.dispatch.on( 'legendClick', function ( series ) {
              d3.selectAll('.nv-series').each(function (e, i) {
                var text = $.trim($('.nv-legend-text', this).text());
                //Get all the legends with text value starting with the clicked legend key excluding the clicked legend
                if (text !== series.key && text.indexOf(series.key + ' ') === 0) {
                  var e = d3.select(this);
                  var d = e.datum();
                  //change only with those which has same disabled value as the click legend
                  if (series.disabled === d.disabled) {
                    e.on("click").apply(this, [d, i]);
                  }
                }
              });
            });

            //this event will trigger everytime lines are drawn/redrawn
            chart.lines.dispatch.on( 'renderEnd.areaOnly', function (  ) {
              customize();
            });

            //$emit focusExtendedLeft/focusExtendedRight when focus x touches either end
            chart.dispatch.on("brush", function(o) {
              if (o.extent[1] !== x2.domain()[1] && o.extent[0] === x2.domain()[0] && !SystemStatus.busy()) {
                scope.$emit('lineChartFocusExtendedLeft', o.extent);
              }
              if (o.extent[0] !== x2.domain()[0] && o.extent[1] === x2.domain()[1] && !SystemStatus.busy()) {
                scope.$emit('lineChartFocusExtendedRight', o.extent);
              }
            });

	          chart.tooltipContent(function (key, y, e, graph) {
	            if (scope.tooltipContentHandler !== undefined) {
	              return scope.tooltipContentHandler.apply(this, arguments);
	            }
	          });

	          chart.tooltips = "false";
	          chart.isArea = "true";

            chart.xAxis
              .axisLabel(xAxisLabel)
              .tickValues(tickValuesX)
              .tickFormat(function (d) {
                if (newVal.xInterval == 'Y') {
                  return d3.time.format("%Y")(new Date(parseInt(d)));
                } else if (newVal.xInterval == 'M') {
                  return d3.time.format("%Y-%m")(new Date(parseInt(d)));
                } else {
                  //make sure d is converted to int before parsing to date
                  return d3.time.format("%Y-%m-%d")(new Date(parseInt(d)));
                }
              });

              chart.x2Axis
                .tickValues(tickValuesX)
                .tickFormat(function (d) {
                  if (newVal.xInterval == 'Y') {
                    return d3.time.format("%Y")(new Date(parseInt(d)));
                  } else if (newVal.xInterval == 'M') {
                    return d3.time.format("%Y-%m")(new Date(parseInt(d)));
                  } else {
                    return d3.time.format("%Y-%m-%d")(new Date(d));
                  }
                });

	              var x2 = chart.lines2.xScale();

	              var getMaxY = function (arr) {
	                var maxY = d3.max(arr, function (d) {
	                  return d3.max(d.values, function (v) {
	                    return ((d.disabled !== true) ? v.y : 0);
	                  });
	                });

                //Added buffer so point don't touch ceiling of chart
                var maxBuffer = maxY * 0.1;
                //round if maxY is greater than 10 or is not normalize
                if (maxY > 10 || !newVal.normalize) {
                  return Math.ceil(maxY + maxBuffer);
                } else {
                  if (maxY < 0.1) {
                    //if smaller than .1, retain decimal values but round to 3 decimals
                    return parseFloat((maxY + maxBuffer).toFixed(3));
                  } else {
                    //if smaller than 10 and greater than equal to .1, retain decimal values but round to 2 decimals
                    return parseFloat((maxY + maxBuffer).toFixed(2));
                  }
                }
              };

              //checks if v has decimal value - max garnularity is 5 decimals - will be rounded to 5 decimals
              var hasDecimalValue = function (v) {
                return v.toFixed(5) !== (Math.floor(v).toString() + ".00000");
              };

              //checks if there are any decimal values also rounds the values to 2 decimals
              var yHasDecimalValue = (function () {
                var has = false;
                for(var i in data) {
                  for(var j in data[i]['values']) {
                    if (hasDecimalValue(data[i]['values'][j].y)) {
                      has = true;
                      //round to two decimals
                      data[i]['values'][j].y = parseFloat((data[i]['values'][j].y).toFixed(2));
                    }
                  }
                }
                return has;
              })();

              //if max y is smaller or equal to 10
              var maxY = getMaxY(data);

              var tickValuesY = [];
              var yIncrementBy;
              if(maxY < maxTicksY) {
                if (yHasDecimalValue) {
                  if (maxY < 0.1) {
                    yIncrementBy = parseFloat((maxY / maxTicksY).toFixed(3));
                  } else {
                    yIncrementBy = parseFloat((maxY / maxTicksY).toFixed(2));
                  }
                } else {
                  yIncrementBy = 1;
                }
              } else {
                yIncrementBy = Math.floor(maxY / maxTicksY);
              }

	            if (yIncrementBy > 0) {
	              (function () {
	                var j = 0;
	                var i = 0;
	                while(true) {
	                  tickValuesY.push(i);
	                  i += yIncrementBy;
	                  if (i > maxY) {
	                    break;
	                  }
	                }
	              })();
	            }

	            (function () {
	              var i = 1;
	              if (tickValuesY.length > maxTicksY) {
	                i = Math.round(tickValuesY.length / maxTicksY);
	                tickValuesY = _.filter(tickValuesY, function (v, index) {
	                  return index % i === 0;
	                });
	              }
	            })();

							var yTickFormat = (function () {
                //show decimal if is a decimal val
                if (yHasDecimalValue) {
                  //if max Y is less than .1, display 3 decimal places
                  if (maxY < 0.1) {
                    return d3.format(',.3f');
                  } else {
                    return d3.format(',.2f');
                  }
                } else {
                  return d3.format(',f');
                }
              })(); 
			  
			/* Cant find a variable that defines ED or AD visits. Parsing the URL instead. */
			var url = window.location.href; 
			
			var chk_AD = url.split("epicurves/phus/AD"); 
			var y_axis_title = 'Visits';
			//If URL contains 'AD' then use Admissions for title. 
			if(chk_AD.length > 1){ 
				y_axis_title = 'Admissions';
			} 
			
              chart.yAxis
                .tickValues(tickValuesY)
                .tickFormat(yTickFormat)
                .axisLabel(newVal.normalize ? '% of total '+y_axis_title : 'Number of visits');
 
              //set min y axis to 0
              chart.forceY([0, maxY]);

              chart.y2Axis
                .showMaxMin(false)
                .tickFormat(yTickFormat);

              chart.margin({left: 75, bottom: 75, right: 50});

              // Need to create a new array for multiline data
              var multilineData = [];
              var count = -1; // so we can start the index at 0ytick
              var tmpClass = "";
              var tmpKey = "";

              for (var i = 0; i < data.length; i++){
								var originalDateFrom = data[i].originalDateFrom;
								var hiddenValues = data[i].hiddenValues;
								var hospitalId = data[i].key;
                data[i].values.forEach(function(value){
									if(tmpClass != value.Class || tmpKey != hospitalId){
										count++;
										tmpKey = hospitalId;
                    $rootScope.colourIndex;
                    multilineData[count] = {};

										/* currently value.Class returns undefined when set to 'all'
										   temporary fix below */
										multilineData[count].key = value.Class + ' ['+hospitalId+']';
                    multilineData[count].values = [];
                    multilineData[count].originalDateFrom = originalDateFrom;
                    multilineData[count].hiddenValues = hiddenValues;
                    multilineData[count].hospitalId = hospitalId;
                    tmpClass = value.Class;
										multilineData[count].values.push(value);
										multilineData[count].color = colourList.getColourWithIndex(count);
                  }
                  multilineData[count].values.push(value);
                  multilineData[count].dash = data[i].dash;
								});
              }

              d3.select('#chart svg')
                .attr('style', 'width:100%; height: 100%;')
                .datum(multilineData)
                .transition()
                .call(chart);

	             (function () {
	                //Add caption to the context chart
	                var nvContext = d3.select('#chart svg .nv-context');
	                //add text element in .nv-context
	                var x = -30;
	                var y = -20;
	                var txtTitle = nvContext.append('text')
                    //apply class
                    .attr('class', 'context-title')
                    //apply in-line styles
                    .style('font-weight', 'bold')
                    .style('font-size', '1.1em')
                    //apply position
                    .attr('transform', 'translate(' + x + ', ' + y + ')')
                    //set text
                    .text('Focus Chart');

	                //add tooltip
	                $(txtTitle).tooltip({
	                  container: 'body',
	                  placement: 'right',
	                  title: 'Click and drag on the chart below to focus.'
	                });
	              })();

	              //tooltip for legends
	              $('.nv-series', chart.container).each(function (index) {
	                if (scope.legendToolTipContentHandler !== undefined) {
	                  $(this)
	                    .attr('title', scope.legendToolTipContentHandler(this))
	                    .tooltip({
	                      container: 'body',
	                      placement: 'top'
	                    });
	                }
	              });
	              scope.chart = chart;
	          });
        }, true);


        scope.download = function(){
          //CHART variables
          var $chart = $('#chart');
          var width = $chart.outerWidth();
          var margin = 20;

          //height of chart less height of context
          var height = $chart.outerHeight() - $('.nvd3 .nv-context', $chart).get(0).getBoundingClientRect().height + margin;

          //Add title
          var titleText = $('#epicurveTitle').text().trim();
          var svgTitle = document.createTextNode(titleText);

          //Add text tags
          var svgText = document.createElement("text");
          svgText.setAttribute("x", (width/2) - (titleText.length*2.3));
					svgText.setAttribute("y", (margin/2));
          svgText.style.textAlign = "center";
          svgText.style.fontSize = 16;
          svgText.style.textDecoration = "underline";
          svgText.appendChild(svgTitle);

          //Add svg
          var svg = document.getElementsByTagName('svg')[0].cloneNode(true);
          svg.style.fontFamily = "Arial";
          svg.style.fontSize = 12;
          svg.id = "svgToPng";
          svg.appendChild(svgText);

          //Add parent div
          var dv = $("<div></div>");
          dv.append(svg);

					//Move chart lower for title
					dv.find('.nvd3.nv-wrap.nv-lineWithFocusChart').attr("transform", "translate(60," + (80) + ")");

          //Update CSS
          dv.find('.nvd3 .nv-groups path.nv-line').css({
            strokeWidth: 1.5,
            fill: 'none',
          });

          dv.find('.nvd3 .nv-legend .nv-disabled circle').css({
            fillOpacity: 0
          });

          //remove scatter
          dv.find('.nvd3.nv-line .nvd3.nv-scatter').remove();

          //grid
          dv.find('.nvd3 .nv-axis line').css({
            fill: 'none',
            stroke: '#e5e5e5',
            shapeRendering: 'crispEdges'
          });

          dv.find('.nvd3 .nv-axis .nv-axisMaxMin text').css({
            fontWeight: 'bold'
          });

          //remove context chart
          dv.find('.nvd3 .nv-context').remove();

          //black line start of x axis
          dv.find('.nvd3 .nv-axis path').css({
            fill: 'none',
            stroke: '#000',
            strokeOpacity: 0.75,
            shapeRendering: 'crispEdges',
          });

          dv.find('.nv-scatterWrap').remove();

          dv.find('.nvd3 .nv-wrap').css({
            marginTop: '20px'
          });

          //Call download to svg utility
          utils.downloadSvg(dv.html(), 'chart', width, height+100);
        };
      }
    };
  }]);
