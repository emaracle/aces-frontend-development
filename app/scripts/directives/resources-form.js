'use strict';

angular.module('acesApp')
    .directive('resourcesForm', ['utils', '$log',
        function (utils, $log) {
        return {
            templateUrl: 'views/resources-form.html?v=' + encodeURIComponent(ACES.buildnumber),
            restrict: 'E',
            scope: {
              params: '=' //will have all the data required to init form
            },
            link: function ($scope, element, attrs) {

              //Resource information
              $scope.resources = [
                {
                  id:'Influenza',
                  title: 'Monthly Influenza-related Data',
                  subtitle:'as reported to NACRS and DAD via CIHI and summarized by the MOHLTC',
                  datasource:'National Ambulatory Care Reporting System (NACRS) and the Discharge Abstract Database (DAD), Canadian Institute for Health Information (CIHI). Please note that due to the varied reporting frequencies by hospitals, data is subject to change.'
                },
                {
                  id:'Opioid',
                  title:'Weekly Emergency Department Visits for Opioid Overdose',
                  subtitle:'as reported to the NACRS Database via CIHI and summarized by the MOHLTC',
                  datasource:'National Ambulatory Care Reporting System (NACRS), Canadian Institute for Health Information (CIHI). Please note that visit counts are subject to change as more information becomes available and NACRS updates are conducted.'
                }
              ];

              //Setting to a specific resource
              $scope.setResource = function (resource) {
                $scope.params.resource = resource;
              };

              var resource = _.find($scope.Resources, function(o) { return o == $scope.params.resource; });
              if (resource === undefined) {
                  resource = $scope.resources[0];
              };

              $scope.setResource(resource);

              //Check is resource has a datasource
              $scope.hasDatasource = function (){
                if ($scope.params.resource.datasource != '') {
                  return true;
                } else {
                  return false;
                }
              }

            }
        };
    }])
;
