/**
This directive is indented to be used with alert-grid and line-listing directives
It expects following scope variables to be defined
    $scope.gridOptions
    $scope.data (actual data)

    //optional variables
    $scope.enablePaging (defaults to true)
    $scope.autoHeight (defaults to false)
    $scope.columnDefs (defaults to auto)
    $scope.callbackModifyDataRow (if defined will be called. We can modify the row in this callback method. For e.g. add/generate additional fields)

*/
(function($) {
    angular
        .module('acesApp')
        .directive('kGrid', ['$compile', '$sortService', 'SystemStatus', function($compile, $sortService, SystemStatus) {
            return {
                restrict: 'E',
                compile: function() {
                    return {
                        pre: function($scope, iElement, iAttrs) {
                            // console.log("DDDDDD-pre", arguments);
                            //used to skip execution of pageOptions $watch defined below
                            var skipPagingOptionsWatch = [];

                            $scope.gridOptions = $scope.gridOptions || {};

                            var plugins = $scope.gridOptions.plugins || [];
                            if ($scope.autoHeight === true) {
                                //set the max height to windows less others (approx 300px)
                                var otherHeights = 250;
                                var height = $(window).height() - otherHeights;
                                // console.log("HEIGHT", height);
                                //make sure that height is not smaller than 400
                                // height = (height > 400 ? height : 400);
                                plugins.push(new ngGridFlexibleHeightPlugin({maxHeight: height, minHeight: height}));
                            }


                            var columnDefs = $scope.gridOptions.columnDefs || [];
                            var hiddenColumns = []; //e.g. ['Alerton', 'AlertFamilyID'];
                            if ($scope.hiddenColumns !== undefined) {
                                hiddenColumns = $scope.hiddenColumns;
                            }
                            //hide hidden columns
                            for (var i in columnDefs) {
                                if (_.indexOf(hiddenColumns, columnDefs[i].field) >= 0) {
                                    columnDefs[i].visible = false;
                                }
                            }

                            var defaultPagingOptions = {
                                    pageSizes: [25, 50, 100, 250, 500, 1000],
                                    pageSize: 25,
                                    currentPage: 1
                                },
                                defaultFilterOptions = {
                                    filterText: "",
                                    useExternalFilter: true
                                },
                                defaultSortInfo = {
                                    fields: [],
                                    columns: [],
                                    directions: []
                                }
                                ;
                            $scope.pagingOptions = $scope.pagingOptions || defaultPagingOptions;
                            $scope.filterOptions = $scope.filterOptions || defaultFilterOptions;
                            $scope.sortInfo = $scope.sortInfo || defaultSortInfo;
                            $scope.myData = [];
                            $scope.totalServerItems = 0;

                            var enablePaging = ($scope.enablePaging !== false ? true : false);

                            $scope.setPagingData = function(data, page, pageSize){
                                if (data === undefined) {
                                    $scope.myData = undefined;
                                    return;
                                }
                                setTimeout(function () {
                                    SystemStatus.processStarted();
                                    $scope.totalServerItems = data.length;
                                    if (enablePaging) {
                                        //set current page to the first page if page is larger than total pages
                                        var totalPages = Math.ceil(data.length / pageSize);
                                        if (page > totalPages) {
                                            //disable pagingOptions $watch defined below
                                            skipPagingOptionsWatch.push(1);
                                            page = 1;
                                            $scope.pagingOptions.currentPage = page;
                                            skipPagingOptionsWatch.pop();
                                        }
                                        var pagedData = data.slice((page - 1) * pageSize, page * pageSize);
                                        $scope.myData = pagedData;
                                    } else {
                                        $scope.myData = data;
                                    }
                                    // console.log("DDDDDD-setPagingDataa", $scope.pagingOptions.currentPage);
                                    if (!$scope.$$phase) {
                                        // console.log("DDDDDD-setPagingData", "Apply");
                                        $scope.$apply();
                                    }
                                    SystemStatus.processCompleted();      
									//Reloading the grid here as there's no way to detect when 
									// the modal is opened on the screen. This is needed to 
									// capture the modals width when drawing the table.
									$scope.gridOptions.$gridServices.DomUtilityService.RebuildGrid($scope.gridOptions.$gridScope, $scope.gridOptions.ngGrid);
                               
                                }, 200);
                            };
                        
                            var dataCopy = [];
                            // var lastSortInfo = {field: '', direction: ''};
                            var lastPagedDataAsyncID = '';
                            $scope.getPagedDataAsync = function (pageSize, page, searchText) {
                                //this method may be called multiple times
                                //create a unique id for each call and after the timeout, run only if the id is the latest 
                                //this will save unccessary execution of codes below
                                var id = _.uniqueId("getPagedDataAsync");
                                lastPagedDataAsyncID = id;
                                // var arg = arguments;
                                setTimeout(function () {
                                    if (lastPagedDataAsyncID !== id) {
                                        return;
                                    }
                                    SystemStatus.processStarted();
                                    //
                                    if ($scope.sortInfo.fields.length > 0) {
                                            $sortService.Sort($scope.sortInfo, dataCopy);
                                    }
                                    var data = angular.copy(dataCopy);
                                    // var data;
                                    if (searchText) {
                                        var ft = searchText.toLowerCase();

                                        // $http.get('largeLoad.json').success(function (largeLoad) {      
                                        //     data = largeLoad.filter(function(item) {
                                        //         return JSON.stringify(item).toLowerCase().indexOf(ft) != -1;
                                        //     });
                                        //     $scope.setPagingData(data,page,pageSize);
                                        // });
                                        data = _.filter(data, function (row) {
                                            for (var i in columnDefs) {
                                                var col = columnDefs[i].field;
                                                if ( row[col] !== undefined && row[col].toString().toLowerCase().indexOf(ft) >= 0) {
                                                    return true;
                                                }
                                            }
                                            return false;
                                        });

                                        $scope.setPagingData(data,page,pageSize);           
                                    } else {
                                        // $http.get('largeLoad.json').success(function (largeLoad) {
                                        $scope.setPagingData(data,page,pageSize);
                                        // });
                                    }
                                    SystemStatus.processCompleted();
                                }, 100);
                            };


                            $scope.groups = [];
                            var lastPageSize = $scope.pagingOptions.pageSize;
                            var lastPageSizes = $scope.pagingOptions.pageSizes;
                            var lastGroupCount = 0;
                            var renderGrid = function (groupsCount) {
                                if (lastGroupCount !== groupsCount) {
                                    //instruct pagingOptions watch to skip execution
                                    //value doesn't matter
                                    skipPagingOptionsWatch.push(1);

                                    lastGroupCount = groupsCount;

                                    //disable paging if grouping is active
                                    if (groupsCount > 0) {
                                        //disable paging
                                        $scope.pagingOptions.pageSize = ($scope.data.data === undefined ? 0 : $scope.data.data.length);
                                        $scope.pagingOptions.pageSizes = [$scope.pagingOptions.pageSize];
                                    } else {
                                        $scope.pagingOptions.pageSize = lastPageSize;
                                        $scope.pagingOptions.pageSizes = lastPageSizes;
                                    }
                                    $scope.pagingOptions.currentPage = 1;
                                    skipPagingOptionsWatch.pop();
                                }
                                $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText);    
                            };


                            var buildData = function (data) {
                                if (!angular.isArray(data)) {
                                    dataCopy = undefined;
                                    return;
                                } else {
                                    dataCopy = angular.copy(data);
                                }
                                //add/generate additional fields 
                                if (typeof $scope.callbackModifyDataRow === "function") {
                                    for (var i in dataCopy) {
                                         $scope.callbackModifyDataRow(dataCopy[i]);
                                    }
                                }
                            };

                            $scope.$watch('data.data', function (newVal, oldVal) {
                                if (undefined !== newVal) {
                                    buildData(newVal);
                                    renderGrid($scope.groups.length);     
                               }
                            });
             
                            $scope.$watch('pagingOptions', function (newVal, oldVal) {
                                //do not reload if skipPagingOptionsWatch has values
                                if (skipPagingOptionsWatch.length === 0 && newVal !== oldVal) {
                                  $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText, 'pagingOptions');
                                }
                            }, true);
                            // $scope.$watch('filterOptions', function (newVal, oldVal) {
                            //     console.log("searchTextfilterOptions", arguments);
                            //     if (newVal !== oldVal) {
                            //         console.log("DDDDDDWatchfilterOptions", arguments);
                            //         $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText, 'filterOptions');
                            //     }
                            // }, true);
                            
                            var lastFilterText = "";
                            var filterTextChangedTimer;
                            $scope.$watch('gridOptions.$gridScope.filterText', function (newVal, oldVal) {
                                if (newVal !== oldVal) {
                                    lastFilterText = newVal;
                                    //if filterTextChangedTimer is defined, cancel it
                                    if (undefined !== filterTextChangedTimer) {
                                        clearTimeout(filterTextChangedTimer)
                                    }
                                    //start search 500 millisecs after typing stops
                                    filterTextChangedTimer = setTimeout(function(){
                                        if (lastFilterText === newVal) {
                                            SystemStatus.processStarted();
                                            $scope.filterOptions.filterText = newVal;
                                            $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText, 'filterOptions');                                            
                                            SystemStatus.processCompleted();
                                        }
                                    }, 500);
                                }
                            }, true);
                            $scope.$watch('sortInfo', function (newVal, oldVal) {
                                //make sure that sort values have changed
                                if (!angular.isArray(newVal.fields) || !angular.isArray(oldVal.fields) || !angular.isArray(newVal.directions) || !angular.isArray(oldVal.directions) || newVal.fields.join("-") !== oldVal.fields.join("-") || newVal.directions.join("-") !== oldVal.directions.join("-")) {
                                    $scope.getPagedDataAsync($scope.pagingOptions.pageSize, $scope.pagingOptions.currentPage, $scope.filterOptions.filterText, 'sortInfo');
                                }
                            }, true);

                           
                            
                            $scope.$on('ngGridEventGroups', function (e, groups) {
                                
                                var newGroups = [];
                                for (var i in groups) {
                                    newGroups.push( groups[i].field );
                                }

                                //this event is triggered multiple times
                                //Refresh only if group has changed
                                if (newGroups.join("-") !== $scope.groups.join("-")) {
                                    $scope.groups = newGroups;
                                    //may be save groups

                                    renderGrid(groups.length);
                                }
                            });



                            //finally rebuild the gridOptions
                            $scope.gridOptions.plugins = plugins;
                            $scope.gridOptions.columnDefs = columnDefs;
                            $scope.gridOptions.enablePaging = enablePaging;
                            $scope.gridOptions.pagingOptions = $scope.pagingOptions;
                            $scope.gridOptions.filterOptions = $scope.filterOptions;
                            $scope.gridOptions.totalServerItems = 'totalServerItems';
                            // $scope.gridOptions.useExternalSorting = false; //$scope.gridOptions.useExternalSorting || true;
                            $scope.gridOptions.sortInfo = $scope.sortInfo;
                            $scope.gridOptions.groups = $scope.groups;
                            $scope.gridOptions.data = 'myData';

                            //additional gridOptions
                            $scope.gridOptions.showFilter = $scope.gridOptions.showFilter || true;
                            $scope.gridOptions.enableColumnResize = $scope.gridOptions.enableColumnResize || true;
                            $scope.gridOptions.showColumnMenu = $scope.gridOptions.showColumnMenu || true;
                            $scope.gridOptions.showFooter = $scope.gridOptions.showFooter || true;
                            $scope.gridOptions.enableCellSelection = $scope.gridOptions.enableCellSelection || false;
                            $scope.gridOptions.multiSelect = $scope.gridOptions.multiSelect || false;
                            $scope.gridOptions.enableRowSelection = $scope.gridOptions.enableRowSelection || true;
                            $scope.gridOptions.enableHighlighting = $scope.gridOptions.enableHighlighting || false;
                            $scope.gridOptions.enableRowReordering = $scope.gridOptions.enableRowReordering || true;
                            $scope.gridOptions.enableColumnReordering = $scope.gridOptions.enableColumnReordering || true;
						 
							if($scope.data.data != undefined){
								buildData($scope.data.data);
								renderGrid($scope.groups.length);
							}
                            iElement.append($compile('<div class="gridStyle" ng-grid="gridOptions"></div>')($scope));
                        }
                    };
                }
            };
        }]);
})(jQuery);
