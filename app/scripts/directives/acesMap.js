'use strict'; 

angular.module('acesApp')
.directive('acesMap', ['$rootScope', '$http', '$q', '$window', '$sce', '$timeout', function($rootScope, $http, $q, $window, $sce, $timeout) {
	return {
		restrict: 'A',
		scope: {
			layers: '=layers',
			currentSearchParams: '=currentSearchParams',
			result: '=result',
			selectableLayers: '=selectableLayers'
		},
		link: function($scope, element, attrs) {
			// $scope.loading = true;

			var timestamp = moment().valueOf();
			$scope.id = 'aces-arcgis-map';
			$scope.homeButtonId = 'home-button';
			$scope.basemapGalleryId = 'basemap-gallery';
			// $scope.infoWindowId = 'info-window';

			$scope.legend = {
					show: false, //false if legend is not available
					display: true //visiblity
			};

			$scope.mapReady = false;
			require([
			"esri/layers/KMLLayer",
			"dojo/_base/Color",
			"dojo/on", 
			"esri/map",
			"esri/dijit/OverviewMap", 
			"esri/dijit/Scalebar", 
			"esri/dijit/HomeButton",
			"esri/lang",
			"esri/graphic",
			"esri/layers/GraphicsLayer",
			"esri/renderers/SimpleRenderer",
			"esri/geometry/Point",
			"esri/symbols/SimpleMarkerSymbol",
			"esri/symbols/SimpleLineSymbol",
			"esri/SpatialReference",  
			"dijit/TooltipDialog",
			// "dijit/Dialog",
			"dijit/popup",
			"esri/geometry/Polygon",
			"esri/symbols/SimpleFillSymbol",    
			// "esri/renderers/ScaleDependentRenderer",
			"esri/dijit/InfoWindow",
			"dojo/json",
			"Layers/KMLLayerModel",
			"Data/LayerInfo"
			],
			function(
			KMLLayer,
			Color,
			on,
			Map,
			OverviewMap,
			Scalebar,
			HomeButton,
			esriLang,
			Graphic,
			GraphicsLayer,
			SimpleRenderer,
			Point,
			SimpleMarkerSymbol,
			SimpleLineSymbol,
			SpatialReference,
			TooltipDialog,
			// Dialog,
			dijitPopup,
			Polygon,
			SimpleFillSymbol,
			// ScaleDependentRenderer,
			InfoWindow,
			JSON,
			KMLLayerModel,
			LayerInfo
			) {
				console.log("LOADED MAPS DIRECTIVE");
				//Destroy all previous instances
				if (typeof $window.lastMapInstance != 'undefined') {
					try {
						$window.lastMapInstance.map.destroy();
						$window.lastMapInstance.overviewMap.destroy();
						$window.lastMapInstance.homeButton.destroy();
					} catch(e){
						// console.log('********************************************88');
						console.log(e);
					}
				} else {
					//create
					$window.lastMapInstance = {};
				}


				var startExtent = new esri.geometry.Extent(-1.059258777443936E7, 5113380.025644123, -8275939.758003992, 7731363.302283124, new esri.SpatialReference({wkid:"102100"}));  
				// console.log("HHHHHHHHHHHHHHHHHHHHHHHHHHHHhhhhh");
				// console.log(startExtent);

				// var infoWindow = new esri.dijit.InfoWindow({}, $scope.infoWindowId);
				// infoWindow.resize(425,315);
				// infoWindow.startup();
				// debugger;
				if (typeof $window.lastMapInstance.dialog == 'undefined') {
					var dialog = new TooltipDialog({
						id: "tooltipDialog_",
						style: "position: absolute; width: 250px; font: normal normal normal 10pt Helvetica;z-index:10000"
					});
					dialog.startup();
					$window.lastMapInstance.dialog = dialog;                    
				} else {
					var dialog = $window.lastMapInstance.dialog;
				}


				var init = function(){
					//Wait till the elements are rendered and available
					if (angular.element('#' + $scope.id).length == 0) {
						//Check again after 100 ms
						setTimeout(function(){
							init();
						}, 100);
					} else{
						$scope.map = new Map($scope.id, { 
							basemap: $scope.currentSearchParams.maps.baseMap,  
							zoom:6,
							extent: startExtent, 
							sliderStyle: "large",
							autoResize: true,
							isZoomSlider: true,
							//minZoom: 5,
							logo: false
							//showInfoWindowOnClick:true,
							// infoWindow: infoWindow,
						});
						// console.log($scope.map);

						$window.lastMapInstance.map = $scope.map;

						$scope.map.on('load', function(e){
							//Doesn't always gets called
							console.log('Map ready');
							$scope.$apply();
							
						});

						// 
						var polygonLayersSet = [];  
						var polygonGraphicsLayers = {};     
						var polygonLayersColors = [[0,0,0],[100,100,100],[255,174,201],[150,150,150]]; 
						var polygonBorderColors = [[0,255,0, 0.0],[0,0,0, 0.0],[255,174,201, 0.4],[0, 0, 0, 0]]; 

						var highlightSymbol = new SimpleLineSymbol(
						SimpleLineSymbol.STYLE_SOLID, 
						new Color([255,0,0]), 1
						); 

						/*Add Polygon Layers to Map */
						var i = 0;
						angular.forEach($scope.layers, function(layer, layerName){
							if (layer.type && layer.type == 'polygonLayer' ) {
								polygonLayersSet[layerName] = 0; 
								polygonGraphicsLayers[layerName] = new GraphicsLayer();
								$scope.map.addLayer(polygonGraphicsLayers[layerName], (i+10));
								i++;                                        
							}
						});
						angular.forEach($scope.layers.selectableLayers, function(layer){
							if (layer.field.type == 'polygonLayer') {                                    
								polygonLayersSet[layer.field.id] = 0; 
								polygonGraphicsLayers[layer.field.id] = new GraphicsLayer();
								$scope.map.addLayer(polygonGraphicsLayers[layer.field.id], (i+10));
								i++;
							}
						});
						
						/*Add Layers to Map */
						//All point layers graphic will be added to this GraphicsLayer
						var additionsGraphicsLayer = new GraphicsLayer(); 
						$scope.map.addLayer(additionsGraphicsLayer, 100); 


						//Watch for basemap change
						$scope.$watch('currentSearchParams.maps.baseMap', function(){
							//change only if $scope.map.loaded
							if ($scope.map.loaded) {
								$scope.map.setBasemap($scope.currentSearchParams.maps.baseMap);  
							}
						});
						//Watch for changes in levelOfGeography 
						//this is defined in MapCtrl and gets updated when user changes levelOfGeography
						$scope.$watch('layers.levelOfGeography.updated', function(){
							//  console.log("$scope.$watch('layers.levelOfGeography.updated', function(){");
							// console.log($scope.layers.levelOfGeography);
							updateLevelOfGeographyMap();
						});
						//Watch for changes in levelOfGeography 
						//this is defined in MapCtrl and gets updated when user changes levelOfGeography
						$scope.$watch('result.updated', function(){
							//  console.log("$scope.$watch('result.updated', function(){");
							// console.log($scope.layers.levelOfGeography);
							updateLevelOfGeographyMap();
						});

						var formatJSON = function (jsonCoords) {
							jsonCoords = JSON.stringify(jsonCoords);
							if(jsonCoords.match(/\[\[\[\[/g))
							{
								jsonCoords = jsonCoords.replace(/\[\[\[/g,"[["); 
								jsonCoords = jsonCoords.replace(/\]\]\]/g,"]]");  
							} 
							jsonCoords = JSON.parse(jsonCoords, true); 
							return jsonCoords; 
							
						};
						var hexToRGB = function (color){
							var hex = color.replace('#','');
							var bigint = parseInt(hex, 16);
							var r = (bigint >> 16) & 255;
							var g = (bigint >> 8) & 255;
							var b = bigint & 255;         
							return [r, g, b];
						};

						//Will hold updated values of $scope.result and $scope.layers.levelOfGeography
						//It is used to check if layers needs to be redraw again
						var lastLevelOfGeographyMapUpdates = {
							result: 0,
							levelOfGeography: 0
						}
						var updateLevelOfGeographyMap = function () {
							
							//Both levelOfGeography and result must be loaded before moving further.
							if (!$scope.layers.levelOfGeography.ready || !$scope.result.ready) {
								console.log($scope.layers.levelOfGeography.ready + '||' + $scope.result.ready);
								console.log($scope.layers.levelOfGeography.updated + '||' + $scope.result.updated);
								return;
							}

							console.log($scope.layers.levelOfGeography.ready + '||' + $scope.result.ready);
							console.log($scope.layers.levelOfGeography.updated + '||' + $scope.result.updated);
							//Check if already rendered
							if (lastLevelOfGeographyMapUpdates.result == $scope.result.updated && lastLevelOfGeographyMapUpdates.levelOfGeography == $scope.layers.levelOfGeography.updated) {
								console.log('Skipping');
								return;
							}
							console.log('Starting');
							lastLevelOfGeographyMapUpdates.result = $scope.result.updated;
							lastLevelOfGeographyMapUpdates.levelOfGeography = $scope.layers.levelOfGeography.updated
							//Hide legend
							$scope.legend.show = false;
							
							$scope.hideLegend = function(){
								$scope.legend.display = false;
							};
							$scope.showLegend = function(){
								$scope.legend.display = true;
							}


							if ($scope.currentSearchParams.data.search) {


								var data = $scope.result.data;
								var map_data = $scope.layers.levelOfGeography.data;
								var geoId2 = $scope.layers.levelOfGeography.field.geoType2Id;
								var geoId1 = $scope.layers.levelOfGeography.field.geoType1Id;
								var nameAttr = $scope.layers.levelOfGeography.field.nameAttr


								var geoServer = [];
								var betaKfla = []; 
								var missing = []; 
								// var numOfRegions=0;
								// var withData = [];

								// numOfRegions = 0;
								// if (map_data.features) {
								//    angular.forEach(map_data.features, function(val, key){
								//        geoServer[key] = val.properties[geoId2];
								//        //Create a array with key of all the records which has data
								//        angular.forEach(data, function(d){
								//            // console.log(d[geoId1] + ' == ' +  val.properties[geoId2]);
								//            if (d[geoId1] == val.properties[geoId2]) {
								//                withData.push(key);
								//            }
								//        });
								//    });
								// }

								polygonGraphicsLayers.levelOfGeography.clear();

								var max = parseFloat($scope.currentSearchParams.data.percentageRangeHigh); 
								var min = parseFloat($scope.currentSearchParams.data.percentageRangeLow); 
								if($scope.currentSearchParams.data.mapStyle == 'prop_symbols'){
									var interval = 0; 
									var nClass = 4;
									var classColors = []; 
									var decreaseColorBy = 0;

									var interval = (max - min) / nClass; 

									classColors[0] = min;
									var q = 1;
									while (q < (nClass+1)) {
										classColors[q] = classColors[q-1] + interval;
										q++; 
									}
									var proportionalSymbolSizes = [8,16,32,64];

									
									// var colorValue = [255,0,0];


									angular.forEach(map_data.features, function(val, key){
										if(val.geometry != undefined){
											angular.forEach(data, function(d){
												// console.log(d[geoId1] + ' == ' +  val.properties[geoId2]);
												if (d[geoId1] == val.properties[geoId2]) {
													// console.log(d);
													// console.log(val);
													var jsonformatted = formatJSON(val.geometry["coordinates"]);
													var totVisits = d.TotalVisits; 
													var v = d.Percent;
													// debugger;
													if (v < min || v > max) {
														return; //don't display the symbol
													}

													var attr = {"Name":val.properties[nameAttr], "Percent": v, "totalVisits": d.Visits, "totalEdVisits": totVisits};   
													// console.log(val.geometry["coordinates"]);
													var latLong = val.geometry["coordinates"]; //JSON.stringify(val.geometry["coordinates"]);
													// latLong = latLong.replace("[",""); 
													// latLong = latLong.replace("]","");
													// latLong = latLong.split(",");
													// console.log(latLong);
													// console.log($scope.map.spatialReference);
													var pt = new Point(parseInt(latLong[0]), parseInt(latLong[1]), $scope.map.spatialReference);
													
													var newGraphic = new Graphic(pt, null, attr); 
													polygonGraphicsLayers.levelOfGeography.add(newGraphic); 
													return;
												}
											});
										}
									});

									
									var borderColor = hexToRGB($scope.currentSearchParams.maps.borderColor);
									var layerColor = hexToRGB($scope.currentSearchParams.maps.layerColor);
									//add opacity
									layerColor.push(.85);

									var markerSym = new SimpleMarkerSymbol();
									markerSym.setColor(new Color(layerColor));
									markerSym.setOutline(markerSym.outline.setColor(new Color(borderColor)));  
									var renderer1 = new SimpleRenderer(markerSym);   
									var proportionalSymbolInfo = {
										field: "Percent",
										minSize: 5,
										maxSize:39,
										minDataValue: min,
										maxDataValue: max
									} 
									renderer1.setProportionalSymbolInfo(proportionalSymbolInfo); 
									polygonGraphicsLayers.levelOfGeography.setRenderer(renderer1); 
									polygonGraphicsLayers.levelOfGeography.redraw();


									$scope.legend.title = $sce.trustAsHtml("Emergency Department Visits <br>(Sample Data)");

									$scope.legend.list = [];

									// var table ='<table>';
									var circleStyle = [{ height: "10px", width: "10px", rad: "6px" }, { height: "15px", width: "15px", rad: "10px" }, { height: "20px", width: "20px", rad: "10px" }, { height: "25px", width: "25px", rad: "15px" }] 
									for (i=0; i< nClass; i++){
										$scope.legend.list.push(
										{
											style:{
												'background-color':'rgba(' + layerColor.join(',') + ')', 
												'opacity':'0.9', 
												'height':circleStyle[i].height, 
												'width':circleStyle[i].width, 
												'border':'1px solid rgb(' + borderColor.join(',') + ')',
												'border-radius': circleStyle[i].rad,
												'margin':'0 auto'
											},
											label: $sce.trustAsHtml((Math.round(classColors[i] * 100) / 100) + ' - ' + (Math.round(classColors[i+1] * 100) / 100) + ' (%)')
										});
										// table += "<tr><td style='padding-right:10px; text-align:center'><div style='background-color:rgb(0,0,255); margin:0 auto; height:"+circleStyle[i].height+"; width:"+circleStyle[i].width+"; border:1px solid blue; border-radius: "+circleStyle[i].rad+" '></div></td><td>"+(Math.round(classColors[i] * 100) / 100)+" - "+(Math.round(classColors[i+1] * 100) / 100)+" (%)</td></tr>";
									}
									// console.log($scope.legend);
									// table += "</table>"; 
									// $scope.legend.content = table;
									$scope.legend.show = true;
								} else { //mapStyle is choropleth

									// var lOfGeo = $scope.currentSearchParams.data.levelOfGeography;
									// var dataClass = document.getElementById("data_classifications");
									var dClass = $scope.currentSearchParams.data.dataClassification;  
									// var numOfClass = document.getElementById("num_of_classifications");
									var nClass = $scope.currentSearchParams.data.noOfClasification;  


									var savedColorCode = hexToRGB($scope.currentSearchParams.maps.layerColor);
									var savedBorderColorCode = hexToRGB($scope.currentSearchParams.maps.borderColor);
									var colorCode = hexToRGB($scope.currentSearchParams.maps.layerColor);
									

									var resetColorCode = function (){ 
										colorCode[0] = savedColorCode[0];
										colorCode[1] = savedColorCode[1];
										colorCode[2] = savedColorCode[2]; 
									};

									var decreaseColorValues = function (value){ 
										colorCode[0] -= value;
										colorCode[1] -= value;
										colorCode[2] -= value; 
									};



									resetColorCode();
									var decreaseColorBy = Math.round(255 / nClass);




									if (dClass == 'equal_int') {
										var interval = 0; 
										var classColors = [];
										
										interval = (max - min) / nClass; 

										classColors[0] = min;
										var q = 1;
										while(q<(nClass+1)){
											classColors[q] = classColors[q-1] + interval;
											q++; 
										}

										angular.forEach(map_data.features, function(val, key){
											if(val.geometry != undefined){
												angular.forEach(data, function(d){
													// console.log(d[geoId1] + ' == ' +  val.properties[geoId2]);
													if (d[geoId1] == val.properties[geoId2]) {
														resetColorCode();    
														
														var totVisits = d.TotalVisits; 
														
														var v = d.Percent;
														
														for (var o=0; o< nClass; o++){ 
															if(v < classColors[0] || v > classColors[nClass])
															{                                            
																var sms = new SimpleFillSymbol(SimpleFillSymbol.STYLE_SOLID,
																new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID,
																new Color([savedBorderColorCode[0],savedBorderColorCode[1],savedBorderColorCode[2]]), 2), new Color([255,255,255, .2])
																);  
																break;
															} 
															else if(v >= classColors[o] && v <= classColors[o+1] )
															{                                            
																var sms = new SimpleFillSymbol(SimpleFillSymbol.STYLE_SOLID,
																new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID,
																new Color([savedBorderColorCode[0],savedBorderColorCode[1],savedBorderColorCode[2]]), 2), new Color([colorCode[0], colorCode[1], colorCode[2], 0.9])
																);   
																break;
															}

															decreaseColorValues(decreaseColorBy); 
														}   
														
														var jsonformatted = formatJSON(val.geometry["coordinates"]); 
														
														var attr = {"Name":val.properties[nameAttr], "Percent": v, "totalVisits": d.Visits, "totalEdVisits": totVisits};   

														var polygonJson = {"rings":jsonformatted,"spatialReference":{"wkid":102100 }}; 
														var polygon = new Polygon(polygonJson);  
														var newGraphic = new Graphic(polygon, sms, attr);  
														polygonGraphicsLayers.levelOfGeography.add(newGraphic);   
														return;
													}
												});
											}
										});  

										
										resetColorCode();

										$scope.legend.title = $sce.trustAsHtml("Emergency Department Visits <br>(Sample Data)");
										$scope.legend.list = [];
										for (i=0; i< nClass; i++){
											$scope.legend.list.push({
												style:{
													'background-color':'rgb(' + colorCode[0] + ',' + colorCode[1] + ',' + colorCode[2] + ')', 
													'opacity':'0.9', 
													'height':'30px', 
													'width':'30px', 
													'border':'1px solid black'
												},
												label: $sce.trustAsHtml((Math.round(classColors[i] * 100) / 100) + ' - ' + (Math.round(classColors[i+1] * 100) / 100) + ' (%)')
											});
											decreaseColorValues(decreaseColorBy);
										}
										$scope.legend.show = true;
										// dom.byId("legend").style.display="block";  
										// dom.byId("legendTitle").innerHTML = "Emergency Department Visits <br> (Sample Data)";
										// table ='<table>';
										// for (i=0; i< nClass; i++)
										// {
										//      table += "<tr><td style='padding-right:10px'><div style='background-color:rgb("+colorCode[0]+","+colorCode[1]+","+colorCode[2]+"); opacity:0.9; height:30px; width:30px; border:1px solid black'></div></td><td>"+(Math.round(classColors[i] * 100) / 100)+" - "+(Math.round(classColors[i+1] * 100) / 100)+" (%)</td></tr>"; 
										//      decreaseColorValues(decreaseColorBy); 
										// } 
										// table += "</table>"; 
										// dom.byId("toggleColors").innerHTML = table;  


									} else if (dClass == 'quantile') {
										var totalVisits = 0;
										var numOfObjects = 0;
										var interval = 0;
										var classColors = [];

										for(var i in data){
											if(data.hasOwnProperty(i)){  
												numOfObjects++; 
											}
										}
										interval = numOfObjects/nClass;  
										classColors[0] = 0;
										var legend_n = 0;
										var q = 1;
										var x = 0; 
										while(q<(parseInt(nClass)+1)){
											classColors[q] = classColors[q-1] + interval;   
											q++; 
										}    

										q=1; 
										var a = 0, groups = [], counter = 0;
										angular.forEach(map_data.features, function(val, key){
											if(val.geometry != undefined){
												angular.forEach(data, function(d){
													// console.log(d[geoId1] + ' == ' +  val.properties[geoId2]);
													if (d[geoId1] == val.properties[geoId2]) {
														if (a > classColors[q]) {
															decreaseColorValues(decreaseColorBy); 
															groups[q] = counter;
															counter = 0;   
															q++;
														}    
														counter++;
														var sms = new SimpleFillSymbol(SimpleFillSymbol.STYLE_SOLID,
														new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID,
														new Color([savedBorderColorCode[0],savedBorderColorCode[1],savedBorderColorCode[2]]), 2), new Color([colorCode[0],colorCode[1],colorCode[2],0.9])
														);   
														
														
														var totVisits = d.TotalVisits; 
														
														var v = d.Percent;
														
														var jsonformatted = formatJSON(val.geometry["coordinates"]); 
														var attr = {"Name":val.properties[nameAttr], "Percent": v, "Quantile": q, "totalVisits": d.Visits, "totalEdVisits": totVisits};
														
														var polygonJson = {"rings":jsonformatted,"spatialReference":{"wkid":102100 }}; 
														
														var polygon = new Polygon(polygonJson); 
														
														var newGraphic = new Graphic(polygon, sms, attr); 
														polygonGraphicsLayers.levelOfGeography.add(newGraphic);      
														a++; 
														return;
													}
												});
											}
										});

										groups[q] = counter;  

										var percentage; 
										resetColorCode();
										$scope.legend.title = $sce.trustAsHtml("Emergency Department Visits - Quantiles <br>(Sample Data)");
										$scope.legend.list = [];

										for (i=1; i<(parseInt(nClass)+1); i++) {
											var label = '';
											// table += "<tr><td style='padding-right:10px'><div style='background-color:rgb("+colorCode[0]+","+colorCode[1]+","+colorCode[2]+"); border:1px black solid; opacity:0.9; height:30px; width:30px;'></div></td><td>"; 
											if (groups[i]) {  
												label += "Q"+i;
												if(i==1){ label +=  "<span style='font-size: .75em'> (Least amount of visits) </span>";}
												if(i == (parseInt(nClass))){ label +=  " <span style='font-size: .75em'> (Most amount of visits)</span> ";}
											} else {
												label += 'N/A'; 
											}
											$scope.legend.list.push({                   
												style:{
													'background-color':'rgb(' + colorCode[0] + ',' + colorCode[1] + ',' + colorCode[2] + ')', 
													'opacity':'0.9', 
													'height':'30px', 
													'width':'30px', 
													'border':'1px solid black'
												},
												label: $sce.trustAsHtml(label)
											});

											decreaseColorValues(decreaseColorBy); 
										}
										$scope.legend.show = true;
									} else if (dClass == 'std_dev') {
										
										if (!$scope.currentSearchParams.data.standDevCompareToOntario) {
											angular.forEach(data, function(d){
												var totalVisitsPerN = 0, avg = 0, s_deviation = 0, variance = 0, variate = 0;
												var numOfObjects = 0;
												var totalVisits = 0;
												angular.forEach(d.ref, function(val){
													totalVisits += val.Visits;
													numOfObjects++; 
												});

												avg = totalVisits/numOfObjects;

												// var low, high, a=0; 
												var numMinusMean = [];
												var numSquared = [];
												var numSum = 0;
												angular.forEach(d.ref, function(val, i){
													numMinusMean[i] = val.Visits - avg;
													numSquared[i] = Math.pow(numMinusMean[i],2);
													numSum += numSquared[i];
												});
												variance = numSum / numOfObjects;
												s_deviation = Math.sqrt(variance);
												if (s_deviation == 0) {
													//?? what should we do?
													d.variate = null;
												} else {
													var sAvg = d.Visits / d.days;
													d.variate = ((sAvg - avg) / s_deviation).toFixed(2); 
												}
											});
										} else {
											var totalVisitsPerN = 0, avg = 0, s_deviation = 0, variance = 0, variate = 0;
											var numOfObjects = 0;
											var totalVisits = 0;
											angular.forEach(data, function(val){
												//Convert percent value to float
												//it is returned as string from server
												val.Percent = parseFloat(val.Percent);
												totalVisits += val.Percent;
												numOfObjects++; 
											});

											avg = totalVisits/numOfObjects;

											var low, high, a=0; 
											var numMinusMean = [];
											var numSquared = [];
											var numSum = 0;
											angular.forEach(data, function(val, i){
												numMinusMean[i] = val.Percent - avg;
												numSquared[i] = Math.pow(numMinusMean[i],2);
												numSum += numSquared[i];
											});
											// for(var i in numMinusMean){  
											//      numSquared[i] = Math.pow(numMinusMean[i],2);   
											// }
											// for(var i in numSquared){  
											//      numSum += parseInt(numSquared[i]);   
											// } 

											variance = numSum / numOfObjects;
											s_deviation = Math.sqrt(variance);  

											angular.forEach(data, function(d){
												d.variate = ((d.Percent - avg) / s_deviation).toFixed(2); 
											});                                                          
										}


										var colorCode = ['255 128 0', '255 255 0', '0 128 0',  '153 153 255', '102 102 255', '51 51 255', '25 25 255', '0 0 255'];  
										var c = ""; 
										var tmp = 0;
										var hasInsufficientData = false;
										//TODO REFACTOR
										angular.forEach(map_data.features, function(val, key){
											if(val.geometry != undefined){
												angular.forEach(data, function(d){
													// console.log(d[geoId1] + ' == ' +  val.properties[geoId2]);
													if (d[geoId1] == val.properties[geoId2]) {
														
														var totVisits = d.TotalVisits; 

														var v = d.Percent;
														
														var variate = d.variate//((d.Percent - avg) / s_deviation).toFixed(2); 
														if (variate === null) {
															var sms = new SimpleFillSymbol(SimpleFillSymbol.STYLE_SOLID,
															new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID,
															new Color([255,255,255]), 1), new Color([202,197,197, 0.9])
															);  
															var jsonformatted = formatJSON(val.geometry["coordinates"]); 
															
															var attr = {"Name":val.properties[nameAttr], "Percent": v, "s_dev": "Insufficient data", "totalVisits": d.Visits, "totalEdVisits": totVisits, "data": d};  
															
															var polygonJson = {"rings":jsonformatted,"spatialReference":{"wkid":102100 }}; 
															var polygon = new Polygon(polygonJson); 
															
															var newGraphic = new Graphic(polygon, sms, attr); 
															polygonGraphicsLayers.levelOfGeography.add(newGraphic); 

															hasInsufficientData = true;
															
														} else {
															var m = 3; 
															if(variate > m) {
																var sms = new SimpleFillSymbol(SimpleFillSymbol.STYLE_SOLID,
																new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID,
																new Color([255,255,255]), 2), new Color([255,128,0, 0.9])
																);  
																var jsonformatted = formatJSON(val.geometry["coordinates"]); 
																
																var attr = {"Name":val.properties[nameAttr], "Percent": v, "s_dev": variate, "totalVisits": d.Visits, "totalEdVisits": totVisits, "data": d};  
																
																var polygonJson = {"rings":jsonformatted,"spatialReference":{"wkid":102100 }}; 
																var polygon = new Polygon(polygonJson); 
																
																var newGraphic = new Graphic(polygon, sms, attr); 
																polygonGraphicsLayers.levelOfGeography.add(newGraphic); 
															}
															for (var l = 0; l < 7; l++) {
																if (variate <= m && variate >= (m-1)) {
																	c = colorCode[l].split(' ');
																	var sms = new SimpleFillSymbol(SimpleFillSymbol.STYLE_SOLID,
																	new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID,
																	new Color([255,255,255]), 2), new Color([parseInt(c[0]),parseInt(c[1]),parseInt(c[2]), 0.9])
																	);  
																	var jsonformatted = formatJSON(val.geometry["coordinates"]); 
																	
																	var attr = {"Name":val.properties[nameAttr], "Percent": v, "s_dev": variate, "totalVisits": d.Visits, "totalEdVisits": totVisits, "data": d}; 
																	
																	var polygonJson = {"rings":jsonformatted,"spatialReference":{"wkid":102100 }}; 
																	var polygon = new Polygon(polygonJson); 
																	
																	var newGraphic = new Graphic(polygon, sms, attr); 
																	polygonGraphicsLayers.levelOfGeography.add(newGraphic);  
																	
																	tmp=1;
																	break;
																}
																m--;
															}
															
															if (variate < m && tmp != 1) { 
																c = colorCode[l].split(' ');
																var sms = new SimpleFillSymbol(SimpleFillSymbol.STYLE_SOLID,
																new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID,
																new Color([255,255,255]), 2), new Color([255,255,255, 0.9])
																);  
																var jsonformatted = formatJSON(val.geometry["coordinates"]); 
																
																var attr = {"Name":val.properties[nameAttr], "Percent": v, "s_dev": variate, "totalVisits": d.Visits, "totalEdVisits": totVisits, "data": d};  
																
																var polygonJson = {"rings":jsonformatted,"spatialReference":{"wkid":102100 }}; 
																var polygon = new Polygon(polygonJson); 
																
																var newGraphic = new Graphic(polygon, sms, attr); 
																polygonGraphicsLayers.levelOfGeography.add(newGraphic); 
																
															}    
															tmp=0;
														}
													}
												});
											}
										});

										$scope.legend.title = $sce.trustAsHtml("Emergency Department Visits - Standard Deviation <br> (Sample Data)");



										$scope.legend.list = [];
										$scope.legend.list.push({
											style:{
												'background-color':'rgb(255,0,0)', 
												'opacity':'0.9', 
												'height':'30px', 
												'width':'30px'
											},
											label: $sce.trustAsHtml('> 3')
										});
										var c, m=3;
										for (i=1; i<7; i++) {
											var label = '';
											if (m>0) {
												label += ''+(m-1)+' + '+m+''; 
											} else {
												label += ''+m+' - ('+(m-1)+')';  
											}

											c = colorCode[(i-1)].split(' ');
											$scope.legend.list.push({
												style:{
													'background-color':'rgb(' + c[0] + ',' + c[1] + ',' + c[2] + ')', 
													'opacity':'0.9', 
													'height':'30px', 
													'width':'30px'
												},
												label: $sce.trustAsHtml(label)
											});
											m--;
										} 
										$scope.legend.list.push({                                  
											style:{
												'background-color':'rgb(0,0,255)', 
												'opacity':'0.9', 
												'height':'30px', 
												'width':'30px',
												'border':'1px black solid'
											},
											label: $sce.trustAsHtml('< -3')
										});
										if (hasInsufficientData) {
											$scope.legend.list.push({          
												style:{
													'background-color':'rgb(202,197,197)', 
													'opacity':'0.9', 
													'height':'30px', 
													'width':'30px',
													'border':'1px black solid'
												},
												label: $sce.trustAsHtml('Insufficient data')
											});
										}
										$scope.legend.show = true;
									}
								}
							} else {
								polygonGraphicsLayers.levelOfGeography.clear();
							}
							console.log('Complete');
							console.log($scope.layers.levelOfGeography.ready + '||' + $scope.result.ready + '||' + $scope.mapReady);

						};


						var graphicIds = [];
						var pointLayersSet = [];    
						var pointLayersDataStore = [];  
						for (var i = 0; i < $scope.selectableLayers.length; i++) { 
							if ($scope.selectableLayers[i].type == 'pointLayer') {
								pointLayersSet[$scope.selectableLayers[i].id] = 0; 
								pointLayersDataStore[$scope.selectableLayers[i].id] = '';                                        
							}
						}


						//Watch point layers changes
						angular.forEach($scope.layers.selectableLayers, function(selectedLayer){
							// console.log('layers.selectableLayers.' + selectedLayer.field.id + '.updated');
							$scope.$watch('layers.selectableLayers.' + selectedLayer.field.id + '.updated', function(){
								updateSelectableLayers(selectedLayer);
							});
						})

						var trafficCamerasLayer = null;
						var hospitalsLayer = null;

						var updateSelectableLayers = function(selectedLayer){
							if (selectedLayer.field.type == 'pointLayer') {
								if (selectedLayer.field.id == 'tCameras') {
									if (selectedLayer.show) {
										if (trafficCamerasLayer == null) {
											//Show loading
											// $scope.layers.selectableLayers.tCameras.ready = false;
											var kmlUrl = 'http://www.cdn.mto.gov.on.ca/kml/trafficcameras.kml';
											trafficCamerasLayer = new KMLLayer(kmlUrl);
											on(trafficCamerasLayer, 'load', function(){ 
												//hide loading..
												// $scope.layers.selectableLayers.tCameras.ready = true;
											});

											$scope.map.addLayer(trafficCamerasLayer);  
											$scope.map.infoWindow.resize(520,420); 

										} else {
											trafficCamerasLayer.show();
										}
									} else {
										if (trafficCamerasLayer != null) {
											trafficCamerasLayer.hide();
										}
									}  
								} 
								else if (selectedLayer.field.id == 'hospitals') {
									if (selectedLayer.show) {
										if (hospitalsLayer == null) {
											//Show loading
											
											var allLayerData = new LayerInfo();
											var layerInfo = allLayerData.GetLayerInfo('Hospitals'); 
											console.log(layerInfo);
											
											hospitalsLayer = new KMLLayerModel(layerInfo[0].id, layerInfo[0].displayName, layerInfo[0].legendURL, layerInfo[0].URLParams, layerInfo[0].metaDataURL, /*layerInfo[0].tooltip,*/ layerInfo[0].baseURL, layerInfo[0].experimental, layerInfo[0].requireProxy, layerInfo[0].opacity, layerInfo[0].isVisible, layerInfo[0].timeParams, layerInfo[0].timeEnabled); 
											
											var startExtent = new esri.geometry.Extent(-13286513.879567528, 4650392.6176555725, -5772448.251023558, 8089447.394261308, new esri.SpatialReference({wkid:"102100"}));  
											 
											$scope.map.addLayer(hospitalsLayer.GenerateLayer(startExtent));  
											$scope.map.infoWindow.resize(520,420); 

										} else {
											hospitalsLayer._newLayer.show();
										}
									} else { 
										if (hospitalsLayer != null) {
											hospitalsLayer._newLayer.hide();
										}
									} 
								} 
								else {
									if (selectedLayer.show) {
										angular.forEach(selectedLayer.data.features, function(feature) {
											createPoint(feature.geometry["coordinates"], feature.properties, selectedLayer);
										});
									} else {
										//Hide all points with this id
										angular.forEach(graphicIds, function(val){
											if (val.id == selectedLayer.field.id) {
												val.graphic.hide();
											}
										});
									}
								}
							} else if (selectedLayer.field.type == 'polygonLayer') {
								if (selectedLayer.show) {
									angular.forEach(selectedLayer.data.features, function(feature) {
										var sms = new SimpleFillSymbol(SimpleFillSymbol.STYLE_SOLID,
										new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID,
										new Color(selectedLayer.field.color), 2), new Color(selectedLayer.field.borderColor)
										);
										var jsonformatted = formatJSON(feature.geometry["coordinates"]);
										var attr = [];                                          
										if (feature.properties["NAME"]) { 
											attr["Name"] = feature.properties["NAME"]; 
										}                                   
										if (feature.properties["City"]) { 
											attr["City"] = feature.properties["City"];
											// console.log("CFSAUID in here???", map_data.features[j].properties);
										}                                   
										if (feature.properties["CFSAUID"]) { 
											attr["CFSAUID"] = feature.properties["CFSAUID"]; 
										} 
										var polygonJson = {"rings":jsonformatted,"spatialReference":{"wkid":102100 }}; 
										var polygon = new Polygon(polygonJson); 

										var newGraphic = new Graphic(polygon, sms, attr);  
										polygonGraphicsLayers[selectedLayer.field.id].add(newGraphic);
									});

								} else {//Hide
									polygonGraphicsLayers[selectedLayer.field.id].clear();
								}

							} else {
								//hide
							}

						};
						var createPoint = function (latLong, info, selectedLayer) {
							var latLong = JSON.stringify(latLong);
							latLong = latLong.replace("[",""); 
							latLong = latLong.replace("]","");
							latLong = latLong.split(","); 

							var pt = new Point(parseInt(latLong[0]), parseInt(latLong[1]), $scope.map.spatialReference);
							
							var colorValue = selectedLayer.field.color; //dom.byId(pointId).value;

							var sms = new SimpleMarkerSymbol();  
							sms.setColor(new dojo.Color(colorValue));
							sms.setStyle(SimpleMarkerSymbol.STYLE_CIRCLE);
							sms.setSize(8); 
							var attr = { "Name":info["NAME"], "City":info["City"]};

							var newGraphic = new Graphic(pt, sms, attr); 
							additionsGraphicsLayer.add(newGraphic);    
							graphicIds.push({"id":selectedLayer.field.id, "graphic": newGraphic}); 
						} 
						//Hover handlers for pointLayers
						additionsGraphicsLayer.on("mouse-over", function(evt){
							//Adds two new propeties to the graphic of the selected graphic
							// hoverGraphic and hoverContent
							
							if (typeof evt.graphic.hoverGraphic == 'undefined') {
								var t = "<b>${Name}</b>";
								if(evt.graphic.attributes["City"])
								{
									t +=  " (${City})";
								}
								if(evt.graphic.attributes["CFSAUID"])
								{
									t =  " (${CFSAUID})";
								}                                                  
								var highlightPt = new SimpleMarkerSymbol(SimpleMarkerSymbol.STYLE_SQUARE, 10,
								new SimpleLineSymbol(SimpleLineSymbol.STYLE_SOLID,
								new Color([255,0,0]), 1),
								new Color([0,255,0,0.25]));
								
								evt.graphic.hoverContent = esriLang.substitute(evt.graphic.attributes, t);
								
								evt.graphic.hoverGraphic = new Graphic(evt.graphic.geometry, highlightPt);
								$scope.map.graphics.add(evt.graphic.hoverGraphic);                                                  
							} else {
								evt.graphic.hoverGraphic.show();
							}
							
							dialog.setContent(evt.graphic.hoverContent);
							
							// domStyle.set(dom.byId("chartNode"), "display", "block");
							
							// domStyle.set(dialog.domNode, "opacity", 1);
							dijitPopup.open({
								popup: dialog, 
								x: evt.pageX + 1,
								y: evt.pageY
							});
							
						});
						additionsGraphicsLayer.on('mouse-out', function (evt) {
							if (typeof evt.graphic.hoverGraphic != 'undefined') {
								evt.graphic.hoverGraphic.hide();
							}
							dijitPopup.close(dialog);
						});  
						//

						//Hover handlers for polygonLayers
						angular.forEach(polygonGraphicsLayers, function(gLayer, gLayerName){


							//show debug info
							gLayer.on("click", function(evt){
								//if ctrl + click
								if (evt.ctrlKey && undefined !== evt.graphic && undefined !== evt.graphic.attributes && undefined !== evt.graphic.attributes.data && undefined !== evt.graphic.attributes.data.ref) {
									var str = "";
									var h = "AdmissionDate\tVisits\tTotalVisits";
									for (var i in evt.graphic.attributes.data.ref) {
										var o = evt.graphic.attributes.data.ref[i];
										str += o.AdmissionDate + "\t" + o.Visits + "\t" + o.TotalVisits;
										str += "\n";
									}
									if (undefined === evt.graphic.debugDom) {
										var onEventUnsubscribe;
										evt.graphic.debugDom = $("<div class='iiii'></div>").appendTo("body"); 
										evt.graphic.debugDom.css({
											position: "absolute",
											zIndex: 100000,
											padding: 10,
											width: 400,
											backgroundColor: "white"
										}).html("<h3>Debug- " + evt.graphic.attributes.Name + "</h3><pre></pre><div class='text-center'><div class='btn btn-primary'>Close</div></div>")
										//horizontal position center of window
										.position({
											of: $(window),
											at: "center top+200px"
										})
										.find(".btn").click(function () {
											evt.graphic.debugDom.remove();
											evt.graphic.debugDom = undefined;
											onEventUnsubscribe();
										});
										evt.graphic.debugDom.draggable();//make it draggable
										onEventUnsubscribe = $scope.$on("$destroy", function() {
											if (undefined !== evt.graphic.debugDom) {
												evt.graphic.debugDom.remove();
												evt.graphic.debugDom = undefined;                                              
											}
										});
									} else {
										//move to bottom of the parent
										//this will make it move above overlapping other debug windows
										evt.graphic.debugDom.appendTo(evt.graphic.debugDom.parent());
										//highlight
										evt.graphic.debugDom.hide().fadeIn();
									}
									
									

									$("pre", evt.graphic.debugDom).css({
										maxHeight: ($(window).height() - 400)
									}).text(h + "\n" + str);
								}
							});

							gLayer.on("mouse-move", function(evt){
								//Adds two new propeties to the graphic of the selected graphic
								// hoverGraphic and hoverContent
								// var e = document.getElementById("class");
								// var classText = e.options[e.selectedIndex].text;
								if (typeof evt.graphic.hoverGraphic == 'undefined') {

									if (gLayerName == 'levelOfGeography') {
										var t = '<b>${Name}</b><hr>';
										
										if(typeof evt.graphic.attributes.Quantile != 'undefined'){
											t += '<b><span style=\'color:green\'>Quantile:</span></b> ${Quantile}<br>';
										}

										if(typeof evt.graphic.attributes.s_dev != 'undefined'){
											t += "<b><span style=\'color:green\'>S-dev:</span></b> ${s_dev}<br>";
										}
										
										t += '<b><span style=\'color:green\'>Total ' +  $scope.currentSearchParams.data.syndrome + ' Visits:</span></b> ${totalVisits}<br><b><span style=\'color:green\'>Total E.D. Visits:</span></b> ${totalEdVisits}<br>'; 
										t += '<b><span style=\'color:green\'>% of Visits:</span></b> ${Percent}%';                                                            
									} else {
										var t = '<b>${Name}</b>';
									}

									evt.graphic.hoverGraphic = new Graphic(evt.graphic.geometry, highlightSymbol);
									$scope.map.graphics.add(evt.graphic.hoverGraphic);
									evt.graphic.hoverContent = esriLang.substitute(evt.graphic.attributes, t);
								} else {
									evt.graphic.hoverGraphic.show();
								}
								
								dialog.setContent(evt.graphic.hoverContent);
								
								// domStyle.set(dialog.domNode, "opacity", 1);
								dijitPopup.open({
									popup: dialog, 
									x: evt.pageX + 1,
									y: evt.pageY
								});
							});
							gLayer.on("mouse-out", function(evt){
								if (typeof evt.graphic.hoverGraphic != 'undefined') {
									evt.graphic.hoverGraphic.hide();
								}
								dijitPopup.close(dialog);
							});
						});
						//
						$scope.mapReady = true;

						// $scope.loading = false;
						// console.log($scope.layers.levelOfGeography.ready + '||' + $scope.result.ready + '||' + $scope.mapReady );
						
						// updateLevelOfGeographyMap();


						//Tasks which are not important and buggy
						//
						try {
							//Add overview map 
							var overviewMapDijit = new OverviewMap({
								map: $scope.map,
								visible: true,
								attachTo: "bottom-right",
								width: 150,
								height:100,
								expandFactor: 5
							});
							overviewMapDijit.startup();
							$window.lastMapInstance.overviewMap = overviewMapDijit;
							
							//Add scale bar
							var scalebar = new Scalebar({
								map: $scope.map, 
								scalebarUnit: "metric",
								attachTo: "bottom-center"
							});
							$window.lastMapInstance.scalebar = scalebar;

							//Add home button
							var home = new HomeButton({
								map: $scope.map
							}, $scope.homeButtonId);
							home.startup(); 
							$window.lastMapInstance.homeButton = home;
						}  catch(e){
							console.log(e);
						}

						// 
					}
				};
				init();

				// var t = function(){
				//      $timeout(function(){
				//            console.log($scope.result);
				//            console.log($scope.layers.levelOfGeography.ready + '||' + $scope.result.ready + '||' + $scope.mapReady);
				//            t();
				//      }, 1000);
				// };
				// t();
			}); 
			// 
		},
		templateUrl: 'views/aces-map.html'
	};
}]);
