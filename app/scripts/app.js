'use strict';

angular.module('acesApp', ['ui.router', 'ngResource', 'ui.date', 'ngGrid', 'nvd3ChartDirectives', 'ui.bootstrap', 'colorpicker.module', 'angular-growl', 'ngAnimate', 'rzModule', 'vcRecaptcha','ngIntlTelInput','ngSanitize', 'ngCsv','ui.toggle'])

    .run(['$rootScope', '$state', '$stateParams', 'Auth', '$timeout', 'alertService', '$injector', 'Profile', function ($rootScope, $state, $stateParams, Auth, $timeout, alertService, $injector, Profile) {

        // State change handler, fires when user changes the page
        $rootScope.$on("$stateChangeStart", function (event, toState, toParams, fromState, fromParams) {
            //  Check if user is authorized to change to requested state
            if (!Auth.authorize(toState.data.access) || (Auth.isLoggedIn() && !Auth.hasAccessToState(toState.name))) {
                // If not authorized, and no pending state change, user is not authorized to change state
                if (!Auth.pendingStateChange) {
                    event.preventDefault();
                    if (Auth.isLoggedIn()) {
                        alertService.add('warning', "Access Denied due to insufficient privileges.");
                    } else {
                        alertService.add('danger', "Please login to access this page.");
                    }
                }
                if (fromState.url === '^') {
                    // debugger;
                    Auth.pendingStateChange = true;
                    // If pending state change is true, and user is logged in, send to landing page
                    if (Auth.isLoggedIn()) {
                        $state.go('app.user');
                    } else {
                        $rootScope.error = null;
                        $state.go('app.home');
                    }
                }
            }
        });

        $rootScope.$on("$stateChangeSuccess", function () {
            Auth.pendingStateChange = false;

            //check if force password change is true
            //if forcePasswordChange returns true and current state is not change password page
            if (Auth.forcePasswordChange() && !$state.is("app.profile.changePassword")) {
                //redirect back to current state after password change
                Profile.changePassword.redirectTo = $state.current.name;
                //redirect to change password page
                $state.go("app.profile.changePassword");
            }
        });

        $rootScope.$on("$stateChangeError", function (event, toState, toParams, fromState, fromParams, error) {

        });
    }])

    .config(['$stateProvider', '$urlRouterProvider', '$locationProvider', '$httpProvider','growlProvider', 'ngIntlTelInputProvider', function ($stateProvider, $urlRouterProvider, $locationProvider, $httpProvider, growlProvider, ngIntlTelInputProvider) {

        $httpProvider.interceptors.push('AuthHttpInterceptor');

        var access = routingConfig.accessLevels;
        // App routes
        $stateProvider
            .state('app', {  // grandfather state
                abstract: true,
                template: '<ui-view/>',
                data: {
                    access: access.public
                },
                resolve: {
                    loggenInUserInit: function(Auth){
                        //Wait for basic variables to init if user logged in before doing anything
                        //this will execute everytime user refreshes a page or load the page
                        //This will also logout the user whose token has expired (session timeout)
                        if (Auth.isLoggedIn()) {
                            return Auth.getBasicData();
                        } else {
                            //If not logged, it will be init after logging in
                            return;
                        }
                    }
                }
            })

            .state('app.home', {
                url: '/',
                templateUrl: 'views/app.home.html',
                controller: 'HomeCtrl',
                data: {
                    access: access.public
                }
            })
            .state('app.forgotpassword', {
                url: '/forgotpassword',
                templateUrl: 'views/profile/forgotPassword.html',
                controller: 'ForgotPasswordCtrl',
                data: {
                    access: access.public
                }
            })
            .state('app.resetpassword', {
                url: '/resetpassword',
                templateUrl: 'views/profile/resetPassword.html',
                controller: 'ResetPasswordCtrl',
                data: {
                    access: access.public
                }
            })
            .state('app.error', {
                url: '/error/:error',
                templateUrl: 'views/error.tpl.html',
                data: {
                    access: access.public
                }
            })
            .state('app.admin', {
                abstract: true,
                url: '/admin',
                templateUrl: 'views/admin/index.html?v=' + encodeURIComponent(ACES.buildnumber),
                controller: 'AdminCtrl',
                data: {
                    access: access.admin
                }
            })
            .state('app.admin.users', {
                abstract: true,
                url: '/users',
                template: '<div ui-view></div>',
                // controller: 'AdminUsersCtrl',
                data: {
                    access: access.admin
                }
            })
            .state('app.admin.users.list', {
                url: '/users/list',
                templateUrl: 'views/admin/users.html?v=' + encodeURIComponent(ACES.buildnumber),
                controller: 'AdminUsersCtrl',
                data: {
                    access: access.admin
                }
            })
            .state('app.admin.users.create', {
                url: '/create',
                templateUrl: 'views/admin/userForm.html?v=' + encodeURIComponent(ACES.buildnumber),
                controller: 'AdminUserCreateCtrl',
                data: {
                    access: access.admin
                }
            })
            // User profile
                .state('app.profile', {
                    url: '/profile',
                    templateUrl: 'views/profile/profile.html',
                    data: {
                        access: access.user
                    },
                    controller: 'ProfileCtrl'
                })
                .state('app.profile.changePassword', {
                    url: '/changepassword',
                    templateUrl: 'views/profile/changePassword.html',
                    data: {
                        access: access.user
                    },
                    controller: 'ProfileCtrl'
                })
                .state('app.profile.keywordAlerts', {
                    url: '/keywordAlerts',
                    templateUrl: 'views/profile/keywordAlerts.html?v=' + encodeURIComponent(ACES.buildnumber),
                    data: {
                        access: access.user
                    },
                    controller: 'keywordAlertsCtrl'
                })
            //

            .state('app.user', {
                url: '/user',
                templateUrl: 'views/user.tpl.html',
                data: {
                    access: access.user
                },
                controller: 'UserCtrl'
            })

            // Epicurves
                .state('app.epicurves', {
                    url: '/epicurves/phus/:type',
                    templateUrl: 'views/epicurves.html?v=' + encodeURIComponent(ACES.version),
                    data: {
                        access: access.user
                    },
                    controller: 'EpicurvesCtrl',
                    resolve: {
                      hospitalLoad: function(hospitals, $interval, $q){
                        var d = $q.defer();
                        var n = $interval(function () {
                            if (undefined != hospitals.phus) {
                                $interval.cancel(n);
                                d.resolve(hospitals);
                            }
                        }, 500);
                        return d.promise;
                      }
                    }
                })
            //
            /* Line Listings */
                .state('app.linelistings', {
                    url: '/linelistings/:type',
                    templateUrl: 'views/lineListings.html',
                    data: {
                        access: access.user
                    },
                    controller: 'LinelistingsCtrl',
                    resolve: {
                      hospitalLoad: function(hospitals, $interval, $q){
                        var d = $q.defer();
                        var n = $interval(function () {
                            if (undefined != hospitals.phus) {
                                $interval.cancel(n);
                                d.resolve(hospitals);
                            }
                        }, 500);
                        return d.promise;
                      }
                    }
                })
            //

            // Resources
                .state('app.resources', {
                    url: '/resources',
                    templateUrl: 'views/resources.html',
                    data: {
                        access: access.user
                    },
                    controller: 'ResourcesCtrl'
                })
            //


            // Maps
                .state('app.maps', {
                    url: '/maps',
                    templateUrl: 'views/maps.html',
                    data: {
                        access: access.admin
                    }
                })
            //
            // Maps New
                .state('app.mapsnew', {
                    url: '/mapsnew',
                    templateUrl: 'views/mapsnew.html',
                    data: {
                        access: access.user
                    },
                    controller: 'mapsCtrl',
                    resolve: {
                        pingServerCheck: ['Auth', function(Auth){
                            //ping the server to check if session is valid or not
                            return Auth.pingServer("ui-route");
                        }]
                    }
                })
            //

            // Alerts
                .state('app.alerts', {
                    abstract: true,
                    url: '/alerts',
                    data: {
                        access: access.user
                    },
                    templateProvider: function () { return '<div ui-view></div>'},
                })
                .state('app.alerts.phus', {
                    url: '/phus',
                    templateUrl: 'views/Alerts.phus.html',
                    data: {
                        access: access.user
                    },
                    controller: 'AlertsCtrl',
                    resolve: {
                        alertTypes: function(AlertTypes){
                            return AlertTypes.get();
                        },
                        syndromesList: function (classifications) {
                            return classifications.getData();
                        }
                    }
                })
                .state('app.alerts.phus.phu', {
                    url: '/:phuId',
                    templateProvider: function () { return '<div ui-view></div>'},
                    data: {
                        access: access.user
                    },
                    controller: 'AlertsphusphuCtrl'
                })
                .state('app.alerts.phus.phu.hospital', {
                    url: '/hospital/:hospitalId',
                    templateProvider: function () { return ''},
                    data: {
                        access: access.user
                    },
                    controller: 'AlertsphusphuhospitalCtrl'
                })
            //


            ;

        $urlRouterProvider.otherwise('/');

        //growl global settings
        growlProvider.globalTimeToLive({success: 5000, error: 7000, warning: 5000, info: 3000});
        growlProvider.globalDisableCountDown(true);
        growlProvider.globalDisableIcons(true);
        growlProvider.globalPosition('bottom-center');

        //ng-intl-tel-input
        ngIntlTelInputProvider.set({defaultCountry: 'ca'});

    }])
;

//Temporary global function
function isQAServer () {
    if (location.href.match(/^https?:\/\/(acesbeta\.kflaphi\.ca|km-qa)/) === null) {
        return false;
    } else {
        return true;
    }
}
