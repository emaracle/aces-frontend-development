angular.module('acesApp')
    .controller('LineListingsModalCtrl', [
        '$scope', '$modalInstance', 'lineListingData', 'classifiers', 'Auth',
        function ($scope, $modalInstance, lineListingData, classifiers, Auth) {
            console.log('alert data is', lineListingData);
            console.log('and row data is', lineListingData.rowData);
            $scope.lineListingData = lineListingData;
            $scope.rowData = lineListingData.rowData;
            //temporary - make this function available in view
            $scope.isQAServer = isQAServer;
            $scope.classifiers = classifiers;
            $scope.Auth = Auth;

            if (lineListingData.keys !== undefined) {
                $scope.keys = lineListingData.keys;
            }

            $scope.formatDate = function (dateToFormat) {
                var date = new Date(dateToFormat);
                var hour = date.getHours();
                if (hour < 10) {
                    hour = "0" + hour;
                }
                var minutes = date.getMinutes();
                var day = date.getDate();
                var month = date.getMonth();
                var year = date.getFullYear();
                var monthString;
                switch (month) {
                    case 1:
                        monthString = "January";
                        break;
                    case 2:
                        monthString = "February";
                        break;
                    case 3:
                        monthString = "March";
                        break;
                    case 4:
                        monthString = "April";
                        break;
                    case 5:
                        monthString = "May";
                        break;
                    case 6:
                        monthString = "June";
                        break;
                    case 7:
                        monthString = "July";
                        break;
                    case 8:
                        monthString = "August";
                        break;
                    case 9:
                        monthString = "September";
                        break;
                    case 10:
                        monthString = "October";
                        break;
                    case 11:
                        monthString = "November";
                        break;
                    case 12:
                        monthString = "December";
                        break;
                    default:
                        monthString = "Invalid month";
                        break;
                }
                return hour + ":" + minutes + " " + day + "-" + monthString + "-" + year;
            };

            console.log('modal graph', $scope.lineListingData);

            //  Function used by nvd3 charts to turn timestamps into dates
            $scope.xAxisTickFormatFunction = function () {
                return function (d) {
                    return d3.time.format('%x')(new Date(d)); //uncomment for date format
                }
            };

            $scope.averageFunction = function () {
                return function (d) {
                    return d.average;
                }
            };

            $scope.ok = function () {
                $scope.alertData = null;
                $modalInstance.close();
            };

            $scope.cancel = function () {
                $scope.alertData = null;
                $modalInstance.dismiss('cancel');
            };
        }
    ]);







angular.module('acesApp')
    .controller('LineListingsModal2Ctrl', [
        '$scope', '$modalInstance', 'lineListings', 'options', 
        function ($scope, $modalInstance, lineListings, options) {
            $scope.title = options.title;
            $scope.data = []; 
            $scope.lineListingsControllerOptions = {data: $scope.data, options: options};
            $scope.lineListingsControllerOptions.options.modalSelected = true;      
            var params = options.params; 
            //do not include admissions
            params.Params.admissionType = params.Params.type;

            lineListings.Visits.getData(params).then( function (data) {
                if (params.HospitalID !== undefined && params.HospitalID != 'All') {
                    $scope.lineListingsControllerOptions.data = _.where(data, {HospitalID: params.HospitalID});   
                    $scope.data = _.where(data, {HospitalID: params.HospitalID});     
                } else {
                    $scope.lineListingsControllerOptions.data = data;
                    $scope.data = data;
                }
            });

            //columns that are hidden
            if (options.hiddenColumns !== undefined) {
                $scope.hiddenColumns = options.hiddenColumns; //['VisitDate', 'FRIScore', 'PatientLHIN', 'EMSArrival'];    
            }

            //columns that are disabled
            if (options.disabledColumns !== undefined) {
                $scope.disabledColumns = options.disabledColumns; 
            }

            $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
            };
        }
    ]);