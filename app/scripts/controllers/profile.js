'use strict';

angular.module('acesApp')
    .controller('ProfileCtrl', ['$scope', '$state', '$rootScope', '$q', '$http', 'Auth', '$timeout', 'alertService', 'Profile',
        function ($scope, $state, $rootScope, $q, $http, Auth, $timeout, alertService, Profile) {
            $scope.user = Auth.user;
            $scope.forcePasswordChange = Auth.forcePasswordChange();
            //Change Password
            $scope.changePassword = {
                currentPassword: '',
                newPassword: '',
                newPassword2: '',
                error: { found: false }, //holds error messages
                submit: function(){
                    this.error.found = false;
                    //Validate
                    if (this.currentPassword == '') {
                        this.error.currentPasswordRequired = true;
                        this.error.found = true;
                    } else {
                        this.error.currentPasswordRequired = false;
                        //Validate current password
                        if (!Profile.changePassword.validateCurrentPassword(this.currentPassword)) {
                            this.error.currentPasswordIncorrect = true;
                            this.error.found = true;
                        } else {
                            this.error.currentPasswordIncorrect = false;
                        }

                    }
                    if (this.newPassword == '') {
                        this.error.newPasswordRequired = true;
                        this.error.found = true;
                    } else {
                        this.error.newPasswordRequired = false;
                        // Add password conformation validation
                        if (Profile.changePassword.validatePasswordFormat(this.newPassword)) {
                            this.error.newPasswordInvalid = true;
                            this.error.found = true;
                        } else {
                            this.error.newPasswordInvalid = false;
                        }
                    }

                    if (this.newPassword != this.newPassword2) {
                        this.error.newPassword2Same = true;
                        this.error.found = true;
                    } else {
                        this.error.newPassword2Same = false;
                    }

                    //If the new passwords don't equal the existing password
                    if (this.newPassword == this.newPassword2 && this.newPassword == this.currentPassword){
                        this.error.reusePassword = true;
                        this.error.found = true;
                    } else {
                        this.error.reusePassword = false;
                    }

                    //Not errors found
                    if (!this.error.found) {

                        Profile.changePassword
                            .do(this.newPassword, Auth.user.username, Auth.user.token)
                            .success(function (response) {
                                alertService.add('success', "Password changed successfully.");
                                if (Profile.changePassword.redirectTo !== "") {
                                    $state.go(Profile.changePassword.redirectTo);
                                    //clear redirectTo
                                    Profile.changePassword.redirectTo = "";
                                } else {//default to profile
                                    $state.go('app.profile');
                                }
                            })
                            .error(function (response, status) {
                                alertService.add('danger', "Failed with " + status + " code.");
                                // console.log(arguments)
                            });
                    }
                }
            };



            $scope.cancel = function () {
                $state.go('app.profile');
            }
        }])

    .controller('ForgotPasswordCtrl', ['$scope', '$state', 'Auth', 'Profile', 'alertService', function ($scope, $state, Auth, Profile, alertService) {
        if (Auth.isLoggedIn()) {
            $state.go('app.home');
        } else {
            $scope.submit = function () {
                //reset error
                $scope.error = {};


                if ($.trim($scope.email).length == 0) {
                    $scope.error.emailRequired = true;
                } else {
                    Profile.forgotPassword
                        .do($scope.email)
                        .success(function (response) {
														if(response[0].hasOwnProperty("Status") && response[0].Status == "ERROR: No such username."){
															alertService.add('danger', "Sorry, this email address does not match our records. Please contact kflaphi@kflapublichealth.ca");
														}
														else if(response[0].hasOwnProperty("Status") && response[0].Status == "ERROR: Account is locked."){
															alertService.add('danger', "Sorry your account has been locked which disables the forget password function. Please contact kflaphi@kflapublichealth.ca");
														}
														else{
															alertService.add('success', "Reset password link has been emailed to your email address.");
															$state.go('app.home');
														}
                        })
                        .error(function (response, status) {
                            alertService.add('danger', "An error has occured during this process. Please notify kflaphi@kflapublichealth.ca.");
                           // console.log(arguments)
                        });
                }
            }
        }
    }])
    .controller('ResetPasswordCtrl', ['$scope', '$state', '$location', 'Auth', 'Profile', 'alertService', function ($scope, $state, $location, Auth, Profile, alertService) {
        if (Auth.isLoggedIn()) {
            $state.go('app.home');
        } else {
            $scope.params = $location.search();
            if ($scope.params.token === undefined || $scope.params.user === undefined) {
                alertService.add('danger', 'Invalid parameters');
                $state.go('app.home');
                return;
            }

            $scope.error = { found: false };
            $scope.newPassword = '';
            $scope.newPassword2 = '';

            $scope.submit = function () {
                $scope.error.found = false;
                //Validate
                if ($scope.newPassword == '') {
                    $scope.error.newPasswordRequired = true;
                    $scope.error.found = true;
                } else {
                    $scope.error.newPasswordRequired = false;
                    // Add password conformation validation
                    if (Profile.changePassword.validatePasswordFormat($scope.newPassword)) {
                        $scope.error.newPasswordInvalid = true;
                        $scope.error.found = true;
                    } else {
                        $scope.error.newPasswordInvalid = false;
                    }
                }

                if ($scope.newPassword != $scope.newPassword2) {
                    $scope.error.newPassword2Same = true;
                    $scope.error.found = true;
                } else {
                    $scope.error.newPassword2Same = false;
                }

                //Not errors found
                if (!$scope.error.found) {

                    Profile.changePassword
                        .do($scope.newPassword, $scope.params.user, $scope.params.token,1)
                        .success(function (response) {
                            alertService.add('success', "Password reset successfully.");
                            $state.go('app.home');
                        })
                        .error(function (response, status) {
                            if (status == 403) {
                                alertService.add('danger', "Failed to reset password.");
                                $state.go('app.home');
                            } else {
                                alertService.add('danger', "Failed to reset password. (" + status + ")");
                            }
                        });
                }
            }
        }
    }])
    .controller('keywordAlertsCtrl', ['$scope', '$state', '$modal', 'Auth', 'Profile', 'alertService','hospitals', 'growl', '$log',
        function ($scope, $state, $modal, Auth, Profile, alertService, hospitals, growl, $log) {

        $scope.log = $log;

        //Set content for select options
        $scope.types = [{id: 1, title: 'Both'},{id: 2, title: 'ED'},{id: 3, title: 'AD'}];
        $scope.showWarning = false;

        $scope.clearKeyword = function (){
          //Sets to defaults
          $scope.alerts = {keywords:null,phus:null,hospitals:null,type:$scope.types[0]};
          $scope.formValidationError = {keywords:false};
          $scope.editMode = false;
          $scope.updateKeywordUUID = null;
        };

        $scope.validateForm = function (){
          //Ensure validation is set to default
          $scope.formValidationError = {keywords:false};

          //Keywords
          if ($scope.alerts.keywords == null){
            //Is null
            $scope.formValidationError.keywords = true;
          } else {
            //Trim whitespace and remove any leading or trailing commas
            $scope.alerts.keywords = $scope.alerts.keywords.replace(/^\s+|\s+$/g,'').replace(/(^,)|(,$)/g, "");

            //Is empty
            if ($scope.alerts.keywords == ""){
              $scope.formValidationError.keywords = true;
            }
          };

          //PHUs
          $scope.validatedPhus = "";
          if (typeof $scope.alerts.phus !== 'undefined'){
            angular.forEach($scope.alerts.phus, function(alertPhu, key) {
              //find the actual phu object
              angular.forEach($scope.phus, function(phu){
                if (alertPhu == phu.phuShortcode){
                  if (key == 0){
                    $scope.validatedPhus += phu.id;
                  } else {
                    $scope.validatedPhus += ", " + phu.id;
                  }
                }
              });
            });
          };

          //Hospitals
          $scope.validatedHospitals = "";
          if (typeof $scope.alerts.hospitals !== 'undefined'){
            angular.forEach($scope.alerts.hospitals, function(alertHospital, key) {
              //find the actual phu object
              angular.forEach($scope.hospitals, function(hospital){
                if (alertHospital == hospital.hospShortcode){
                  if (key == 0){
                    $scope.validatedHospitals += hospital.id;
                  } else {
                    $scope.validatedHospitals += ", " + hospital.id;
                  }
                }
              });
            });
          };

          //Visit Type
          $scope.validatedVisitType = "";
          if ($scope.alerts.type.id != 1){
              $scope.validatedVisitType = $scope.alerts.type.title.toLowerCase();
          };
        }

        //Adds a new keyword alert to the db
        $scope.addKeyword = function (){

          //Validate data
          $scope.validateForm();

          //If form is valid add new record
          if (!$scope.formValidationError.keywords){

            //Call Add Function
            Profile.addKeyword
            .do(Auth.user.useruuid,
              Auth.user.token,
              $scope.alerts.keywords,
              1,
              3600,
              $scope.validatedPhus,
              $scope.validatedHospitals,
              $scope.validatedVisitType)
            .success(function (object) {
              $scope.clearKeyword();
              $scope.getKeywordAlerts();
              growl.success("<strong>SUCCESS:</strong> Keyword Alert added.");
            })
            .error(function (response, status) {
              console.error("Error adding keyword - " + status);
              growl.error("<strong>ERROR:</strong> Unable to add new Keyword Alert.");
            });
          };
        };

        //Deletes a keyword alert from the db
        $scope.deleteKeyword = function (keywordUUID){
          //Reset the form
          $scope.clearKeyword();

          //Call delete function
          Profile.deleteKeyword
          .do(Auth.user.useruuid,Auth.user.token,keywordUUID)
          .success(function (object) {
            $scope.getKeywordAlerts();
            growl.success("<strong>SUCCESS:</strong> Keyword Alert removed.");
          })
          .error(function (response, status) {
            console.error("Error deleting keyword - " + status);
          });
        };

        //Toggles the keyword alert from active to inactive in the db
        $scope.updateKeywordActiveStatus = function (keywordUUID,active){

          //Sent the new status to the db
          Profile.updateKeywordActiveStatus
            .do(Auth.user.useruuid,
              Auth.user.token,
              keywordUUID,
              active)           
            .error(function (response, status) {
              console.error("Error changing keyword active status - " + status);
              growl.error("<strong>ERROR:</strong> Unable to change active status of Keyword Alert.");
            });                 
        }


        //Saves an edited keyword alert to the db
        $scope.updateKeyword = function (){
          //Validate data
          $scope.validateForm();

          //If form is valid update
          if (!$scope.formValidationError.keywords){
            //Call Function
            Profile.updateKeyword
            .do(Auth.user.useruuid,
              Auth.user.token,
              $scope.updateKeywordUUID,
              $scope.alerts.keywords,
              1,
              3600,
              $scope.validatedPhus,
              $scope.validatedHospitals,
              $scope.validatedVisitType)
            .success(function (object) {
              $scope.clearKeyword();
              $scope.getKeywordAlerts();
              growl.success("<strong>SUCCESS:</strong> Keyword Alert updated.");
            })
            .error(function (response, status) {
              console.error("Error updating keyword - " + status);
              growl.error("<strong>ERROR:</strong> Unable to update Keyword Alert.");
            });
          };
        };

        $scope.modeKeyword = function (keywordUUID){

          //Clear existing form
          $scope.clearKeyword();

          var foundKeyword = false;

          //Go through array to find keyword
          angular.forEach($scope.keywordAlerts, function(keyword) {

            if (keywordUUID == keyword.KeywordUUID){

              foundKeyword = true;

              //Variables
              var phuList = [];
              var hospitalList = [];

              //Test PHUs
              if (typeof keyword.PHU !== 'undefined'){

                var phuString = keyword.PHU;
                var phuMatchs = phuString.split(',');

                angular.forEach(phuMatchs, function(phu) {
                  phuList.push(phu.replace(/^\s+|\s+$/g,''));
                });
              }

              //Test Hospitals
              if (typeof keyword.HospitalID !== 'undefined'){

                var hospitalString = keyword.HospitalID;
                var hospitalMatchs = hospitalString.split(',');

                angular.forEach(hospitalMatchs, function(hospital) {
                  hospitalList.push(hospital.replace(/^\s+|\s+$/g,''));
                });
              }

              //Test PHUs
              var allowPhuAccess = true;
              angular.forEach(phuList, function(phuListObject) {
                var phuPermissionExists = false;

                angular.forEach($scope.phus, function(phuScopeObject) {
                  if (phuListObject == phuScopeObject.phuShortcode){
                    phuPermissionExists = true;
                  }
                });

                if (!phuPermissionExists){
                  allowPhuAccess = false;
                }
              });

              //Test Hospitals
              var allowHospitalAccess = true;
              angular.forEach(hospitalList, function(hospitalListObject) {

                var hospitalPermissionExists = false;

                angular.forEach($scope.hospitals, function(hospitalScopeObject) {
                  if (hospitalListObject == hospitalScopeObject.hospShortcode){
                    hospitalPermissionExists = true;
                  }
                });

                if (!hospitalPermissionExists){
                  allowHospitalAccess = false;
                }
              });

              if (allowPhuAccess){
                if (allowHospitalAccess){
                  //Put user into edit mode
                  $scope.editMode = true;
                  //Set keywordUUID to update
                  $scope.updateKeywordUUID = keyword.KeywordUUID;

                  //Keywords
                  if (typeof keyword.Keyword !== 'undefined'){
                    $scope.alerts.keywords = keyword.Keyword;
                  } else {
                    $scope.alerts.keywords = null;
                  };

                  //PHUs
                  if (phuList.length > 0){
                    $scope.alerts.phus = phuList;
                  } else {
                    $scope.alerts.phus = null;
                  };

                  //Hospitals
                  if (hospitalList.length > 0){
                    $scope.alerts.hospitals = hospitalList;
                  } else {
                    $scope.alerts.hospitals = null;
                  };

                  //Visit Types
                  if (typeof keyword.VisitType == 'undefined'){
                    $scope.alerts.type = $scope.types[0];
                  } else if (keyword.VisitType == 'ed'){
                    $scope.alerts.type = $scope.types[1];
                  } else {
                    $scope.alerts.type = $scope.types[2];
                  };
                } else {
                  //Warn the user that keyword can't updated due to hospital
                  growl.error("<strong>ERROR:</strong> Invalid Hospital in Keyword Alert.");
                }
              } else {
                //Warn the user that keyword can't updated due to PHU
                growl.error("<strong>ERROR:</strong> Invalid PHU in Keyword Alert.");
              }
            }
          });

          //Can't find keywordUUID
          if (!foundKeyword) {
            growl.error("<strong>ERROR:</strong> Unable to retrieve selected Keyword Alert");
          };
        };

        //Gets the existing keyword alerts from the db
        $scope.getKeywordAlerts = function () {

          $scope.keywordAlerts = [];

          Profile.keywordAlerts
            .do(Auth.user.useruuid, Auth.user.token)
            .success(function (object) {
              //Add endpoint object to scope variable array
              angular.forEach(object, function(keyword) {
                /*PROCESS PHUs*/
                //Only process keywords PHU is not null
                if (keyword.PHU != null){
                  var keywordPhuString = keyword.PHU;
                  var keywordPhuMatchs = keywordPhuString.split(',');
                  var keywordPhuList = "";

                  angular.forEach(keywordPhuMatchs, function(phuMatch) {
                      var cleanPhu = phuMatch.replace(/^\s+|\s+$/g,'');

                      //find the actual phu object
                      angular.forEach($scope.phus, function(phu){
                        if (phu.id == cleanPhu){
                          //Add phu shortcode to the keyword obj
                          if (keywordPhuList == ""){
                            keywordPhuList += phu.phuShortcode;
                          } else {
                            keywordPhuList += ", " + phu.phuShortcode;
                          }
                        }
                      });
                  });

                  //Put the phu shortcode list back in the keyword object
                  keyword.PHU = keywordPhuList;
                }

                /*PROCESS HOSPITALs*/
                //Only process keywords HOSPITAL is not null
                if (keyword.HospitalID != null){
                  var keywordHospitalString = keyword.HospitalID;
                  var keywordHospitalMatchs = keywordHospitalString.split(',');
                  var keywordHospitalList = "";

                  angular.forEach(keywordHospitalMatchs, function(hospitalMatch) {
                      var cleanHospital = hospitalMatch.replace(/^\s+|\s+$/g,'');

                      //find the actual hospital object
                      angular.forEach($scope.hospitals, function(hospital){
                        if (hospital.id == cleanHospital){
                          //Add phu shortcode to the keyword obj
                          if (keywordHospitalList == ""){
                            keywordHospitalList += hospital.hospShortcode;
                          } else {
                            keywordHospitalList += ", " + hospital.hospShortcode;
                          }
                        }
                      });
                  });
                  //Put the hospital shortcode list back in the keyword object
                  keyword.HospitalID = keywordHospitalList;
                }

                //Cast the active string as bool
                keyword.Active = (keyword.Active == 'true' ? true : false);

                $scope.keywordAlerts.push(keyword);
              });

              if($scope.keywordAlerts.length == 0){
                $scope.showWarning = true;
              } else {
                $scope.showWarning = false;
              }
            })
            .error(function (response, status) {
              console.error("Error retrieving keyword - " + status);
            });
        };

        //Shows counts when at least 1 item is selected
        $scope.showCount = function (itemList){
          if (typeof itemList !== 'undefined'){
            if (itemList > 0){
              return true;
            } else {
              return false;
            }
          } else {
            return false;
          }
        };

        //Counts selected items from a list
        $scope.countLength = function (selectLength){
          if (selectLength <= 10){
            return selectLength;
          } else {
            return 10;
          }
        };

        //Selected which items should be highlighted in the list
        $scope.highlightControl = function (calledFrom){
          if (calledFrom == 'phu'){
            if ($scope.alerts.phus != null && $scope.alerts.phus.length > 0){
              $scope.alerts.phus = null;
            } else {
              var phuList = [];
              angular.forEach($scope.phus, function (phu) {
                phuList.push(phu.phuShortcode);
              });
              $scope.alerts.phus = phuList;
            };
          } else {
            if ($scope.alerts.hospitals != null && $scope.alerts.hospitals.length > 0){
              $scope.alerts.hospitals = null;
            } else {
              var hospitalList = [];
              angular.forEach($scope.hospitals, function (hospital) {
                hospitalList.push(hospital.hospShortcode);
              });
              $scope.alerts.hospitals = hospitalList;
            };
          }
        };

        //Monitors for changed with selected PHUs
        $scope.$watch('alerts.phus', function() {
          if ($scope.alerts.phus != null && $scope.alerts.phus.length > 0){
            var hospitalList = [];
            //Go through all hosptal list to get selected hospitals
            angular.forEach($scope.allHospitals, function (hospital) {
              //No need to process if no hospitals selected
              if ($scope.alerts.hospitals != null){
                //Find where hospital is in select hospital list
                if ($scope.alerts.hospitals.indexOf(hospital.hospShortcode) !== -1){
                  //No need to process if no phus selected
                  if ($scope.alerts.phus != null){
                    //Find where hospitals phu is in select phu list
                    if ($scope.alerts.phus.indexOf(hospital.phuShortcode) !== -1){
                      //If hospital not already in the list add it
                      if (hospitalList.indexOf(hospital.hospShortcode) === -1){
                        hospitalList.push(hospital.hospShortcode);
                      }
                    }
                  }
                }
              }
            });
            $scope.alerts.hospitals = hospitalList;
          } else {
            $scope.alerts.hospitals = null;
          }
        });

        //Hides hospitals from unselected PHUs
        $scope.hospitalInPhu = function (hospital){
          //if there are phus selected, only show associated hospitals
          if ($scope.alerts.phus != null && $scope.alerts.phus.length > 0){
            //flag that hopital is found
            var foundHospitalinPhu = false;

            //Find all the occurances in all hospitals of that hospital id
            for (var i in $scope.allHospitals) {
              if ($scope.allHospitals[i].id == hospital.id){
                //Check if phu id is in selected phu list
                if($scope.alerts.phus.indexOf($scope.allHospitals[i].phuShortcode) !== -1) {
                  foundHospitalinPhu = true;
                };
              };
            };

            //Return flag results
            if(foundHospitalinPhu) {
              return true;
            } else {
              return false;
            }
          } else {
              return true;
          }
        };

        //Init Page
        $scope.clearKeyword();
        $scope.getKeywordAlerts();

        //Get PHU and Hopsital lists
        hospitals.getData().then( function(data) {

          $scope.phus = [];
          //Add endpoint object to scope variable array
          angular.forEach(data.phus, function (phu) {
            if (phu.id.toLowerCase() != 'all') {
              $scope.phus.push(phu);
            }
          });

          $scope.allHospitals = [];
          //Add endpoint object to scope variable array if id not all
          angular.forEach(data.hospitals, function (hospital) {
            if (hospital.id.toLowerCase() != 'all') {
              $scope.allHospitals.push(hospital);
            };
          });

          $scope.hospitals = [];
          //Add endpoint object to scope variable array if id not all
          angular.forEach(data.hospitals, function (hospital) {
            if (hospital.id.toLowerCase() != 'all') {
              $scope.hospitals.push(hospital);
            };
          });

          //Clear duplicate hospitals
          $scope.hospitals = $scope.hospitals.reduce(function(hospitals, hospital1){
            var matches = hospitals.filter(function(hospital2){return hospital1.id == hospital2.id});
            if (matches.length == 0){
              hospitals.push(hospital1);
            } return hospitals;
          }, []);
        });

        $scope.details = function (keyword) {
            var modalInstance = $modal.open({
                templateUrl: 'views/profile/keywordAlertsNotifications.html?v=' + encodeURIComponent(ACES.buildnumber),
                controller: 'keywordAlertsNotificationsCtrl',
                size: 'lg',
                resolve: {
                    'notifications': function () {
                        return Profile.keywordNotifications.get(keyword.KeywordUUID);
                    },
                    'options': {
                        keyword: keyword
                    }
                }
            });
        };
    }])
    .controller('keywordAlertsNotificationsCtrl', ['$scope', '$modalInstance', '$modal', 'Profile', 'growl', 'notifications', 'options',
        function ($scope, $modalInstance, $modal, Profile, growl, notifications, options) {
            $scope.title = 'Notifications for Keyword ID ' + options.keyword.KeywordUUID;
			$scope.data = {data: {}, options: {}};
			$scope.data.data = notifications;
            $scope.pagingOptions = {
                pageSizes: [25, 50, 100, 250, 500, 1000],
                pageSize: 25,
                currentPage: 1
            };

            $scope.open = function (row) {
                console.log('open', row.entity);
                // Creates a modal when called
                var modalInstance = $modal.open({
                    templateUrl: 'views/profile/keywordAlertsNotificationDetail.html?v=' + encodeURIComponent(ACES.buildnumber),
                    controller: 'keywordAlertsNotificationDetailModalCtrl',
                    size: 'lg',
                    resolve: {
                        'options': function () {
                            return {
                                'notification': row.entity
                            }
                        },
                    }
                });
            };

            $scope.gridOptions = {
                rowTemplate: '<div ng-style="{\'cursor\': row.cursor, \'z-index\': col.zIndex() }" ng-click="open(row)" ng-repeat="col in renderedColumns" ng-class="col.colIndex()" class="ngCell {{col.cellClass}}" ng-cell></div>',
                columnDefs: [
                    {field: 'CreatedOn', cellFilter: 'date:"yyyy-MM-dd HH:mm:ss"', displayName: 'Date', width: '150px'},
                    //{field: 'EDVisitUUID', displayName: 'Last ED Visit', width: '300px', cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()"><span ng-cell-text ng-click="detail(\'ed\', row); $event.stopPropagation();" class="line-listings-cell-tooltip" tooltip-placement="top" tooltip-append-to-body="true" tooltip="Click for detail">{{row.entity[col.field]}}</span></div>'},
                    {field: 'EDVisitUUID', displayName: 'Last ED Visit', width: '330px'},
                    {field: 'ADVisitUUID', displayName: 'Last AD Visit', width: '200px'},
                    //{field: 'Visits', displayName: 'Visits'}
                ]
            };
            $scope.sortInfo = { fields: ['CreatedOn'], directions: ['desc'] };



            $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
            };
    }])
    .controller('keywordAlertsNotificationDetailModalCtrl', ['$scope', '$modalInstance', 'Profile', 'options',
        function ($scope, $modalInstance, Profile, options) {
            $scope.title = 'Visits';
			$scope.data = {data: {}, options: {}};
            Profile.keywordNotifications.getNotificationDetail(options.notification.AlertUUID).then(function (data) {
                $scope.data.data  = data;
                console.log(data);
            });

            $scope.hiddenColumns = ['EMSArrival', 'CTAS'];
            $scope.disabledColumns = ['PatientLHIN', 'ClassName'];

            $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
            };
    }]);
