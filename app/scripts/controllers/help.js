'use strict';

angular.module('acesApp')
    .controller('OpenTicketCtrl', ['$scope', '$modalInstance', 'AcesHelp',
        function ($scope, $modalInstance, AcesHelp) {
            $scope.showSuccess = false;
            $scope.busy = false;

            $scope.categories = AcesHelp.ticket.getCategories();

            $scope.error = { found: false };
            $scope.data = {
                category: $scope.categories[0].value,
                summary: '',
                description: ''
            };

            $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
            };

            $scope.test = "test";

            $scope.submit = function () {
                //reset error
                $scope.error = { found: false };
                //Validate

                if ($scope.data.category == '') {
                    $scope.error.category = true;
                    $scope.error.found = true;
                }
                if ($scope.data.summary == '') {
                    $scope.error.summary = true;
                    $scope.error.found = true;
                }
                if ($scope.data.description.length <= 10) {
                    $scope.error.description = true;
                    $scope.error.found = true;
                }

                //Not errors found
                if (!$scope.error.found) {
                    $scope.busy = true;
                    AcesHelp.ticket.submit($scope.data)
                        .then(function successCallback(response) {
                                //check if response data has error message
                                if (response.data.error !== undefined) {
                                    $scope.showSuccess = false;
                                    $scope.error.submit = {message: response.data.error};
                                } else {
                                    $scope.showSuccess = true;
                                }
                                $scope.busy = false;
                            }, function errorCallback(response) {
                                // called asynchronously if an error occurs
                                // or server returns response with an error status.

                                $scope.busy = false;
                                $scope.showSuccess = false;
                                $scope.error.submit = {status: response.status};
                                // console.log(response);
                            });
                }
            };
        }])


;

