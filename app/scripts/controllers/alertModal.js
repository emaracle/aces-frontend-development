'use strict';

angular.module('acesApp')
    .controller('AlertModalCtrl', [
        '$scope', 
        '$modalInstance',
        '$q',
        'alertGraphs', 
        'utils',
        function ($scope, $modalInstance, $q, alertGraphs, utils) {
            $scope.formatDate = function (dt) {
                return moment(dt).format('HH:mm YYYY-MM-DD');
            };

            $scope.slider = {
                value: 1,
                options: {
                    floor: 1,
                    ceil: alertGraphs.length,
                    showSelectionBar: true,
                    showTicks: true,
                    translate: function(value, sliderId, label) {
                        return moment(alertGraphs[value-1].alertRow.Date).format('YYYY-MM-DD HH:mm:ss');
                    },
                    onChange: function (sliderId, modelValue, highValue, pointerType) {
                        $scope.currentData = alertGraphs[modelValue-1];
                        $scope.draw();

                    }
                }
            };

            $scope.alertGraphs = alertGraphs;
            $scope.currentData = alertGraphs[0];

            $scope.draw = function () {
                console.log($scope.currentData);

                var alertData  = $scope.currentData.precessedData;

                $scope.displayGeogName = alertData.rowData.fullGeog;
                $scope.displayDate = moment(alertData.rowData.AlertOn).format('YYYY-MM-DD HH:mm:ss');
            
                //console.log('alert data is', alertData);
                //console.log('and row data is', alertData.rowData);
                // console.log($scope);
                $scope.alertData = alertData;
                // console.log('AlertModalCtrl');
                // console.log(alertData);
                $scope.rowData = alertData.rowData;

                $scope.isCusum = $scope.rowData.AlertFamilyID === "CUSUM";

                //Init/Defines variables related to ForceY and xAxisTicks 
                ( function () {
                    var identifyTicks = function (noDays, ticks) {
                        if (ticks <= 1) {
                            return 1;
                        }
                        var increment = Math.floor(noDays / ticks);
                        if (increment <= 1) {
                            return identifyTicks(noDays, ticks - 1);
                        }
                        return ticks;
                    };
                    var minY = 0, maxY = 0;
                    var minX = moment().valueOf(), maxX = 0;
                    angular.forEach(alertData.alertGraph, function (o) {
                        angular.forEach(o.values, function (a) {
                            if (a[1] < minY) {
                                minY = a[1];
                            }
                            if (a[1] > maxY) {
                                maxY = a[1];
                            }
                            if (a[0] < minX) {
                                minX = a[0];
                            }
                            if (a[0] > maxX) {
                                maxX = a[0];
                            }
                        });
                    });
                    //add some space to the top
                    $scope.forceY = [Math.floor(minY), Math.ceil(maxY * 1.1)];
                    if ($scope.isCusum) {
                        ( function () {
                            var minY = 0, maxY = 0;
                            angular.forEach(alertData.alertGraphCusumVisits, function (o) {
                                angular.forEach(o.values, function (a) {
                                    if (a[1] < minY) {
                                        minY = a[1];
                                    }
                                    if (a[1] > maxY) {
                                        maxY = a[1];
                                    }
                                });
                            });
                            //add some space to the top
                            $scope.cusumForceY = [Math.floor(minY), Math.ceil(maxY * 1.05)];
                        } )();
                    }
                    //create xAxisTicks
                    var ticks = 7;
                    var noDays = moment(maxX).diff(moment(minX), "days") + 1;
                    ticks = identifyTicks(noDays, ticks);
                    var increment = Math.floor(noDays / ticks);
                    //define xaxisticks
                    var xAxisTicks = [];
                    var sDate = moment(minX);
                    for (var j = 0; j <= ticks; j++) {
                        if (sDate.valueOf() <= maxX) {
                            xAxisTicks.push(sDate.valueOf());
                            sDate.add(increment, "days");
                        } else {
                            xAxisTicks.push(maxX);
                            break;
                        }
                    }
                    $scope.xAxisTickValues = function () { 
                        return xAxisTicks;
                    }
                } )();

            }

            $scope.draw();


            //Function used by nvd3 charts to turn timestamps into dates
            $scope.xAxisTickFormatFunction = function () {
                return function (d) {
                    // return d3.time.format('%x')(new Date(d));
                    //remove year from display to accomodate tick labels properly
                    //to prevent nvd3 from removing overlapping first and last tick labels and lines
                    return d3.time.format("%Y-%m-%d")(new Date(d));
                }
            };

            $scope.averageFunction = function () {
                return function (d) {
                    return d.average;
                }
            };

            $scope.ok = function () {
                // $scope.alertData = null;
                $modalInstance.close();
            };

            $scope.cancel = function () {
                // $scope.alertData = null;
                $modalInstance.dismiss('cancel');
            };

            $scope.download = function () {
                var svg = $('<svg />');
                var topPadding = 0;
                var transRegex = /.*translate\((\d+)([, ]+)(\d+)\).*/i;
                $("#alert-modal-charts svg").each(function () {
                    var clone = $(this).clone();
                    //find the translate in transform attribute and change the translateY value
                    var transform = clone.find('.nv-lineChart').attr('transform');
                    if (undefined !== transform) {
                        var arr = transRegex.exec(transform);
                        if (null !== arr) {
                            var newTransform = transform.replace('translate(' + arr[1] + arr[2] + arr[3] + ')', 'translate(' + arr[1] + ',' + (parseInt(arr[3], 10) + topPadding) + ')');
                            clone.find('.nv-lineChart').attr('transform', newTransform);
                        }
                    }
                    clone.children().appendTo(svg);

                    // html += clone.html();
                    topPadding += $(this).height();
                });
                
                var dv = $('<div />');
                //Apply styles to the svg
                svg
                    .attr("version", 1.1)
                    .attr("xmlns", "http://www.w3.org/2000/svg")
                    .attr("font-family", "Arial")
                    .attr("font-size", 12)
                    .appendTo(dv);

                $('.nvd3 .nv-groups path.nv-line', dv).css({
                        strokeWidth: 1.5,
                        fill: 'none',
                    });
                $('.nvd3.nv-line .nvd3.nv-scatter g.nv-point-paths', dv).remove();
                $('.nvd3.nv-line .nvd3.nv-scatter .kfla-nv-series-show-points .nv-point', dv).css({
                    fillOpacity: 1,
                    strokeOpacity: 1,
                    strokeWidth: 5
                });
                $('.nvd3.nv-line .nvd3.nv-scatter .nv-point.alert-highlight-point', dv).css({
                    fillOpacity: 1,
                    strokeOpacity: 1,
                    strokeWidth: 9,
                    fill: '#0000ff',
                    stroke: '#0000ff'
                });
                $('.nvd3.nv-line .alert-end-point', dv).css({
                    fillOpacity: 1,
                    strokeOpacity: 1,
                    strokeWidth: 9,
                    fill: '#ff0000',
                    stroke: '#ff0000'
                });
                $('.nvd3.nv-line .nvd3.nv-scatter path.nv-point:not(.alert-end-point, .kfla-nv-point-show)', dv).remove();
                //
                //grid
                $('.nvd3 .nv-axis line', dv).css({
                        fill: 'none',
                        stroke: '#e5e5e5',
                        shapeRendering: 'crispEdges'
                    });
                

                $('.nvd3 .nv-axis .nv-axisMaxMin text', dv).css({
                    fontWeight: 'bold'
                });

                //black line start of x axis
                $('.nvd3 .nv-axis path', dv).css({
                    fill: 'none',
                    stroke: '#000',
                    strokeOpacity: 0.75,
                    shapeRendering: 'crispEdges',
                });

                var $chart = $('#alert-modal-charts');
                var width = $chart.width();
                //height of chart less height of context
                var height = $chart.height();
                utils.downloadSvg(dv.html(), 'alert-chart', width, height);
            };
        }
    ]);