'use strict';

angular.module('acesApp')
    .controller('AlertsphusphuCtrl', ['$rootScope', '$scope', '$state', '$stateParams', 'AlertsByPhu', function ($rootScope, $scope, $state, $stateParams, AlertsByPhu) {


        //validate phuid in the url
        var found = false;
        var firstPhu = null;
        for(var i in $scope.phus) {
            var phu = $scope.phus[i];
            if (firstPhu == null) {
                firstPhu = phu;
            }
            if (phu.id == $state.params.phuId) {
                $rootScope.phu = phu;
                found = true;
                break;
            }
        };
        // debugger;
        //redirect to default phu
        //invalid phu in url
        if (!found) {
            var phuId = $rootScope.phu.id;
            if (phuId == $stateParams.phuId) {
                phuId = firstPhu.id;
            }
            //invalid phu, redirect to default phu
            $state.go('app.alerts.phus.phu', {phuId: phuId});
        } else {
            if (!$state.is('app.alerts.phus.phu.hospital')) {

                $scope.setTitle($rootScope.phu.phu);

                $scope.setPhu($rootScope.phu);
                $scope.query();
            }
        }

    }]);

