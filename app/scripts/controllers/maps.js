'use strict';

angular.module('acesApp')
    .controller('mapsCtrl', [
        '$scope',
        '$http',
        '$rootScope',
        '$q',
        'hospitals',
        'classifiers',
        'classifications',
        'acesGeo',
        'MapQuery',
        'growl',
        function ($scope, $http, $rootScope, $q, hospitals, classifiers, classifications, acesGeo, MapQuery, growl) {
            //On logout, clear the saved search settings
            $scope.$on("logout", function(){
                sessionStorage.removeItem('acesMapSettings');
            });


            var saveParams = function(){
                //Save to local storage
                sessionStorage.setItem('acesMapSettings', JSON.stringify($scope.currentSearchParams));
            };
            var getSavedParams = function(){
                return JSON.parse(sessionStorage.getItem('acesMapSettings'));
            };

            var momentDateFormat = 'YYYY-MM-DD';
            //Default Values
            var defaultParams = {
                data: {
                    search: false,
                    mapStyle: 'choropleth',
                    classifier: 'ME',
                    classification: 'S2014',
                    syndrome: 'AST',
                    levelOfGeography: 'FSA',
                    dataClassification: 'equal_int',
                    noOfClasification: 4,
                    percentageRangeLow: 0,
                    percentageRangeHigh: 10,
                    dateRangeLow: moment().subtract(6, 'days').format(momentDateFormat), //one week
                    dateRangeHigh: moment().format(momentDateFormat), //Today
                    gender: 'All',
                    ageGroup: 'All',
                    ageRangeLow: 0,
                    ageRangeHigh: 130,
                    standDevCompareToOntario: false,
                    errors: {}
                },
                selectableLayers: {
                    daycares: 0
                },
                maps: {
                    baseMap: 'topo',
                    borderColor: '#000000',
                    layerColor: '#ffffff',
                    legend: true
                },
                visible: true
            };

            //This will hold temporary parameters
            $scope.params = {};
            //This will hold the parameters used for search
            $scope.currentSearchParams = {};

            //overwrite the default params with the saved if found
            //angular.extend doesn't support nested objects
            jQuery.extend(true, $scope.params, defaultParams, getSavedParams());
            jQuery.extend(true, $scope.currentSearchParams, defaultParams, getSavedParams());

            // console.log($scope.params);

            //Map styles
            $scope.mapStyles = [
                {id: 'choropleth', name: 'Choropleth'},
                {id: 'prop_symbols', name: 'Proportional Symbols'}
            ];


            //Load classifiers
            classifiers.getData().then(function(factory){
                $scope.classifiers = factory.list;
                // console.log(factory);
            }, function(factory) {
                $scope.classifiers = [];
            });

            //Load classifications
            classifications.getData().then(function(factory){
                $scope.classifications = factory.list;
                // console.log(factory);
                loadSyndromes();
            }, function(factory) {
                $scope.classifications = [];
            });

            var loadSyndromes = function(){
                //$scope.syndromes list depends on clasification selected
                $scope.syndromes = []; //clear the current list if any
                //set selected classification
               for (var i in $scope.classifications){
                    var classification = $scope.classifications[i];
                    if (classification.id == $scope.params.data.classification) {
                        $scope.syndromes = classification.classes;

                        var selectedIndex = 0;
                        var foundSelected = false;
                        for (var j in classification.classes) {
                            if (classification.classes[j].id == $scope.params.data.syndrome) {
                                selectedIndex = j;
                                foundSelected = true;
                                break;
                            }
                        }
                        //Set the first object which is not 'All' as selected
                        if (!foundSelected && classification.classes[selectedIndex].id == 'All') {
                            selectedIndex = 1;
                        }
                        $scope.params.data.syndrome = classification.classes[selectedIndex].id;
                        break;
                    }
                }
            };
            //Filter used to exclude 'All'
            $scope.filterSyndrome = function(s){
                if (s.id == 'All') {
                    return false;
                }
                return true;
            };


            $scope.$watch('params.data.classification', function(){
                // console.log(arguments);
                loadSyndromes();
            });

            //Load Levels of Geography
            $scope.levelsOfGeography = acesGeo.getLevelsOfGeography();
            //Load Data Classifications
            $scope.dataClassifications = [
                {id: 'equal_int', title: 'Equal Interval'},
                {id: 'quantile', title: 'Quantile'},
                {id: 'std_dev', title: 'Standard Deviation'}
            ];

            //Load No. of Classifications
            $scope.noOfClasifications = [4,5,6,10];

            //Convert to date type for UI Bootstrap datepicker
            $scope.params.data.dateRangeLow = moment($scope.params.data.dateRangeLow, momentDateFormat).toDate();
            $scope.params.data.dateRangeHigh = moment($scope.params.data.dateRangeHigh, momentDateFormat).toDate();

            //Date Range datepicker settings
            $scope.params.openDateRangeLowPopup = function($event) {
                $event.preventDefault();
                $event.stopPropagation();

                $scope.params.dateRangeLowOpened = true;
                $scope.params.dateRangeHighOpened = false;
            };

            $scope.params.openDateRangeHigPopup = function($event) {
                $event.preventDefault();
                $event.stopPropagation();

                $scope.params.dateRangeHighOpened = true;
                $scope.params.dateRangeLowOpened = false;
            };

            $scope.params.dateOptions = {
                formatDay: {
                    formatDay: 'dd',
                    formatMonth: 'MM',
                    formatYear: 'yyyy',
                    startingDay: 0,
                    minMode: 'day',
                    showWeeks:'false'
                },
                minDate: moment("01-01-2006", "DD-MM-YYYY").toDate(),
                maxDate: moment().endOf('day').toDate()
            }

            //Load Gender
            $scope.genders = [
                {id: 'All', title: 'All'},
                {id: 'M', title: 'Male'},
                {id: 'F', title: 'Female'}
            ];
            //Load Age Groups
            $scope.ageGroups = [
                {id: 'All', title: 'All', min: 0, max: 130},
                {id: 'Child', title: 'Child', min: 0, max: 17},
                {id: 'School Child', title: 'School Child', min: 5, max: 17},
                {id: 'Adult', title: 'Adult', min: 18, max: 64},
                {id: 'Senior', title: 'Senior', min: 65, max: 130},
                {id: 'Adult + Senior', title: 'Adult + Senior', min: 18, max: 130}
            ];

            //Load Selectable Layers
            $scope.selectableLayers = acesGeo.getSelectableLayers();


            //Load Base Maps
            $scope.baseMaps =acesGeo.getBaseMaps();


            $scope.search = function(){
                //Validate
                var errors = {};
                //var numRegex = /^\d+\.*\d+$/; //number with optional decimal
                var numRegex = /^\d+$/; //number only
                //http://www.regular-expressions.info/floatingpoint.html
                var floatRegex = /^[0-9]*\.?[0-9]+$/;
                //Validate percentages only when map styple selected is prop_symbols or choropleth and data classification is equal_int
                if ($scope.params.data.mapStyle == 'prop_symbols' || ($scope.params.data.mapStyle == 'choropleth' && $scope.params.data.dataClassification == 'equal_int')) {
                    var percentageRangeLow = $scope.params.data.percentageRangeLow;
                    var percentageRangeHigh = $scope.params.data.percentageRangeHigh;
                    //Must be a valid number
                    if (!floatRegex.test(percentageRangeLow)) {
                        errors.percentageRangeLow = 'Min % must be a valid number';
                    }
                    if (!floatRegex.test(percentageRangeHigh)) {
                        errors.percentageRangeHigh = 'Max % must be a valid number';
                    }

                    if (typeof errors.percentageRangeHigh == 'undefined' && typeof errors.percentageRangeLow == 'undefined') {
                        percentageRangeLow = parseFloat(percentageRangeLow);
                        percentageRangeHigh = parseFloat(percentageRangeHigh);

                        if (percentageRangeLow < 0) {
                            errors.percentageRangeLow = 'Min % must be >= 0';
                        } else if (percentageRangeLow > 100 ) {
                            errors.percentageRangeLow = 'Min % must be <= 100';
                        } else if (percentageRangeLow > percentageRangeHigh ) {
                            errors.percentageRangeLow = 'Min % must be <= Max %';
                        }
                        if (percentageRangeHigh < 0) {
                            errors.percentageRangeHigh = 'Max % must be >= 0';
                        } else if (percentageRangeHigh > 100 ) {
                            errors.percentageRangeHigh = 'Max % must be <= 100';
                        } else if (percentageRangeHigh < percentageRangeLow ) {
                            errors.percentageRangeHigh = 'Max % must be >= Min %';
                        }
                    }
                }
                //Validate age
                var ageRegex = /^(-1|[0-9]|([1-9][0-9])|([1-9][0-9][0-9]))$/;
                var ageRangeLow = $scope.params.data.ageRangeLow;
                var ageRangeHigh = $scope.params.data.ageRangeHigh;

                //Must be a valid number
                if (!ageRegex.test(ageRangeLow)) {
                    errors.ageRangeLow = "'Age From' must be a valid number";
                }
                if (!ageRegex.test(ageRangeHigh)) {
                    errors.ageRangeHigh = "'Age To' must be a valid number";
                }

                if (typeof errors.ageRangeHigh == 'undefined' && typeof errors.ageRangeLow == 'undefined') {
                    ageRangeLow = parseInt(ageRangeLow, 10);
                    ageRangeHigh = parseInt(ageRangeHigh, 10);

                    if (ageRangeLow < -1 || ageRangeLow > ageRangeHigh) {
                        errors.ageRangeLow = "'Age From' must be >= 0 and < 'Age To'";
                    }
                    if (ageRangeHigh < ageRangeLow || ageRangeHigh > 999 ) {
                        errors.ageRangeHigh =  "'Age To' must be > 'Age From' and <= 130";
                    }
                }
                //Validate dates
                var dateRangeLow = moment($scope.params.data.dateRangeLow);
                var dateRangeHigh = moment($scope.params.data.dateRangeHigh);

                if (dateRangeHigh < dateRangeLow) {
                    errors.dateRangeHigh =  "'Date From' cannot be greater than the 'Date To'";
                }

                $scope.params.data.errors = errors;
                //No errors
                if (angular.equals(errors, {})) {
                    //Set search to
                    $scope.params.data.search = true;
                    //Copy temporary parameters to currentSearch parameters
                    angular.copy($scope.params.data, $scope.currentSearchParams.data);
                    //Save
                    saveParams();
                    // loadLevelOfGeography();
                    query();
                    //load data and map
                    initSelectableLayers();
                }
            }
            $scope.clearData = function(){

                MapQuery.clearCache();
                // console.log($scope.params);
                angular.copy(defaultParams.data, $scope.currentSearchParams.data);
                angular.copy(defaultParams.data, $scope.params.data);
                //Set search to
                $scope.currentSearchParams.data.search = false;
                $scope.params.data.search = false;

                //Convert to date type for UI Bootstrap datepicker
                $scope.params.data.dateRangeLow = moment($scope.params.data.dateRangeLow).toDate();
                $scope.params.data.dateRangeHigh = moment($scope.params.data.dateRangeHigh).toDate();

                // console.log($scope.params);
                saveParams();
                query();
                initSelectableLayers();
            }
            $scope.clearSelectableLayers = function(){
                angular.copy(defaultParams.selectableLayers, $scope.currentSearchParams.selectableLayers);
                angular.copy(defaultParams.selectableLayers, $scope.params.selectableLayers);
                saveParams();
                initSelectableLayers();
            }
            $scope.clearMaps = function(){
                // console.log($scope.params);
                angular.copy(defaultParams.maps, $scope.currentSearchParams.maps);
                angular.copy(defaultParams.maps, $scope.params.maps);

                // console.log($scope.params);
                saveParams();
                loadLevelOfGeography();
            }

            $scope.saveMaps = function(){
                //Copy temporary parameters to currentSearch parameters
                angular.copy($scope.params.maps, $scope.currentSearchParams.maps);
                //Save
                saveParams();
                loadLevelOfGeography();
                // query();
            }
            //
            $scope.result = {
                data: [],
                updated: moment().valueOf(), //map will watch this variable
                ready: true, //Will be used to check if data is loaded or not, by default it is ready but show is false
                show: false
            };
            $scope.layers = {
                levelOfGeography: {
                    field: null, //levelOfGeography to which the data belongs
                    data: [],
                    updated: moment().valueOf(), //map will watch this variable
                    ready: true, //Will be used to check if data is loaded or not
                    type: 'polygonLayer'
                },
                selectableLayers: {}
            };
            angular.forEach($scope.selectableLayers, function(layer){
                $scope.layers.selectableLayers[layer.id] = {
                    data: [],
                    updated: moment().valueOf(), //map will watch this variable
                    ready: false, //Will be used to check if data is loaded or not
                    show: 0,
                    field: layer,
                };
            });

            var loadLevelOfGeography = function(){
                $scope.layers.levelOfGeography.ready = false;
                if ($scope.currentSearchParams.data.search) {
                    var selected = $scope.currentSearchParams.data.levelOfGeography;
                    var field = acesGeo.getLevelOfGeography(selected);
                    acesGeo
                        .getJSON(field.jsonFileName + ($scope.currentSearchParams.data.mapStyle == 'choropleth' ? 'info' : 'centroid'))
                        .then(function(data) {
                            $scope.layers.levelOfGeography.field = field;
                            $scope.layers.levelOfGeography.data = data;
                            $scope.layers.levelOfGeography.updated = moment().valueOf();
                            $scope.layers.levelOfGeography.ready = true;
                        }, function(data) {

                        });
                } else {
                    $scope.layers.levelOfGeography.updated = moment().valueOf();
                    $scope.layers.levelOfGeography.ready = true;
                }
            };

            var lastQueryPath = '';
            var query = function(){
                loadLevelOfGeography();

                if ($scope.currentSearchParams.data.search) {
                    $scope.result.ready = false;
                    var p = angular.copy($scope.currentSearchParams.data);
                    p.dateRangeLow = moment($scope.params.data.dateRangeLow).valueOf();
                    p.dateRangeHigh = moment($scope.params.data.dateRangeHigh).valueOf();
                    var performSecondStdDevQuery = (!p.standDevCompareToOntario && p.dataClassification === "std_dev");
                    if (performSecondStdDevQuery) {
                        var baseDateRangeHigh = moment($scope.params.data.dateRangeHigh);
                        var baseDateRangeLow = moment($scope.params.data.dateRangeLow);
                        var diff = baseDateRangeHigh.diff(baseDateRangeLow, "day");
                        var refDays = 0;
                        baseDateRangeHigh = moment(baseDateRangeLow.valueOf()).add(-1, "day");
                        switch (true) {
                            case (diff <= 7):
                                refDays = 7;
                            break;
                            case (diff <= 14):
                                refDays = 14;
                            break;
                            case (diff <= 28):
                                refDays = 28;
                            break;
                            default:
                                refDays = 60;
                            break;
                        }
                        baseDateRangeLow = moment(baseDateRangeHigh.valueOf()).add(((refDays - 1) * -1), "day");
                    }


                    var processes = 0;
                    var completedProcesses = 0;
                    var bData = [];
                    var complete = function () {
                        completedProcesses++;
                        if (completedProcesses >= processes) {
                            if (performSecondStdDevQuery) {
                                var type = p.levelOfGeography;
                                if (type === "CD") type = "CDID";
                                if (type === "CSD") type = "CSDID";
                                // console.log(type);
                                // console.log(completedProcesses + ' === ' + processes);
                                for (var i in $scope.result.data) {
                                    var o = $scope.result.data[i];
                                    o.ref = [];
                                    if(undefined !== o[type]) {
                                        for (var j in bData) {
                                            if (bData[j][type] == o[type]) {
                                                o.ref.push(bData[j]);
                                            }
                                        }
                                    }
                                    //if a particular day has no visits, endpoint wont return it
                                    //add replacement for missing days
                                    if (o.ref.length < refDays) {
                                        // console.log("DDD", o);
                                        for (var j = o.ref.length; j < refDays; j++) {
                                            o.ref.push({
                                                TotalVisits: 0,
                                                Visits: 0
                                            });
                                        }
                                    }
                                    o.days = diff + 1;
                                }
                            }

                            $scope.result.updated = moment().valueOf();
                            $scope.result.ready = true;
                            $scope.result.show = true;
                        }
                    };
                    processes++;
                    MapQuery
                        .query(p)
                        .then(function (data) {
                            //success
                            $scope.result.data = data;
                            complete();
                        },function(status){
                            //fail
                            $scope.clearData();
                            growl.error("<strong>ERROR:</strong> Unable to retrieve requested data");
                        });

                    if (performSecondStdDevQuery) {
                        processes++;
                        p.returnType = 2;
                        p.dateRangeLow = baseDateRangeLow.valueOf();
                        p.dateRangeHigh = baseDateRangeHigh.valueOf();
                        MapQuery
                            .query(p)
                            .then(function (data) {
                                bData = data;
                                complete();
                            });
                    }
                } else {
                    $scope.result.updated = moment().valueOf();
                    $scope.result.ready = true;
                    $scope.result.show = false;
                }

            };
            //
            query();


            $scope.selectableLayerChanged = function(layer){
                // console.log(arguments);
                // if (typeof $scope.params.selectableLayers[layer.id] == 'undefined') {
                // 	return;
                // }

                // console.log( $scope.params.selectableLayers[layer.id]);
                angular.copy($scope.params.selectableLayers, $scope.currentSearchParams.selectableLayers);
                saveParams();
                // $scope.currentSearchParams.selectableLayers[layer.id] = $scope.params.selectableLayers[layer.id];
                // debugger;
                if (typeof $scope.currentSearchParams.selectableLayers[layer.id] != 'undefined' && $scope.currentSearchParams.selectableLayers[layer.id] == '1') {
                    $scope.layers.selectableLayers[layer.id].ready = false
                    acesGeo
                        .getJSON(layer.id)
                        .then(function(data) {
                            // $scope.layers.selectableLayers[layer.id].field = field;
                            $scope.layers.selectableLayers[layer.id].data = data;
                            $scope.layers.selectableLayers[layer.id].updated = moment().valueOf();
                            $scope.layers.selectableLayers[layer.id].show = 1;
                            $scope.layers.selectableLayers[layer.id].ready = true;
                            // console.log("~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~``");
                            // console.log(data);
                        }, function(data) {

                        });
                } else {
                    $scope.layers.selectableLayers[layer.id].updated = moment().valueOf();
                    $scope.layers.selectableLayers[layer.id].show = 0;
                }
                // jQuery.extend(true, $scope.currentSearchParams.selectableLayers, $scope.params.selectableLayers);
            };
            $scope.ageGroupChanged = function(){
                // console.log(arguments);
                var selected = $scope.params.data.ageGroup;
                angular.forEach($scope.ageGroups, function(val){
                    if (val.id == selected) {
                        $scope.params.data.ageRangeLow = val.min;
                        $scope.params.data.ageRangeHigh = val.max;
                        return;
                    }
                });
            }

            var initSelectableLayers = function(){
                //init selected selectable layers
                angular.forEach($scope.selectableLayers, function(layer){
                    $scope.selectableLayerChanged(layer);
                });
            };
            initSelectableLayers();



            $scope.hideOptions = function(){
            	$scope.params.visible = false;
            	$scope.currentSearchParams.visible = false;
            	saveParams();
            }
            $scope.showOptions = function(){
            	$scope.params.visible = true;
            	$scope.currentSearchParams.visible = true;
            	saveParams();
            }

       }
    ]);
