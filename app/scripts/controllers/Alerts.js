'use strict';

angular.module('acesApp')
  .controller('AlertsCtrl', ['$rootScope', '$scope', '$state', '$timeout', 'Alerts', '$q', 'alertTypes', 'syndromesList',
    function ($rootScope, $scope, $state, $timeout, Alerts, $q, alertTypes, syndromesList) {
      // $scope.initiated = false;
      $scope.data = undefined;
      $scope.form = {};
      $scope.title = '';
      $scope.alertsControllerOptions = {data: $scope.data, options: {modalSelected: false}};

      $scope.form.dateOptions = {
        formatDay: {
          formatDay: 'dd',
          formatMonth: 'MM',
          formatYear: 'yyyy',
          startingDay: 0,
          minMode: 'day',
          showWeeks:'false'
        },
        minDate: moment("22-05-2012", "DD-MM-YYYY").toDate(),
        maxDate: moment().endOf('day').toDate()
      };

      $scope.form.openDateFromPopup = function($event) {
        $event.preventDefault();
        $event.stopPropagation();

        $scope.form.dateFromOpened = true;
        $scope.form.dateToOpened = false;
      };

      $scope.form.openDateToPopup = function($event) {
        $event.preventDefault();
        $event.stopPropagation();

        $scope.form.dateToOpened = true;
        $scope.form.dateFromOpened = false;
      };

      //Filter and sort alertTypes
      (function() {
        var removeAlertTypes = ['RLS', 'CD','Tendency', 'Oscillation', 'Bias', 'Ignorance', 'NMG', 'NRC'];
        var sortOrder = ['Extreme', 'OnEdge', 'Trend', 'CUSUM1', 'CUSUM2', 'CUSUM3'];
        $scope.alertTypes = _.map(alertTypes, function (o) {
          var newO = angular.extend({}, o);
          var position =  sortOrder.indexOf(o.AlertTypeID);
          if (position === -1) {
              //if not found, set position as end
              position = sortOrder.length;
          }
          newO.position = position;

          if (removeAlertTypes.indexOf(o.AlertTypeID) !== -1) {
              newO.active = false;
          } else {
              newO.active = true;
          }
          return newO;
        });

        //remove inactive
        $scope.alertTypes = _.filter($scope.alertTypes, function (o) { return o.active; });
      })();

      //Filter and sort Syndromes
      var loadSyndromes = function () {

          $scope.toggleSyndrome = function (syndrome) {
            if (!syndrome.selected) {
              syndrome.selected = true;
              //if optional
              if (syndrome.optional) {
                $scope.selectedOptionalSyndromes++;
              } else {
                $scope.selectedVisibleSyndromes++;
              }
            } else {
              syndrome.selected = false;
              //if optional
              if (syndrome.optional) {
                $scope.selectedOptionalSyndromes--;
              } else {
                $scope.selectedVisibleSyndromes--;
              }
            }
          };

          $scope.selectAllSyndromes = function (v, optional) {
            if (v) { //select all
              if (optional) {
                $scope.selectedOptionalSyndromes = $scope.optionalSyndromes.length;
                for (i in $scope.optionalSyndromes) {
                  $scope.optionalSyndromes[i].selected = true;
                }
              } else {
                $scope.selectedVisibleSyndromes = $scope.visibleSyndromes.length;
                for (i in $scope.visibleSyndromes) {
                  $scope.visibleSyndromes[i].selected = true;
                }
              }
            } else { //select none
              if (optional) {
                $scope.selectedOptionalSyndromes = 0;
                for (i in $scope.optionalSyndromes) {
                  $scope.optionalSyndromes[i].selected = false;
                }
              } else {
                $scope.selectedVisibleSyndromes = 0;
                for (i in $scope.visibleSyndromes) {
                  $scope.visibleSyndromes[i].selected = false;
                }
              }
            }
          };

          var syndromes = [];
          var i;
          $scope.allSyndromes = [];
          $scope.optionalSyndromes = [];
          $scope.visibleSyndromes = [];
          $scope.selectedOptionalSyndromes = 0;
          $scope.selectedVisibleSyndromes = 0;
	        var syndromesNeeded = ["AST", "BITE", "BRONCH", "CELL", "CO", "COPD", "CROUP", "DEHY", "DERM", "ENVIRO", "EOH", "GASTRO", "HEP", "ILI", "INF", "MEN", "MH", "MHS", "NEC", "OPI", "PN", "REPORT", "RESP", "SEP", "SI", "TICKS", "TOX", "TRMVC", "VOM"];
          for (i in syndromesList.list) {
            if ("S2014" === syndromesList.list[i].id) {
              $scope.allSyndromes = angular.copy(syndromesList.list[i].classes);
              //remove all
              $scope.allSyndromes = _.filter($scope.allSyndromes, function (o) {

		          var include = false;
		          if(o.id !== "All"){
					      for(var j = 0 ; j < syndromesNeeded.length; j++){
						      if(syndromesNeeded[j] == o.id){
							      include = true;
							      break;
						      }
					      }
				      }

				    return include;
			     });
           break;
          }
        }

        //Syndromes which should be visible in the form
        var defaultVisibleList = [ "RESP", "GASTRO", "ILI", "ENVIRO", "TOX", "MH", "AST", "EOH", "OPI", "DERM", "CROUP", "SEP", "CELL", "BRONCH", "COPD" ];
        var index;
        for(i in $scope.allSyndromes) {
          index = defaultVisibleList.indexOf($scope.allSyndromes[i].id);
          if (index === -1) {
            $scope.allSyndromes[i].position = defaultVisibleList.length;
            $scope.allSyndromes[i].optional = true;
            $scope.optionalSyndromes.push($scope.allSyndromes[i]);
          } else {
            $scope.allSyndromes[i].position = index;
            $scope.visibleSyndromes.push($scope.allSyndromes[i]);
          }

          if ($scope.params.syndromes.indexOf($scope.allSyndromes[i].id) === -1) {
            $scope.allSyndromes[i].selected = false;
          } else {
            $scope.allSyndromes[i].selected = true;
            if ($scope.allSyndromes[i].optional) {
              $scope.selectedOptionalSyndromes++;
            } else {
              $scope.selectedVisibleSyndromes++;
            }
          }
        }
      };

      $scope.setTitle = function (title) {
        $scope.title = title;
      }

      // $scope.phu = $rootScope.phu;
      $scope.setHospital = function (hospital) {
        $scope.params.hospital = hospital;

        if ($scope.params.hospital.id != 'All'){
          $scope.displayGeoType = false;
        } else {
          $scope.displayGeoType = true;
        }
      };

      $scope.setPhu = function (phu) {
        $scope.params.phu = phu;

        $scope.hospitals = phu.hospitals;
        //Change the selected hospital to the first if it doesn't exists in the hospitals list
        var found = false;
        for (var i in $scope.hospitals) {
          if ($scope.params.hospital.id == $scope.hospitals[i].id) {
            $scope.setHospital($scope.hospitals[i]);
            found = true;
            break;
          }
        }
        if (!found) {
          //select the first hospital
          $scope.setHospital($scope.hospitals[0]);
        }
      };

      $scope.toggleAlertType = function (AlertTypeID) {
        var index = $scope.params.alertTypes.indexOf(AlertTypeID);
        if (index === -1) {
            $scope.params.alertTypes.push(AlertTypeID);
        } else {
            $scope.params.alertTypes.splice(index, 1);
        }
      };

      //local params
      $scope.initFormParams  = function () {
        var p = Alerts.getParams();
        $scope.params = {
          phu: $rootScope.phu,
          hospital: p.Queries.hospital,
          dateFrom: new Date(p.Queries.datefrom),
          dateTo: new Date(p.Queries.dateto),
          alertTypes: p.Queries.alertTypes,
          syndromes: p.Queries.syndromes,
          geoType:p.Queries.geoType
        }

        loadSyndromes();

        $scope.setPhu($rootScope.phu);
      };
      $scope.initFormParams();

      $scope.geoTypes = [
        {id: 'phu', title:'PHU'},
        {id: 'hosp', title:'HOSP'},
        {id: 'all', title:'All'}
      ];

      $scope.setGeoType = function (geoType) {
        $scope.params.geoType = geoType;
      };

      var geoType = _.find($scope.geoTypes, function(o) { return o == $scope.params.geoType; });
      if (geoType === undefined) {
        geoType = $scope.geoTypes[0];
      };

      $scope.setGeoType(geoType);

      $scope.$watch('displayGeoType', function() {
        if ($scope.displayGeoType == true){
          $scope.setGeoType($scope.geoTypes[0]);
        } else {
          $scope.setGeoType($scope.geoTypes[2]);
        }
      });

      $scope.isActiveAlertType = function (AlertTypeID) {
        return $scope.params.alertTypes.indexOf(AlertTypeID) !== -1;
      };

      $scope.selectAllAlertTypes = function ($v) {
        if ($v) { //select all
          $scope.params.alertTypes = _.map($scope.alertTypes, function (o) { return o.AlertTypeID; });
        } else { //select none
          $scope.params.alertTypes = [];
        }
      };

      $scope.query = function (saveParams, performQuery, reload) {
        if (typeof saveParams === 'undefined') {
          saveParams = true;
        }
        if (typeof performQuery === 'undefined') {
          performQuery = true;
        }
        if (typeof reload === 'undefined') {
          reload = false;
        }

        if (saveParams) {
          Alerts.setParams({
            Params: {
              phuId: $scope.params.phu.id
            },
            Queries: {
              hospital: $scope.params.hospital,
              datefrom: moment($scope.params.dateFrom).valueOf(),
              dateto:  moment($scope.params.dateTo).endOf("day").valueOf(),
              alertTypes: $scope.params.alertTypes,
              syndromes: $scope.params.syndromes,
              geoType:$scope.params.geoType.id
            }
          });
        }

        if (performQuery) {
          $scope.data = undefined;
          Alerts.getVisits(reload).then( function (data) {
            $scope.alertsControllerOptions.data = data;
          }, function () {
              //failed
          });
        }
      };

      $scope.submit = function () {
        $scope.errors = {};

        //validate
        var inValidDate = false;
        if ($scope.params.dateFrom == undefined) {
          inValidDate = true;
          $scope.errors.dateFrom = {
            invalidDate: true
          };
        };

        if ($scope.params.dateTo == undefined) {
          inValidDate = true;
          $scope.errors.dateTo = {
            invalidDate: true
          };
        };

        if (!inValidDate) {
          var dateFrom = moment($scope.params.dateFrom);
          var dateTo = moment($scope.params.dateTo);
          var now = moment();
          if (dateFrom > now) {
            $scope.errors.dateFrom = {
              futureDate: true
            };
          } else {
            if (dateFrom > dateTo) {
              $scope.errors.dateFrom = {
                mustBeSmallerThanDateTo: true
              };
            }
          }
          if (dateTo > now.endOf("day")) {
            $scope.errors.dateTo = {
              futureDate: true
            };
          }
        }

        if ($scope.params.alertTypes.length <= 0) {
          $scope.errors.alertTypes = {
            noSelection: true
          };
        }

        var i;
        $scope.params.syndromes = [];
        for(i in $scope.allSyndromes) {
          if ($scope.allSyndromes[i].selected) {
            $scope.params.syndromes.push($scope.allSyndromes[i].id);
          }
        }

        //no errors
        if (angular.equals($scope.errors, {})) {
          $rootScope.phu = $scope.params.phu;

          var currentHospital = $state.params.hospitalId;
          if (currentHospital === undefined) currentHospital = 'All';

          if ($scope.params.hospital.id !== currentHospital || $scope.params.phu.id !== $state.params.phuId) {
            //just save params. do not perform query
            $scope.query(true, false);
            if ($scope.params.hospital.id == 'All') {
              $state.go('app.alerts.phus.phu', {phuId: $rootScope.phu.id}, {reload: true});
            } else {
              $state.go('app.alerts.phus.phu.hospital', {phuId: $rootScope.phu.id, hospitalId: $scope.params.hospital.id});
            }
          } else {
            //save the params and perform query
            //last parameter reload result from server
            $scope.query(true, true, true);
          }
        }
      }

      //if not hospital url but has hospital in param set
      if (!$state.is('app.alerts.phus.phu.hospital') && $scope.params.hospital.id != 'All') {
        $state.go('app.alerts.phus.phu.hospital', {phuId: $rootScope.phu.id, hospitalId: $scope.params.hospital.id});
      } else if ($state.is('app.alerts.phus')) {
        $state.go('app.alerts.phus.phu', {phuId: $scope.phu.id});
      }
    }
  ])
;
