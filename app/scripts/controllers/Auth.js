'use strict';

angular.module('acesApp')
    .controller('LoginCtrl',
        ['$rootScope', '$scope', '$modal', 'Auth', 'alertService', 'SystemStatus', '$state', '$stateParams', function ($rootScope, $scope, $modal, Auth, alertService, SystemStatus, $state, $stateParams) {
            $rootScope.requestTokens = [];
            $scope.currentyear = new Date().getFullYear();
            $scope.auth = Auth;

            $scope.SystemStatus = SystemStatus;

            //Keep this blank to use relative path 
            // or set the value for e.g. '//km-prod:8000' to test prod endpoint 
            $rootScope.endpoint = ACES.endpoint;
            $rootScope.ACESVersion = ACES.version;

            $scope.$state = $state;
            $scope.$stateParams = $stateParams;
			
            $rootScope.closeAlert = alertService.closeAlert;

            $scope.login = function () {
                if ($scope.login.userid && $scope.login.pswd) {
                    alertService.add('info', 'Logging in...');
                    Auth.login({
                        userid: $scope.login.userid,
                        pswd: $scope.login.pswd
                    });
                }
                else {
                    alertService.add('warning', 'Please enter your username and password');
                }
            };
            $scope.logout = function () {
                Auth.logout();
            };

            $scope.openContactUs = function () {
                var modalInstance = $modal.open({
                    templateUrl: 'views/help/open-ticket.html?v=' + encodeURIComponent(ACES.buildnumber),
                    controller: 'OpenTicketCtrl',
                    size: 'lg'
                });
            };

        }]);
