'use strict';

angular.module('acesApp')
  .controller('LinelistingsCtrl', ['$scope', '$state', '$stateParams', 'hospitals', 'hospitalLoad', 'lineListings', 'epicurves', '$injector', 'fsas', 'alertService', 'SystemStatus', function
    ($scope, $state, $stateParams, hospitals, hospitalLoad, lineListings, epicurves, $injector, fsas, alertService, SystemStatus) {

      //if invalid type, redirect to home
      if ($stateParams.type !== "ED" && $stateParams.type !== "AD") {
        $state.go('app.home');
        return;
      }

      console.log("$stateParams", $stateParams);
      var $rootScope = $injector.get('$rootScope');
      //clear exisiting list if any
      lineListings.Visits.all = undefined;
      $scope.lineListings = lineListings;
      $scope.lineListingsByPhu = lineListings.Visits;
      $scope.title = "";//will be set later
      //hide the 'AdmissionType', 'EMSArrival', 'PatientLHIN' columns by default
      $scope.hiddenColumns = ['EMSArrival', 'PatientLHIN','PatientPHU','AdmissionType'];
      //remove CTA column for AD
      $scope.disabledColumns = ($stateParams.type === "AD" ? ['CTAS'] : []);

      //this data will be passed to lineListings form directive
      var initFormParams = function () {
        $scope.formParams = {
          phu: lineListings.Queries.phu,
          hospital: lineListings.Queries.hospital,
          dateFrom: new Date(lineListings.Queries.datefrom).toDateString(),
          dateTo: new Date(lineListings.Queries.dateto).toDateString(),
          fromAge: lineListings.Queries.agefrom,
          toAge: lineListings.Queries.ageto,
          gender: lineListings.Queries.gender,
          classification: lineListings.Queries.classification,
          bucket: lineListings.Queries.bucket,
          syndromes: lineListings.Queries.class,
          classifier: lineListings.Queries.classifier,
          cta: lineListings.Queries.ctas,

          patientLocality: lineListings.getPatientLocality(lineListings.Params.patient),
          hospitalLocality: lineListings.getHospitalLocality(lineListings.Params.locality),
          fsa: {title: lineListings.Queries.filterByFSA},

          admissionType: $stateParams.type,
          fltrAmType: lineListings.Params.fltrAmType
        };

        if ($stateParams.type == 'AD') {
          $scope.formParams.syndromes = lineListings.Queries.classAd;
        } else if ($stateParams.type == 'ED') {
          $scope.formParams.syndromes = lineListings.Queries.classEd;
        };
      };

      var saveParams = function (p, copyToEpicurves) {
        //keep the reference to original obj as it is used in the queries
        _.extend(lineListings.Queries, p.Queries);
        _.extend(lineListings.Params, p.Params);

        lineListings.Params['type'] = $stateParams.type;
        lineListings.saveParams();
      };

      //check the $scope.formParams for exact mappings
      $scope.submit = function (params, copyToEpicurves) {
        //validate
        //limit date range to one week
        var dateTo = moment(params.dateTo).startOf("day");
        var maxDate = moment(params.dateFrom).startOf("day").add(6, "days");
        if (dateTo.valueOf() > maxDate.valueOf()) {
          alertService.add("danger", "Date range cannot be more than 1 week.");
          return false;
        }
        var p = {
          Queries: {},
          Params: {}
        };
        //Convert to simple dates (no locality or timestamps) for endpoint
        var formattedDateFrom = moment(params.dateFrom).startOf('day').format('ll');
        var formattedDateTo = moment(params.dateTo).startOf('day').format('ll');

        p.Params.phuId = params.phu.id;
        p.Queries.hospital = params.hospital;
        p.Queries.datefrom = moment(formattedDateFrom).valueOf();
        p.Queries.dateto = moment(formattedDateTo).valueOf();
        p.Queries.agefrom = params.fromAge;
        p.Queries.ageto = params.toAge;
        p.Queries.gender = params.gender;
        p.Queries.classification = params.classification;
        p.Queries.bucket = params.bucket;
        p.Queries.class = params.syndromes;
        if ($stateParams.type == 'AD') {
          p.Queries.classAd = params.syndromes;
        } else if ($stateParams.type == 'ED') {
          p.Queries.classEd = params.syndromes;
        }
        p.Queries.classifier = params.classifier;
        p.Queries.ctas = params.cta;
        p.Params.patient = params.patientLocality.id;
        p.Params.locality = params.hospitalLocality.id;
        p.Queries.filterByFSA = params.fsa.title;
        p.Params.fltrAmType = params.fltrAmType;

        p.Params.admissionType = $stateParams.type;
        console.log("PARAMS", params);

        //set title
        if (params.hospital.id !== "All") {
          $scope.title = params.hospital.title + " (" + params.hospital.phu + ")";
        } else {
          $scope.title = params.phu.phu;
        }


        var range = p.Queries.dateto - p.Queries.datefrom;
        if (range === 0) {
          p.Queries.pagesize = 1;
        } else {
          p.Queries.pagesize = Math.round(range / 60 / 60 / 24 / 1000) + 1;
        }

        saveParams(p, copyToEpicurves);

        $scope.data = [];
        $scope.lineListingsControllerOptions = {data: $scope.data, options: p};
        $scope.lineListingsControllerOptions.options.modalSelected = false;
        lineListings.Visits.getData(p).then( function (data) {
          if (p.Params.HospitalID !== undefined && p.Params.HospitalID != 'All') {
            $scope.lineListingsControllerOptions.data = _.where(data, {HospitalID: p.Params.HospitalID});
            $scope.data = _.where(data, {HospitalID: p.Params.HospitalID});
          } else {
            $scope.lineListingsControllerOptions.data = data;
            $scope.data = data;
          }

          //Process the cvs data in a formatted array
          $scope.csvFilename = ("ACES_linelistings_" + params.phu.id + "_" + moment(params.dateFrom).format('YYYYMMDD') + "-" + moment(params.dateTo).format('YYYYMMDD') + ".csv").toLowerCase();
          $scope.rawCSVData = lineListings.Visits.all;
        });
      };

      //Get object of unchecked filter options
      $scope.returnCSVOptions = function(){
        //Find all unchecked items on page
        var uncheckedOptions = $(".ngColListCheckbox.ng-empty").map(function(){
          return {
            option:$(this).closest('label').text()
          };
        }).get();
        //Return unchecked options as object
        return uncheckedOptions;
      }

      //Process CSV Data
      $scope.processCSVData = function (allVisits){
        //Create csv data array
        var csvData = [];

        //Iterate through all raw visit data
        for (var key in allVisits){
          //Skip loop if the property is from prototype
          if (!allVisits.hasOwnProperty(key)) continue;

          var obj = allVisits[key];

          //Create template visit object
          propVisit = {propDate:"", propTime:"", propAge:"", propSex:"", propSyndrome:"", propComplaint:"", propHospital:"",
            propFSALDU:"", propCTAS:"", propAdmissionType:"", propEMSArrival:"", propPatientLHIN:"",propPatientPHU:""};

          //Iterate through a visits property data
          for (var prop in obj) {
            //Skip loop if the property is from prototype
            if(!obj.hasOwnProperty(prop)) continue;

            //Process each property type and either remove it if unchecked or assign it data from property
            switch (prop){
              case "VisitDate": propVisit.propDate = obj[prop]; break;
              case "VisitTime": propVisit.propTime = obj[prop]; break;
              case "Age": propVisit.propAge = obj[prop]; break;
              case "Gender": propVisit.propSex = obj[prop]; break;
              case "ClassName": propVisit.propSyndrome = obj[prop]; break;
              case "ChiefComplaint": propVisit.propComplaint = obj[prop]; break;
              case "HospitalID": propVisit.propHospital = obj[prop]; break;
              case "FSALDU": propVisit.propFSALDU = obj[prop]; break;
              case "CTAS": propVisit.propCTAS = obj[prop]; break;
              case "AdmissionType": propVisit.propAdmissionType = obj[prop]; break;
              case "EMSArrival": propVisit.propEMSArrival = obj[prop]; break;
              case "PatientLHIN": propVisit.propPatientLHIN = obj[prop]; break;
              case "PatientPHU": propVisit.propPatientPHU = obj[prop]; break;
            };
          };

          //Has to be a seperate check since not all visit contain all fields
          //Get unchecked options object
          var uncheckedOptions = $scope.returnCSVOptions();
          for (var key in uncheckedOptions){
            //Process each item in the unchecked option object for removal from the visit data
            switch (uncheckedOptions[key].option){
              case "Date": delete propVisit.propDate; break;
              case "Time": delete propVisit.propTime; break;
              case "Age": delete propVisit.propAge; break;
              case "Sex": delete propVisit.propSex; break;
              case "Syndrome": delete propVisit.propSyndrome; break;
              case "Complaint": delete propVisit.propComplaint; break;
              case "Hospital": delete propVisit.propHospital; break;
              case "FSALDU": delete propVisit.propFSALDU; break;
              case "CTAS": delete propVisit.propCTAS; break;
              case "Admission Type": delete propVisit.propAdmissionType; break;
              case "EMS Arrival": delete propVisit.propEMSArrival; break;
              case "Patient LHIN": delete propVisit.propPatientLHIN; break;
              case "Patient PHU": delete propVisit.propPatientPHU; break;
            };
          }

          //Remove CTAS from return data if on AD tab
          if ($stateParams.type == "AD") {
            delete propVisit.propCTAS;
          }

          //Building the data array with formatted visit objects
          csvData.push(propVisit);
        };
        //Return visit data as an formatted array of objects
        return csvData;
      }

      //Create CSV header from object properties
      $scope.returnCSVHeader = function(){
        var csvHeader = ["Date", "Time", "Age", "Sex", "Syndrome", "Complaint", "Hospital",
          "FSALDU", "CTAS", "Admission Type", "EMS Arrival", "Patient LHIN","Patient PHU"];
        //remove titles that have been unchecked in options
        var uncheckedOptions = $scope.returnCSVOptions();
        for (var key in uncheckedOptions){
          csvHeader.splice(csvHeader.indexOf(uncheckedOptions[key].option),1);
        }

        //Remove CTAS from return header if on AD tab
        if ($stateParams.type == "AD") {
          csvHeader.splice(csvHeader.indexOf("CTAS"),1);
        }

        //Return header title data for csv as an array
        return csvHeader;
      }

      //Return data for CSV as an array of objects
      $scope.returnCSVData = function(){
        return $scope.processCSVData($scope.rawCSVData);;
      }

      $scope.reset = function (button) {
        var a = lineListings.getDefault();
        //if first reset button
        if (button == 1) {
          //Do not reset Params
          a.Params = {};
          //Do not reset filterByFSA from Queries
          delete a.Queries.filterByFSA;
        } else if (button == 2) {
          //Only reset filterByFSA from Queries
          a.Queries = {
            filterByFSA: a.Queries.filterByFSA
          };
        }
        saveParams(a);
        initFormParams();
        lineListings.Visits.getData();
      };

      (function () {
        lineListings.initParams($stateParams.type);
        var found = false;
        var currentPhu = lineListings.Params.phuId;
        angular.forEach(hospitalLoad.phus, function (phu) {
          if (!found && currentPhu != undefined && phu.id == currentPhu) {
            lineListings.Queries.phu = phu;
            found = true;
          }
        });

        if (!found) {
          lineListings.Queries.phu = hospitalLoad.phus[0];
          lineListings.Params.phuId = hospitalLoad.phus[0].id;
        }

        initFormParams();
        $scope.submit($scope.formParams, false);
      })();

  }]);
