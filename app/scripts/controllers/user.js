'use strict';

angular.module('acesApp')
    .controller('UserCtrl', ['$scope', '$timeout', '$window', '$state', '$stateParams', '$rootScope', 'userServices', '$q', '$http', 'Auth', 'classifications',
        function ($scope, $timeout, $window, $state, $stateParams, $rootScope, userServices, $q, $http, Auth, classifications) {
            $scope.graphData = [];
			$scope.data = { data: {} }; 
            var defaultSyndromes = {
                'ed': ['ALL', 'RESP', 'GI', 'ENVIRO', 'ILI', 'AST'],
                'ad': ['ALL', 'PN', 'GASTRO', 'ILI', 'SEP', 'MH']
            };
            //init 
            var landingPageSettings = Auth.getSetting('landingPageSettings');
            if (undefined === landingPageSettings) {
                landingPageSettings = {
                    syndromes: defaultSyndromes
                };
            }
            if (undefined === landingPageSettings.syndromes) {
                landingPageSettings.syndromes = defaultSyndromes;
            }

            var getVisitTypeCharts = function (visitType) {
                var charts = [];
                for(var i in landingPageSettings.syndromes[visitType]) {
                    charts.push({key1: landingPageSettings.syndromes[visitType][i], title: '', editing: false, lines: []});
                }
                return charts;
            };

            //add ed
            $scope.graphData .push( {
                visitType: 'ed',
                title: 'Current Emergency Department Visit Counts by Syndrome',
                loaded: false,
                charts: getVisitTypeCharts( 'ed' ),
                // [
                //     {key1: 'ALL', title: '', editing: false, lines: []},
                //     {key1: 'RESP', title: '', editing: false, lines: []},
                //     {key1: 'GI', title: '', editing: false, lines: []},
                //     {key1: 'ENVIRO', title: '', editing: false, lines: []},
                //     {key1: 'ILI', title: '', editing: false, lines: []},
                //     {key1: 'AST', title: '', editing: false, lines: []}
                // ],
                data: []
            } );
            //add ad
            $scope.graphData .push( {
                visitType: 'ad',
                title: 'Current Admission Counts by Syndrome',
                loaded: false,
                charts: getVisitTypeCharts( 'ad' ),
                data: []
            } );

            $scope.onSyndromeChange = function (index, visitTypeObj) {
                var chart = visitTypeObj.charts[index];
                chart.key1 = chart.syndrome.id.toUpperCase();
                buildSyndromeChart(chart, visitTypeObj);
                //save with settings
                landingPageSettings.syndromes[visitTypeObj.visitType] = _.map(visitTypeObj.charts, function (chart) { return chart.key1; });
                console.log(landingPageSettings.syndromes);
                Auth.setSetting('landingPageSettings', landingPageSettings);
            };

            $scope.onSyndromeClick = function (visitType, index) {
                // timeout makes sure that it is invoked after any other event has been triggered.
                $timeout(function() {
                    //set the focus to the select element
                    var selectId = 'select-' + visitType + '-syndrome-' + index;
                    var element = $window.document.getElementById(selectId);
                    if(element)
                        element.focus();
                });
            };

            var loadSyndromesCounts = function ( visitTypeObj, done ) {
                userServices.getSyndromes( visitTypeObj.visitType ).then(function (response) {
                    var syndromesCounts = {};
                    // Cycle through the data returned by endpoint, add to array via switch
                    angular.forEach(response.data, function (item) {
                        // Turn date value into timestamp, which is needed by NVD3 for mapping dates
                        var visitDate = moment(item.AdmissionDate, "YYYY-MM-DD").valueOf();
                        if (undefined === syndromesCounts[item.Class]) {
                            syndromesCounts[item.Class] = {};
                        }
                        syndromesCounts[item.Class][visitDate] = item.Total;
                    });
                    visitTypeObj.syndromesCounts = syndromesCounts;
                    //
                    done ( );
                });
            };

            var buildLines = function (syndromID, syndromesCounts) {
                var lines = [];
                lines.push({key: syndromID, values: []});
                //populate first line, also add missing dates
                var totalDays = 67; //Must match with [spGetACESProvinceVisits]
                var dt = moment().startOf('day').subtract(totalDays, "days");
                var ts;
                for (var i = 0; i <= totalDays; i++) {
                    ts = dt.valueOf();
                    if (undefined !== syndromesCounts[syndromID] && undefined !== syndromesCounts[syndromID][ts]) {
                        lines[0].values.push([ts, syndromesCounts[syndromID][ts]]);
                    } else {
                        lines[0].values.push([ts, 0]);
                    }
                    dt.add(1, "day");
                }
                var generateMovingAverage = function (days, data) {
                    var movingSum = 0;
                    var avg = 0;
                    var mAvg = {
                        "key": days + " Day moving average",
                        "values": [],
                        "dash": "2,4",
                        "style": "dashed",
                        "color": "#E01B5D"
                    };
                    for (var i = 0; i < days; i++) {
                        movingSum += data.values[i][1];
                    }
                    avg = movingSum / days;

                    for (var i = days; i < data.values.length; i++) {
                        movingSum = movingSum - data.values[i - days][1] + data.values[i][1];
                        avg = movingSum / days;
                        mAvg.values.push([
                            data.values[i][0],
                            Math.floor(avg)
                        ]);
                    }

                    //remove the first 7 values from data as we have them to  calculate the moving average
                    //we do not need to draw graph for those values
                    data.values = _.filter(data.values, function (o, index) {
                        return !(index < days);
                    });

                    return mAvg;
                };

                var ma = generateMovingAverage(7, lines[0]);
                lines.push(ma);
                return lines;
            };

            var buildSyndromeChart = function (chart, visitTypeObj) {
                chart.syndrome = _.find(visitTypeObj.syndromes, function (syndrome) { return syndrome.id.toUpperCase() == chart.key1 });
                chart.lines = buildLines( chart.key1, visitTypeObj.syndromesCounts);
                chart.title = (chart.syndrome !== undefined ? chart.syndrome.title : chart.key1);
            };

            //loop thru each visit types
            angular.forEach($scope.graphData, function(visitTypeObj) {
                //get all the sydrome counts for this visit type
                loadSyndromesCounts( visitTypeObj, function () {
                    //get syndromes details
                    classifications.getData().then(function (data) {
                        for (var i in data.list) {
                            if (data.list[i].id === "S2014") {
                                //build array of sydromes which has data
                                visitTypeObj.syndromes = _.filter(data.list[i].classes, function (syndrome) {
                                    return undefined !== visitTypeObj.syndromesCounts[syndrome.id.toUpperCase()];
                                });
                                //build lines for each charts
                                for (var i in visitTypeObj.charts) {
                                    var chart = visitTypeObj.charts[i];
                                    buildSyndromeChart(chart, visitTypeObj);
                                }
                                visitTypeObj.data = visitTypeObj.charts;
                                visitTypeObj.loaded = true;
                                break;
                            }
                        }
                    });
                } );
            } );

            // Get alerts from service
            userServices.getAlerts()
                .then(function (response) {
					// Assign alerts to scope 
					$scope.data = {data: response.data, options: ''};
                    console.log("look here!!!", $rootScope.phu.id);
                });

            //  Function used by nvd3 charts to turn timestamps into dates
            $scope.xAxisTickFormatFunction = function () {
                return function (d) {
                    return d3.time.format("%Y-%m-%d")(new Date(d)); //uncomment for date format
                }
            };
        }])
;

