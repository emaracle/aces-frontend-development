'use strict';

angular.module('acesApp')
    .controller('EpicurvesCtrl',
        ['$scope', '$rootScope', '$state', '$stateParams', '$http', 'epicurves', '$injector', 'fsas', 'alertService', 'buckets', 'syndromes', 'hospitals', 'hospitalLoad', 'lineListings', 'EpiAlerts', 'AlertTypes', '$modal', 'Auth', '$log', function
        ($scope, $rootScope, $state, $stateParams, $http, epicurves, $injector, fsas, alertService, buckets, syndromes, hospitals, hospitalLoad, lineListings, EpiAlerts, AlertTypes, $modal, Auth, $log) {

        //For debugging
        $scope.log = $log;

        console.log("epi", $stateParams);

        //if invalid type, redirect to home
        if ($stateParams.type !== "ED" && $stateParams.type !== "AD") {
          $state.go('app.home');
          return;
        }

        $scope.$on('lineChartFocusExtendedLeft', function (angularEvent, extent) {
          console.log(arguments);
          var difference = extent[1] - extent[0];
          difference = difference.toFixed(0);
          epicurves.Queries.datefrom = epicurves.Queries.datefrom - difference;
          $scope.ho = 'all';
          $scope.performGet();
          $scope.formParams.dateTo = new Date(epicurves.Queries.dateto);
          $scope.formParams.dateFrom = new Date(epicurves.Queries.datefrom);
        });

        $scope.$on('lineChartFocusExtendedRight', function (angularEvent, extent) {
          var difference = extent[1] - extent[0];
          var currentdate = moment.unix(moment(moment().format('MM-DD-YYYY')).unix())._i;
          difference = difference.toFixed(0);
          epicurves.Queries.dateto = parseInt(epicurves.Queries.dateto) + parseInt(difference);
          if (epicurves.Queries.dateto > currentdate) {
            alertService.add("danger", "You have tried to load data past the current date/time, only data up until the current date time will be displayed.");
          }
          $scope.ho = 'all';
          $scope.performGet();
          $scope.formParams.dateTo = new Date(epicurves.Queries.dateto);
          $scope.formParams.dateFrom = new Date(epicurves.Queries.datefrom);
        });

        //stores list of keys which can be clicked
        var clickableKeys = {};
        //add all and all the hospital id to clickable keys
        clickableKeys['All'] = true;
        hospitals.getData().then(function (data) {
          angular.forEach(data.hospitals, function (hospital) {
            clickableKeys[hospital.id] = true;
          });
        });

        $scope.isClickable = function (key) {
          var access = routingConfig.accessLevels;

          //If current phu is all and the user is not admin or dates are being grouped - disable clickable
          if ((!Auth.authorize(access.admin) && epicurves.Params.phuId == 'ALL') || $scope.dateGrouped == true) {
            return false;
          }
          return clickableKeys[key] !== undefined;
        };

        $scope.$on('lineChartElementClick', function(angularEvent, e){
          //Check is user is restricted from seeing line listings
          if(!Auth.checkPermission('app.linelistings')){
            //display info messaqge to the user
            alertService.add('warning', "Access Denied due to insufficient privileges.");
          } else {
            //launch line listing modal
            var hospitalId = e.series.original.hospitalId;
		        var syndrome = (e.series.key).replace(/\s\[.*\]/,'');

            //do nothing if series key has 'STD DEV' or 'MVG AVG'
            //transfer to linelisting only if series key is valid hospital id or All
            if ($scope.isClickable(hospitalId)) {
              var epoch = e.point.x; //moment(e.point.x).unix();
              var custom = {
                  Params: {},
                  Queries: {}
              };

              custom.Params = angular.copy(epicurves.Params);
              custom.Queries = angular.copy(epicurves.Queries);
              //Find selected syndrome and remove others from list.
              //The object needs to be copied as we can't assume its parameters
              var selectedSyndrome;
              for(var i = 0; i < (custom.Queries.class).length; i++){
                if(syndrome == custom.Queries.class[i].id){
                  selectedSyndrome = custom.Queries.class[i];
                }
  					  };

    					if(selectedSyndrome != undefined){
    						custom.Queries.class = selectedSyndrome;
    					};

    					custom.HospitalID = hospitalId;
              custom.Queries.dateto = epoch;
              custom.Queries.datefrom = epoch;
              // debugger;

              //Allow for different states depending on calling page
              var modeTitle;
              var modeDisabledColumns;
              var modeHiddenColumns;

              if ($stateParams.type === "ED"){
                modeTitle = 'Visits ';
                modeDisabledColumns = [];
                modeHiddenColumns = ['PatientLHIN', 'EMSArrival', 'AdmissionType','PatientPHU'];
              } else {
                modeTitle = 'Admissions ';
                modeDisabledColumns = ['CTAS'];
                modeHiddenColumns = ['PatientLHIN', 'EMSArrival', 'AdmissionType', 'PatientPHU'];
              }

              var title = modeTitle;
              if (hospitalId !== 'All') {
                  title += ' - ' + e.point.title;//Hospital name
              }
              title += ' (' + moment(parseInt(epoch)).format('YYYY-MM-DD') + ')';

              // Creates a modal when called
              var modalInstance = $modal.open({
                templateUrl: 'views/lineListingsModal.html',
                controller: 'LineListingsModal2Ctrl',
                size: 'lg',
                resolve: {
                  'options': function () {
                    return {
                      'params': custom,
                      'title': title,
                      //hide these columns
                      'hiddenColumns': modeHiddenColumns,
                      //disable these columns
                      'disabledColumns': modeDisabledColumns
                    }
                  },
                }
              });
            }
          } // END IF
        });

        //click event hander for alert area
        $scope.$on('alertChartElementClick', function(angularEvent, e, alert){
          console.log(alert);
        });

        $scope.disabledLegends = [];
        $scope.$on('lineChartDisabledLegends', function (e, list) {
          $scope.disabledLegends = list;
        });

        $scope.tooltipContentHandler = function (key, y, e, graph){
          var modeTitle;
          if ($stateParams.type === "ED"){
            modeTitle = 'Visits:'
          } else {
            modeTitle = 'Admissions:'
          }
          var str = '<h3>' + key + '</h3><p>' + y + '</p> <p>' + modeTitle + e;
		       if ($scope.isClickable(key.replace(/\s\[.*\]/, '')) && Auth.checkPermission('app.linelistings')) {
             str += '</p><p><small>Click to see line listings</small>';
           }
           str += '</p>';
           return str;
        }

        $scope.legendToolTipContentHandler = function (element) {
          //converts hospital id to hospital name
          var text = $(element).text();
          var hospitalName = text;
          var hospitalId = text;
          var appendText = '';
          //matches
          // 7 day AVG
          // 14 day AVG
          // max day AVG
          // STD DEV 1
          // STD DEV 2
          // - Normalized
          var matches = text.match(/((\d|\bmax\b)+ day AVG)|(STD DEV \d)|(- Normalized)/g);
          if (matches !== null) {
            text = text.replace(new RegExp(matches[0], 'g'), '');
            hospitalId = $.trim(text);
            appendText = ' ' + matches[0];
          }
          if (hospitalId == 'All') {
            hospitalName = hospitalId;
          } else {
            for (var i in hospitals.hospitals) {
              if (hospitals.hospitals[i].id == hospitalId) {
                hospitalName = hospitals.hospitals[i].title;
                break;
              }
            }
          }
          return hospitalName + appendText;
        };

        //generate content for alert tooltip
        $scope.alertTooltipContentHandler = function (alert) {
          var content = '';
          content += '<h3>Alert</h3>';
          content += '<p>Type: ' + alert.AlertType + '</p>';
          if (alert.Class) {
            content += '<p>Syndrome: ' + alert.Class + '</p>';
          }
          content += '<p>Geog. Type: ' + alert.GeogType + '</p>';
          content += '<p>Geog. Value: ' + alert.GeogValue + '</p>';
          content += '<p>Start Date: ' + moment(alert.StartDate).format('llll') + '</p>';
          content += '<p>End Date: ' + moment(alert.EndDate).format('llll') + '</p>';
          return content;
        }

        $scope.setNormalize = function (nm, saveOnly) {
          saveParams({Params:{normalize: nm}});
          $scope.nm = nm;
          if ($scope.result.ready) {
            if (saveOnly !== true) {
              $scope.result.ready = false;
              epicurves.Visits.Normalize(nm);
              $scope.result.data = epicurves.Visits.all;
              $scope.result.ready = true;
            }
          }
        };

        $scope.result = {
          qid: 0,
          ready: false,
          normalize: 0,
          xInterval: 'D',
          data: [],
          alerts: [],
          alertParams: {
            phuId: '',
            hospitalId: '',
            fsa: '',
            class: ''
          }
        };

        $scope.$watch('nm', function (newVal) {
            $scope.result.normalize = newVal;
        });
        //use normalize value in saved param only if class id is not all
        //else default to 0
        $scope.nm = ((undefined !== epicurves.Params.normalize && epicurves.Queries.class.id !== "All") ? epicurves.Params.normalize : 0);
        //used to display/hide normalize functionality on the page
        $scope.normalize = false;

        $scope.epicurves = epicurves;
        $scope.hospitalsByPhu = {};
        $scope.hospitalsByPhu.list = [];

        $scope.ho = 'all';
        $scope.hospital = {id: "Hospitals"};
        $scope.setHospital = function (hospital) {
            $scope.hospital = hospital;
            $scope.ho = hospital;
        };

        //wil be used to toggle alerts
        $scope.allAlerts = [];
        $scope.allAlertTypes = []; //will hold array of alertType obj
        $scope.activeAlertTypes = []; //will hold array of alertType IDs in string
        $scope.disableAlerts = true; //disable alerts
        var defaultActiveAlertTypes = 'all';

        $scope.isActiveAlertType = function (alertTypeID) {
          return $scope.activeAlertTypes.indexOf(alertTypeID) !== -1;
        };

        var refreshActiveAlerts = function (){
          $scope.result.alerts = _.filter($scope.allAlerts, function (a) { return $scope.isActiveAlertType(a.AlertType); });
        }

        $scope.toggleAlertType = function (alertType) {
          if (alertType === 'all') {
            defaultActiveAlertTypes = 'all';
            $scope.activeAlertTypes = _.map($scope.allAlertTypes, function (alertType) { return alertType.AlertTypeID; });
          } else if (alertType === 'none') {
            defaultActiveAlertTypes = 'none';
            $scope.activeAlertTypes = [];
          } else {
            defaultActiveAlertTypes = undefined;
            console.log(alertType);
            if (!$scope.isActiveAlertType(alertType.AlertTypeID)) {
              $scope.activeAlertTypes.push(alertType.AlertTypeID);
            } else {
              //remove it from array
              $scope.activeAlertTypes = _.filter($scope.activeAlertTypes, function (AlertTypeID) { return AlertTypeID !== alertType.AlertTypeID; });
            }
            console.log($scope.activeAlertTypes);
          }
          refreshActiveAlerts();
        };

        //return the stddev ma value
        $scope.getStdDevMa = function(){
          var stdDevMa = 7;
          if ($scope.maSlider.value != 'None'){
            stdDevMa = $scope.maSlider.value;
          }

          return stdDevMa;
        };

        //also called from epicurvesPhusPhu.js
        $scope.performGet = function (){
          var qid = moment().valueOf();
          $scope.result.qid = qid;
          $scope.result.data = [];
          $scope.result.alerts = [];
          $scope.result.ready = false;
          var c = 0;
          var resolve = function (qid){
            //do not do anything is not same query
            if (qid != $scope.result.qid) return;

            c++;
            if (c >= 2) {
              $scope.result.ready = true;
              //refresh normalize incase if on
              $scope.setNormalize($scope.nm);
            }
          };

          var range = epicurves.Queries.dateto - epicurves.Queries.datefrom;
          epicurves.Queries.pagesize = Math.round(range / 60 / 60 / 24 / 1000);

          if (epicurves.Queries.class.id === "All") {
            //reset normalize value to 0
            $scope.setNormalize(0, true);
            //hide normalize functionality
            $scope.normalize = false;
            epicurves.Visits.get($scope.std1, $scope.std2, $scope.getStdDevMa(), $scope.ma).then(function () {
            	$scope.result.data = epicurves.Visits.all;
                resolve(qid);
            });
          } else {
            //show normalize functionality
            $scope.normalize = true;
            epicurves.Visits.getClassified($scope.std1, $scope.std2, $scope.getStdDevMa(), $scope.ma).then(function () {
              $scope.result.data = epicurves.Visits.all;
              resolve(qid);
            });
          }

          //epicurve alerts
          if (!$scope.disableAlerts) {
            EpiAlerts
              .setParams({
                Queries: epicurves.Queries,
                Params: epicurves.Params
              })
              .getAlerts()
              .then( function (data) {

                  $scope.result.alertParams.phu = epicurves.Params.phuId;
                  $scope.result.alertParams.hospital = epicurves.Queries.hospital.id;
                  $scope.result.alertParams.locality = epicurves.Params.locality;
                  $scope.result.alertParams.fsa = epicurves.Queries.filterByFSA;
                  $scope.result.alertParams.class = epicurves.Queries.class.id;

                  //Filter out alerts which are not disaplayable
                  data = data.filter(function (d) {
                    //disable filter.
                    if (window['skip-epicurves-alerts-filter']) return true;

                    switch(d.GeogType){
                      case 'HOSP':
                        //If hospital is set to all and hospital locality is not local
                        if($scope.result.alertParams.hospital == 'All' &&  $scope.result.alertParams.locality != 'local'){
                              return false;
                        }
                        break;
                      case 'PHU':
                        //if phu is set to all or value not same as select phu
                        if ($scope.result.alertParams.phu == 'All' || $scope.result.alertParams.phu != d.GeogValue){
                          return false;
                        }
                        break;
                      }
                      return true;
                  });

                  $scope.allAlertTypes = [];
                  AlertTypes.get().then(function (list) {
                    data.forEach( function (d) {
                      //if alerttype already exists in allAlertTypes
                      if (_.find($scope.allAlertTypes, function (o) { return d.AlertType === o.AlertTypeID;}) === undefined) {
                        list.forEach( function (a) {
                          if (a.AlertTypeID == d.AlertType) {
                            var n = angular.copy(a);
                            //get total alerts with this alerttype
                            n.alertsCount = _.filter(data, function (aa) { return d.AlertType == aa.AlertType }).length;
                            $scope.allAlertTypes.push( n );
                            return;//exit list foreach
                          }
                        });
                      }
                    });

                    //sort $scope.allAlertTypes
                    $scope.allAlertTypes = _.sortBy($scope.allAlertTypes, function (o) { return o.AlertTypeID; });

                    $scope.allAlerts = data;
                    switch( defaultActiveAlertTypes ){
                      //defaultActiveAlertTypes is undefined if user has specifically set activeAlertTypes
                      case undefined:
                        //retain current active alert types
                        var tArr = _.filter($scope.allAlertTypes, function (alertType) { return $scope.isActiveAlertType(alertType.AlertTypeID); });
                        $scope.activeAlertTypes = _.map(tArr, function (alertType) { return alertType.AlertTypeID; });
                      break;
                      case 'all':
                        $scope.activeAlertTypes = _.map($scope.allAlertTypes, function (alertType) { return alertType.AlertTypeID; });
                      break;
                      case 'none':
                        $scope.activeAlertTypes = [];
                      break;
                      default:
                        $scope.activeAlertTypes = defaultActiveAlertTypes;
                      break;
                    }

                    refreshActiveAlerts();
                    resolve(qid);
                  });


              });
          } else {
            resolve(qid);
          }
        };

        $scope.mostLikely = true;
        $scope.setMostLikely = function () {
          $scope.mostLikely = !$scope.mostLikely;
        };
        $scope.getMostLikely = function () {
          epicurves.Visits.getClassified();
        };

        $scope.ma = ((undefined !== epicurves.Params.movingAverage) ? epicurves.Params.movingAverage : 0);
        $scope.movingAverage = function (days) {
          saveParams({Params:{movingAverage: days}});
          if ($scope.result.ready) {
            $scope.result.ready = false;
            epicurves.Visits.movingAverage(days);
            $scope.ma = days;
            $scope.result.data = epicurves.Visits.all;
            $scope.result.ready = true;
          }
        };

        $scope.std1 = epicurves.Params.std1;
        $scope.std2 = epicurves.Params.std2;
		    $scope.dateGrouped = false;

        //Moving average slider value/options
        $scope.maSlider = {
          value: 'None',
          options:{
            showTicksValues: true,
            stepsArray: [
              {value: 'None'},
              {value: 1},
              {value: 2},
              {value: 3},
              {value: 4},
              {value: 5},
              {value: 6},
              {value: 7},
              {value: 8},
              {value: 9},
              {value: 10},
              {value: 11},
              {value: 12},
              {value: 13},
              {value: 14},
              {value: 15},
              {value: 16},
              {value: 17},
              {value: 18},
              {value: 19},
              {value: 20},
              {value: 21},
              {value: 22},
              {value: 23},
              {value: 24},
              {value: 25},
              {value: 26},
              {value: 27},
              {value: 28},
              {value: 29},
              {value: 30}
            ]
          }
        };

        //Moving average modal
        $scope.openMovingAverageModal = function () {
          $scope.maSlider.value = $scope.ma;
          var modalInstance = $modal.open({
            templateUrl: 'views/movingAverageModal.html',
            size: 'lg',
            controller: function ($scope, $modalInstance, values) {
              //Copy of the object to keep original values in parent controller
              $scope.maSlider = JSON.parse(JSON.stringify(values));

              $scope.ok = function () {
                  $modalInstance.close($scope.maSlider);
              };

              $scope.cancel = function () {
                  $modalInstance.dismiss();
              };
            },
            resolve: {
              values: function () {
                return $scope.maSlider;
              }
            }
          });
          //Set scope value to the value selected in the modal
          modalInstance.result.then(function (maSlider) {
            $scope.maSlider = maSlider;
            if ($scope.maSlider.value == 'None'){
              $scope.movingAverage(0);
            } else {
              $scope.movingAverage($scope.maSlider.value);
            }

            $scope.resetStdDev();
          });
          //Force refresh sliders on render
          modalInstance.rendered.then(function () {
            $rootScope.$broadcast('rzSliderForceRender');
          });
        };

        //Resets the chosen std dev options once a new moving average is selected
        $scope.resetStdDev = function(){
          var stdDev1Selected = false;
          var stdDev2Selected = false;

          if ($("#btnStdDev1").hasClass("active")) stdDev1Selected = true;
          if ($("#btnStdDev2").hasClass("active")) stdDev2Selected = true;

          $scope.setStandardDeviation(0);

          if (stdDev1Selected == true) $scope.setStandardDeviation(1);
          if (stdDev2Selected == true) $scope.setStandardDeviation(2);
        };

        $scope.setStandardDeviation = function (value) {
          if ($scope.result.ready) {
            $scope.result.ready = false;

            if (value === 0) $scope.std1 = $scope.std2 = false;
            if (value === 1) $scope.std1 = !$scope.std1;
            if (value === 2) $scope.std2 = !$scope.std2;
            saveParams({Params:{std1: $scope.std1, std2: $scope.std2}});
            epicurves.Visits.standardDeviation($scope.std1, $scope.std2, $scope.getStdDevMa());

            $scope.result.data = epicurves.Visits.all;
            $scope.result.ready = true;
          }
        };

        //this data will be passed to epicurves form directive
        var initFormParams  = function () {
          $scope.formParams = {
            phu: epicurves.Queries.phu,
            hospital: epicurves.Queries.hospital,
            dateFrom: new Date(epicurves.Queries.datefrom),
            dateTo: new Date(epicurves.Queries.dateto),
            fromAge: epicurves.Queries.agefrom,
            toAge: epicurves.Queries.ageto,
            gender: epicurves.Queries.gender,
            classification: epicurves.Queries.classification,
            bucket: epicurves.Queries.bucket,
            syndromes: epicurves.Queries.class,
            classifier: epicurves.Queries.classifier,
            cta: epicurves.Queries.ctas,
            patientLocality: epicurves.getPatientLocality(epicurves.Params.patient),
            hospitalLocality: epicurves.getHospitalLocality(epicurves.Params.locality),
            fsa: {title: epicurves.Queries.filterByFSA},
            admissionType: $stateParams.type,
            includeAllPhus: true,
            isEpicurve: true,
            selectedGroup: epicurves.Params.grouping,
            weekStart: epicurves.Params.weekStart,
            fltrAmType: epicurves.Params.fltrAmType
          };

          if ($stateParams.type == 'AD') {
            $scope.formParams.syndromes = epicurves.Queries.classAd;
          } else if ($stateParams.type == 'ED') {
            $scope.formParams.syndromes = epicurves.Queries.classEd;
          }

          $scope.result.xInterval = epicurves.Params.grouping;
        };

        // initFormParams();

        var saveParams = function (p) {
          //keep the reference to original obj as it is used in the queries
          _.extend(epicurves.Queries, p.Queries);
          _.extend(epicurves.Params, p.Params);

          epicurves.Params['type'] = $stateParams.type;
          epicurves.saveParams();
        };

        //check the $scope.formParams for exact mappings
        $scope.submit = function (params) {
          var p = {
            Queries: {},
            Params: {}
          };

          //Convert to simple dates (no locality or timestamps) for endpoint
          var formattedDateFrom = moment(params.dateFrom).startOf('day').format('ll');
          var formattedDateTo = moment(params.dateTo).startOf('day').format('ll');

          p.Params.phuId = params.phu.id;
          p.Queries.hospital = params.hospital;
          p.Queries.datefrom = moment(formattedDateFrom).valueOf();
          p.Queries.dateto = moment(formattedDateTo).valueOf();
          p.Queries.agefrom = params.fromAge;
          p.Queries.ageto = params.toAge;
          p.Queries.gender = params.gender;
          p.Queries.classification = params.classification;
          p.Queries.bucket = params.bucket;
          p.Queries.class = params.syndromes;
          if ($stateParams.type == 'AD') {
              p.Queries.classAd = params.syndromes;
          } else if ($stateParams.type == 'ED') {
              p.Queries.classEd = params.syndromes;
          }
          p.Queries.classifier = params.classifier;
          p.Queries.ctas = params.cta;
          p.Params.patient = params.patientLocality.id;
          p.Params.locality = params.hospitalLocality.id;
          p.Queries.filterByFSA = params.fsa.title;

          p.Params.grouping = params.selectedGroup;
          p.Params.weekStart = params.weekStart;
          p.Params.fltrAmType = params.fltrAmType;

          saveParams(p);



          $scope.phu = params.phu;

      		//Disable MA and StdDev if date is grouped
      		if (p.Params.grouping != 'D'){
      			//Set to defaults
      			$scope.ma = 0;
      			$scope.std1 = false;
      			$scope.std2 = false;
      			//Disable
      			$scope.dateGrouped = true;
      		} else {
      			//Enable
      			$scope.dateGrouped = false;
      		}

          $rootScope.colourIndex = 0;
          $scope.ho = 'all';
          $scope.result.xInterval = epicurves.Params.grouping;
          $scope.performGet();
        };

        $scope.reset = function (button) {
          var a = epicurves.getDefault();
          //if first reset button
          if (button == 1) {
            //Do not reset Params
            a.Params = {};
            //Do not reset filterByFSA from Queries
            delete a.Queries.filterByFSA;
          } else if (button == 2) { //if second reset button
            //Only reset filterByFSA from Queries
            a.Queries = {
              filterByFSA: a.Queries.filterByFSA
            };
          }
          saveParams(a);
          initFormParams();
          $rootScope.colourIndex = 0;
          $scope.performGet();
        };


        (function () {
          epicurves.initParams($stateParams.type);
          var found = false;
          var currentPhu = epicurves.Params.phuId;
          angular.forEach(hospitalLoad.phus, function (phu) {
            if (!found && currentPhu != undefined && phu.id == currentPhu) {
              epicurves.Queries.phu = phu;
              found = true;
            }
          });

          if (!found) {
            epicurves.Queries.phu = hospitalLoad.phus[0];
            epicurves.Params.phuId = hospitalLoad.phus[0].id;
          }

          initFormParams();
          $scope.submit($scope.formParams, false);
        })();


    }]);
