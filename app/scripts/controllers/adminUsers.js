'use strict';

angular.module('acesApp')
    .controller('AdminUsersCtrl', ['$scope', '$modal', 'AcesAdmin',
        function ($scope, $modal, AcesAdmin) {

			      $scope.data = [];
			      $scope.data = {data: $scope.data, options: {modalSelected: false}};

            //function to grab all users
            var getUsers = function () {
              AcesAdmin.Users.getUsers().then( function (data) {
                  $scope.data.data = data;
              } );
            };

            getUsers();

            $scope.open = function (row) {
                //Add refresh to user variable reference to refresh data after update
                row.refresh = function () {
                  getUsers();
                };
                // Creates a modal when called
                var modalInstance = $modal.open({
                    templateUrl: 'views/admin/userModal.html?v=' + encodeURIComponent(ACES.buildnumber),
                    controller: 'AdminUserDetailModalCtrl',
                    // size: 'lg',
                    resolve: {
                        'options': function () {
                            return {
                                'row': row
                            }
                        }
                    }
                });
            };

            var plugins = [];
            var columnDefs = [
                {field: 'UserName', displayName: 'Username'},
                {field: 'FName', displayName: 'First Name'},
                {field: 'LName', displayName: 'Last Name'},
                {field: 'Email', displayName: 'Email'},
                {field: 'CreatedOn', cellFilter: 'date:"yyyy-MM-dd HH:mm:ss"', displayName: 'Created On'},
                {field: 'LastActive', cellFilter: 'date:"yyyy-MM-dd HH:mm:ss"', displayName: 'Last Active'},
                {field: 'isActive', displayName: 'Active', cellClass:"text-center", width: '80px', cellTemplate: '<div class="ngCellText" ng-class="col.colIndex()">{{COL_FIELD ? "Yes" : "No" }}</div>'}
            ];
            $scope.sortInfo = {
                fields: ['LastActive'],
                directions: ['desc']
            };
            $scope.gridOptions = {
                rowTemplate: '<div ng-style="{\'cursor\': row.cursor, \'z-index\': col.zIndex() }" ng-dblclick="open(row)" ng-repeat="col in renderedColumns" ng-class="col.colIndex()" class="ngCell {{col.cellClass}}" ng-cell></div>',
                columnDefs: columnDefs,
                selectWithCheckboxOnly: true
            };
        }])
    .controller('AdminUserDetailModalCtrl', ['$scope', '$modalInstance', 'growl', 'hospitals', 'AcesAdmin', 'options',
        function ($scope, $modalInstance, growl, hospitals, AcesAdmin, options) {

            console.log(options.row);
            $scope.user = options.row.entity;
            $scope.refresh = options.row.refresh;
            $scope.editMode = false;
            var params = options.params;

            //get the list of application settings available
            $scope.getAppSettings = function () {
              AcesAdmin.Users.getAppSettings($scope.user.UserUUID)
                .then( function (data) {

                  if (typeof data.Perms !== "undefined"){
                    //Convert Permission list from XML to JSON and add it back to the user object.
                    var x2js = new X2JS();
                    var PermsJSON = x2js.xml_str2json("<perms>" + (data.Perms).toString() + "</perms>");

                    //Check if returned JSON is in array to always set the user variable in an array
                    if (PermsJSON.perms.row instanceof Array){
                      $scope.appSettings = PermsJSON.perms.row;
                    } else {
                      var permArray = [];
                      permArray.push(PermsJSON.perms.row);
                      $scope.appSettings = permArray;
                    }

                    //loop through Array and fix permission value bool state
                    angular.forEach($scope.appSettings, function (userPermission) {
                        userPermission.PermissionValue = userPermission.PermissionValue == 'TRUE';
                    });
                  }
                });
            };

            $scope.getAppSettings();

            //Update the change permission value on the db
            $scope.updateAppSetting = function (appSetting) {
              AcesAdmin.Users.updateAppSetting($scope.user.UserUUID,appSetting)
                .then( function (data) {
                    growl.success("<strong>SUCCESS:</strong> User application setting updated.");
                }, function (statusText) {

                    //Revert value to previous state
                    appSetting.PermissionValue = !appSetting.PermissionValue;

                    //display failed msg
                    growl.error("<strong>ERROR:</strong> Unable to update user application setting.");
                    console.log("ERROR: " + statusText);
                });
            }

            // Set the default value of inputType
            $scope.inputType = 'password';

            // Hide & show password function
            $scope.hideShowPassword = function(){
                if ($scope.inputType == 'password'){
                    $scope.inputType = 'text';
                }else{
                    $scope.inputType = 'password';
                };
            };

            $scope.setNewPassword = function(){
                AcesAdmin.Users.setPasswordAdmin($scope.user.UserUUID,$scope.newPassword)
                    .then( function (data) {
                        //Show success message
                        growl.success("<strong>SUCCESS:</strong> User password updated.");
                        $scope.newPassword = '';
                    },function (statusText) {
                        //failed
                        growl.error("<strong>ERROR:</strong> " + statusText);
                    });
            };

            $scope.openEditMode = function () {
                $scope.editUser = angular.copy($scope.user);
                $scope.editMode = true;
            };

            $scope.cancelEditMode = function () {
                $scope.errors = {};
                $scope.editMode = false;
            };

            $scope.saveUserInfo = function () {

                $scope.errors = {};

                //First Name
                if(typeof $scope.editUser.FName !== "undefined"){
                    if ($scope.editUser.FName !== null){
                        $scope.editUser.FName = $scope.editUser.FName.trim().length > 0 ? $scope.editUser.FName.trim() : null;
                    };
                } else {
                    $scope.editUser.FName = null;
                };

                if ($scope.editUser.FName !== null){
                    if ($scope.editUser.FName.length > 20){
                        $scope.errors.fName = {
                            exceedsLength: true
                        }
                    };
                } else {
                    $scope.errors.fName = {
                        blank: true
                    };
                };

                //Last Name
                if(typeof $scope.editUser.LName !== "undefined"){
                    if ($scope.editUser.LName !== null){
                        $scope.editUser.LName = $scope.editUser.LName.trim().length > 0 ? $scope.editUser.LName.trim() : null;
                    };
                } else {
                    $scope.editUser.LName = null;
                };

                if ($scope.editUser.LName !== null){
                    if ($scope.editUser.LName.length > 20){
                        $scope.errors.lName = {
                            exceedsLength: true
                        }
                    };
                } else {
                    $scope.errors.lName = {
                        blank: true
                    };
                };

                //Email
                 if(typeof $scope.editUser.Email !== "undefined"){
                    if ($scope.editUser.Email !== null){
                        $scope.editUser.Email = $scope.editUser.Email.trim().length > 0 ? $scope.editUser.Email.trim() : null;
                    };
                } else {
                    $scope.editUser.Email = null;
                };

                if ($scope.editUser.Email !== null){
                    if ($scope.editUser.Email.length > 250){
                        $scope.errors.email = {
                            exceedsLength: true
                        }
                    };
                } else {
                    $scope.errors.email = {
                        blank: true
                    };
                };

                //Telephone
                if(typeof $scope.editUser.Telephone !== "undefined"){
                    if ($scope.editUser.Telephone !== null){
                        $scope.editUser.Telephone = $scope.editUser.Telephone.trim().length > 0 ? $scope.editUser.Telephone.trim() : null;
                    };
                } else {
                    $scope.editUser.Telephone = null;
                };

                if ($scope.editUser.Telephone !== null){
                    if ($scope.editUser.Telephone.length > 50){
                        $scope.errors.telephone = {
                            exceedsLength: true
                        }
                    };
                };

                //Agency
                if(typeof $scope.editUser.Agency !== "undefined"){
                    if ($scope.editUser.Agency !== null){
                        $scope.editUser.Agency = $scope.editUser.Agency.trim().length > 0 ? $scope.editUser.Agency.trim() : null;
                    };
                } else {
                    $scope.editUser.Agency = null;
                };

                if ($scope.editUser.Agency !== null){
                    if ($scope.editUser.Agency.length > 500){
                        $scope.errors.agency = {
                            exceedsLength: true
                        }
                    };
                };

                //Position
                if(typeof $scope.editUser.Position !== "undefined"){
                    if ($scope.editUser.Position !== null){
                        $scope.editUser.Position = $scope.editUser.Position.trim().length > 0 ? $scope.editUser.Position.trim() : null;
                    }
                } else {
                    $scope.editUser.Position = null;
                };

                if ($scope.editUser.Position !== null){
                    if ($scope.editUser.Position.length > 500){
                        $scope.errors.position = {
                            exceedsLength: true
                        }
                    };
                };

                //no errors
                if (angular.equals($scope.errors, {})) {
                    AcesAdmin.Users.updateUserDetails($scope.user.UserUUID, $scope.editUser.FName, $scope.editUser.LName, $scope.editUser.Email, $scope.editUser.Telephone, $scope.editUser.Agency, $scope.editUser.Position)
                    .then( function (data) {
                        //Show success message
                        growl.success("<strong>SUCCESS:</strong> User Information updated.");
                    });

                    $scope.user =  $scope.editUser;
                    $scope.editMode = false;
                };
            };

            //archive selected user
            $scope.archiveUser = function () {
                //Call function to set user archived to true
                AcesAdmin.Users.updateUserArchived($scope.user.UserUUID)
                .then( function (data) {
                    //Show success message
                    growl.success("<strong>SUCCESS:</strong> User has been archived.");
                    //refresh user list
                    $scope.refresh();
                    //close modal
                    $modalInstance.close();
                }, function (statusText) {
                    //failed
                    growl.error(statusText);
                });
            };

            $scope.cancel = function () {
                $modalInstance.close();
            };

            //Update the user active status on the db
            $scope.updateStatus = function () {
              AcesAdmin.Users.updateStatus($scope.user.UserUUID, $scope.user.isActive)
                .then( function (data) {
                    growl.success("<strong>SUCCESS:</strong> User account status updated.");
                }, function (statusText) {

                    //Revert value to previous state
                    $scope.user.isActive = !$scope.user.isActive;

                    //display failed msg
                    growl.error("<strong>ERROR:</strong> Unable to update account status.");
                    console.log("ERROR: " + statusText);
                });
            }

            $scope.permissionTypes = AcesAdmin.Users.getResourcePermissionTypes();
            $scope.permissionValues = [];

            var loadUserResources = function () {
                $scope.resourcesLoading = true
                $scope.resources = [];
                AcesAdmin.Users.getUserResources($scope.user.UserUUID)
                    .then(function (data) {
                        $scope.resources = data;
                        $scope.resourcesLoading = false;
                    });
            };
            loadUserResources();

            $scope.currentEditResource = {};
            $scope.editResource = function (row) {
                //reset delete and add modes
                $scope.currentDeleteResource = {};
                $scope.currentAddResource = {};

                AcesAdmin.Users.getResourcePermissionValues(row.PermissionType)
                    .then( function (list) {
                        $scope.currentEditResource = row;
                        $scope.permissionValues = list;
                    });
            };
            $scope.doEditResource = function (row) {
                AcesAdmin.Users.updateUserResource($scope.user.UserUUID, $scope.currentEditResource)
                    .then( function (data) {
                        $scope.currentEditResource = {};
                        loadUserResources();
                    });
            };
            $scope.cancelEditResource = function () {
                $scope.currentEditResource = {};
            };


            $scope.currentDeleteResource = {};
            $scope.deleteResource = function (row) {
                //reset edit and add modes
                $scope.currentEditResource = {};
                $scope.currentAddResource = {};

                $scope.currentDeleteResource = row;
            };
            $scope.doDeleteResource = function (row) {
                AcesAdmin.Users.deleteUserResource($scope.user.UserUUID, $scope.currentDeleteResource.ID)
                    .then( function (data) {
                        $scope.currentDeleteResource = {};
                        loadUserResources();
                    });
            };
            $scope.cancelDeleteResource = function () {
                $scope.currentDeleteResource = {};
            };

            $scope.currentAddResource = {};
            $scope.$watch('currentAddResource.PermissionType', function (newVal, oldVal) {
                console.log('newVal', newVal);
                console.log('oldVal', oldVal);
                if (newVal != oldVal) {
                    AcesAdmin.Users.getResourcePermissionValues(newVal)
                        .then( function (list) {
                            $scope.permissionValues = list;
                        });
                }
            });
            $scope.addResource = function () {
                //reset edit and delete modes
                $scope.currentEditResource = {};
                $scope.currentDeleteResource = {};


                $scope.currentAddResource = {
                    ID: 0,
                    PermissionType: null,
                    permissionValue: null
                };
            };
            $scope.addError = {};
            $scope.doAddResource = function () {
                //reset error
                var hasError = false;
                $scope.addError = {};
                //validate
                if ($scope.permissionTypes.indexOf($scope.currentAddResource.PermissionType) === -1) {
                    $scope.addError.PermissionType = true;
                    hasError = true;
                }
                if ($scope.permissionValues == undefined || $scope.permissionValues.length == 0 || $scope.currentAddResource.PermissionValue == undefined || _.find($scope.permissionValues, function (row) { return row.id == $scope.currentAddResource.PermissionValue.id; }) == undefined) {
                    $scope.addError.PermissionValue = true;
                    hasError = true;
                }

                if (!hasError) {
                    AcesAdmin.Users.addUserResource($scope.user.UserUUID, $scope.currentAddResource)
                        .then( function (data) {
                            $scope.currentAddResource = {};
                            loadUserResources();
                        });
                }
            };
            $scope.cancelAddResource = function () {
                $scope.currentAddResource = {};
            };

        }])
    .controller('AdminUserCreateCtrl', ['$scope', 'growl', 'Profile', 'AcesAdmin',
        function ($scope, growl, Profile, AcesAdmin) {
            var initUser = function () {
                return {
                    UserName: '',
                    Password: '',
                    Password2: '',
                    Email: '',
                    FName: '',
                    LName: '',
                    Agency: '',
                    Position: '',
                    Telephone: '',
                    isActive: true,
                    PermissionType: null,
                    PermissionValue: null
                };
            };

            $scope.user = initUser();

            var userNameAlreadyTaken = false;
            $scope.usernameValidating = false;
            $scope.$watch('user.UserName', function() {
                delete $scope.errors.UserName;
                if (AcesAdmin.Users.validations.validUsername($scope.user.UserName)) {
                    //delay username check
                    var lastUsername = $scope.user.UserName;
                    setTimeout( function () {
                        //check if username value has not changed
                        if (lastUsername == $scope.user.UserName) {
                            $scope.usernameValidating = true;
                            AcesAdmin.Users.validations.validNewUsername($scope.user.UserName)
                                .then(function (valid) {
                                    $scope.usernameValidating = false;
                                    //check again if username value has not changed
                                    if (lastUsername == $scope.user.UserName) {
                                        if (!valid) {
                                            userNameAlreadyTaken = true;
                                        } else {
                                         userNameAlreadyTaken = false;
                                        }
                                        if (userNameAlreadyTaken) {
                                            $scope.errors.UserName = {taken: true};
                                        }
                                    }
                                }, function (statusText) {
                                    console.log(statusText);
                                });
                        }
                    }, 1000);
                } else {
                    userNameAlreadyTaken = false;
                }
            });

            var growlMessage = null;

            $scope.mode = 'create';

            $scope.title = 'Create User';

            $scope.errors = {};

            $scope.submit = function () {
                //validate

                //reset errors
                $scope.errors = {};
                //remove the last growl message if defined
                if (growlMessage != null) {
                    growlMessage.destroy();
                }
                if (userNameAlreadyTaken) {
                    $scope.errors.UserName = {taken: true};
                } else if (!AcesAdmin.Users.validations.validUsername($scope.user.UserName)) {
                    $scope.errors.UserName = {invalidLength: true};
                }

                if (Profile.changePassword.validatePasswordFormat($scope.user.Password)) {
                    $scope.errors.Password = {invalid: true};
                }
                if ($scope.user.Password != $scope.user.Password2) {
                    $scope.errors.Password2 = {notSame: true};
                }

                if (!AcesAdmin.Users.validations.validFirstName($scope.user.FName)) {
                    $scope.errors.FName = {required: true};
                }

                if (!AcesAdmin.Users.validations.validEmail($scope.user.Email)) {
                    $scope.errors.Email = {invalid: true};
                }

                if (!AcesAdmin.Users.validations.validEmail($scope.user.Email)) {
                    $scope.errors.Email = {invalid: true};
                }

                if ($scope.permissionTypes.indexOf($scope.user.PermissionType) === -1) {
                    $scope.errors.PermissionType = {invalid: true};
                }

                if ($scope.permissionValues == undefined || $scope.permissionValues.length == 0 || $scope.user.PermissionValue == undefined || _.find($scope.permissionValues, function (row) { return row.id == $scope.user.PermissionValue; }) == undefined) {
                    $scope.errors.PermissionValue = {invalid: true};
                }
                // debugger;
                //
                var growlConfig = {
                    referenceId: 1,
                    ttl: -1 //do not autoclose
                };
                if (Object.keys($scope.errors).length == 0) {
                    AcesAdmin.Users.createUser($scope.user)
                        .then(function (userUUID) {
                            $scope.user = initUser();
                            growlMessage = growl.success("<strong>SUCCESS:</strong> User account created.", growlConfig);
                        }, function (statusText) {
                            growlMessage = growl.error(statusText, growlConfig);
                        });
                }
            };

            $scope.permissionTypes = AcesAdmin.Users.getResourcePermissionTypes();
            $scope.permissionValues = [];
            $scope.$watch('user.PermissionType', function (newVal, oldVal) {
                if (newVal != oldVal) {
                    AcesAdmin.Users.getResourcePermissionValues(newVal)
                        .then( function (list) {
                            $scope.permissionValues = list;
                        });
                }
            });
        }])
;
