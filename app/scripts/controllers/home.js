'use strict';

angular.module('acesApp')
    //Added to prevent LoginCtrl which is the main controller from executing twice
    .controller('HomeCtrl', ['$scope', '$modal', 'Auth' ,
    	function($scope, $modal, Auth){

    	$scope.registerModal = function () {
            // Creates a modal when called
            var modalInstance = $modal.open({
                templateUrl: 'views/register.html?v=' + encodeURIComponent(ACES.buildnumber),
                controller: 'HomeRegisterModalCtrl',
                windowClass: 'register-modal-window'
            });
            modalInstance.result.then(
	            //close
	            function (result) {
	                var a = result;
	            },
	            //dismiss
	            function (result) {
	                var a = result;
	            }
	        );
       	};
        
    }])
    .controller('HomeRegisterModalCtrl', ['$scope', '$modalInstance', 'growl', 'AcesRegistration', 
        function ($scope, $modalInstance, growl, AcesRegistration) {
		
        var initUser = function () {
            return {
                UserName: null,
                Password1: null,
                Password2: null,
                Email: null,
                FName: null,
                LName: null,
                Telephone: null,
                Agency: null,
                Position: null,               
                Recaptcha: null
            };
        };

        $scope.errors = {};
        $scope.newUser = initUser();
   		var userNameAlreadyTaken = false;
   		     
      	$scope.$watch('newUser.UserName', function() {

      		if ($scope.newUser.UserName != null){

      			$scope.newUser.UserName = $scope.newUser.UserName.toLowerCase().replace(/\s+/g,'');
         		
   				//Only check is username is populated      		   			
	            if ($scope.newUser.UserName != ""){
		      		userNameAlreadyTaken = false;
		            delete $scope.errors.UserName;
		                     
		            AcesRegistration.RegisterAccount.validNewUsername($scope.newUser.UserName)
		                .then(function (valid) {
		                    
		                    if (!valid) {
		                        userNameAlreadyTaken = true;
		                    } else {
		                     	userNameAlreadyTaken = false;
		                    }

		                    if (userNameAlreadyTaken) {
		                        $scope.errors.UserName = {taken: true};
		                    }
		                    
		                }, function (statusText) {
		                    console.log(statusText);
		                });
	            };			        
	        };                     
        });

        $scope.cancel = function () {             
            $modalInstance.dismiss('cancel');
        };

        $scope.submit = function () {
            /*Validation*/   

            //reset errors
            $scope.errors = {};
            
            //Username
            if(typeof $scope.newUser.UserName !== "undefined"){
                if ($scope.newUser.UserName !== null){
                    $scope.newUser.UserName = $scope.newUser.UserName.trim().length > 0 ? $scope.newUser.UserName.trim() : null;
                };
            } else {
                $scope.newUser.UserName = null;
            };

            if ($scope.newUser.UserName !== null){   

            	//Check again to make sure they didn't auto populate the field
            	AcesRegistration.RegisterAccount.validNewUsername($scope.newUser.UserName)
	                .then(function (valid) {
	                    
	                    if (!valid) {
	                        userNameAlreadyTaken = true;
	                    } else {
	                     	userNameAlreadyTaken = false;
	                    }
                    	                    
	                }, function (statusText) {
	                    console.log(statusText);
	                });

                if ($scope.newUser.UserName.length < 4 || $scope.newUser.UserName.length > 40){
                    $scope.errors.UserName = {invalidLength: true};
                } else {
                	if (userNameAlreadyTaken) {
            			$scope.errors.UserName = {taken: true};             
        			};
                };              
            } else {        
                $scope.errors.UserName = {required: true};
            };

            //Password1
        	if(typeof $scope.newUser.Password1 !== "undefined"){
                if ($scope.newUser.Password1 !== null){
                    $scope.newUser.Password1 = $scope.newUser.Password1.trim().length > 0 ? $scope.newUser.Password1.trim() : null;
                };
            } else {
                $scope.newUser.Password1 = null;
            };   

            if ($scope.newUser.Password1 !== null){
                if (AcesRegistration.RegisterAccount.validPasswordFormat($scope.newUser.Password1)){
            		$scope.errors.Password1 = {invalid: true};
            	};               
            } else {
                $scope.errors.Password1 = {required: true};
            };

            //Password2 (Re-type Password)
        	if(typeof $scope.newUser.Password2 !== "undefined"){
                if ($scope.newUser.Password2 !== null){
                    $scope.newUser.Password2 = $scope.newUser.Password2.trim().length > 0 ? $scope.newUser.Password2.trim() : null;
                };
            } else {
                $scope.newUser.Password2 = null;
            };

            if ($scope.newUser.Password2 !== null){
                if ($scope.newUser.Password1 !== null){
 					if ($scope.newUser.Password2 != $scope.newUser.Password1){
						$scope.errors.Password2 = {notSame: true};
					};
                };
            } else {
                $scope.errors.Password2 = {required: true};
            };
            
        	//First Name
            if(typeof $scope.newUser.FName !== "undefined"){
                if ($scope.newUser.FName !== null){
                    $scope.newUser.FName = $scope.newUser.FName.trim().length > 0 ? $scope.newUser.FName.trim() : null;
                };
            } else {
                $scope.newUser.FName = null;
            };

            if ($scope.newUser.FName !== null){
                if ($scope.newUser.FName.length > 20){
                    $scope.errors.FName = {invalidLength: true};
                };                 
            } else {
                $scope.errors.FName = {required: true};
            };
            
            //Last Name
            if(typeof $scope.newUser.LName !== "undefined"){
                if ($scope.newUser.LName !== null){
                    $scope.newUser.LName = $scope.newUser.LName.trim().length > 0 ? $scope.newUser.LName.trim() : null;
                };
            } else {
                $scope.newUser.LName = null;
            };

            if ($scope.newUser.LName !== null){
                if ($scope.newUser.LName.length > 20){
                    $scope.errors.LName = {invalidLength: true};
                };
            } else {
                $scope.errors.LName = {required: true};               
            };
         
            //Email
            if(typeof $scope.newUser.Email !== "undefined"){
                if ($scope.newUser.Email !== null){
                    $scope.newUser.Email = $scope.newUser.Email.trim().length > 0 ? $scope.newUser.Email.trim() : null;
                };
            } else {
                $scope.newUser.Email = null;
            };
      
            if ($scope.newUser.Email !== null){
                if ($scope.newUser.Email.length > 250){
                    $scope.errors.Email = {invalidLength: true};
                } else {             	
            		if (!(AcesRegistration.RegisterAccount.validEmailFormat($scope.newUser.Email))){
            			$scope.errors.Email = {invalid: true};
            		};
                };
            } else {
                $scope.errors.Email = {required: true};                   
            };

            //Telephone  
            if(typeof $scope.newUser.Telephone !== "undefined"){   
            	if ($scope.newUser.Telephone !== null){  
            		$scope.newUser.Telephone = $scope.newUser.Telephone.trim().length > 0 ? $scope.newUser.Telephone.trim() : null;  
            	};   	            
	        } else {
	        	$scope.errors.Telephone = {invalidLength: true};
	        };
            

            //Agency
            if(typeof $scope.newUser.Agency !== "undefined"){
                if ($scope.newUser.Agency !== null){
                    $scope.newUser.Agency = $scope.newUser.Agency.trim().length > 0 ? $scope.newUser.Agency.trim() : null;
                };
            } else {
                $scope.newUser.Agency = null;
            };

            if ($scope.newUser.Agency !== null){
                if ($scope.newUser.Agency.length > 500){
                    $scope.errors.Agency = {invalidLength: true};
                };
            };

            //Position
            if(typeof $scope.newUser.Position !== "undefined"){
                if ($scope.newUser.Position !== null){
                    $scope.newUser.Position = $scope.newUser.Position.trim().length > 0 ? $scope.newUser.Position.trim() : null;
                }
            } else {
                $scope.newUser.Position = null;
            };

            if ($scope.newUser.Position !== null){
                if ($scope.newUser.Position.length > 500){
                    $scope.errors.Position = {invalidLength: true};
                };
            };

            //Recaptcha
            if(typeof $scope.newUser.Position !== "undefined"){
	            if ($scope.newUser.Recaptcha == null || $scope.newUser.Recaptcha == ""){               
	                $scope.errors.Recaptcha = {required: true};               
	            };
	        } else {
	        	$scope.errors.Recaptcha = {required: true}; 
	        }
        
            //No errors           
            if (angular.equals($scope.errors, {})) {
           
            	//Save the new user data
                AcesRegistration.RegisterAccount.registerNewUser($scope.newUser)
					.then( function (data) {
	                    //Show success message
	                    growl.success("<strong>SUCCESS:</strong> Registration submitted.");			                   									
	                }, function (statusText) {
	                	//Show error message
	            		growl.error("<strong>ERROR:</strong> Unable to submit registration.");
        				console.log(statusText);
    				});
	                 
	            $modalInstance.dismiss('cancel'); 		             
            };
        };  
    }]);