'use strict';

angular.module('acesApp')
    .controller('AlertsphusphuhospitalCtrl', ['$scope', '$rootScope', '$state', 'AlertsByPhu', function ($scope, $rootScope, $state, AlertsByPhu) {
        var hospital = _.findWhere($scope.phu.hospitals, {id: $state.params.hospitalId});
        //validate phu
        if (hospital === undefined) {
            //invalid hospital, redirect to phu
            $state.go('app.alerts.phus.phu', {phuId: $scope.phu});
        } else {
            $scope.setTitle($rootScope.phu.phu + ' (' + hospital.title + ')');

            $scope.setHospital(hospital);
            $scope.query();
        }
    }]);