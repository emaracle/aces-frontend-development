'use strict';

angular.module('acesApp')
  .controller('ResourcesCtrl', ['$scope','$http','$rootScope','$q','growl','utils',
    function ($scope,$http,$rootScope,$q,growl,utils) {

      var savedParams;

      //Check if section is visible
      $scope.displayResource = function(resourceID){
        if ($scope.formParams.resource.id == resourceID){
          return true;
        } else {
          return false;
        }
      }

      //Initialize Params
      var initParams = function(){
        savedParams = utils.getSavedParams('resource_params');
        $scope.formParams = {
          resource: savedParams.Params.resource
        }
      }

      initParams();
    }
  ]);
