/* jshint -W084 */
angular.module('mock', ['ngMockE2E'])
    .factory('delayHTTP', function ($q, $timeout) {
        return {
            request: function (request) {
                var delayedResponse = $q.defer();
                $timeout(function () {
                    delayedResponse.resolve(request);
                }, 700);
                return delayedResponse.promise;
            },
            response: function (response) {
                var deferResponse = $q.defer();

                if (response.config.timeout && response.config.timeout.then) {
                    response.config.timeout.then(function () {
                        deferResponse.reject();
                    });
                } else {
                    deferResponse.resolve(response);
                }

                return $timeout(function () {
                    deferResponse.resolve(response);
                    return deferResponse.promise;
                });
            }
        };
    })
// delay HTTP
    .config(['$httpProvider', function ($httpProvider) {
        $httpProvider.interceptors.push('delayHTTP');
    }])
    .constant('loginExampleData', {
        version: '0.2.0'
    })
    .run(function ($httpBackend, $log, loginExampleData) {
        var userStorage = angular.fromJson(localStorage.getItem('userStorage')),
            emailStorage = angular.fromJson(localStorage.getItem('emailStorage')),
            tokenStorage = angular.fromJson(localStorage.getItem('tokenStorage')) || {},
            loginExample = angular.fromJson(localStorage.getItem('loginExample'));

        // Check and corrects old localStorage values, backward-compatibility!
        if (!loginExample || loginExample.version !== loginExampleData.version) {
            userStorage = null;
            tokenStorage = {};
            localStorage.setItem('loginExample', angular.toJson(loginExampleData));
        }

        if (userStorage === null || emailStorage === null) {
            userStorage = {
                'johnm': { name: 'John', username: 'johnm', password: 'hello', email: 'john.dott@myemail.com', userRole: userRoles.user, tokens: [] },
                'sandrab': { name: 'Sandra', username: 'sandrab', password: 'world', email: 'bitter.s@provider.com', userRole: userRoles.admin, tokens: [] },
                'cliffc': { name: 'Cliff', username: 'cliffc', password: '1234', email: 'ccoulter@kflapublichealth.ca', userRole: userRoles.admin, tokens: [] },
                'avandijk': { name: 'Adam', username: 'avandijk', password: '1234', email: 'avandijk@kflapublichealth.ca', userRole: userRoles.admin, useruuid: '520F2C6E-8A50-E311-93F5-D89D67239D41', tokens: [] },
                'avarrette': { name: 'Allan', username: 'avarrette', password: '1234', email: 'avarrette@kflapublichealth.ca', userRole: userRoles.admin, useruuid: '5EF5A75B-8A50-E311-93F5-D89D67239D41', tokens: [] },
                'testhospital': { name: 'TestHospital', username: 'testhospital', password: '1234', email: 'testhospital@kflapublichealth.ca', userRole: userRoles.user, useruuid: 'B8483F75-8A50-E311-93F5-D89D67239D41', tokens: [] },
                'testphu': { name: 'TestPhu', username: 'testphu', password: '1234', email: 'testphu@kflapublichealth.ca', userRole: userRoles.user, useruuid: 'B6F94182-8A50-E311-93F5-D89D67239D41', tokens: [] }

            };
            emailStorage = {
                'john.dott@myemail.com': 'johnm',
                'bitter.s@provider.com': 'sandrab',
                'ccoulter@kflapublichealth.ca': 'cliffc'

            };
            localStorage.setItem('userStorage', angular.toJson(userStorage));
            localStorage.setItem('emailStorage', angular.toJson(emailStorage));
        }

        /**
         * Generates random Token
         */
        var randomUUID = function () {
            var charSet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
            var randomToken = '';
            for (var i = 0; i < 36; i++) {
                if (i === 8 || i === 13 || i === 18 || i === 23) {
                    randomToken += '';
                    continue;
                }
                var randomPoz = Math.floor(Math.random() * charSet.length);
                randomToken += charSet.substring(randomPoz, randomPoz + 1);
            }
            return randomToken;
        };

        $httpBackend.when('GET', 'views/app.home.html').passThrough();
        $httpBackend.when('GET', 'views/error.tpl.html').passThrough();
        $httpBackend.when('GET', 'views/admin.tpl.html').passThrough();
        $httpBackend.when('GET', 'views/epicurves.html').passThrough();
        $httpBackend.when('GET', 'views/epicurves.phus.html').passThrough();
        $httpBackend.when('GET', 'views/epicurves.phus.phu.html').passThrough();
        $httpBackend.when('GET', 'views/lineListings.html').passThrough();
        $httpBackend.when('GET', 'views/lineListings.phus.html').passThrough();
        $httpBackend.when('GET', 'views/lineListings.phus.phu.html').passThrough();
        $httpBackend.when('GET', 'views/lineListings.phus.phu.hospital.html').passThrough();
        $httpBackend.when('GET', 'http://beta.kflaphi.ca:8080/AcesEpiEndpoint/aces/lookup/phus-and-hosps?query=%5Bobject+Object%5D').passThrough();
        $httpBackend.when('GET', 'http://beta.kflaphi.ca:8080/AcesEpiEndpoint/aces/visits/phus/KFLA?query=%5Bobject+Object%5D').passThrough();
        $httpBackend.when('GET', 'http://beta.kflaphi.ca:8080/AcesEpiEndpoint/aces/lookup/ctas?query=%5Bobject+Object%5D').passThrough();
        $httpBackend.when('GET', 'https://beta.kflaphi.ca:8181/AcesEpiEndpoint/aces/lookup/classifications?query=%5Bobject+Object%5D').passThrough();
        $httpBackend.when('GET', 'https://beta.kflaphi.ca:8181/AcesEpiEndpoint/aces/lookup/classifiers?query=%5Bobject+Object%5D').passThrough();
        $httpBackend.when('GET', 'http://beta.kflaphi.ca:8080/AcesEpiEndpoint/aces/classified-line-listings/phus/KFLA?query=%5Bobject+Object%5D').passThrough();
        $httpBackend.when('POST', 'http://beta.kflaphi.ca:8080/AcesEpiEndpoint/aces/authenticate/login').passThrough();
        // fakeLogin
        $httpBackend.when('POST', '/login').respond(function (method, url, data, headers) {
            console.log("login mock");
            var postData = angular.fromJson(data),
                user = userStorage[postData.username],
                newToken,
                tokenObj;
            $log.info(method, '->', url);
            console.log(localStorage);
            console.log("angular.isDefined(user), postData.user", angular.isDefined(user), postData.username);

            if (angular.isDefined(user) && user.password === postData.password) {
                newToken = randomUUID();
                user.tokens.push(newToken);
                tokenStorage[newToken] = postData.username;
                localStorage.setItem('userStorage', angular.toJson(userStorage));
                localStorage.setItem('tokenStorage', angular.toJson(tokenStorage));
                console.log("good");
                return [200, { name: user.name, userRole: user.userRole, token: newToken }, {}];
            } else {
                console.log("bad");
                return [401, 'wrong combination username/password', {}];
            }
        });

        // fakeLogout
        $httpBackend.when('GET', '/logout').respond(function (method, url, data, headers) {
            var queryToken, userTokens;
            $log.info(method, '->', url);

            if (queryToken = headers['X-Token']) {
                if (angular.isDefined(tokenStorage[queryToken])) {
                    userTokens = userStorage[tokenStorage[queryToken]].tokens;
                    // Update userStorage AND tokenStorage
                    userTokens.splice(userTokens.indexOf(queryToken));
                    delete tokenStorage[queryToken];
                    localStorage.setItem('userStorage', angular.toJson(userStorage));
                    localStorage.setItem('tokenStorage', angular.toJson(tokenStorage));
                    return [200, {}, {}];
                } else {
                    return [401, 'auth token invalid or expired', {}];
                }
            } else {
                return [401, 'auth token invalid or expired', {}];
            }
        });

        // fakeUser
        $httpBackend.when('GET', '/user').respond(function (method, url, data, headers) {
            var queryToken, userObject;
            $log.info(method, '->', url);

            // if is present in a registered users array.
            if (queryToken = headers['X-Token']) {
                if (angular.isDefined(tokenStorage[queryToken])) {
                    userObject = userStorage[tokenStorage[queryToken]];
                    return [200, { token: queryToken, name: userObject.name, userRole: userObject.userRole }, {}];
                } else {
                    return [401, 'auth token invalid or expired', {}];
                }
            } else {
                return [401, 'auth token invalid or expired', {}];
            }
        });

        // fakeRegister
        $httpBackend.when('POST', '/user').respond(function (method, url, data, headers) {
            var postData = angular.fromJson(data),
                newUser,
                errors = [];
            $log.info(method, '->', url);

            if (angular.isDefined(userStorage[postData.username])) {
                errors.push({ field: 'username', name: 'used' });
            }

            if (angular.isDefined(emailStorage[postData.email])) {
                errors.push({ field: 'email', name: 'used' });
            }

            if (errors.length) {
                return [409, {
                    valid: false,
                    errors: errors
                }, {}];
            } else {
                newUser = angular.extend(postData, { userRole: userRoles[postData.role], tokens: [] });
                delete newUser.role;

                userStorage[newUser.username] = newUser;
                emailStorage[newUser.email] = newUser.username;
                localStorage.setItem('userStorage', angular.toJson(userStorage));
                localStorage.setItem('emailStorage', angular.toJson(emailStorage));
                return [201, { valid: true, creationDate: Date.now() }, {}];
            }
        });

    });