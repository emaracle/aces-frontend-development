'use strict';

angular.module('acesApp')
    .factory('visitsByPhu', ['$resource', '$rootScope', '$q', 'hospitals', 'utils','colourList', function ($resource, $rootScope, $q, hospitals, utils, colourList) {

        console.log('visitsByPhu service');
        var path = $rootScope.endpoint + '/secure/classify/:locality/phus/:phuId';
        var visitsByPhu = $resource(path, {
            query: {
                method: 'GET',
                isArray: true
            }
        });

        var getHospitalTitle = function (hospitalID) {
            var title = "";

            _(hospitals.hospitals).each(function (hospital) {
                if (hospital.id === hospitalID) {
                    title = hospital.title;
                }
            });

            return title;
        };

        var collect_categories = function (o, e) {
            o[e.HID];
            return o;
        };
        var unpack = function (phu, id) {
            return { phu: phu, id: id};
        };

        var factory = {};
        factory.all = function (params) {
            return utils.smartQuery({
                qid: "visitsByPhu.all", //this must be unique for each query types
                //will be used to generate unique cacheid
                params: params,
                //whether to cache or not
                //cache if dateto in date range is pass date
                cache: (moment().startOf("day") > params.dateto),
                //this will be called if query is not found in the cache, it must return promise
                query: function () {
                    var _visitsByPhu = [];
                    var defer = $q.defer();
                    // $rootScope.loading = true;
                    var defaultstyle = "solid";
                    visitsByPhu.query(params, function (data, headers) {
                        var numtest = visitsByPhu.length;

                        var sum = {key: "All", style: defaultstyle, color: colourList.getColour(), values: [], total: true};

                        _(data).chain()
                            .map(function (item) {
                                return {key: item.HID, x: item.Date, y: item.Visits, title: "", Class: item.Class};
                            })
                            .groupBy(function (item) {
                                return item.key
                            })
                            .map(function (o, e) {
                                _visitsByPhu.push({key: e, color: colourList.getColour(), style: defaultstyle, values: _(o).map(function (item) {
                                    return {x: item.x, y: item.y, title: getHospitalTitle(item.key), Class: item.Class}
                                })});
                            })
                            .value();

                        console.log("Visits", _visitsByPhu);
                        if (_visitsByPhu.length > 1) {
                            // Calculates totals
                            _(data).chain()
                                .groupBy(function (item) {
                                    return item.Date
                                })
                                .map(function (o, e) {
                                    sum.values.push(
                                        {
                                            y: _.reduce(_.pluck(o, 'Visits'), function (memo, num) {
                                                return memo + num
                                            }),
                                            x: Number(e),
                                            title: "All"});
                                })
                                .value();

                            _visitsByPhu.push(sum);
                        }

                        // $rootScope.loading = false;
                        defer.resolve(_visitsByPhu);
                    });
                    return defer.promise;
                }
            });
        };


        return factory;

    }]);

