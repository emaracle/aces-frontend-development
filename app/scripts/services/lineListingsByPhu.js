'use strict';

angular.module('acesApp')
    .factory('lineListingsByPhu', ['$resource', '$rootScope', '$q', 'utils', function ($resource, $rootScope, $q, utils) {

        console.log('lineListingsByPhu factory');

        var path = $rootScope.endpoint + '/secure/visits/classified-line-listings/:locality/phus/:phuId';
        var lineListingsByPhu = $resource(path, {
            query: {
                method: 'GET',
                isArray: true
            }
        });

        var factory = {};
        factory.all = function (params) {
            return utils.smartQuery({
                qid: "lineListingsByPhu.all", //this must be unique for each query types
                //will be used to generate unique cacheid
                params: params,
                //whether to cache or not
                //cache if dateto in date range is pass date
                cache: (moment().startOf("day") > params.dateto),
                //this will be called if query is not found in the cache, it must return promise
                query: function () {
                    var _lineListingsByPhu = [];
                    var defer = $q.defer();
                    lineListingsByPhu.query(params, function (response) {
                        _(response).each(function (linelisting) {
                            _lineListingsByPhu.push(linelisting);
                        });
                        defer.resolve(_lineListingsByPhu);
                    }, function (error) {
                        defer.reject(error);
                    });

                    return defer.promise;
                }
            });
        };

        return factory;

    }]);
