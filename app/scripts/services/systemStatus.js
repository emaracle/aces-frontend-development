'use strict';
/**
* This service keeps track of proccess running which require system halt
*/
angular.module('acesApp')
    .service('SystemStatus',
        ['$rootScope', '$log', function($rootScope, $log) {
            var pendingProcesses = 0;
            var service = {
                processStarted: function () {
                    pendingProcesses++;
                    $log.debug("SystemStatus: Process Added; Total Processes:" + pendingProcesses);
                    if (pendingProcesses == 1) {
                        if (!$rootScope.$$phase) {
                           $rootScope.$apply();
                        }
                    }                    
                },
                processCompleted: function () {
                    //delay complete to remove flicker 
                    setTimeout(function () {
                        pendingProcesses--;
                        $log.debug("SystemStatus: Process Removed; Total Processes:" + pendingProcesses);
                        if (pendingProcesses == 0) {
                            if (!$rootScope.$$phase) {
                               $rootScope.$apply();
                            }
                        }

                    }, 200);
                },
                busy: function() {
                    return pendingProcesses > 0;
                }
            };
            return service;
        }]);
