'use strict';

angular.module('acesApp')
    .factory('utils', ['$injector', 'Auth', 'colourList', function ($injector, Auth, colourList) {
        //used along with the saved params
        //if the version of the saved parameter is different, it will be discarded
        //change version number everytime parameter structure changes
        var paramVersion = '1.02';

        var $rootScope = $injector.get('$rootScope');
        console.log('utils service');
        var Defaults = {};

        Defaults.Queries = {};
        Defaults.Queries.gender = 'All';
        Defaults.Queries.agefrom = 0;
        Defaults.Queries.ageto = 130;
        Defaults.Queries.dateto = moment().valueOf();
        Defaults.Queries.datefrom = moment(Defaults.Queries.dateto).subtract('days', 20).valueOf();
        Defaults.Queries.ctas = "YYYYYY";
        Defaults.Queries.pagesize = 30;
        Defaults.Queries.classification = {id: "S2014"};
        Defaults.Queries.classifier = {ClassifierID: "ME", Classifier: "Maximum Entropy", ClassifierDesc: ""};
        Defaults.Queries.bucket = {BucketID: "All", Bucket: "All"};
        Defaults.Queries.hospital = {id: "All", title: "All"};
        Defaults.Queries.phu = {};
        Defaults.Queries.class = [{title: "All", id: "All"}];
        Defaults.Queries.classAd = Defaults.Queries.class;
        Defaults.Queries.classEd = Defaults.Queries.class;
        Defaults.Queries.filterByFSA = "All";
        Defaults.Queries.geoType = {id:'phu',title:'PHU'};

        Defaults.Params = {};

        Defaults.Params.locality = "All";
        Defaults.Params.patient = "local";
        Defaults.Params.grouping = "D";
        Defaults.Params.weekStart = 7;
        Defaults.Params.fltrAmType = {id: "All", title: "All"};

        Defaults.Params.resource = {
          id:'Influenza',
          title: 'Monthly Influenza-related Data',
          subtitle:'as reported to NACRS and DAD via CIHI and summarized by the MOHLTC',
          datasource:'National Ambulatory Care Reporting System (NACRS) and the Discharge Abstract Database (DAD), Canadian Institute for Health Information (CIHI). Please note that due to the varied reporting frequencies by hospitals, data is subject to change.'
        };

        Defaults.ClassifiedQueries = {};

        Defaults.localizedVisitsByPhuQueries = {};
        Defaults.localizedVisitsByPhuQueries.locality = "local";
        Defaults.localizedVisitsByPhuQueries.patientOrigin = "all";

        var where = function (list, value) {
          return _.where(list, { id: value });
        };

        var findWhere = function (list, value) {
          return _.findWhere(list, { id: value });
        };

        var filterByProperty = function (target, source) {
          var object = {};
          console.log(target, "target");
          console.log(source, "source");
          object = angular.copy(source);
          for (var key in target) {
            if (!(target[key] === source[key])) {
              var property = {};
              try {
                  property[key] = target[key];
                  _.extend(object, property);
              } catch (err) {
              }
            }
          }
          console.log("finalobject", object);
          return object;
        };

        var calculateAverage = function (days, series) {
          var returnhospitals = [];
          var _hospitals = angular.copy(series);

          if (days === 0) return _hospitals;
          _hospitals.forEach(function (hospital) {

            var h = {
                key: hospital.key + " " + days + " day AVG",
                color: colourList.getColour(),
                dash: "2,4",
                title: hospital.title,
                values: [],
                mvAvg: true
            }

            var movingSum = 0;
            var avg = 0;

            // If user selects max, or is showing fewer days than they've selected, show average number of visits
            if ((days === "max") || (days >= hospital.values.length)) {

                for (var i = 0; i < hospital.values.length; i++)
                  movingSum += hospital.values[i].y;

                avg = movingSum / hospital.values.length;

                for (var i = 0; i < hospital.values.length; i++) {
                  h.values.push({
                    x: hospital.values[i].x,
                    y: avg,
                    title: hospital.values[i].title,
                    Class: hospital.values[i].Class + " day AVG"
                  });
                }

                returnhospitals.push(h);

            } else { // Else show 'moving average'
              for (var i = 0; i < days; i++) {
                  movingSum += hospital.values[i].y;
              }

              avg = movingSum / days;
              h.values.push({
                x: hospital.values[i-1].x,
                y: avg,
                title: hospital.values[i-1].title,
                Class: hospital.values[i-1].Class + " day AVG"
              });

              for (var i = days; i < hospital.values.length; i++) {
                movingSum = movingSum - hospital.values[i - days].y + hospital.values[i].y;
                avg = movingSum / days;
                h.values.push({
                  x: hospital.values[i].x,
                  y: avg,
                  title: hospital.values[i].title,
                  Class: hospital.values[i].Class + " day AVG"
                });

              }
              returnhospitals.push(h);
            }
          });

          return returnhospitals;
        };

        var Normalize = function (series) {
          var _newhospitals = [];
          var _hospitals = angular.copy(series);

          _hospitals.forEach(function (hospital) {
            console.log("hospitalscount", _hospitals.length);

            if (hospital.key !== "All" || _hospitals.length === 1) {
              var h = {
                key: hospital.key + " - Normalized",
                color: colourList.getColour(),
                title: hospital.title,
                values: [],
                mvAvg: true
              }

              for (var i = 0; i < hospital.values.length; i++) {
                var per;
                if (hospital.values[i].z == 0) {
                  per = 0;
                } else {
                  per = (hospital.values[i].y / hospital.values[i].z) * 100;
                }
                h.values.push({
                  x: hospital.values[i].x,
                  y: per,
                  title: hospital.values[i].title,
                  Class: hospital.values[i].Class + ' Normalized'
                });
              }
              _newhospitals.push(h);
            }
          });

          return _newhospitals;
        };

        var RunningStat = function () {
          var m_n = 0;
          var m_oldM, m_newM, m_oldS, m_newS = 0;

          var clear = function () {
            m_n = 0;
          };

          this.push = function (x) {
            m_n++;

            if (m_n == 1) {
              m_oldM = m_newM = x;
              m_oldS = 0.0;
            } else {
              m_newM = m_oldM + (x - m_oldM) / m_n;
              m_newS = m_oldS + (x - m_oldM) * (x - m_newM);

              // set up for next iteration
              m_oldM = m_newM;
              m_oldS = m_newS;
            }
          };

          this.numDataValues = function () {
            return m_n;
          };

          this.mean = function () {
            return (m_n > 0) ? m_newM : 0.0;
          };

          this.variance = function () {
            return ((m_n > 1) ? m_newS / (m_n - 1) : 0.0);
          };

          this.standardDeviation = function () {
            return Math.sqrt(this.variance());
          };
        };

        var standardDeviation = function (series, one, two, days) {
          var _hospitals = angular.copy(series);
          var returnhospitals = [];

          if (days === 0) return _hospitals;
          if (!one && !two) return _hospitals;
          _hospitals.forEach(function (hospital) {

            if (one) {
              var h1 = {
                key: hospital.key + " STD DEV 1 ",
                color: colourList.getColour(),
                dash: "7,7",
                values: [],
                bollinger: true
              };
            }

            if (two) {
              var h2 = {
                key: hospital.key + " STD DEV 2 ",
                color: colourList.getColour(),
                dash: "5,5,10,10,20,10",
                values: [],
                bollinger2: true
              };
            }

            if (hospital.key !== "All Hospitals") {
              for (var i = 0; i <= hospital.values.length - days; i++) {
                var rs = new RunningStat();
                for (var j = i; j < i + days; j++)
                    rs.push(hospital.values[j].y);
                var mean = rs.mean();
                var stdDev = rs.standardDeviation();
                var stdDev2 = 2 * stdDev;
                var bollinger = mean + stdDev;
                var bollinger2 = mean + stdDev2;
                mean = mean;

                if (one) {
                  var v = {
                    x: hospital.values[i - 1 + days].x,
                    y: bollinger,
                    title: hospital.values[i - 1 + days].title,
                    Class: hospital.values[i - 1 + days].Class + " STD 1"
                  };
                  h1.values.push(v);
                }

                if (two) {
                  var v2 = {
                    x: hospital.values[i - 1 + days].x,
                    y: bollinger2,
                    title: hospital.values[i - 1 + days].title,
                    Class: hospital.values[i - 1 + days].Class + " STD 2"
                  };
                  h2.values.push(v2);
                }
              }
            }

            if (one) {
              returnhospitals.push(h1);
            }
            if (two) {
              returnhospitals.push(h2);
            }
          });
          return returnhospitals;
        };

        var saveParams = function(Params, Queries, key){
          //if key is not passed, default it to 'epicurve_params'
          if (typeof key === 'undefined') key = 'epicurve_params_ED';

          var params = {
            Params: Params,
            Queries: Queries,
            version: paramVersion,
            UserUUID: Auth.user.useruuid
          };
          //Save to session storage
          sessionStorage.setItem(key, JSON.stringify(params));
        };

        var getSavedParams = function(key){
          //if key is not passed, default it to 'epicurve_params'
          if (typeof key === 'undefined') key = 'epicurve_params_ED';

          var savedParams = JSON.parse(sessionStorage.getItem(key));
          //if no saved params or version of the saved params is different or saved version is for different user, return default
          if (savedParams == null || savedParams.version !== paramVersion || savedParams.UserUUID !== Auth.user.useruuid) {
            return angular.copy(Defaults);
            // return {
            //     Params: angular.copy(Defaults.Params),
            //     Queries: angular.copy(Defaults.Queries),
            //     version: paramVersion
            // }
          } else {
            return savedParams;
          }
        }

        var patientLocalities = [
          {id: 'local', title: 'Local PHU Patients'},
          {id: 'non-local', title: 'Outside of PHU Patients'},
          {id: 'All', title: 'All Patients'}
        ];

        var hospitalLocalities = [
          {id: 'local', title: 'Local PHU Hospitals'},
          {id: 'non-local', title: 'Outside of PHU Hospitals'},
          {id: 'All', title: 'All Hospitals'}
        ];

        /**
        * @return promise
        *
        * e.g. smartQuery({
        *           qid: "visitsByPhu.all", //this must be unique for each query types
        *           //will be used to generate unique cacheid
        *           params: params,
        *           //whether to cache or not
        *           //cache if dateto in date range is passed date
        *           cache: (moment().startOf("day") > params.dateto),
        *           //this will be called if query is not found in the cache, it must return promise
        *           query: function () {
        *               //perform the query
        *           },
        *           //optional, pingServer if the query is found in cache
        *           //important to extend session as well as to check session
        *           //should be true for most cases
        *           pingServer: true //defaults to true if undefined
        *        });
        */

        var smartQuery = (function () {
          var cachedQueries = {};

          return function ( config ) {
            var cacheId;
            //cache only past dates
            //cache query if dateto is smaller than today
            if (config.cache === true) {
              cacheId = JSON.stringify(config.params);
            }

            if (undefined === cachedQueries[config.qid]) {
              cachedQueries[config.qid] = {};
            }

            //if no cache found
            if (undefined === cacheId || undefined === cachedQueries[config.qid][cacheId]) {
              //call the query callback
              var promise = config.query();
              //cache the promise
              if (undefined !== cacheId) {
                cachedQueries[config.qid][cacheId] = promise;
              }
              //if failed, remove from cache
              promise.then( function() {
                //success, do nothing
                //calling methods will handle it
              }, function (error) {
                //remove from cache
                delete cachedQueries[config.qid][cacheId];
              });
              return promise;
            } else {
              //pingServer unless specifically instructed not to
              if (config.pingServer !== false) {
                //ping server to refresh session
                // and to check if session is valid or not
                Auth.pingServer();
              }
              //return cached promised
              return cachedQueries[config.qid][cacheId];
            };
          };
        })();


        var downloadSvg = function (svg, name, width, height) {
          var dv = $('<div />').append($(svg).attr("width", width).attr("height", height).attr("version", 1.1).attr("xmlns", "http://www.w3.org/2000/svg"));
          var frm = $('<form method="post" action="' + $rootScope.endpoint + '/utils/svgtoimage"><input type="hidden" name="svg"><input type="hidden" name="name" value="' + name + '"></form>');
          $('input[name="svg"]', frm).val(dv.html());
          frm.appendTo("body").submit().remove();
        };

        return {
          where: where,
          findWhere: findWhere,
          filterByProperty: filterByProperty,
          Defaults: Defaults,
          calculateAverage: calculateAverage,
          Normalize: Normalize,
          standardDeviation: standardDeviation,
          saveParams: saveParams,
          // clearSavedParams: clearSavedParams,
          getSavedParams: getSavedParams,

          smartQuery: smartQuery,

          getPatientLocalities: function () {
            return patientLocalities;
          },
          getPatientLocality: function (id) {
            for (var i in patientLocalities) {
              if (patientLocalities[i].id == id) {
                return patientLocalities[i];
              }
            }
          },
          getHospitalLocalities: function () {
            return hospitalLocalities;
          },
          getHospitalLocality: function (id) {
            for (var i in hospitalLocalities) {
              if (hospitalLocalities[i].id == id) {
                return hospitalLocalities[i];
              }
            }
          },
          downloadSvg: downloadSvg
        };
    }])
;
