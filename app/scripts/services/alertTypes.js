'use strict';

angular.module('acesApp')
    .factory('AlertTypes', 
        ['$q', '$http', '$rootScope', '$modal', '$log', 
        function ($q,  $http, $rootScope, $modal, $log) {

        //holds cached alert details
        var defer = null;

        // var phu = $rootScope.phu;

        //returns promise
        //if reload is passed, it will perform a http query regardless of previous status
        var get = function (reload) {
            if (reload === undefined) reload = false;


            if (defer === null || reload) {
                defer = $q.defer();
                $http
                    .get($rootScope.endpoint + '/lookup/alerttypes')
                    .success(function (data, status, headers, config) {
                        defer.resolve(data);
                    })
                    .error( function (data, status, headers, config) {
                        defer.reject(status);
                    });
            }
            return defer.promise;
        };
        
        // Public API here
        return {
            get: get
        };
    }]);
