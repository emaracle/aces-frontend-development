'use strict';

angular.module('acesApp')
    .factory('buckets', ['$rootScope', '$resource', '$q', function ($rootScope, $resource, $q) {

        console.log("Buckets service");


        var all = $resource($rootScope.endpoint + '/lookup/buckets', {
            query: {
                method: 'GET',
                isArray: true
            }
        });

        var fullList = [];
        var run = false;
        return {
            getData: function (classificationID) {
                var resolve = function() {
                    //Create a list and add all to the list
                    var list = [{BucketID: "All", Bucket: "All"}];
                    angular.forEach(fullList, function (item) {
                        var bucket = {
                            BucketID: item.BucketID,
                            Bucket: item.Bucket
                        };
                        // if (classification == undefined || item.ClassificationID == classification) {
                        if (classificationID !== undefined && item.ClassificationID == classificationID) {
                            list.push(bucket);
                        }
                    });

                    defer.resolve(list);
                };

                var defer = $q.defer();
                if (run) {
                    resolve();
                } else {
                    // factory.list = [];
                    all.query({}, function (data) {
                        fullList = data;
                        resolve();
                        run = true;
                    });
                }

                return defer.promise;
            }
        };
    }]);
