'use strict';

angular.module('acesApp')
    .factory('EpiAlerts', ['$q', '$resource', 'utils', 'hospitals', 'alertService', '$rootScope', '$modal', '$log', function ($q,  $resource, utils, hospitals, alertService, $rootScope, $modal, $log) {

        var savedParams = utils.getSavedParams('acesEpiAlertsQueryParams');
        var Queries = savedParams.Queries;
        var Params = savedParams.Params;

        var getParams = function () { 
            return {
                Params: Params,
                Queries: Queries
            }
        };
        
        var setParams = function (p) {
            Queries = p.Queries;
            Params = p.Params;

            utils.saveParams(Params, Queries, 'acesEpiAlertsQueryParams');
            return this;
        };

        //last cached getAlerts query
        var lastGetQuery = {
            qid: undefined,
            defer: undefined
        };

        var getAlerts = function (reload) {
            //reload result from server even if cache is available
            //by default uses cache if available
            if (typeof reload === 'undefined') reload = true;

            var queries = utils.filterByProperty(Queries, utils.Defaults.Queries);
            var totalParams = _.extend(queries, Params);
            totalParams = _.extend(totalParams, {classifier: queries.classifier.ClassifierID, classification: queries.classification.id, class: queries.class.id, bucket: Queries.bucket.BucketID, hospital: Queries.hospital.id});

            var defer;
            var qid = JSON.stringify(totalParams);
            
            if (lastGetQuery.qid == qid && !reload) {
                defer = lastGetQuery.defer;
            } else {
                defer = $q.defer();

                var path = $rootScope.endpoint + '/secure/alerts/epi/phus/:phuId';
                // var path = 'http://km-dev:8000' + '/secure/alerts/epi/phus/:phuId';
                var q = $resource(path, {
                    query: {
                        method: 'GET',
                        isArray: true
                    }
                }).query(totalParams, function (data, headers) {
                    defer.resolve(data);
                });;

                //cache
                lastGetQuery.qid = qid;
                lastGetQuery.defer = defer;
            }
            return defer.promise;
        };

        // Public API here
        return {
            getParams: getParams,
            setParams: setParams,
            getAlerts: getAlerts
        };
    }]);
