'use strict';

angular.module('acesApp')
    .factory('classifiers', ['$resource', '$q', '$rootScope', function ($resource, $q, $rootScope) {
        // Service logic
        // ...
        console.log('classifiers service');
        var run = false;

//        var all = $resource('http://beta.kflaphi.ca\\:8080/AcesEpiEndpoint/aces/lookup/classifiers', {
        var all = $resource($rootScope.endpoint + '/lookup/classifiers', {
            query: {
                method: 'GET',
                isArray: true
            }
        });

        var factory = {};
        factory.list = [];
        factory.getData = function () {
            var defer = $q.defer();
            if (run) {
                defer.resolve(factory);
                return defer.promise;
            }
            factory.list = [];
            all.query({}, function (data) {
                _(data).each(function (item) {
                    factory.list.push(item);
                });
                defer.resolve(factory);
                run = true;
            });
            return defer.promise;
        };

        factory.getDataByID = function (id) {
            var defer = $q.defer();
            factory.getData().then(function (data) {
                for (var i in data.list) {
                    if (data.list[i].ClassifierID == id) {
                        defer.resolve(data.list[i]);
                        return;
                    }
                }

                //not found
                defer.reject('Failed to find the item');
            }, function (result) {
                defer.reject(result);
            });
            return defer.promise;  
        };

        return factory;
    }]);
