'use strict';

angular.module('acesApp')
    .factory('Profile', ['$rootScope', '$q', '$http', '$log', 'Auth', 'AuthToken', 'utils', function ($rootScope, $q,  $http, $log, Auth, AuthToken, utils) {

        //
        var changePassword = {
            //Validates if pswd passed is current password
            //It only validates pswd against one save during login
            validateCurrentPassword: function (pswd) {
                //pswd saved in Auth.user.pswd is double hashed
                return Auth.getHash(Auth.getHash(pswd)) === Auth.user.pswd;
            },

            //Validate password conformation
            //http://www.w3resource.com/javascript/form/password-validation.php
            //To check a password with minimum 6 characters which contain at least one lowercase letter, one uppercase letter, one numeric digit, and one special character
            //@todo check for user's account name and parts of the user's full name that exceed two consecutive characters
            validatePasswordFormat: function (pwd) {
                var r = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{6,}$/;
                if (pwd.match(r) === null) {
                    return true;
                } else {
                    return false;
                }
            },
            //
            do: function (newPassword, username, token) {

                var data = $.param({
                    userid: username,
                    pswd: Auth.getHash(newPassword),
                    token: token //AuthToken.get()
                });

                var url = $rootScope.endpoint + '/authenticate/changePassword';

                return $http({
                    method: 'POST',
                    url: url,
                    data: data,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                    custom: {
                        //Instruct AuthHttpInterceptor not to handle 403. We will handle it in fail handler
                        //AuthHttpInterceptor will not show "Forbidden" message
                        handles: [403]
                    }
                })
                .success(function () {
                    if (Auth.forcePasswordChange()) {
                        if (typeof Auth.getSetting("forcePasswordChange",false) !== "undefined"){
                            //AppSettings are set by endpoint (pass false)
                            Auth.setSetting("forcePasswordChange", false, false);
                        } else {
                            //set the force password change in user settings to false
                            Auth.setSetting("forcePasswordChange", false, true);
                        }
                        
                    }
                    //Update the session with double hashed pass
                    Auth.updateSession(Auth.getHash(Auth.getHash(newPassword)));
                });

            },
            redirectTo: '' //State name to redirect to after password is changed successfully
        };

        var forgotPassword = {
            do: function (email) {

                var data = $.param({
                    email: email,
                    //replace hash from forgot password to resetpassword
                    changePwdURL: location.href.replace('#/forgotpassword', '#/resetpassword')
                });

                var url = $rootScope.endpoint + '/authenticate/forgotPassword';

                return $http({
                    method: 'POST',
                    url: url,
                    data: data,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                });
            }
        };

        var keywordAlerts = {
            do: function (userid,token) {

                var url = $rootScope.endpoint + '/secure/custom-user-keyword-alerts/' + userid;

                return $http({
                    method: 'GET',
                    url: url,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                });
            }
        };

        var addKeyword = {
            do: function (userid,token,keyword,period,threshold,phu,hospitalid,visittype) {

                var data = $.param({
                    keyword: keyword,
                    period: period,
                    threshold: threshold,
                    phu: phu,
                    hospitalid: hospitalid,
                    visittype:visittype 
                });

                var url = $rootScope.endpoint + '/secure/custom-user-keyword-alerts/' + userid + '/add';

                return $http({
                    method: 'POST',
                    url: url,
                    data: data,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                });
            }
        };

        var updateKeyword = {
            do: function (userid,token,keywordUUID,keyword,period,threshold,phu,hospitalid,visittype) {

                var data = $.param({
                    keyword: keyword,
                    period: period,
                    threshold: threshold,
                    phu: phu,
                    hospitalid: hospitalid,
                    visittype:visittype 
                });

                var url = $rootScope.endpoint + '/secure/custom-user-keyword-alerts/' + userid + '/update/' + keywordUUID;

                return $http({
                    method: 'POST',
                    url: url,
                    data: data,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                });
            }
        };

        var updateKeywordActiveStatus = {
            do: function (userid,token,keywordUUID,active) {

                var data = $.param({
                    active: active
                });

                var url = $rootScope.endpoint + '/secure/custom-user-keyword-alerts/' + userid + '/updateActiveStatus/' + keywordUUID;

                return $http({
                    method: 'POST',
                    url: url,
                    data: data,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                });
            }
        };

        var deleteKeyword = {
            do: function (userid,token,keywordUUID) {

                var url = $rootScope.endpoint + '/secure/custom-user-keyword-alerts/' + userid + '/delete/' + keywordUUID;

                return $http({
                    method: 'POST',
                    url: url,
                    headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                });
            }
        };

        var keywordNotifications = {
            get: function (keywordUUID) {
                var url = $rootScope.endpoint + '/secure/custom-user-keyword-alerts/notifications/' + keywordUUID;
                return $q(function (resolve, reject) {
                    var data = [];
                    $http({
                        method: 'GET',
                        url: url
                    }).then(function (response) {
                        resolve(response.data);
                    }, function (response) {
                        reject(response);
                    });
                });
            },
            getNotificationDetail: function (alertUUID) {
                return utils.smartQuery({
                    qid: "Profile.getNotificationDetail", //this must be unique for each query types
                    //will be used to generate unique cacheid
                    params: alertUUID,
                    //cache the query
                    cache: true,
                    //this will be called if query is not found in the cache, it must return promise
                    query: function () {
                        var defer = $q.defer();
                        var url = $rootScope.endpoint + '/secure/custom-user-keyword-alerts/notification/' + alertUUID;
                        $http({
                            method: 'GET',
                            url: url
                        }).then(function (response) {
                            var data = response.data;
                            //add isAdmission field to all the rows
                            //also add VisitTimestamp which will be used to sort the merged data
                            angular.forEach(data, function(row) {
                                console.log(data);
                                row.IsAdmission = (row.VisitType == 'ad');
                                row.VisitTimestamp = moment(row.VisitDate + ' ' + row.VisitTime).valueOf();
                                //used for group by time
                                row.VisitTimeHr = moment(row.VisitDate + ' ' + row.VisitTime).format("HH");
                            });
                            defer.resolve(data);
                        }, function (response) {
                            defer.reject(response);
                        });
                    
                        return defer.promise;                        
                    }
                });
            }
        };

        // Public API here
        return {
            changePassword: changePassword,
            forgotPassword: forgotPassword,
            keywordAlerts: keywordAlerts,
            addKeyword: addKeyword,
            updateKeyword: updateKeyword,
            updateKeywordActiveStatus: updateKeywordActiveStatus,
            deleteKeyword: deleteKeyword,
            keywordNotifications: keywordNotifications
        };
    }]);



