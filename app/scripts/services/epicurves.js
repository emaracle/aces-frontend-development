'use strict';

angular.module('acesApp')
  .factory('epicurves', ['$rootScope', 'visitsByPhu', '$q', 'utils', 'lineListings', 'ctas', 'classifications', 'classifiers', 'classifiedVisitsByPhu', 'hospitals', 'localizedVisitsByPhu', 'Auth', 'fsas', 'alertService', 'classifiedByProbabilityVisitsByPhu', 'buckets', 'colourList', '$stateParams', function
    ($rootScope, visitsByPhu, $q, utils, lineListings, ctas, classifications, classifiers, classifiedVisitsByPhu, hospitals, localizedVisitsByPhu, Auth, fsas, alertService, classifiedByProbabilityVisitsByPhu, buckets, colourList, $stateParams) {

      console.log("EpiLoginService", Auth.user.token);

      var savedParams, Queries, Params;
      var phu;
      var ClassifiedQueries;
      var ClassifiedByProbabilityParams;
      var localizedVisitsByPhuQueries;
      var Visits;
      var CTAS;

      //get the extra days to calculate it standard deviation and moving averages
      var addedExtraDays = 29; //29 days which will be used to calculate
      var doAddedExtraDays = function (totalParams) {
        //if extra days required, change the datefrom value
        if (addedExtraDays > 0) {
          totalParams.datefrom = moment(totalParams.datefrom).subtract(addedExtraDays, "days").valueOf();
        }
      };

      //hides extra days from graph
      var doHideExtraDays = function (originalDateFrom, arr) {
        //if extra days added, remove them from the array. Extra days are used only to calculate moving average and stddv.
        if (addedExtraDays > 0) {
          arr = _.map(arr, function (o) {
            o.originalDateFrom = originalDateFrom;
            if (undefined === o.hiddenValues) {
              o.hiddenValues = [];
            }
            o.values = _.filter(o.values, function (d) {
              if (d.x >= originalDateFrom) {
                return true;
              } else {
                o.hiddenValues.push(d);
                return false;
              }
            });
            return o;
          });
        }
      };

      //removes unnecessary parameters before sending to server
      var cleanParams = function (params) {
        //remove normalize, movingAvg, std1 and std2 from Params
        var removeList = ['movingAverage', 'std1', 'std2', 'normalize'];
        for(var i in removeList) {
          if (undefined !== params[removeList[i]]) {
            delete params[removeList[i]];
          }
        }
      };

      //adds back hidden days to the main array
      //returns originalDateFrom or null
      var doRestoreHiddenExtraDays = function (arr) {
        if (addedExtraDays > 0) {
          var originalDateFrom;
          arr = _.map(arr, function (o) {
            if (undefined !== o.hiddenValues) {
              originalDateFrom = o.originalDateFrom;
              o.values = o.hiddenValues.concat(o.values);
              delete o.hiddenValues;
            }
            return o;
          });
          return originalDateFrom;
        }
        return undefined;
      };

      //resets the colour
      var previousColours = {};
      //returns colour previously assigned to the line/key
      var getPreviousColourAssignedForKey = function (key) {
        var colour;
        _.each(previousColours, function (v, k) {
          if (v == key) {
            colour = k;
          }
        });
        return colour;
      };

      var keyExistsInArray = function (key, arr) {
        var i = _.find(arr, function (o) {
          return o.key == key;
        });
        return i !== undefined;
      };

      var resetColour = function (arr) {
        var newColours = {};
        //reset the colour index to 0
        $rootScope.colourIndex = 0;
        _.each(arr, function (o, i) {
          //check if the key has colour assigned in previous run
          var colour = getPreviousColourAssignedForKey(o.key);

          if (colour != undefined) {
            o.color = colour;
            newColours[colour] = o.key;
          } else {
            var i = 0;
            while(true) {
              colour = colourList.getColour();
              //check if colour assigned to other line/key previously
              //or that key doesn't exists in the current array anymore
              if (previousColours[colour] == undefined || !keyExistsInArray(previousColours[colour], arr)) {
                o.color = colour;
                newColours[colour] = o.key;
                break;
              }

              //prevent infinite loop
              i++
              if (i > colourList.getLength()) {
                //assign the colour anyway
                o.color = colour;
                console.log('test', 'Exiting loop');
                break;
              }
            }
          }
        });

        previousColours = newColours;
      };

      var initParam = function(type){
        savedParams = utils.getSavedParams('epicurve_params_' + type);
        Queries = savedParams.Queries;
        Params = savedParams.Params;

        if (undefined === savedParams.Params.phuId) {
          phu = $rootScope.phu;
          Params.phuId = phu.id;
          Queries.phu = phu;
        }
        
        fsas.getData({phu: Params.phuId});
        console.log("TESTFSA", fsas.fsas);
        ClassifiedQueries = angular.copy(utils.Defaults.ClassifiedQueries);
        ClassifiedByProbabilityParams = {};
        localizedVisitsByPhuQueries = angular.copy(utils.Defaults.localizedVisitsByPhuQueries);

        reassignReturnObj();
      };

      var init = function () {
        initParam('ED');
        Visits = {};
        Visits.all = [];
        Visits.source = [];
        Visits.test = [];
        Visits.ma = [];
        Visits.std = [];

        Visits.get = function (std1, std2, stddays, ma) {
          // debugger;
          console.log("QUERIES", Queries);
          var queries = utils.filterByProperty(Queries, utils.Defaults.Queries);
          var totalParams = _.extend(queries, Params);

          //save to session
          Params.phuId = $rootScope.phu.id;
          totalParams = _.extend(totalParams, {classifier: Queries.classifier.ClassifierID, classification: Queries.classification.id, class: Queries.class.id, bucket: Queries.bucket.BucketID, hospital: Queries.hospital.id});
          totalParams = _.extend(totalParams, Params);
          var defer = $q.defer();
          var originalDateFrom = totalParams.datefrom;

          //if grouping by day and extra days required, change the datefrom value
          if (Params.grouping == 'D'){
              doAddedExtraDays(totalParams);
          };

          cleanParams(totalParams);
          totalParams.fltrAmType = totalParams.fltrAmType.id;

          visitsByPhu.all(totalParams).then(function (data, status, headers) {
            //work on copy
            data = angular.copy(data);
            //Filter hospitals
            if (Queries.hospital.id != 'All'){
              data = _.filter(data, function (o) {
                //change key to hospital id if it is all
                if (o.key == 'All') {
                  o.key = Queries.hospital.id;
                  //change title for all the values which will be "All Hospitals" to the hosiptal title
                  o.values = _.map(o.values, function (d) { d.title = Queries.hospital.title; return d });
                }
                return o.key === Queries.hospital.id;
              });
            }

            defer.resolve(data);
            Visits.all = data;
            Visits.source = data;
            Visits.test = data;
            Visits.standardDeviation(std1, std2, stddays);
            Visits.movingAverage(ma);

            //If grouping is by Day, hide extra days. Extra days are used only to calculate moving average and stddv.
            if (Params.grouping == 'D'){
                doHideExtraDays(originalDateFrom, Visits.all);
            };
            //reset the colours
            resetColour(Visits.all);

          }, function (error) {
            alertService.add('danger', 'Failed to get data' + error);
          });

          return defer.promise;
        };

        CTAS = ctas;

        Visits.getClassified = function (std1, std2, stddays, ma) {
          //Create comma-seperated string from class array
          var classList = "";
          for (var i in Queries.class){
            if (i < 1){
              classList = Queries.class[i].id;
            } else {
              classList = classList + "," + Queries.class[i].id;
            }
          }

          var queries = utils.filterByProperty(Queries, utils.Defaults.Queries);
          var totalParams = _.extend(queries, Params);
          totalParams = _.extend(totalParams, {classifier: Queries.classifier.ClassifierID, classification: Queries.classification.id, class: classList, bucket: Queries.bucket.BucketID, hospital: Queries.hospital.id});
          var originalDateFrom = totalParams.datefrom;

          //if grouping by day and extra days required, change the datefrom value
          if (Params.grouping == 'D'){
            doAddedExtraDays(totalParams);
          };

          totalParams.fltrAmType = totalParams.fltrAmType.id;
          cleanParams(totalParams);
          var defer = $q.defer();
          classifiedVisitsByPhu.all(totalParams).then(function (data) {
            //work on copy
            data = angular.copy(data);
            //If query specific to a hospital
            //@see ACES-91
            if (Queries.hospital.id != 'All'){
              data = _.map(data, function (o) {
                //change key to hospital id if it is all
                if (o.key == 'All') {
                  o.key = Queries.hospital.id;
                  //change title for all the values which will be "All Hospitals" to the hosiptal title
                  o.values = _.map(o.values, function (d) { d.title = Queries.hospital.title; return d });
                }
                return o;
              });
            }

            defer.resolve(data);
            Visits.all = data;
            Visits.source = data;
            Visits.standardDeviation(std1, std2, stddays);
            Visits.movingAverage(ma);

            //If grouping is by Day, hide extra days. Extra days are used only to calculate moving average and stddv.
            if (Params.grouping == 'D'){
              doHideExtraDays(originalDateFrom, Visits.all);
            };
            //reset the colours
            resetColour(Visits.all);

          }, function (error) {
              alertService.add('danger', 'Failed to get data' + error);
          });

          return defer.promise;
        };

        Visits.getClassifiedByProbability = function () {
          var queries = utils.filterByProperty(Queries, utils.Defaults.Queries);
          var totalParams = _.extend(queries, Params);
          totalParams = _.extend(totalParams, ClassifiedByProbabilityParams);
          var defer = $q.defer();
          classifiedByProbabilityVisitsByPhu.all(totalParams).then(function (data) {
            defer.resolve(data);
            Visits.all = data;
            Visits.source = data;
          }, function (error) {
            alertService.add('danger', 'Failed to get data' + error);
          });
          return defer.promise;
        };

        Visits.getlocalizedVisits = function () {
          if (localizedVisitsByPhuQueries.patientOrigin === 'all') {
            return Visits.get();
          } else {
            var queries = utils.filterByProperty(Queries, utils.Defaults.Queries);
            var totalParams = _.extend(queries, Params);
            totalParams = _.extend(totalParams, localizedVisitsByPhuQueries);
            var defer = $q.defer();
            localizedVisitsByPhu.all(totalParams).then(function (data) {
              defer.resolve(data);
              Visits.all = data;
            }, function (error) {
                alertService.add('danger', 'Failed to get data' + error);
            });
            return defer.promise;
          }
        };

        Visits.movingAverage = function (days) {
          //Visits.all=Visits.test;
          if (days === 0) {
            Visits.ma = [];
            Visits.all = Visits.source.concat(Visits.std);
          } else {
            var originalDateFrom = doRestoreHiddenExtraDays(Visits.source);
            Visits.ma = utils.calculateAverage(days, Visits.source);
            Visits.all = Visits.source.concat(Visits.ma, Visits.std);
            if (undefined !== originalDateFrom) {
              doHideExtraDays(originalDateFrom, Visits.all);
            }
            //reset the colours
            resetColour(Visits.all);
          }
        };

        Visits.Normalize = function (nm) {
          console.log("normalize", nm);
          //Visits.all = Visits.source;
          if (nm !== 0) {
            Visits.all = utils.Normalize(Visits.source);
          } else {
            Visits.all = Visits.source.concat(Visits.ma, Visits.std);
          }
          console.log("MA", Visits.all);
        };

        Visits.getFsas = function (fsaphu) {
          fsas.getData({phu: fsaphu});
          console.log("TESTFSA", fsas.fsas);
        };

        Visits.standardDeviation = function (std1, std2, days) {
          if (!std1 && !std2) {
            Visits.std = [];
            Visits.all = Visits.source.concat(Visits.ma);
          } else {
            var originalDateFrom = doRestoreHiddenExtraDays(Visits.source);
            Visits.std = utils.standardDeviation(Visits.source, std1, std2, days);
            Visits.all = Visits.source.concat(Visits.std, Visits.ma);
            if (undefined !== originalDateFrom) {
              doHideExtraDays(originalDateFrom, Visits.all);
            }

            //reset the colours
            resetColour(Visits.all);
          }
        };
      };

      //clear saved params when logout
      $rootScope.$on("logout", function(){
        init();
      });

      var saveParams = function() {
        if (undefined !== Params['type']) {
          utils.saveParams(Params, Queries, 'epicurve_params_' + Params['type']);
        }
      };

      var returnObj = {};
      var reassignReturnObj = function () {
        returnObj.Params = Params;
        returnObj.Queries = Queries;
        returnObj.initParams = initParam;
        returnObj.Visits = Visits;
        returnObj.CTAS = CTAS;
        returnObj.classifications = classifications;
        returnObj.classifiers = classifiers;
        returnObj.phu = phu;
        returnObj.localizedVisitsByPhuQueries = localizedVisitsByPhuQueries;
        returnObj.saveParams = saveParams;
        returnObj.getDefault = function () {return angular.copy(utils.Defaults);};
        returnObj.getPatientLocality = function (id) {return utils.getPatientLocality(id);};
        returnObj.getHospitalLocality = function (id) {return utils.getHospitalLocality(id);};
      }

      init();

      return returnObj;
  }]
);
