'use strict';

angular.module('acesApp')
    .factory('Alerts', ['AlertsByPhu', '$q', '$http', 'utils', 'hospitals', 'alertService', '$rootScope', '$modal', '$log', function (AlertsByPhu, $q,  $http, utils, hospitals, alertService, $rootScope, $modal, $log) {
        var Defaults = {
            Params: {},
            Queries: {
                dateto: moment().valueOf(),
                datefrom: moment().subtract('days', 7).valueOf(),
                alertTypes: ['CUSUM1', 'Trend', 'Extreme'],
                syndromes: [ "RESP", "GASTRO", "ILI", "ENVIRO", "TOX", "MH", "AST", "EOH", "OPI", "DERM", "CROUP", "SEP", "CELL", "BRONCH", "COPD" ]
            }
        };

        //   var Queries = angular.copy(utils.Defaults.Queries);
        //   var Params = angular.copy(utils.Defaults.Params);
        //var Queries = {};

        //holds cached alert details
        var alertDetails = {};
        // Function to get details of an alert
        function getAlertData(alertId) {
            if (alertDetails[alertId] === undefined) {
                var deferred = $q.defer();
                $http({
                    url: $rootScope.endpoint + '/secure/alerts/alertid/' + alertId,
                    method: 'GET',
                    transformResponse: function (data) {
                        var x2js = new X2JS();
                        var j = x2js.xml_str2json(data);
                        //if just one row, the above statement converts row property to object instead of array
                        //convert it to array
                        if (j.xml.row.XML !== undefined) {
                            j.xml.row = [j.xml.row];
                        }
                        for(var i in j.xml.row) {
                            for(var k in j.xml.row[i].XML) {
                                j.xml.row[i].data = j.xml.row[i].XML[k];
                                break;
                            }
                        }
                        //return the rows
                        return j.xml.row;
                    }
                })
                    .success(function (rows, status, headers, config) {
                        var results = {};
                        results.headers = headers();
                        results.status = status;
                        results.config = config;
                        results.rows = rows;

                        deferred.resolve(results);
                    }).
                    error(function (data, status, headers, config) {
                        deferred.reject(data, status, headers, config);
                        //remove from cache
                        delete alertDetails[alertId];
                    });
                alertDetails[alertId] = deferred

            } else {
                var deferred = alertDetails[alertId];
            }
            return deferred.promise;
        }


        // var phu = $rootScope.phu;

        var savedParams = utils.getSavedParams('acesAlertsQueryParams');
        var Queries = angular.extend({}, Defaults.Queries, savedParams.Queries);
        var Params = angular.extend({}, Defaults.Params, savedParams.Params);

        var openAlertDetailModal = function(row) {
            var alertID = row.AlertUUID;
            var modalInstance;
            // Initialize arrays
            var processData = function (allData) {
                var data = allData.alertGraph;
                //if Trend
                var highLightedPoints = {
                    Extreme: 1,
                    Bias: 9,
                    Trend: 6,
                    Oscillation: 14,
                    OnEdge: 3,
                    Tendency: 5,
                    Ignorance: 15,
                    NMG: 8,
                    NRC: 10
                };

                data.forEach( function(o) {
                    //remove NaN values
                    o.values = _.filter(o.values, function (v) {
                            return !isNaN(v[1]);
                        });

                    if ("Actual" === o.key || "CuSum" === o.key) {
                        var l = o.values.length;
                        //add class to last point
                        o.values[l - 1].class = "alert-end-point";
                        var alertTypeID = allData.rowData.AlertTypeID;
                        if (undefined !== highLightedPoints[alertTypeID]) {
                            //skip last
                            for(var i = highLightedPoints[alertTypeID]; i > 0; i--) {
                                o.values[l - i].class = (o.values[l - i].class ? o.values[l - i].class : "") + " alert-highlight-point";
                            }
                        }
                    }
                });

                allData.alertGraph = data;
                return allData;
            };


            // Function used to sort by timestamp
            function timestamp(a, b) {
                if (a[0] < b[0]) return -1;
                if (a[0] > b[0]) return 1;
                return 0;
            }
            if (row.AlertFamilyID == "CUSUM") {
                modalInstance = $modal.open({
                    templateUrl: 'views/alertModal.html?v=' + encodeURIComponent(ACES.buildnumber),
                    controller: 'AlertModalCtrl',
                    size: 'lg',
                    resolve: {
                        'alertGraphs': function () {
                            var deferred = $q.defer();
                            var alertOn = moment(row.AlertOn, "YYYY-MM-DD HH:mm:ss").valueOf(); //2015-05-20 20:38:00
                            var thresholdValue = 3;
                            //set the threshold value to 4 if the alert is before may 27 11:05 am
                            if (alertOn <= moment("2015-05-27 11:05", "YYYY-MM-DD HH:mm").valueOf()) {
                                thresholdValue = 4;
                            }
                            getAlertData(alertID).then(
                                function (success) {
                                    var alertGraphs = [];
                                    for(var r in success.rows) {
                                        // console.log(success);
                                        // console.log('success', success.rows[r].data);
                                        var data = success.rows[r].data;
                                        //plot the last 2 weeks of the day 'Triggered On' only (14 days)
                                        var days = 14;
                                        if (data.length > days) {
                                            data.splice(days, data.length - days);
                                        }
                                        var alertDetails = [];
                                        var alertGraph = [];
                                        var alertGraphVisits = [];
                                        //Cycle through the data returned by endpoint, add to array via switch
                                        angular.forEach(data, function (item) {

                                            // Turn date value into timestamp, which is needed by NVD3 for mapping dates
                                            var visitDate = moment(item._dtAdm, "YYYY-MM-DD").valueOf();
                                            var cusumValue = parseFloat(item._CUSUM);
                                            var visitCount = parseInt(item._Visits);

                                            if (isNaN(cusumValue)) {
                                                cusumValue = 0;
                                            }

                                            alertGraph.push(
                                            {
                                                0: visitDate,
                                                1: cusumValue
                                            });
                                            alertGraphVisits.push(
                                            {
                                                0: visitDate,
                                                1: visitCount
                                            });
                                            alertDetails.push(item);
                                        });

                                        var allValues = [alertGraph, alertGraphVisits];
                                        //Sort all arrays by timestamp.
                                        angular.forEach(allValues, function (array) {
                                            array.sort(timestamp);
                                        });

                                        // Create scope model to be used for the chart

                                        var allData =
                                        {
                                            'alertGraphCusumVisits': [
                                                {
                                                    "key": "Visits",
                                                    "showPoint": true,
                                                    "color": "#000000",
                                                    "values": alertGraphVisits
                                                }
                                            ],
                                            'alertGraph': [
                                                {
                                                    "key": "CuSum",
                                                    "values": alertGraph
                                                },
                                                {
                                                    "key": "Threshold",
                                                    dash: "8,4",
                                                    color: "#817D7D",
                                                    "values": _.map(alertGraph, function (d) { return {0:d[0], 1:thresholdValue, interactive: false}; })
                                                }
                                            ],
                                            'alertDetails': alertDetails,
                                            'rowData': row
                                        };
                                        alertGraphs.push({
                                            alertRow: success.rows[r],
                                            precessedData: processData(allData)
                                        });
                                        // console.log("allData", allData);
                                        //deferred.resolve(processData(allData));
                                    }
                                    deferred.resolve(alertGraphs);
                                },
                                function (error) {

                                }
                            );
                            return deferred.promise;
                        }
                    }
                });

            }
            else { // SPC ALERT
                modalInstance = $modal.open({
                    templateUrl: 'views/alertModal.html',
                    controller: 'AlertModalCtrl',
                    size: 'lg',
                    resolve: {
                        'alertGraphs': function () {
                            var deferred = $q.defer();
                            getAlertData(alertID).then(
                                function (success) {
                                    console.log(success);
                                    var alertGraphs = [];
                                    for(var r in success.rows) {
                                        // console.log('success', success.rows[r].data);
                                        var data = success.rows[r].data;
                                        // console.log('data init', data);
                                        //plot the last 3 weeks of the day 'Triggered On' only (21 days)
                                        var days = 21;
                                        if (data.length > days) {
                                            data.splice(days, data.length - days);
                                        }
                                        var uclValues = [];
                                        var ucl2Values = [];
                                        var ucl3Values = [];
                                        var actualValues = [];
                                        var lclValues = [];
                                        var lcl2Values = [];
                                        var lcl3Values = [];
                                        var averageValues = [];
                                        var alertDetails = [];
                                        //Cycle through the data returned by endpoint, add to array via switch
                                        angular.forEach(data, function (item) {
                                            // Turn date value into timestamp, which is needed by NVD3 for mapping dates
                                            var visitDate = moment(item._dtAdm, "YYYY-MM-DD").valueOf();
                                            var actual = parseInt(item._Visits);
                                            var ucl = parseFloat(item._UCL);
                                            var ucl2 = parseFloat(item._U2CL);
                                            var ucl3 = parseFloat(item._U3CL);
                                            var lcl = parseFloat(item._NUCL);
                                            var lcl2 = parseFloat(item._N2UCL);
                                            var lcl3 = parseFloat(item._N3UCL);
                                            var average = parseFloat(item._xVisits);

                                            uclValues.push(
                                                {
                                                    0: visitDate,
                                                    1: ucl
                                                }
                                            );
                                            ucl2Values.push(
                                                {
                                                    0: visitDate,
                                                    1: ucl2
                                                }
                                            );
                                            ucl3Values.push(
                                                {
                                                    0: visitDate,
                                                    1: ucl3
                                                }
                                            );
                                            actualValues.push(
                                                {
                                                    0: visitDate,
                                                    1: actual
                                                }
                                            );
                                            lclValues.push(
                                                {
                                                    0: visitDate,
                                                    1: lcl
                                                }
                                            );
                                            lcl2Values.push(
                                                {
                                                    0: visitDate,
                                                    1: lcl2
                                                }
                                            );
                                            lcl3Values.push(
                                                {
                                                    0: visitDate,
                                                    1: lcl3
                                                }
                                            );
                                            averageValues.push(
                                                {
                                                    0: visitDate,
                                                    1: average
                                                }
                                            );

                                            alertDetails.push(item);
                                        });



                                        var allValues = [averageValues, uclValues, ucl2Values, ucl3Values, actualValues, lclValues, lcl2Values, lcl3Values];
                                        //Sort all arrays by timestamp.
                                        angular.forEach(allValues, function (arr) {
                                            arr.sort(timestamp);
                                        });

                                        //Create scope model to be used for the chart

                                        var allData =
                                        {
                                            'alertGraph': [
                                                {
                                                    "key": "Actual",
                                                    "color": "black",
                                                    "shape": "circle",
                                                    "showPoint": true,
                                                    "values": actualValues
                                                },
                                                {

                                                    "key": "Average",
                                                    "color": "#19A3FF",
                                                    "values": averageValues
                                                },
                                                {
                                                    "key": "UCL",
                                                    "color": '#00FF00',
                                                    "dash": "3,9",
                                                    "values": uclValues
                                                },
                                                {
                                                    "key": "UCL2",
                                                    "color": '#00CC00',
                                                    "dash": "15,9",
                                                    "values": ucl2Values
                                                },
                                                {
                                                    "key": "UCL3",
                                                    "color": '#006600',
                                                    "values": ucl3Values
                                                },
                                                {
                                                    "key": "LCL",
                                                    "color": '#F2F2F2',
                                                    "dash": "6,9",
                                                    "values": lclValues
                                                },
                                                {
                                                    "key": "LCL2",
                                                    "color": '#E0E0E0',
                                                    "dash": "15,9",
                                                    "values": lcl2Values
                                                },
                                                {
                                                    "key": "LCL3",
                                                    "color": '#E3E3E3',
                                                    "values": lcl3Values
                                                }
                                            ],
                                            'alertDetails': alertDetails,
                                            'rowData': row
                                        };

                                        alertGraphs.push({
                                            alertRow: success.rows[r],
                                            precessedData: processData(allData)
                                        });
                                    };

                                    deferred.resolve(alertGraphs);
                                },
                                function (error) {

                                }
                            );
                            return deferred.promise;
                        }
                    }
                });
            }

            // This function allows us to get at the result of user clicking OK or CANCEL
            modalInstance.result.then(function () {
                $log.info('Modal OKAYED');
            }, function () {
                $log.info('Modal cancelled');
            });
        };

        var getParams = function () {
            return {
                Params: Params,
                Queries: Queries
            }
        };

        var setParams = function (p) {
            Queries = p.Queries;
            Params = p.Params;

            utils.saveParams(Params, Queries, 'acesAlertsQueryParams');
        };

        //last cached getVisits query
        var lastGetVisitsQuery = {
            qid: undefined,
            defer: undefined
        };

        var getVisits = function (reload) {
            //reload result from server even if cache is available
            //by default uses cache if available
            if (typeof reload === 'undefined') reload = false;

            var queries = utils.filterByProperty(Queries, Defaults.Queries);
            var totalParams = _.extend(queries, Params);
            // totalParams = _.extend(totalParams, {classifier: queries.classifier.ClassifierID, classification: queries.classification.id, class: queries.class.id, bucket: Queries.bucket.BucketID, hospital: Queries.hospital.id});
            totalParams = _.extend(totalParams, {hospital: Queries.hospital.id});

            //convert alertTypes from array to comma delimited string
            if (undefined !== totalParams.alertTypes) {
                totalParams.alertTypes = totalParams.alertTypes.join();
            }
            //convert syndromes from array to comma delimited string
            if (undefined !== totalParams.syndromes) {
                totalParams.syndromes = totalParams.syndromes.join();
            }

            var defer;
            var qid = JSON.stringify(totalParams);
            if (lastGetVisitsQuery.qid == qid && !reload) {
                defer = lastGetVisitsQuery.defer;
            } else {
                defer = $q.defer();
                AlertsByPhu.all(totalParams).then(function (data, headers) {
                    defer.resolve(data);
                }, function(error) {
                    alertService.add('danger', 'Failed to get data' + error);
                });

                //cache
                lastGetVisitsQuery.qid = qid;
                lastGetVisitsQuery.defer = defer;
            }
            return defer.promise;
        };

        // Public API here
        return {
            getParams: getParams,
            setParams: setParams,
            getVisits: getVisits,
            getAlertData: getAlertData,
            openAlertDetailModal: openAlertDetailModal
        };
    }]);
