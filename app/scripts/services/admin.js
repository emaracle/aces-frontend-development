
'use strict';

angular.module('acesApp')
    .factory('AcesAdmin', ['$rootScope', '$http', '$q', 'Auth', 'hospitals',
        function ($rootScope, $http, $q, Auth, hospitals) {
            //cache permissionValues
            var permissionValues = null;

            var Users = {
                validations: {
                    validNewUsername: function (username) {
                        return $q(function (resolve, reject) {
                            var url = $rootScope.endpoint + '/secure/admin/user/useruuid-by-username/' + encodeURIComponent(username);
                            $http({
                                method: 'GET',
                                url: url,
                                custom: {
                                    handlesLoading: true //instructs AuthHttpInterceptor not to show loading graphics since we will be handling it locally
                                }
                            }).then( function (response) {
                                if (response.data.length == 0) {
                                    resolve(true);
                                } else {
                                    if (response.data[0].Status == undefined) {
                                        resolve(false);
                                    } else {
                                        reject(response.data[0].Status)
                                    }
                                }
                            }, function (response) {
                                reject(response.statusText);
                            });
                        });
                    },
                    validUsername: function (username) {
                        return !(username == undefined || username.length < 4);
                    },
                    validFirstName: function (firstname) {
                        return !(firstname == undefined || firstname.length < 1);
                    },
                    validEmail: function (email) {
                        //taken from https://github.com/jzaefferer/jquery-validation/blob/master/src/core.js
                        // From https://html.spec.whatwg.org/multipage/forms.html#valid-e-mail-address
                        // Retrieved 2014-01-14
                        // If you have a problem with this implementation, report a bug against the above spec
                        // Or use custom methods to implement your own email validation
                        var re = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;
                        return re.test(email);
                    }
                },
                getUsers: function () {
                    var url = $rootScope.endpoint + '/secure/admin/users';
                    return $q(function (resolve, reject) {
                        var data = [];
                        $http({
                            method: 'GET',
                            url: url
                        }).then(function (response) {
                            data = _.map(response.data, function (user) {
                                    //convert isActive value from string to boolean
                                    user.isActive = (user.isActive == 'true' ? true : false);
                                    return user;
                                });
                            resolve(data);
                        }, function (response) {
                            reject(response);
                        });
                    });
                },
                getAppSettings: function (UserUUID) {
                    var url = $rootScope.endpoint + '/secure/admin/user/' + UserUUID + '/getAppSettings';
                    return $q(function (resolve, reject) {
                        $http({
                            method: 'POST',
                            url: url,
                        }).then(function (response) {
                            if (response.data.length == 0) {
                                reject('Returned blank response');
                            } else {
                                if (response.data[0].UserUUID != undefined) {
                                    resolve(response.data[0]);
                                } else if (response.data[0].Status != undefined) {
                                    reject(response.data[0].Status);
                                } else {
                                    reject('Unknown');
                                }
                            }
                        }, function (response) {
                            reject(response.statusText);
                        });
                    });
                },
                updateAppSetting: function (UserUUID, appSetting) {
                  var data = {
                      PermissionUUID: appSetting.PermissionUUID,
                      PermissionValue: appSetting.PermissionValue.toString().toUpperCase()
                  };
                  var url = $rootScope.endpoint + '/secure/admin/user/' + UserUUID + '/updateAppSetting';
                  return $q(function (resolve, reject) {
                      $http({
                          method: 'POST',
                          url: url,
                          data: $.param(data),
                          headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                      }).then(function (response) {
                          if (response.data.length == 0) {
                              reject('Returned blank response');
                          } else {
                              if (response.data[0].UserUUID != undefined) {
                                  resolve(response.data[0]);
                              } else if (response.data[0].Status != undefined) {
                                  reject(response.data[0].Status);
                              } else {
                                  reject('Unknown');
                              }
                          }
                      }, function (response) {
                          reject(response.statusText);
                      });
                  });
                },
                getResourcePermissionTypes: function () {
                    return ['PHU', 'HOSP'];
                },
                getResourcePermissionValues: function (permissionType) {
                    return $q(function (resolve, reject) {
                            if (permissionValues == null) {

                                var hospitalsList = [];
                                var phusList = [];
                                hospitals.getData(false).then(function (data) {
                                    permissionValues = {};

                                    hospitalsList = data.hospitals;
                                    phusList = data.phus;

                                    //remove duplicate hospitals and 'All'
                                    var addedHospital = [];
                                    hospitalsList = _.filter(data.hospitals, function (hospital) {
                                            if (addedHospital.indexOf(hospital.id) == -1 && hospital.id != 'All') {
                                                addedHospital.push(hospital.id);
                                                return true;
                                            } else {
                                                return false;
                                            }
                                        });
                                    //rebuild
                                    permissionValues.HOSP = _.map(hospitalsList, function (hospital) {
                                            return {
                                                id: hospital.id,
                                                title: $.trim(hospital.title)
                                            };
                                        });

                                    //build PHU list
                                    permissionValues.PHU = _.map(data.phus, function (phu) {
                                            return {
                                                id: phu.id,
                                                title: $.trim(phu.phu)
                                            }
                                        });

                                    resolve(permissionValues[permissionType]);
                                });
                            } else {
                                resolve(permissionValues[permissionType]);
                            }
                    });
                },
                getUserResources: function (UserUUID) {
                    var url = $rootScope.endpoint + '/secure/admin/user/' + UserUUID + '/resources';
                    return $q(function (resolve, reject) {
                        hospitals.getData(false).then( function(data) {
                            var hospitalsData = data;
                            var getHospitalInPhu = function (phuID, hospitalID) {
                                for(var i in phus) {
                                    if (phus[i].id == phuID) {
                                        for(var j in phus[i].hospitals) {
                                            if (phus[i].hospitals[j].id == hospitalID) {
                                                return phus[i].hospitals[j];
                                            }
                                        }
                                    }
                                }
                                return null;
                            };
                            $http({
                                method: 'GET',
                                url: url
                            }).then(function (response) {
                                var data = _.map(response.data, function (resource) {
                                        var val = {
                                            ID: resource.ID,
                                            PermissionType: resource.PermissionType,
                                            PermissionValue: null,
                                            CanEdit: false
                                        };
                                        switch (resource.PermissionType) {
                                            case 'PHU':
                                                for(var i in hospitalsData.phus) {
                                                    if (hospitalsData.phus[i].id == resource.PermissionValue) {
                                                        val.PermissionValue = {
                                                            id: hospitalsData.phus[i].id,
                                                            title: hospitalsData.phus[i].phu
                                                        };
                                                        val.CanEdit = true;
                                                        break;
                                                    }
                                                }
                                            break;
                                            case 'HOSP':
                                                for(var i in hospitalsData.hospitals) {
                                                    if (hospitalsData.hospitals[i].id == resource.PermissionValue) {
                                                        val.PermissionValue = {
                                                            id: hospitalsData.hospitals[i].id,
                                                            title: hospitalsData.hospitals[i].title
                                                        };
                                                        val.CanEdit = true;
                                                        break;
                                                    }
                                                }
                                            break;
                                            case 'SUPERUSER':
                                                val.CanEdit = false;
                                                val.PermissionValue = {
                                                            id: (resource.PermissionValue == 'TRUE' ? true : false),
                                                            title: (resource.PermissionValue == 'TRUE' ? 'Yes' : 'No')
                                                        };
                                            break;
                                        }
                                        return val;
                                    });
                                //remove null values
                                resolve(_.filter(data, function(val){ return val.PermissionValue != null; }));
                            }, function (response) {
                                reject(response);
                            });
                        });
                    });
                },
                addUserResource: function (UserUUID, resource) {
                    var data = {
                        PermissionType: resource.PermissionType,
                        PermissionValue: resource.PermissionValue.id
                    };
                    var url = $rootScope.endpoint + '/secure/admin/user/' + UserUUID + '/resources';
                    return $q(function (resolve, reject) {
                        $http({
                            method: 'POST',
                            url: url,
                            data: $.param(data),
                            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                        }).then(function (response) {
                            if (response.data.length == 0) {
                                reject('Returned blank response');
                            } else {
                                if (response.data[0].UserUUID != undefined) {
                                    resolve(response.data[0]);
                                } else if (response.data[0].Status != undefined) {
                                    reject(response.data[0].Status);
                                } else {
                                    reject('Unknown');
                                }
                            }
                        }, function (response) {
                            reject(response.statusText);
                        });
                    });
                },
                updateUserResource: function (UserUUID, resource) {
                    var data = {
                        PermissionType: resource.PermissionType,
                        PermissionValue: resource.PermissionValue.id
                    };
                    var url = $rootScope.endpoint + '/secure/admin/user/' + UserUUID + '/resources/' + resource.ID;
                    return $q(function (resolve, reject) {
                        $http({
                            method: 'POST',
                            url: url,
                            data: $.param(data),
                            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                        }).then(function (response) {
                            if (response.data.length == 0) {
                                reject('Returned blank response');
                            } else {
                                if (response.data[0].UserUUID != undefined) {
                                    resolve(response.data[0]);
                                } else if (response.data[0].Status != undefined) {
                                    reject(response.data[0].Status);
                                } else {
                                    reject('Unknown');
                                }
                            }
                        }, function (response) {
                            reject(response.statusText);
                        });
                    });
                },
                deleteUserResource: function (UserUUID, resourceID) {
                    var url = $rootScope.endpoint + '/secure/admin/user/' + UserUUID + '/resources/' + resourceID + '/delete';
                    return $q(function (resolve, reject) {
                        $http({
                            method: 'POST',
                            url: url,
                            // data: $.param(data),
                            // headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                        }).then(function (response) {
                            if (response.data.length == 0) {
                                reject('Returned blank response');
                            } else {
                                if (response.data[0].Deleted != undefined) {
                                    resolve(response.data[0]);
                                } else if (response.data[0].Status != undefined) {
                                    reject(response.data[0].Status);
                                } else {
                                    reject('Unknown');
                                }
                            }
                        }, function (response) {
                            reject(response.statusText);
                        });
                    });
                },
                updateStatus: function (UserUUID, isActive) {
                    var data = {
                        isActive: isActive
                    };
                    var url = $rootScope.endpoint + '/secure/admin/user/' + UserUUID + '/status';
                    return $q(function (resolve, reject) {
                        $http({
                            method: 'POST',
                            url: url,
                            data: $.param(data),
                            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                        }).then(function (response) {
                            if (response.data.length == 0) {
                                reject('Returned blank response');
                            } else {
                                if (response.data[0].UserUUID != undefined) {
                                    resolve(response.data[0]);
                                } else if (response.data[0].Status != undefined) {
                                    reject(response.data[0].Status);
                                } else {
                                    reject('Unknown');
                                }
                            }
                        }, function (response) {
                            reject(response.statusText);
                        });
                    });
                },
                updateUserArchived: function (UserUUID) {
                    var url = $rootScope.endpoint + '/secure/admin/user/' + UserUUID + '/archived';
                    return $q(function (resolve, reject) {
                        $http({
                            method: 'POST',
                            url: url,
                        }).then(function (response) {
                            if (response.data.length == 0) {
                                reject('Returned blank response');
                            } else {
                                if (response.data[0].UserUUID != undefined) {
                                    resolve(response.data[0]);
                                } else if (response.data[0].Status != undefined) {
                                    reject(response.data[0].Status);
                                } else {
                                    reject('Unknown');
                                }
                            }
                        }, function (response) {
                            reject(response.statusText);
                        });
                    });
                },
                updateUserDetails: function (UserUUID, FName, LName, Email, Telephone, Agency, Position) {
                    var data = {
                        FName: FName,
                        LName: LName,
                        Email: Email,
                        Telephone: Telephone,
                        Agency: Agency,
                        Position: Position
                    };
                    var url = $rootScope.endpoint + '/secure/admin/user/' + UserUUID + '/updateUserDetails';
                    return $q(function (resolve, reject) {
                        $http({
                            method: 'POST',
                            url: url,
                            data: $.param(data),
                            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                        }).then(function (response) {
                            if (response.data.length == 0) {
                                reject('Returned blank response');
                            } else {
                                if (response.data[0].UserUUID != undefined) {
                                    resolve(response.data[0]);
                                } else if (response.data[0].Status != undefined) {
                                    reject(response.data[0].Status);
                                } else {
                                    reject('Unknown');
                                }
                            }
                        }, function (response) {
                            reject(response.statusText);
                        });
                    });
                },
                setPasswordAdmin: function (UserUUID, pswd) {
                    var data = {
                        pswd: Auth.getHash(pswd)
                    };
                    var url = $rootScope.endpoint + '/secure/admin/user/' + UserUUID + '/setPasswordAdmin';
                    return $q(function (resolve, reject) {
                        $http({
                            method: 'POST',
                            url: url,
                            data: $.param(data),
                            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                        }).then(function (response) {
                            if (response.data.length == 0) {
                                reject('Returned blank response');
                            } else if (response.data[0].UserUUID != undefined) {
                                resolve(response.data[0]);
                            } else if (response.data[0].Status != undefined) {
                                reject(response.data[0].Status);
                            } else {
                                reject('Unknown');
                            }
                        }, function (response) {
                            reject(response.statusText);
                        });
                    });
                },
                createUser: function (user) {
                    var url = $rootScope.endpoint + '/secure/admin/user';
                    var data = {
                        UserName: user.UserName,
                        Password: Auth.getHash(user.Password),
                        Email: user.Email,
                        FName: user.FName,
                        LName: user.LName,
                        Agency: user.Agency,
                        Position: user.Position,
                        Telephone: user.Telephone,
                        isActive: user.isActive,
                        PermissionType: user.PermissionType,
                        PermissionValue: user.PermissionValue
                    };
                    return $q(function (resolve, reject) {
                        $http({
                            method: 'POST',
                            url: url,
                            data: $.param(data),
                            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                        }).then(function (response) {
                                if (response.data.length == 0) {
                                    reject('Returned blank response');
                                } else {
                                    if (response.data[0].UserUUID != undefined) {
                                        resolve(response.data[0].UserUUID);
                                    } else if (response.data[0].Status != undefined) {
                                        reject(response.data[0].Status);
                                    } else {
                                        reject('Unknown');
                                    }
                                }
                        }, function (response) {
                            reject(response.statusText);
                        });
                    });
                }
            };
            return {
                Users: Users
            };
        }
    ]);
