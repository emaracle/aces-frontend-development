'use strict';

angular.module('acesApp')
    .factory('classifiedVisitsByPhuOld', ['$resource', '$rootScope', '$q', 'hospitals', function ($resource, $rootScope, $q, hospitals) {

        console.log('classified visitsByPhu factory old');
//        var path = 'http://beta.kflaphi.ca\\:8080/AcesEpiEndpoint/aces/classify/phus/:phuId';
        var path = 'http://' + $rootScope.endpoint + '/AcesEpiEndpoint/aces/classify/phus/:phuId';
        var visitsByPhu = $resource(path, {
            query: {
                method: 'GET',
                isArray: true
            }
        });

        var getHospitalTitle = function (hospitalID) {
            var title = "";

            _(hospitals.hospitals).each(function (hospital) {
                if (hospital.id === hospitalID) {
                    title = hospital.title;
                    return;
                }
                //console.log("here");
            });
            return title;
        };

        var collect_categories = function (o, e) {
            o[e.HID];
            return o;
        }
        var unpack = function (phu, id) {
            return { phu: phu, id: id};
        }

        var factory = {};
        factory.all = function (params) {
            var _visitsByPhu = [];
            var defer = $q.defer();
            $rootScope.loading = true;
            visitsByPhu.query(params, function (data) {

                var sum = {key: "All", values: [], total: true};

                _(data).chain()
                    .map(function (item) {
                        return {key: item.HID, x: item.Date, y: item.Visits, title: ""};
                    })
                    .groupBy(function (item) {
                        return item.key
                    })
                    .map(function (o, e) {
                        _visitsByPhu.push({key: e, values: _(o).map(function (item) {
                            return {x: item.x, y: item.y, title: getHospitalTitle(item.key)}
                        })});
                    })
                    .value();

                // Calculates totals
                _(data).chain()
                    .groupBy(function (item) {
                        return item.Date
                    })
                    .map(function (o, e) {
                        sum.values.push(
                            {
                                y: _.reduce(_.pluck(o, 'ClassVisits'), function (memo, num) {
                                    return memo + num
                                }),
                                x: Number(e),
                                title: "All"});
                    })
                    .value();

                _visitsByPhu.push(sum);

                $rootScope.loading = false;
                defer.resolve(_visitsByPhu);
            })
            return defer.promise;
        };


        return factory;

    }]);

