'use strict';

angular.module('acesApp')
    .factory('classifiedByProbabilityVisitsByPhu', ['$resource', '$rootScope', '$q', 'Auth', function ($resource, $rootScope, $q, Auth) {

        console.log('classified by prob visitsByPhu factory');

//        var path = 'https://beta.kflaphi.ca\\:8181/AcesEpiEndpoint/aces/classify/byProbability/phus/:id';
        var path = $rootScope.endpoint + '/classify/byProbability/phus/:id';

        var classifiedByProbabilityVisitsByPhu = $resource(path, {
            query: {
                method: 'GET',
                isArray: true
            }
        });

        var collect_categories = function (o, e) {
            o[e.HID];
            return o;
        };
        var unpack = function (phu, id) {
            return { phu: phu, id: id};
        };

        var factory = {};
        factory.all = function (params) {
            var _visitsByPhu = [];
            var defer = $q.defer();
            classifiedByProbabilityVisitsByPhu.query(params, function (data, headers) {
                var sum = {key: "All", values: [], total: true};

                _(data).chain()
                    .map(function (item) {
                        return {key: item.HID, x: item.Date, y: item.Visits, title: item.HID};
                    })
                    .groupBy(function (item) {
                        return item.key
                    })
                    .map(function (o, e) {
                        _visitsByPhu.push({key: e, values: _(o).map(function (item) {
                            return {x: item.x, y: item.y, title: item.title}
                        })});
                    })
                    .value();

                // Calculates totals
                _(data).chain()
                    .groupBy(function (item) {
                        return item.Date
                    })
                    .map(function (o, e) {
                        sum.values.push(
                            {
                                y: _.reduce(_.pluck(o, 'Visits'), function (memo, num) {
                                    return memo + num
                                }),
                                x: e,
                                title: "All"});
                    })
                    .value();

                _visitsByPhu.push(sum);
                defer.resolve(_visitsByPhu);
            });
            return defer.promise;
        };


        return factory;

    }]);

