'use strict';

angular.module('acesApp')
	    .service('acesGeo', ['$http', '$q', function($http, $q){
        var results = {};
        var baseMaps = [
                {id: "streets", title: "Street"},
                {id: "satellite", title: "Satellite"},
                {id: "hybrid", title: "Hybrid"},
                {id: "topo", title: "Topo"},
                {id: "gray", title: "Gray"},
                {id: "oceans", title: "Oceans"},
                {id: "national-geographic", title: "National Geographic"},
                {id: "osm", title: "OSM"}
            ];
        var levelsOfGeography = [
                    {id: 'FSA', title: 'FSA', jsonFileName: 'geo_fsa_', geoType1Id: 'FSA', geoType2Id: 'CFSAUID', nameAttr: 'CFSAUID', type: 'polygonLayer'},
                    {id: 'CD', title: 'County', jsonFileName: 'geo_cd_', geoType1Id: 'CDID', geoType2Id: 'CDUID', nameAttr: 'NAME', type: 'polygonLayer'},
                    {id: 'PHU', title: 'Public Health Unit', jsonFileName: 'geo_phu_', geoType1Id: 'PHU', geoType2Id: 'NAMEID', nameAttr: 'NAME', type: 'polygonLayer'},
                    {id: 'LHIN', title: 'LHIN', jsonFileName: 'geo_lhin_', geoType1Id: 'LHIN', geoType2Id: 'HRUID2007', nameAttr: 'NAME', type: 'polygonLayer'}
                ];
        var selectableLayers = [
                {id: "daycares", title: "Daycares", type: 'pointLayer', color: [255,0,0]}, 
                {id: "pharmacies", title: "Pharmacies", type: 'pointLayer', color: [0,255,0]}, 
                {id: "phu_satellite_offices", title: "PHU Satellite Offices", type: 'pointLayer', color: [0,0,255]}, 
                {id: "geo_phu_info", title: "Public Health Units", type: 'polygonLayer', color: [0,0,0], borderColor: [0,255,0,0.0]},
                {id: "ltc", title: "Long Term Care", type: 'pointLayer', color: [255,255,0]}, 
                {id: "schools", title: "Schools", type: 'pointLayer', color: [0,255,255]},
                {id: "geo_fsa_info", title: "FSA", type: 'polygonLayer', color: [100,100,100], borderColor: [0,0,0,0.0]},
                {id: "tCameras", title: "Traffic Cameras", type: 'pointLayer', color: [255,255,255]},
                {id: "urban_areas", title: "Urban Areas", type: 'polygonLayer', color: [255,174,201], borderColor: [255,174,201, 0.4]},
                {id: "lhin", title: "LHIN", type: 'polygonLayer', color: [150,150,150], borderColor: [0, 0, 0, 0]},
                {id: "hospitals", title: "Hospitals", type: 'pointLayer', color: [255,255,255]}
            ];

        return {
            getBaseMaps: function(){ return baseMaps; },
            getSelectableLayers: function(){ return selectableLayers; },
            getLevelsOfGeography: function(){
                return levelsOfGeography;
            },
            getLevelOfGeography: function(id){
                var levelOfGeography;
                angular.forEach(levelsOfGeography, function(value){
                    if (id == value.id) {
                        levelOfGeography = value;
                        return; //Exit forEach                        
                    }
                });
                return levelOfGeography;
            },
            //Returns promise
            getJSON: function(fileName) {
                //Check for validity
                //
                var defer = $q.defer();
                // console.log(defer);
                //If already fetched
                if (results[fileName]) {
                    if (!results[fileName].pending) {
                        defer.resolve(results[fileName].data);    
                    } else {
                        //Previous http request is pending, add the defere to the defer list
                        //It will be resolved when the previous pending request is completed
                        results[fileName].defers.push(defer);
                    }
                } else {
                    results[fileName] = {
                        pending : true,
                        defers: [defer]
                    }


                    //jason file is not available for tCameras
                    if (fileName == 'tCameras') {
                        //Do not do anything, actual data will be loaded by acesMap
                        results[fileName].pending = false;
                        results[fileName].data = null;
                        angular.forEach(results[fileName].defers, function(d){
                            d.resolve(results[fileName].data);
                        });
                        // var path = "http://www.cdn.mto.gov.on.ca/kml/trafficcameras.kml";

                    } 
										else if (fileName == 'hospitals') {
                        //Do not do anything, actual data will be loaded by acesMap
                        results[fileName].pending = false;
                        results[fileName].data = null;
                        angular.forEach(results[fileName].defers, function(d){
                            d.resolve(results[fileName].data);
                        });  
                    }
										else {
                        var path = "geoJson/" + fileName + ".json";    
                        
                        
                        $http({
                            url: path,
                            method: 'GET',
                            custom: {
                                handlesLoading: true //instructs AuthHttpInterceptor not to show loading graphics since we are handling it locally 
                            }
                        })
                        .success(function (data, status, headers, config) {
                            results[fileName].pending = false;
                            results[fileName].data = data;
                            angular.forEach(results[fileName].defers, function(d){
                                d.resolve(results[fileName].data);
                            });
                            results[fileName].defers = [];
                            console.log('Loaded ' + fileName);
                        })
                        .error(function(data, status, headers, config) {
                            results[fileName].pending = false;
                            angular.forEach(results[fileName].defers, function(d){
                                d.reject(data);
                            });
                            delete results[fileName];
                        });
                    }
                }
                return defer.promise;
            }
        }
    }]);