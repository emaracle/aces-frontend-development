'use strict';

angular.module('acesApp')
    .factory('fsas', ['$resource', '$q', '$rootScope', 'alertService', function ($resource, $q, $rootScope, alertService) {
        console.log('fsa');

        var path = $rootScope.endpoint + '/lookup/fsas';

        var getFSAs = $resource(path, {
            query: {
                method: 'GET',
                isArray: true
            }
        });

        //used to cache lists
        var fsas = {};

        var Data = {};
        Data.fsas = [];
        Data.getData = function (params) {
            var defer = $q.defer();
            var phu = 'default';
            if (params !== undefined) {
                phu = params.phu;
            }
            //already exists
            if (fsas[phu] !== undefined) {
                Data.fsas = fsas[phu];
                defer.resolve(Data);
            } else {
                getFSAs.query(params, function (data) {
                    fsas[phu] = [];
                    _(data).each(function (item) {
                        fsas[phu].push({
                            title: item.FSA
                        });
                    });
                    fsas[phu].push({title: "All"});

                    Data.fsas = fsas[phu];
                    defer.resolve(Data);
                }, function (error) {
                    alertService.add('danger', 'Failed to get data' + error);
                });
            }
            return defer.promise;
        };


        return Data;

    }]);
