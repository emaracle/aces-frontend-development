'use strict';

angular.module('acesApp')
    .factory('GeogTypes', 
        ['$q', '$http', '$rootScope', '$modal', '$log', 
        function ($q,  $http, $rootScope, $modal, $log) {

        //cached
        var defer = null;

        //returns promise
        //if reload is passed, it will perform a http query regardless of previous status
        var get = function (reload) {
            if (reload === undefined) reload = false;


            if (defer === null || reload) {
                defer = $q.defer();
                $http
                    .get($rootScope.endpoint + '/lookup/geogs')
                    .success(function (data, status, headers, config) {
                        defer.resolve(data);
                    })
                    .error( function (data, status, headers, config) {
                        defer.reject(status);
                    });
            }
            return defer.promise;
        };
        
        // Public API here
        return {
            get: get
        };
    }]);
