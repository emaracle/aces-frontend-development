'use strict';

angular.module('acesApp')
    .factory('hospitals', [
        '$resource', '$q', '$rootScope', 'alertService', 'Auth', 'AuthToken', function (
            $resource, $q, $rootScope, alertService, Auth, AuthToken) {
        console.log('hospitals service');

        var path = $rootScope.endpoint + '/secure/lookup/phus-and-hosps';

        var getHospitals = $resource(path, {
            query: {
                method: 'GET',
                isArray: true
            }
        });
        //used to determine if fresh list needs to be retrived from server
        //since the list based on user
        var lastUser = null;

        var Data = {};
        var _phus = [];
        var _rebuildPhusList = function (includeAllPhus) {
            //rebuilts phus based on includeAllPhus parameter
            Data.phus = _phus.filter(function (phu) {
                    if (!includeAllPhus) {
                        if (phu.id == 'ALL') {
                            return false;
                        }
                    }
                    return true;
                });
        };
        //optional includeAllPhus, by default false for non admins and true for admins
        Data.getData = function (includeAllPhus) {
            //if not passed
            if (typeof includeAllPhus == "undefined") {
                var access = routingConfig.accessLevels;
                //true if admin
                if (Auth.authorize(access.admin)) {
                    includeAllPhus = true;
                } else {
                    includeAllPhus = false;
                }
            }
            var defer = $q.defer();
            //Cached data found and cached date is for the same user
            if (Data.phus !== undefined && Auth.user.useruuid != undefined && lastUser == Auth.user.useruuid) {
                _rebuildPhusList(includeAllPhus);
                defer.resolve(Data);
            } else {
                _phus = [];
                Data.hospitals = [];
                getHospitals.query({}, function (data, headers) {
                     //a temporary variable
                    var phus = {};
                    var allHospital = {
                                    id: 'All',
                                    title: 'All Hospitals',
                                    hospShortcode: 'All'
                                };
                    Data.hospitals.push(allHospital);
                    angular.forEach(data, function(item){
                        var hospital = {
                            id: item.HID,
                            title: item.Hosp,
                            phu: item.PHU,
                            phuId: item.PhuID,
                            phuShortcode: item.PHUShortcode,
                            hospShortcode: item.HospitalShortcode
                        }

                        if (phus[hospital.phuId] === undefined) {
                            phus[hospital.phuId] = {
                                id: hospital.phuId,
                                phu: hospital.phu,
                                phuShortcode: hospital.phuShortcode,
                                hospitals: [allHospital]//add all by default
                            };
                        }
                        phus[hospital.phuId].hospitals.push(hospital);
                        Data.hospitals.push(hospital);
                    });


                    var access = routingConfig.accessLevels;
                    var hospitalIds = {};
                    _phus.push({
                        id: 'ALL',
                        phu: 'All Health Units',
                        phuShortcode: 'ALL',
                        //remove duplicate hospitals
                        hospitals: _.filter(Data.hospitals, function (hospital) {
                                if (hospitalIds[hospital.id] == undefined) {
                                    hospitalIds[hospital.id] = true;
                                    return true;
                                } else {
                                    return false;
                                }
                            })
                    });

                    angular.forEach(phus, function(phu){
                        _phus.push(phu);
                    });

                    _rebuildPhusList(includeAllPhus);

                    //save the user id
                    lastUser = (Auth.user.useruuid ? Auth.user.useruuid : null);

                    defer.resolve(Data);
                }, function (error) {
                    //if session timed out, don't show any message
                    //message will be displayed by AuthHttpInterceptor
                    if (error.status != 440) {
                        console.log('Failed to get data' + error);
                        alertService.add('danger', 'Failed to get data' + error);
                    }
                });
            }

            return defer.promise;
        };

        return Data;
    }]);
