'use strict';

angular.module('acesApp')
    .factory('AcesHelp', ['$rootScope', '$q', '$http', 
        function ($rootScope, $q,  $http, $log, Auth, AuthToken) {
            var ticket = {
                getCategories: function () {
                        return [
                                {value: 'User Account Issue', caption: 'User Account Issue'},
                                {value: 'Application Error', caption: 'Application Error'},
                                {value: 'Application Improvement', caption: 'Application Improvement'},
                                {value: 'Information Request', caption: 'Information Request'},
                                {value: 'Other Request', caption: 'Other Request'}
                            ];
                    },
                submit: function (data) {
                        var url = $rootScope.endpoint + '/secure/help/create-ticket';

                        return $http({
                            method: 'POST',
                            url: url,
                            data: $.param(data),
                            headers: {'Content-Type': 'application/x-www-form-urlencoded'},
                            custom: {
                                handlesLoading: true //instructs AuthHttpInterceptor not to show loading graphics since we will be handling it locally
                            }
                        });
                    }
            };
            return {
                ticket: ticket
            };
        }
    ]);
