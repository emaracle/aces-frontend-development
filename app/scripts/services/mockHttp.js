'use strict';

angular.module('acesApp')
    .factory('mockHttp', ['$q', function ($q) {
        // Service logic
        // ...

        var userStorage = angular.fromJson(localStorage.getItem('userStorage')),
            emailStorage = angular.fromJson(localStorage.getItem('emailStorage')),
            tokenStorage = angular.fromJson(localStorage.getItem('tokenStorage')) || {},
            loginExample = angular.fromJson(localStorage.getItem('loginExample'));

        userStorage = {
            'johnm': { name: 'John', username: 'johnm', password: 'hello', email: 'john.dott@myemail.com', userRole: userRoles.user, tokens: [] },
            'sandrab': { name: 'Sandra', username: 'sandrab', password: 'world', email: 'bitter.s@provider.com', userRole: userRoles.admin, tokens: [] },
            'cliffc': { name: 'Cliff', username: 'cliffc', password: '1234', email: 'ccoulter@kflapublichealth.ca', userRole: userRoles.admin, tokens: [] },
            'avandijk': { name: 'Adam', username: 'avandijk', password: '1234', email: 'avandijk@kflapublichealth.ca', userRole: userRoles.admin, useruuid: '520F2C6E-8A50-E311-93F5-D89D67239D41', tokens: [] },
            'avarrette': { name: 'Allan', username: 'avarrette', password: '1234', email: 'avarrette@kflapublichealth.ca', userRole: userRoles.admin, useruuid: '5EF5A75B-8A50-E311-93F5-D89D67239D41', tokens: [] },
            'testhospital': { name: 'TestHospital', username: 'testhospital', password: '1234', email: 'testhospital@kflapublichealth.ca', userRole: userRoles.user, useruuid: 'B8483F75-8A50-E311-93F5-D89D67239D41', tokens: [] },
            'testphu': { name: 'TestPhu', username: 'testphu', password: '1234', email: 'testphu@kflapublichealth.ca', userRole: userRoles.user, useruuid: 'B6F94182-8A50-E311-93F5-D89D67239D41', tokens: [] }

        };
        emailStorage = {
            'john.dott@myemail.com': 'johnm',
            'bitter.s@provider.com': 'sandrab',
            'ccoulter@kflapublichealth.ca': 'cliffc'

        };
        localStorage.setItem('userStorage', angular.toJson(userStorage));
        localStorage.setItem('emailStorage', angular.toJson(emailStorage));

        var randomUUID = function () {
            var charSet = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
            var randomToken = '';
            for (var i = 0; i < 36; i++) {
                if (i === 8 || i === 13 || i === 18 || i === 23) {
                    randomToken += '';
                    continue;
                }
                var randomPoz = Math.floor(Math.random() * charSet.length);
                randomToken += charSet.substring(randomPoz, randomPoz + 1);
            }
            return randomToken;
        };

        var login = function (method, url, data, headers) {
            console.log("login mock");
            var postData = angular.fromJson(data),
                user = userStorage[postData.username],
                newToken,
                tokenObj;
            $log.info(method, '->', url);
            console.log(localStorage);
            console.log("angular.isDefined(user), postData.user", angular.isDefined(user), postData.username);

            if (angular.isDefined(user) && user.password === postData.password) {
                newToken = randomUUID();
                user.tokens.push(newToken);
                tokenStorage[newToken] = postData.username;
                localStorage.setItem('userStorage', angular.toJson(userStorage));
                localStorage.setItem('tokenStorage', angular.toJson(tokenStorage));
                console.log("good");
                return [200, { name: user.name, userRole: user.userRole, token: newToken }, {}];
            } else {
                console.log("bad");
                return [401, 'wrong combination username/password', {}];
            }
        };

        var logout = function (method, url, data, headers) {
            var queryToken, userTokens;
            $log.info(method, '->', url);

            if (queryToken = headers['X-Token']) {
                if (angular.isDefined(tokenStorage[queryToken])) {
                    userTokens = userStorage[tokenStorage[queryToken]].tokens;
                    // Update userStorage AND tokenStorage
                    userTokens.splice(userTokens.indexOf(queryToken));
                    delete tokenStorage[queryToken];
                    localStorage.setItem('userStorage', angular.toJson(userStorage));
                    localStorage.setItem('tokenStorage', angular.toJson(tokenStorage));
                    return [200, {}, {}];
                } else {
                    return [401, 'auth token invalid or expired', {}];
                }
            } else {
                return [401, 'auth token invalid or expired', {}];
            }
        };

        var user = function (method, url, data, headers) {
            var queryToken, userObject;
            $log.info(method, '->', url);

            // if is present in a registered users array.
            if (queryToken = headers['X-Token']) {
                if (angular.isDefined(tokenStorage[queryToken])) {
                    userObject = userStorage[tokenStorage[queryToken]];
                    return [200, { token: queryToken, name: userObject.name, userRole: userObject.userRole }, {}];
                } else {
                    return [401, 'auth token invalid or expired', {}];
                }
            } else {
                return [401, 'auth token invalid or expired', {}];
            }
        };

        // Public API here
        return {
            login: login,
            logout: logout,
            user: user
        };
    }]);
