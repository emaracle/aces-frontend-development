
'use strict';

angular.module('acesApp')
    .factory('AcesRegistration', ['$rootScope', '$http', '$q',
        function ($rootScope, $http, $q) {
            
            var RegisterAccount = {
                validNewUsername: function (username) {
                    return $q(function (resolve, reject) {
                        var url = $rootScope.endpoint + '/registration/user/useruuid-by-username/' + encodeURIComponent(username);
                        $http({
                            method: 'GET',
                            url: url,
                            custom: {
                                handlesLoading: true //instructs AuthHttpInterceptor not to show loading graphics since we will be handling it locally
                            }
                        }).then( function (response) {
                            if (response.data.length == 0) {
                                resolve(true);
                            } else {
                                if (response.data[0].Status == undefined) {
                                    resolve(false);
                                } else {
                                    reject(response.data[0].Status)
                                }
                            }
                        }, function (response) {
                            reject(response.statusText);
                        });
                    });
                },
                validPasswordFormat: function (pwd) {
                    var r = /^(?=.*\d)(?=.*[a-z])(?=.*[A-Z])(?=.*[^a-zA-Z0-9])(?!.*\s).{6,}$/;
                    if (pwd.match(r) === null) {
                        return true;
                    } else {
                        return false;
                    }
                }, 
                validEmailFormat: function (email) {
                    //Email
                    var re = /^[a-zA-Z0-9.!#$%&'*+\/=?^_`{|}~-]+@[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?(?:\.[a-zA-Z0-9](?:[a-zA-Z0-9-]{0,61}[a-zA-Z0-9])?)*$/;
                    return re.test(email);
                },              
                registerNewUser: function (newUser) {
                    var data = {
                        UserName: newUser.UserName,                 
                        Password: newUser.Password1,                       
                        Email: newUser.Email,
                        FName: newUser.FName,
                        LName: newUser.LName,                    
                        Position: newUser.Position,
                        Agency: newUser.Agency,
                        Telephone: newUser.Telephone,
                        Recaptcha: newUser.Recaptcha
                    };

                    var url = $rootScope.endpoint + '/registration/user/register-new-user';

                    return $q(function (resolve, reject) {
                        $http({
                            method: 'POST',
                            url: url,
                            data: $.param(data),
                            headers: {'Content-Type': 'application/x-www-form-urlencoded'}
                        }).then(function (response) {
                            if (response == "true"){
                                resolve(true);
                            } else {
                                resolve(false);
                            };                                                      
                        });
                    });
                }
            };

            return {
                RegisterAccount: RegisterAccount
            };
        }     
    ]);
