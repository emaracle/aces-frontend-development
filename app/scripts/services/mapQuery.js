
angular.module('acesApp')
    .factory('MapQuery',
        ['$q', '$http', '$rootScope', '$log', 'Auth',
        function ($q,  $http, $rootScope, $log, Auth) {
            var queryCache = {};
            var query = function(params){
                //ping the server to check if session is valid or not
                Auth.pingServer("Query");

                var path = $rootScope.endpoint + "/maps/choropleth?";
                // var path = "//" + $rootScope.endpoint + "/maps/choropleth?";
                path += "classifier=" + params.classifier
                path += "&classification=" + params.classification;

                if ('All' != params.gender){
                    path += "&gender=" + params.gender;
                }

                path += "&type=" + params.levelOfGeography;
                path += "&datefrom=" + params.dateRangeLow;
                path += "&dateto=" + params.dateRangeHigh;
                path += "&ageto=" + params.ageRangeHigh;
                path +="&agefrom=" + params.ageRangeLow;
                path += "&class=" + params.syndrome;
                if (undefined !== params.returnType) {
                    path += "&returnType=" + params.returnType;
                }

                var qid = path;
                
                if (undefined === queryCache[qid]) {

                    queryCache[qid] = $q.defer();

                    //If different query
                    $http({
                        url: path,
                        method: 'GET',
                        custom: {
                            handlesLoading: true //instructs AuthHttpInterceptor not to show loading graphics since we are handling it locally
                        }
                    })
                    .success(function (data, status, headers, config) {
                        if (undefined !== queryCache[qid]) {
                            queryCache[qid].resolve(data);    
                        }
                        // console.log('Loaded ' + path);
                    })
                    .error(function(data, status, headers, config){
                        if (undefined !== queryCache[qid]) {
                            queryCache[qid].reject(status);
                        }
                        //display error message
                    });
                }
                console.log("QID", queryCache);
                return queryCache[qid].promise;
            };

            return {
                query: query,
                clearCache: function () {
                    queryCache = {};
                }
            }
        }
    ]);
