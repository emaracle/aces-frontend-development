'use strict';

angular.module('acesApp')
    .factory('AlertsByPhu', ['$resource', '$rootScope', '$q', 'Auth', function ($resource, $rootScope, $q, Auth) {

        console.log('AlertsByPhu factory');

        var path = $rootScope.endpoint + '/secure/alerts/phus/:phuId';

        var AlertsByPhu = $resource(path, {
            query: {
                method: 'GET',
                isArray: true
            }
        });

        var factory = {};
        factory.all = function (params) {
            var _AlertsByPhu = [];
            var defer = $q.defer();
            AlertsByPhu.query(params, function (response) {
                _(response).each(function (Alerts) {
                    _AlertsByPhu.push(Alerts);
                });
                defer.resolve(_AlertsByPhu);
            });


            return defer.promise;
        };

        return factory;

    }]);
