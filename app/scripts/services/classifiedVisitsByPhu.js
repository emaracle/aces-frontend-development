'use strict';

angular.module('acesApp')
    .factory('classifiedVisitsByPhu', ['$resource', '$rootScope', '$q', 'Auth', 'utils','colourList', function ($resource, $rootScope, $q, Auth, utils, colourList) {

        console.log('classified visitsByPhu factory');
        var path = $rootScope.endpoint + '/secure/classify/:locality/phus/:phuId';

        var classifiedVisitsByPhu = $resource(path, {
            query: {
                method: 'GET',
                isArray: true
            }
        });

        var collect_categories = function (o, e) {
            o[e.HID];
            return o;
        };
        var unpack = function (phu, id) {
            return { phu: phu, id: id};
        };
        
        var factory = {};
        factory.all = function (params) {
            return utils.smartQuery({
                qid: "classifiedVisitsByPhu.all", //this must be unique for each query types
                //will be used to generate unique cacheid
                params: params,
                //whether to cache or not
                //cache if dateto in date range is pass date
                cache: (moment().startOf("day") > params.dateto),
                //this will be called if query is not found in the cache, it must return promise
                query: function () {
                    var _visitsByPhu = [];
                    var defer = $q.defer();

                    var defaultstyle = "solid";
                    classifiedVisitsByPhu.query(params, function (data) {
                        var sum = {key: "All", style: defaultstyle, color: colourList.getColour(), values: [], total: true};
                        _(data).chain()
                            .map(function (item) {
                                return {key: item.HID, x: item.Date, y: item.Visits, title: item.HID, z: item.AllVisits, Class: item.Class};
                            })
                            .groupBy(function (item) {
                                return item.key
                            })
                            .map(function (o, e) {
                                _visitsByPhu.push({key: e, color: colourList.getColour(), style: defaultstyle, values: _(o).map(function (item) {
                                    return {x: item.x, y: item.y, title: item.title, z: item.z, Class: item.Class}
                                })});
                            })
							.value(); 
                        if (_visitsByPhu.length > 1) {
                            // Calculates totals
                            _(data).chain()
                                .groupBy(function (item) {
                                    return item.Date
                                })
                                .map(function (o, e) {
                                    sum.values.push(
                                        {
                                            y: _.reduce(_.pluck(o, 'Visits'), function (memo, num) {
                                                return memo + num
                                            }),
                                            x: e,
                                            title: "All",
											Class: "All"});
                                })
                                .value();

                            _visitsByPhu.push(sum);
						} 
                        defer.resolve(_visitsByPhu);
                    });
                    return defer.promise;
                }
            });
        };


        return factory;

    }]);
