'use strict';

angular.module('acesApp')
    .factory('admissionsLineListingsByPhu', ['$resource', '$rootScope', '$q', 'utils', function ($resource, $rootScope, $q, utils) {

        var path = $rootScope.endpoint + '/secure/admissions/classified-line-listings/:locality/phus/:phuId';
        var resource = $resource(path, {
            query: {
                method: 'GET',
                isArray: true
            }
        });

        var factory = {};
        factory.query = function (params) {
            return utils.smartQuery({
                qid: "admissionsLineListingsByPhu.all", //this must be unique for each query types
                //will be used to generate unique cacheid
                params: params,
                //whether to cache or not
                //cache if dateto in date range is pass date
                cache: (moment().startOf("day") > params.dateto),
                //this will be called if query is not found in the cache, it must return promise
                query: function () {
                    var result = [];
                    var defer = $q.defer();
                    resource.query(params, function (response) {
                        _(response).each(function (row) {
                            result.push(row);
                        });
                        defer.resolve(result);
                    }, function (error) {
                        defer.reject(error);
                    });

                    return defer.promise;
                }
            });
        };

        return factory;

    }]);
