'use strict';

angular.module('acesApp')
    .factory('classifications', ['$resource', '$q', '$rootScope', function ($resource, $q, $rootScope) {

        var run = false;


//        var all = $resource('http://beta.kflaphi.ca\\:8080/AcesEpiEndpoint/aces/lookup/classifications', {
        var all = $resource($rootScope.endpoint + '/lookup/classifications', {
            query: {
                method: 'GET',
                isArray: true
            }
        });

        var factory = {};
        factory.list = [];
        factory.getData = function () {
            var defer = $q.defer();
            if (run) {
                defer.resolve(factory);
                return defer.promise;
            }
            all.query({}, function (data) {
                factory.list = [];
                //a temporary variable
                var classifications = {};
                angular.forEach(data, function (item) {
                    //if classification not already defined
                    if (classifications[item.ClassificationID] === undefined) {
                        var classification = {
                            id: item.ClassificationID,
                            title: item.Classification,
                            desc: item.ClassificationDesc,
                            classes: [],
                            _class: []
                        };
                        classification.classes.push({id: "All", title: "All"});

                        classifications[item.ClassificationID]= classification;
                    }

                    classifications[item.ClassificationID].classes.push({
                                id: item.ClassID,
                                title: item.Class
                            });
                });

                //add each classifications to the list
                angular.forEach(classifications, function(classification){
                    factory.list.push(classification);
                });
                
                defer.resolve(factory);
                run = true;
            });
            return defer.promise;
        };

        return factory;

    }]);
