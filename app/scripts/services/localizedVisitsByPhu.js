'use strict';

angular.module('acesApp')
    .factory('localizedVisitsByPhu', ['$resource', '$rootScope', '$q', 'hospitals', 'Auth', function ($resource, $rootScope, $q, hospitals, Auth) {

        console.log('localizedVisitsByPhu factory');

        var path = $rootScope.endpoint + '/visits/:locality/phus/:phuId';
        var localizedVisitsByPhu = $resource(path, {
            query: {
                method: 'GET',
                isArray: true
            }
        });

        var getHospitalTitle = function (hospitalID) {
            var title = "";
            _(hospitals.hospitals).each(function (hospital) {
                if (hospital.id === hospitalID) {
                    console.log(hospital.id, hospitalID);
                    title = hospital.title;
                }
            });
            return title;
        };

        var collect_categories = function (o, e) {
            o[e.HID];
            return o;
        };
        var unpack = function (phu, id) {
            return { phu: phu, id: id};
        };

        var factory = {};
        factory.all = function (params) {
            console.log(path);
            var _visitsByPhu = [];
            var defer = $q.defer();
            localizedVisitsByPhu.query(params, function (data, headers) {
                var sum = {key: "All", values: [], total: true};

                _(data).chain()
                    .map(function (item) {
                        return {key: item.HospitalID, x: item.Date, y: item.Visits, title: ""};
                    })
                    .groupBy(function (item) {
                        return item.key
                    })
                    .map(function (o, e) {
                        _visitsByPhu.push({key: e, values: _(o).map(function (item) {
                            return {x: item.x, y: item.y, title: getHospitalTitle(item.key)}
                        })});
                    })
                    .value();

                // Calculates totals
                _(data).chain()
                    .sortBy(function (item) {
                        return item.Date
                    })
                    .groupBy(function (item) {
                        return item.Date
                    })
                    .map(function (o, e) {
                        sum.values.push(
                            {
                                y: _.reduce(_.pluck(o, 'Visits'), function (memo, num) {
                                    return memo + num
                                }),
                                x: Number(e),
                                title: "All"});
                    })
                    .value();

                _visitsByPhu.push(sum);

                _(_visitsByPhu).each(function (item) {
                    _.sortBy(item.values, function (o) {
                        return o.x
                    });
                    console.log(item.values);
                });

                defer.resolve(_visitsByPhu);
            });
            return defer.promise;
        };


        return factory;

    }]);
