'use strict';

angular.module('acesApp')
    .factory('syndromes', ['$rootScope', '$resource', '$q', function ($rootScope, $resource, $q) {

        console.log("Syndromes service");

        var all = $resource($rootScope.endpoint + '/lookup/syndromes', {
            query: {
                method: 'GET',
                isArray: true
            }
        });

        //holds cached list
        var cachedList = {};

        //
        return {
            getData: function (classification, bucket) {
                var param = {
                    classification: classification,
                    bucket: (bucket ===undefined) ? 'All' : bucket
                };
                //
                var qid = JSON.stringify(param);


                var resolve = function() {
                    //Create a blank list
                    var list = [];
                    //Add all to the list
                    list = [{id: "All", title: "All", description: "", AD: 1, ED: 1}];
                    angular.forEach(cachedList[qid], function(item){
                        list.push({
                            id: item.ClassID,
                            title: item.Class,
                            description: item.Description,
                            AD: item.AD,
                            ED: item.ED
                        });
                    });
                    //resolve with array
                    defer.resolve(list);
                };

                var defer = $q.defer();
                //If cached found
                if (cachedList[qid] !== undefined) {
                    resolve();
                } else {
                    // factory.list = [];

                    all.query(param, function (data) {
                        cachedList[qid] = data;
                        resolve();
                    });
                }
                return defer.promise;
            }
        }
    }]);
