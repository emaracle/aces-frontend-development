'use strict';

angular.module('acesApp')
    .factory('colourList', ['$rootScope', 'Auth', function ($rootScope, Auth) {

        //Predefined high contrast colour set (count: 86)
        var hslColours = ["#000000", "#FF4A46", "#1CE6FF", "#FF34FF", "#008941", "#006FA6", "#A30059",
            "#7A4900", "#0000A6", "#63FFAC", "#B79762", "#004D43", "#8FB0FF", "#997D87",
            "#5A0007", "#809693", "#1B4400", "#4FC601", "#3B5DFF", "#4A3B53", "#FF2F80",
            "#61615A", "#BA0900", "#6B7900", "#00C2A0", "#FF90C9", "#B903AA", "#D16100",
            "#000035", "#7B4F4B", "#300018", "#0AA6D8", "#013349", "#00846F",
            "#372101", "#FFB500", "#A079BF", "#CC0744", "#001E09",
            "#00489C", "#6F0062", "#0CBD66", "#456D75", "#B77B68", "#7A87A1", "#788D66",
            "#885578", "#FF8A9A", "#D157A0", "#BEC459", "#456648", "#0086ED", "#886F4C",
            "#34362D", "#00A6AA", "#452C2C", "#636375", "#FF913F", "#938A81",
            "#575329", "#00FECF", "#B05B6F", "#8CD0FF", "#3B9700", "#04F757", "#1E6E00",
            "#7900D7", "#A77500", "#6367A9", "#A05837", "#6B002C", "#772600", "#D790FF", "#9B9700",
            "#549E79", "#201625", "#72418F", "#BC23FF", "#3A2465", "#922329",
            "#5B4534", "#404E55", "#0089A3", "#CB7E98", "#324E72", "#6A3A4C"];

        //Return a colour from the set
        var getColour = function() {

            var colour = hslColours[$rootScope.colourIndex]; 

            //Ensure the index doesn't exceed number of colours in array, reset if necessary
            if ($rootScope.colourIndex < (hslColours.length-1)){
                $rootScope.colourIndex = $rootScope.colourIndex + 1;
            } else {
                $rootScope.colourIndex = 0;
            }
			return colour;
        };
		
		// Used for multiline epicurve in directives/line-chart.js as the function 
		// above uses a global variable which is already in use on page load. 
		var getColourWithIndex = function(index) {

			var colour = hslColours[index];  
			 
            if (index > (hslColours.length-1)){
				color = hslColours[0] 
			} 
            return colour;
        };

        var getLength = function () {
            return hslColours.length;
        };

    return {
        getColour:getColour,
        getColourWithIndex:getColourWithIndex,
        getLength: getLength
    };
        
    }]);
