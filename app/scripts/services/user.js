'use strict';

angular.module('acesApp')
    .service('userServices', ['$http', '$q', '$rootScope', function ($http, $q, $rootScope) {

        // Function to get current syndromes
        function getSyndromes(visitType) {
            if (undefined === visitType) {
                visitType = 'ed';
            }
            var deferred = $q.defer();
            $http({
                url: $rootScope.endpoint + '/secure/classify/ON',
                method: 'GET',
                params: {
                    visitType: visitType
                }
            })
                .success(function (data, status, headers, config) {

                    var results = {};
                    results.data = data;
                    results.headers = headers();
                    results.status = status;
                    results.config = config;
                    
                    deferred.resolve(results);
                }).
                error(function (data, status, headers, config) {
                    deferred.reject(data, status, headers, config);
                });
            return deferred.promise;
        }


        // Function to get current alerts
        function getAlerts() {
            var deferred = $q.defer();
            $http({
                url: $rootScope.endpoint + '/secure/alerts/phus/ALL',
                method: 'GET',
                params: {
                    alertTypes: 'CUSUM1,Extreme,Trend',
                    syndromes: 'RESP,GASTRO,AST,ENVIRO,ILI,EOH,CELL,COPD,TOX,OPI,MH,ORTHOF,SEP,SI,DERM,VOM'
                }
            })
                .success(function (data, status, headers, config) {
                    var results = [];
                    results.data = data;
                    results.headers = headers();
                    results.status = status;
                    results.config = config;

                    deferred.resolve(results);
                }).
                error(function (data, status, headers, config) {
                    deferred.reject(data, status, headers, config);
                });
            return deferred.promise;
        }

        return {
            getSyndromes: getSyndromes,
            getAlerts: getAlerts
        }
    }]);