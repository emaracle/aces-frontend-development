'use strict';

angular.module('acesApp')
    .factory('ctas', ['$resource', '$q', '$rootScope', function ($resource, $q, $rootScope) {

        console.log('ctas service');
        var run = false;

//        var all = $resource('http://beta.kflaphi.ca\\:8080/AcesEpiEndpoint/aces/lookup/ctas', {
        var all = $resource($rootScope.endpoint + '/lookup/ctas', {
            query: {
                method: 'GET',
                isArray: true
            }
        });

        var factory = {};
        factory.list = [];
        factory.getData = function () {
            var defer = $q.defer();
            if (run) {
                defer.resolve(factory);
                return defer.promise;
            }
            factory.list = [];
            all.query({}, function (data) {
                _(data).each(function (item) {
                    _.extend(item, {value: false});
                    factory.list.unshift(item);
                });
                factory.list.push({TriageID: "All", value: true});
                defer.resolve(factory);
                run = true;
            });
            return defer.promise;
        };

        return factory;

    }]);

