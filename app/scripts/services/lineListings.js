'use strict';

angular.module('acesApp')
  .factory('lineListings', ['$filter', 'lineListingsByPhu', 'admissionsLineListingsByPhu', '$q', 'utils', 'hospitals', 'Auth', 'fsas', 'alertService', '$http', '$rootScope', 'SystemStatus', function
    ($filter, lineListingsByPhu, admissionsLineListingsByPhu, $q, utils, hospitals, Auth, fsas, alertService, $http, $rootScope, SystemStatus) {

      // Create variables
      var savedParams, Queries, Params;
      var phu;
      var Visits;

      // Function to get details of a visit
      function getLineListingsData(visitId, includeAlert, visitType) {
        if (undefined === includeAlert) {
            includeAlert = true;
        }
        //default visit type to ed
        if (undefined === visitType) {
            visitType = 'ed';
        }
        SystemStatus.processStarted();
        var results = {};
        var deferred = $q.defer();
        var processes = 0;
        var completedProcesses = 0;
        var complete = function () {
          completedProcesses++;
          if (completedProcesses >= processes) {
            console.log("COMPLETE");
            SystemStatus.processCompleted();
            deferred.resolve(results);
          }
        };

        processes++;
        $http({
          url: $rootScope.endpoint + '/secure/visits/' + visitId + '?visitType=' + visitType,
          method: 'GET'
        })
          .success(function (data, status, headers, config) {
            results.data = data;
            results.headers = headers();
            results.status = status;
            results.config = config;
            complete();
          })
            .error(function (data, status, headers, config) {
              deferred.reject(data, status, headers, config);
              complete();
          });

        processes++;
        $http({
          url: $rootScope.endpoint + '/secure/visits/alerts/' + visitId,
          method: 'GET'
        })
        .success(function (data, status, headers, config) {
          results.alerts = data;
          complete();
        })
        .error(function (data, status, headers, config) {
          deferred.reject(data, status, headers, config);
          complete();
        });

        return deferred.promise;
      };

      var initParam = function(type){
        savedParams = utils.getSavedParams('linelistings_params_' + type);
        Queries = savedParams.Queries;
        Params = savedParams.Params;
        
        if (undefined === savedParams.Params.phuId) {
          phu = $rootScope.phu;
          Params.phuId = phu.id;
          Queries.phu = phu;
        }

        //set to same date by default
        Queries.datefrom = moment(Queries.dateto).subtract('days', 1).valueOf();

        reassignReturnObj();
      };

      var init = function () {

        Visits = {};
        Visits.all = [];
        Visits.getData = function (custom) {
          if (custom !== undefined) {
            var params = angular.copy(custom.Params);
            var queries = angular.copy(custom.Queries);
          } else {
            var params = angular.copy(Params);
            var queries = angular.copy(Queries);
          }

          //if admissionType is defined, remove it from params we do not want to sent it in as parameter to the endpoint
          var admissionType = params.admissionType;

          if (params.admissionType !== undefined) {
            delete params.admissionType;
          }

          //Create comma-seperated string from class array
		      var classList = "";
          if(Array.isArray(queries.class)){
            for (var i = 0; i< (queries.class).length; i++){
              if (i < 1){
                  classList = queries.class[i].id;
              } else {
                  classList = classList + "," + queries.class[i].id;
              }
            }
          } else {
            classList = queries.class.id;
		      }

          var filteredQueries = utils.filterByProperty(queries, utils.Defaults.Queries);
          // params.phuId = $rootScope.phu.id;
          var totalParams = _.extend(filteredQueries, params);
          totalParams = _.extend(totalParams, {classifier: queries.classifier.ClassifierID, classification: queries.classification.id, class: classList, bucket: Queries.bucket.BucketID, hospital: Queries.hospital.id});
          totalParams = _.extend(totalParams, params);
		      totalParams = _.extend(totalParams);
          totalParams.fltrAmType = totalParams.fltrAmType.id;

          var defer = $q.defer();
          var result = [];
          var completedTasks = 0;
          var totalTasks = 0;
          var requiredFields = []; // ['Age', 'Gender', 'FSALDU', 'HospitalID', 'ChiefComplaint', 'FRIScore', 'ClassName', 'CTAS', 'EMSArrival', 'PatientLHIN'];
          var complete = function (data, isAdmission) {
            completedTasks++;
            //add isAdmission field to all the rows also add VisitTimestamp which will be used to sort the merged data
            angular.forEach(data, function(row) {
              row.IsAdmission = isAdmission;
              row.VisitTimestamp = moment(row.VisitDate + ' ' + row.VisitTime).valueOf();
              //used for group by time
              row.VisitTimeHr = moment(row.VisitDate + ' ' + row.VisitTime).format("HH");

              //if required field is not found, set it to NULL
              angular.forEach(requiredFields, function(field) {
                if (undefined === row[field]) {
                  row[field] = null;
                }
              });
            });

            //concat the data to the result
            result = result.concat(data);
            if (completedTasks >= totalTasks) {
              //by default order the results by datetime showing latest on the top
              defer.resolve(result);
              Visits.all = result;
            }
          };

          if (admissionType === "ED") {
            totalTasks++;
            lineListingsByPhu.all(totalParams).then(function (data) {
              data = angular.copy(data);
              complete(data, false);
            }, function (error) {
              alertService.add('danger', 'Failed to get data' + error);
            });
          }

          if (admissionType === "AD") {
            totalTasks++;
            admissionsLineListingsByPhu.query(totalParams).then(function (data) {
              data = angular.copy(data);
              complete(data, true);
            }, function (error) {
              alertService.add('danger', 'Failed to get data' + error);
            });
          }
          return defer.promise;
        };

        Visits.getFsas = function (fsaphu) {
          fsas.getData({phu: fsaphu});
          console.log("TESTFSA", fsas.fsas);
        };

        initParam('ED');
      };

      var saveParams = function() {
        if (undefined !== Params['type']) {
          utils.saveParams(Params, Queries, 'linelistings_params_' + Params['type']);
        }
      };

      var returnObj = {};
      var reassignReturnObj = function () {
        returnObj.Params = Params;
        returnObj.Queries = Queries;
        returnObj.initParams = initParam;
        returnObj.Visits = Visits;
        returnObj.getLineListingsData = getLineListingsData;
        returnObj.saveParams = saveParams;
        returnObj.getDefault = function () {return angular.copy(utils.Defaults);};
        returnObj.getPatientLocality = function (id) {return utils.getPatientLocality(id);};
        returnObj.getHospitalLocality = function (id) {return utils.getHospitalLocality(id);};
      }

      init();

      return returnObj;
  }]
);
