﻿define(["dojo/_base/declare"], function (declare)
{
	return declare(null,
	{  
		_layerInfo: undefined,
		constructor: function ()
		{   
			this._layerInfo =   
				[
				{
					"id": "1A177D86-FADD-E412-80CA-111111111111",
					"dataSourceType": "WMS",
					"dataSourceFormat": "KML",
					"displayName": "Hospitals",
					"baseURL": "//proxy.kflaphi.ca/?http://geo2.kflaphi.ca/geoserver/KFLA/wms",
					//"baseURL": "http://prodcl1.kflaphi.ca/geoserver/kfla/wms?",
					"url": "//proxy.kflaphi.ca/?http://geo2.kflaphi.ca/geoserver/KFLA/wms?service=WMS&version=1.1.0&request=GetMap&layers=KFLA:Hospitals_ER&styles=&bbox=-1.059258602933216E7,5117780.904521567,-8275939.582823402,7731361.2294783145&width=680&height=768&srs=EPSG:3857&format=application%2Fvnd.google-earth.kml%2Bxml",
					"metaDataURL": "",
					"legendURL": "https://s3.amazonaws.com/geoserver-images/lhin.png",
					"requireProxy": false,
					"experimental": false,
					"timeEnabled": false,
					"defaultLayer": true,
					"isVisible": true,
					"opacity": 0,
					"folderName": "",
					"folderID": "",
					"URLParams":  
					{
						"request": "GetMap",
						"srs": "EPSG:3857",
						"service": "WMS",
						"layers": "KFLA:Hospitals_ER",
						"format": "application/vnd.google-earth.kml+xml",
						"version": "1.1.0",
						"styles": "hospitals_available", 
						"bbox": "-1.059258602933216E7,5117780.904521567,-8275939.582823402,7731361.2294783145" 
					},
					"timeParams": { 
						"min": "2",
						"max": "20",
						"interval": "1",
						"units": "esriTimeUnitsHours" 
					}  
				}
			];
		},
		GetLayerInfo: function(val){
			var layer = null;
			for(var i in this._layerInfo)
			{
				if(this._layerInfo[i].displayName == val)
				{
					layer = this._layerInfo[i];
				}
			}
			return this._layerInfo;
		}		    
	});
});  
