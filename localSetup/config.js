//Global variable
var ACES = (function () {
	var version = '[VERSIONNUMBER]';
	//Keep domain blank to use relative path 
	// or set the value for e.g. '//km-prod:8000' to test prod endpoint 
	var domain = '';
	//var path = '/AcesEpiEndpoint/aces';
	var path = 'http://localhost:8080/aces';

	return {
		version: version,
		endpoint: domain + path,
		buildnumber: '[BUILDNUMBER]'
	};
})();